let router = SimpleDrawerNav.stackNav({
  "BudgetCreate": {
    "screen": (props) => {
      <BudgetCreate onBudgetCreated=((_) => props##navigation##goBack(Js.null))
      onCategoryPickerRequested=(
        (~allowSplit, ~allowParentSelection, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
          props##navigation##navigate(
            "CategoryPicker",
            RParams.make(
              ~allowSplit,
              ~allowParentSelection,
              ~onSplitSelected?,
              ~categoryId?,
              ~excludedCategoryIds,
              ~onCategorySelected,
              ()
            )
          )
        )
      />
    },
    "navigationOptions": {
      "title": "New budget"
    }
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
