let router = SimpleDrawerNav.stackNav({
  "TemplateEdit": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch params##templateId {
      | None => ReasonReact.null
      | Some(templateId) =>
        <TemplateEdit
          templateId
          onTemplateUpdated=((_) => props##navigation##goBack(Js.null))
          onCategoryPickerRequested=(
            (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
              props##navigation##navigate(
                "CategoryPicker",
                RParams.make(
                  ~allowSplit,
                  ~onSplitSelected?,
                  ~categoryId?,
                  ~excludedCategoryIds,
                  ~onCategorySelected,
                  ()
                )
              )
            )
            onSplitFormRequested=((optParts, onSplit) => {
              props##navigation##navigate("SpendingSplit", RParams.make(~parts=?optParts, ~onSplit, ()))
            })
        />
      }
    },
    "navigationOptions": {
      "title": "Edit template"
    }
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "SpendingSplit": {
    "screen": SpendingSplitStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
