let router = SimpleDrawerNav.stackNav({
  "TravelMode": {
    "screen": (props) => {
      <TravelModeScreen onGoBack=(() => props##navigation##goBack(Js.null))
        onSelectTags=(
          (selectedTags) =>
            Js.Promise.make(
              (~resolve, ~reject as _reject) =>
                props##navigation##navigate(
                  "TagsSelect",
                  RParams.make(~selectedTags, ~onSelectTags=(optId) => [@bs] resolve(optId), ())
                )
            )
        )
      />
    },
    "navigationOptions": {
      "title": "Travel mode",
    }
  },
  "TagsSelect": {
    "screen": TagsSelectStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps alwaysShowBack=true />
    }
  }
});
