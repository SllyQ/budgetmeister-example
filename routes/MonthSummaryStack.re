let router = SimpleDrawerNav.stackNav({
  "MonthSummary": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch (params##period) {
        | Some(period) => <PeriodSummary period />
        | None => ReasonReact.null
      }
    },
    "navigationOptions": (props) => {
      let params: RParams.t = props##navigation##state##params;
      let title = switch (params##date) {
        | Some(date) => (date |> DateFns.format("YYYY MMMM")) ++ " summary"
        | None => "Period summary"
      };
      {
        "title": title
      }
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
