let router = SimpleDrawerNav.stackNav({
  "SharingForm": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      <SharingFormScreen
        sharing=?params##sharing
        onShareConfigSelected=((shareConfig) => {
          switch (params##onSharingSelected) {
            | Some(cb) => {
              Log.log(~key="Sharing selected", shareConfig);
              cb(shareConfig)
            }
            | None => ()
          };
          props##navigation##goBack(Js.null)
        })
      />
    },
    "navigationOptions": {
      "title": "Expense sharing"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
