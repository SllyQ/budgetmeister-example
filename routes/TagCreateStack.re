let router = SimpleDrawerNav.stackNav({
  "TagCreate": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      let optOnCreate = params##onCreateTag;
      <TagCreate
        onTagCreate=(
          (optId) => {
            switch optOnCreate {
            | None => ()
            | Some(onCreate) => onCreate(optId)
            };
            props##navigation##goBack(Js.null)
          }
        )
      />
    },
    "navigationOptions": {
      "title": "New tag"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
