let router = SimpleDrawerNav.stackNav({
  "ReminderEdit": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch params##reminderId {
      | None => ReasonReact.null
      | Some(reminderId) =>
        <ReminderEdit
          reminderId
          onReminderUpdated=((_) => props##navigation##goBack(Js.null))
        />
      }
    },
    "navigationOptions": {
      "title": "Edit reminder"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
