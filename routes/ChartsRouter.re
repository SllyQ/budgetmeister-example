let router = SimpleDrawerNav.stackNav({
  "Charts": {
    "screen": (_props) => {
      <DebounceInitialRender>
        <ChartsTabs />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "Charts",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
