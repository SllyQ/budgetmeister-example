let router = SimpleDrawerNav.stackNav({
  "RecurringExpenseEdit": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch params##recurringExpenseId {
        | None => ReasonReact.null
        | Some(recurringExpenseId) => {
          <RecurringExpenseEdit
            recurringExpenseId
            onRecurringExpenseUpdated=(() => props##navigation##goBack(Js.null))
            onCategoryPickerRequested=(
              (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
                props##navigation##navigate(
                  "CategoryPicker",
                  RParams.make(
                    ~allowSplit,
                    ~onSplitSelected?,
                    ~categoryId?,
                    ~excludedCategoryIds,
                    ~onCategorySelected,
                    ()
                  )
                )
              )
            onSplitFormRequested=((optParts, onSplit) => {
              props##navigation##navigate("SpendingSplit", RParams.make(~parts=?optParts, ~onSplit, ()))
            })
            onSelectTags=((selectedTags) =>
              Js.Promise.make(
                (~resolve, ~reject as _reject) =>
                  props##navigation##navigate(
                    "TagsSelect",
                    RParams.make(~selectedTags, ~onSelectTags=(optId) => [@bs] resolve(optId), ())
                  )
              )
            )
          />
        }
      }
    },
    "navigationOptions": {
      "title": "New recurring expense"
    }
  },
  "TagsSelect": {
    screen: TagsSelectStack.router
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "SpendingSplit": {
    "screen": SpendingSplitStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
