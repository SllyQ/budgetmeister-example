let router = SimpleDrawerNav.stackNav({
  "ExpenseCreate": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      <ExpenseCreate
        initialExpenseDate=?params##date
        onExpenseCreated=(() => props##navigation##goBack(Js.null))
        onSharingFormRequested=((sharing, onSharingSelected) => {
          props##navigation##navigate("SharingForm", RParams.make((), ~sharing?, ~onSharingSelected))
        })
        onCreateCategory=(
          (optParentCatId) => Js.Promise.make(
            (~resolve, ~reject as _reject) =>
              props##navigation##navigate(
                "CategoryCreate",
                RParams.make(
                  ~parentCategoryId=?optParentCatId,
                  ~onCreateCategory=(optId) => [@bs] resolve(optId),
                  ()
                )
              )
          )
        )
        onCreateTemplate=((_) =>
          Js.Promise.make(
            (~resolve, ~reject as _reject) =>
              props##navigation##navigate(
                "TemplateCreate",
                RParams.make(~onCreateTemplate=(optId) => [@bs] resolve(optId), ())
              )
          )
        )
        onCategoryPickerRequested=(
          (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
            props##navigation##navigate(
              "CategoryPicker",
              RParams.make(
                ~allowSplit,
                ~onSplitSelected?,
                ~categoryId?,
                ~excludedCategoryIds,
                ~onCategorySelected,
                ()
              )
            )
          )
        onSelectTags=((selectedTags) =>
          Js.Promise.make(
            (~resolve, ~reject as _reject) =>
              props##navigation##navigate(
                "TagsSelect",
                RParams.make(~selectedTags, ~onSelectTags=(optId) => [@bs] resolve(optId), ())
              )
          )
        )
        onSplitFormRequested=((optParts, onSplit) => {
          props##navigation##navigate("SpendingSplit", RParams.make(~parts=?optParts, ~onSplit, ()))
        })
      />
    },
    "navigationOptions": {
      "title": "New Expense"
    }
  },
  "TagsSelect": {
    screen: TagsSelectStack.router
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "TemplateCreate": {
    "screen": TemplateCreateStack.router
  },
  "CategoryCreate": {
    "screen": CategoryCreateStack.router
  },
  "SpendingSplit": {
    "screen": SpendingSplitStack.router
  },
  "SharingForm": {
    "screen": SharingFormStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
