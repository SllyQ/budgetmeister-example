let router = SimpleDrawerNav.stackNav({
  "Settings": {
    "screen": (_) => {
      <DebounceInitialRender>
        <Connect_SelectedTheme render=((_) => {
          <Settings />
        }) />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "Settings",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
