let router = SimpleDrawerNav.stackNav({
  "Feedback": {
    "screen": (props) => {
      <Feedback onFeedbackSubmitted=(() => props##navigation##goBack(Js.null)) />
    },
    "navigationOptions": {
      "title": "Feedback",
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps alwaysShowBack=true />
    }
  }
});
