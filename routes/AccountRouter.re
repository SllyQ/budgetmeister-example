let router = SimpleDrawerNav.stackNav({
  "Account": {
    "screen": (_props) => {
      <DebounceInitialRender>
        <Account />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "Account",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
