let router = SimpleDrawerNav.stackNav({
  "HelloWorld": {
    "screen": (props) => {
      <HelloWorld onSetupSubmitted=(() => props##navigation##navigate("Home", RParams.null)) />
    },
    "navigationOptions": {
      "title": "Welcome to BudgetMeister",
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps noElements=true />
    }
  }
});
