let router = SimpleDrawerNav.stackNav({
  "CategoryPicker": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      <CategorySelectPicker
        onGoBack=(() => props##navigation##goBack(Js.null))
        selectedCategoryId=params##categoryId
        allowParentSelection=?params##allowParentSelection
        onCategorySelected=(
          (categoryId) => {
            switch params##onCategorySelected {
            | Some(cb) => cb(Some(categoryId))
            | None => ()
            };
            props##navigation##goBack(Js.null);
          }
        )
        allowSplit=?params##allowSplit
        excludedCategoryIds=?params##excludedCategoryIds
        onSplitSelected=?(switch params##onSplitSelected {
          | None => None
          | Some(cb) => Some((parts) => {
            Js.Global.setTimeout(() => {
              cb(parts);
            }, 0) |> ignore;
            props##navigation##goBack(Js.null);
          })
        })
        onCreateCategory=(
          (optParentCatId) => Js.Promise.make(
            (~resolve, ~reject as _reject) =>
              props##navigation##navigate(
                "CategoryCreate",
                RParams.make(
                  ~parentCategoryId=?optParentCatId,
                  ~onCreateCategory=(optId) => [@bs] resolve(optId),
                  ()
                )
              )
          )
        )
      />
    },
    "navigationOptions": {
      "title": "Select category"
    }
  },
  "CategoryCreate": {
    "screen": CategoryCreateStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});

let params = (~excludedCategoryIds=[], ~onSplitSelected=?, ~onCategorySelected, ~categoryId, ()): RParams.t =>
  RParams.make(
    ~allowSplit=Js.Option.isSome(onSplitSelected),
    ~excludedCategoryIds,
    ~onSplitSelected?,
    ~onCategorySelected,
    ~categoryId?,
    ()
  );
