let router = SimpleDrawerNav.stackNav({
  "Drawer": {
    "screen": DrawerRouter.router
  },
  "Feedback": {
    "screen": FeedbackRouter.router
  },
  "TripMode": {
    "screen": TravelModeRouter.router
  }
}, {
  "headerMode": "none"
});

let make = (children) => ReasonReact.wrapJsForReason(
  ~reactClass=router,
  ~props=Js.Obj.empty(),
  children
)
