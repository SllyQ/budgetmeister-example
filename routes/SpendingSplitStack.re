let router = SimpleDrawerNav.stackNav({
  "SpendingSplit": {
    "screen": (props) => {
      let navigation = props##navigation;
      let params: RParams.t = navigation##state##params;
      switch params##onSplit {
        | None => ReasonReact.null
        | Some(onSplit) => {
          <ExpenseSplitForm
            parts=?params##parts
            onPartsSubmitted=(parts => {
              onSplit(parts);
              navigation##goBack(Js.null)
            })
            onCategoryPickerRequested=(
              (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
                props##navigation##navigate(
                  "CategoryPicker",
                  RParams.make(
                    ~allowSplit,
                    ~onSplitSelected?,
                    ~categoryId?,
                    ~excludedCategoryIds,
                    ~onCategorySelected,
                    ()
                  )
                )
              )
          />
        }
      }
    },
    "navigationOptions": {
      "title": "Expenses"
    }
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
