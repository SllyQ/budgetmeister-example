let router = SimpleDrawerNav.stackNav({
  "ReminderCreate": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      let optOnCreate = params##onCreateReminder;
      <ReminderCreate
        onReminderCreated=(
          () => {
            switch optOnCreate {
            | None => ()
            | Some(onCreate) => onCreate()
            };
            props##navigation##goBack(Js.null)
          }
        )
      />
    },
    "navigationOptions": {
      "title": "New reminder"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
