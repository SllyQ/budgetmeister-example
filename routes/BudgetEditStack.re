let router = SimpleDrawerNav.stackNav({
  "BudgetEdit": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch params##budgetId {
      | None => ReasonReact.null
      | Some(budgetId) =>
        <BudgetEdit budgetId
          onBudgetUpdated=(() => props##navigation##goBack(Js.null))
          onCategoryPickerRequested=(
            (~allowSplit, ~allowParentSelection, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
              props##navigation##navigate(
                "CategoryPicker",
                RParams.make(
                  ~allowSplit,
                  ~allowParentSelection,
                  ~onSplitSelected?,
                  ~categoryId?,
                  ~excludedCategoryIds,
                  ~onCategorySelected,
                  ()
                )
              )
            )
        />
      }
    },
    "navigationOptions": {
      "title": "Edit budget"
    }
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
