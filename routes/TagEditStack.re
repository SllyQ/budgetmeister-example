let router = SimpleDrawerNav.stackNav({
  "TagEdit": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch params##tagId {
      | None => ReasonReact.null
      | Some(tagId) => <TagEdit tagId onTagUpdated=((_) => props##navigation##goBack(Js.null)) />
      }
    },
    "navigationOptions": {
      "title": "Edit tag"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
