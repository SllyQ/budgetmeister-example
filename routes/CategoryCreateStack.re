let router = SimpleDrawerNav.stackNav({
  "CategoryCreate": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      let optOnCreate = params##onCreateCategory;
      <CategoryCreate
        initialParentCategory=?params##parentCategoryId
        onCategoryPickerRequested=(
          (~allowSplit, ~allowParentSelection, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
            props##navigation##navigate(
              "CategoryPicker",
              RParams.make(
                ~allowSplit,
                ~onSplitSelected?,
                ~categoryId?,
                ~allowParentSelection,
                ~excludedCategoryIds,
                ~onCategorySelected,
                ()
              )
            )
          )
        onCategoryCreate=(
          (optId) => {
            switch optOnCreate {
            | None => ()
            | Some((onCreate: option(Category.id) => unit)) => onCreate(optId)
            };
            props##navigation##goBack(Js.null)
          }
        )
      />
    },
    "navigationOptions": {
      "title": "New category"
    }
  },
  "CategoryPicker": {
    "screen": ExistingCategoryPickerStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
