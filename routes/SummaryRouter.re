let router = SimpleDrawerNav.stackNav({
  "Summary": {
    "screen": (_props) => {
      <DebounceInitialRender>
        <SummaryTabs />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "Summary",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
