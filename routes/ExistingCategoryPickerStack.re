/* Its here to avoid cyclical dependency problem with calling it from categorySelect */

let router = SimpleDrawerNav.stackNav({
  "CategoryPicker": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      <CategorySelectPicker
        onGoBack=(() => props##navigation##goBack(Js.null))
        selectedCategoryId=params##categoryId
        allowParentSelection=?params##allowParentSelection
        onCategorySelected=(
          (categoryId) => {
            switch params##onCategorySelected {
            | Some(cb) => cb(Some(categoryId))
            | None => ()
            };
            props##navigation##goBack(Js.null);
          }
        )
        allowSplit=?params##allowSplit
        excludedCategoryIds=?params##excludedCategoryIds
        onSplitSelected=?(switch params##onSplitSelected {
          | None => None
          | Some(cb) => Some((parts) => {
            cb(parts);
            props##navigation##goBack(Js.null);
          })
        })
      />
    },
    "navigationOptions": {
      "title": "Select category"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
