let router = SimpleDrawerNav.stackNav({
  "Expenses": {
    "screen": (props) => {
      if (!HistoryDb.get().initialMainCurrencySelected) {
        props##navigation##navigate("HelloWorld", RParams.null);
      };
      <Home onCreateNewExpense=((~date=?, ()) => props##navigation##navigate("ExpenseCreate", RParams.make(~date?, ())))
        onEditExpense=((expenseId) => props##navigation##navigate("ExpenseEdit", RParams.make(~expenseId, ())))
        onEditRecurringExpense=((reId) => props##navigation##navigate("RecurringExpenseEdit", RParams.make(~recurringExpenseId=reId, ())))
        onPeriodSummaryRequested=(period => props##navigation##navigate("MonthSummary", RParams.make(~period, ())))
        onBudgetExpensesRequested=((budgetFilter: RParams.budgetFilter) => props##navigation##navigate("ExpensesList", RParams.make(~budgetFilter, ())))
        onCategoryPickerRequested=(
          (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
            props##navigation##navigate(
              "CategoryPicker",
              RParams.make(
                ~allowSplit,
                ~onSplitSelected?,
                ~categoryId?,
                ~excludedCategoryIds,
                ~onCategorySelected,
                ()
              )
            )
          )
      />
    },
    "navigationOptions": {
      "title": "Home",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  },
  "ExpenseCreate": {
    "screen": ExpenseCreateStack.router
  },
  "ExpenseEdit": {
    "screen": ExpenseEditStack.router
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "RecurringExpenseEdit": {
    "screen": RecurringExpenseEditStack.router
  },
  "MonthSummary": {
    "screen": MonthSummaryStack.router
  },
  "ExpensesList": {
    "screen": ExpensesListStack.router
  },
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
