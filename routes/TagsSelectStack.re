let router = SimpleDrawerNav.stackNav({
  "TagsSelect": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      let selectedTagIds =
        switch params##selectedTags {
        | Some(tags) => tags
        | None => []
        };
      <TagsSelectScreen
        selectedTagIds
        onTagsSelected=(
          (optTagIds) => {
            switch params##onSelectTags {
            | Some(cb) => cb(optTagIds)
            | None => ()
            };
            props##navigation##goBack(Js.null)
          }
        )
        onCreateTag=(
          (_) => Js.Promise.make(
            (~resolve, ~reject as _reject) =>
              props##navigation##navigate(
                "TagCreate",
                RParams.make(~onCreateTag=(optId) => [@bs] resolve(optId), ())
              )
          )
        )
      />
    },
    "navigationOptions": {
      "title": "Select tags"
    }
  },
  "TagCreate": {
    "screen": TagCreateStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
