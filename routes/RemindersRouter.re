let router = SimpleDrawerNav.stackNav({
  "Reminders": {
    "screen": (props) => {
      <DebounceInitialRender>
        <Reminders
          onCreateReminder=((_) => {
            Js.log("Create reminder");
            props##navigation##navigate(
              "ReminderCreate",
              RParams.make()
            )
          })
          onEditReminder=(
            (reminderId) =>
              props##navigation##navigate(
                "ReminderEdit",
                RParams.make(~reminderId, ())
              )
          )
        />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "Reminders",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  },
  "ReminderCreate": {
    "screen": ReminderCreateStack.router
  },
  "ReminderEdit": {
    "screen": ReminderEditStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
