let router = SimpleDrawerNav.stackNav({
  "Budget": {
    "screen": (props) => {
      let navigation = props##navigation;
      <DebounceInitialRender>
        <Budgets
          onBudgetCreateRequested=(() => {
            navigation##navigate("BudgetCreate", RParams.null)
          })
          onBudgetEditRequested=((budgetId) => {
            navigation##navigate("BudgetEdit", RParams.make(~budgetId, ()))
          })
        />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "Budget",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  },
  "BudgetCreate": {
    "screen": BudgetCreateStack.router
  },
  "BudgetEdit": {
    "screen": BudgetEditStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
