let router = SimpleDrawerNav.stackNav({
  "CategoryEdit": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch params##categoryId {
      | None => ReasonReact.null
      | Some(categoryId) =>
        <CategoryEdit categoryId onCategoryUpdated=((_) => props##navigation##goBack(Js.null))
          onCategoryPickerRequested=(
            (~allowSplit, ~allowParentSelection, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
              props##navigation##navigate(
                "CategoryPicker",
                RParams.make(
                  ~allowSplit,
                  ~onSplitSelected?,
                  ~categoryId?,
                  ~allowParentSelection,
                  ~excludedCategoryIds,
                  ~onCategorySelected,
                  ()
                )
              )
            )
        />
      }
    },
    "navigationOptions": {
      "title": "Edit category"
    }
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
