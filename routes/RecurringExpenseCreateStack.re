let router = SimpleDrawerNav.stackNav({
  "RecurringExpenseCreate": {
    "screen": (props) => {
      <RecurringExpenseCreate
        onRecurringExpenseCreated=(() => props##navigation##goBack(Js.null))
        onCategoryPickerRequested=(
          (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
            props##navigation##navigate(
              "CategoryPicker",
              RParams.make(
                ~allowSplit,
                ~onSplitSelected?,
                ~categoryId?,
                ~excludedCategoryIds,
                ~onCategorySelected,
                ()
              )
            )
          )
          onSplitFormRequested=((optParts, onSplit) => {
            props##navigation##navigate("SpendingSplit", RParams.make(~parts=?optParts, ~onSplit, ()))
          })
          onCreateCategory=(
            (optParentCatId) => Js.Promise.make(
              (~resolve, ~reject as _reject) =>
                props##navigation##navigate(
                  "CategoryCreate",
                  RParams.make(
                    ~parentCategoryId=?optParentCatId,
                    ~onCreateCategory=(optId) => [@bs] resolve(optId),
                    ()
                  )
                )
            )
          )
          onSelectTags=((selectedTags) =>
            Js.Promise.make(
              (~resolve, ~reject as _reject) =>
                props##navigation##navigate(
                  "TagsSelect",
                  RParams.make(~selectedTags, ~onSelectTags=(optId) => [@bs] resolve(optId), ())
                )
            )
          )
      />
    },
    "navigationOptions": {
      "title": "New recurring expense"
    }
  },
  "TagsSelect": {
    screen: TagsSelectStack.router
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "SpendingSplit": {
    "screen": SpendingSplitStack.router
  },
  "CategoryCreate": {
    "screen": CategoryCreateStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
