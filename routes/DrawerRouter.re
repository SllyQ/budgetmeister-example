let router = SimpleDrawerNav.drawerNav({
  "HelloWorld": {
    "screen": HelloWorldRouter.router
  },
  "Account": {
    "screen": AccountRouter.router
  },
  "Budget": {
    "screen": BudgetRouter.router
  },
  "Categories": {
    "screen": CategoriesRouter.router
  },
  "DevLog": {
    "screen": DevLogRouter.router
  },
  "Home": {
    "screen": HomeRouter.router
  },
  "RecurringExpenses": {
    "screen": RecurringExpensesRouter.router
  },
  "Settings": {
    "screen": SettingsRouter.router
  },
  "Summary": {
    "screen": SummaryRouter.router
  },
  "Reminders": {
    "screen": RemindersRouter.router
  },
  "SharedExpenses": {
    "screen": SharedExpensesRouter.router
  },
  "Charts": {
    "screen": ChartsRouter.router
  }
}, {
  "drawerOpenRoute": "DrawerOpen",
  "drawerCloseRoute": "DrawerClose",
  "drawerToggleRoute": "DrawerToggle",
  "initialRouteName": Config.initialRoute,
  "contentComponent": Drawer.jsComponent
});

let make = (children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass=router,
    ~props=Js.Obj.empty(),
    children
  );
