let router = SimpleDrawerNav.stackNav({
  "SharedExpenses": {
    "screen": (props) => {
      <DebounceInitialRender>
        <SharedExpenses />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "Shared Expenses",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
