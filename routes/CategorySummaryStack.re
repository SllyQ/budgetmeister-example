let router = SimpleDrawerNav.stackNav({
  "CategorySummaryStack": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      switch (params##categoryId) {
        | Some(id) => {
          <CategorySummary id />
        }
        | None => ReasonReact.null
      }
    },
    "navigationOptions": {
      "title": "Category Summary"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
