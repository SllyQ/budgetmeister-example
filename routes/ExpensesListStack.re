let router = SimpleDrawerNav.stackNav({
  "ExpensesList": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      <ExpensesListScreen
        onEditExpense=((expenseId) => props##navigation##navigate("ExpenseEdit", RParams.make(~expenseId, ())))
        onEditRecurringExpense=((reId) => props##navigation##navigate("RecurringExpenseEdit", RParams.make(~recurringExpenseId=reId, ())))
        budgetFilter=?params##budgetFilter
        categoryId=?params##categoryId
        tagId=?params##tagId
        timeRange=?params##timeRange
        onCategoryPickerRequested=(
          (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
            props##navigation##navigate(
              "CategoryPicker",
              RParams.make(
                ~allowSplit,
                ~onSplitSelected?,
                ~categoryId?,
                ~excludedCategoryIds,
                ~onCategorySelected,
                ()
              )
            )
          )
      />
    },
    "navigationOptions": {
      "title": "Expenses"
    }
  },
  "ExpenseEdit": {
    "screen": ExpenseEditStack.router
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "RecurringExpenseEdit": {
    "screen": RecurringExpenseEditStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
