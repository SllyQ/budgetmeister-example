let router = SimpleDrawerNav.stackNav({
  "Categories": {
    "screen": (props) => {
      <CategoryTagTabs
        onCategoryExpensesRequested=(categoryId => props##navigation##navigate(
          "ExpensesList",
          RParams.make(~categoryId, ())
        ))
        onCategorySummaryRequested=(categoryId => props##navigation##navigate(
          "CategorySummary",
          RParams.make(~categoryId, ())
        ))
        onCreateCategory=(
          (_) =>
            props##navigation##navigate("CategoryCreate", RParams.make())
        )
        onEditCategory=(
          (categoryId) =>
            props##navigation##navigate(
              "CategoryEdit",
              RParams.make(~categoryId, ())
            )
        )
        onCategoryPickerRequested=(
          (~allowSplit, ~allowParentSelection, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
            props##navigation##navigate(
              "CategoryPicker",
              RParams.make(
                ~allowSplit,
                ~onSplitSelected?,
                ~categoryId?,
                ~allowParentSelection,
                ~excludedCategoryIds,
                ~onCategorySelected,
                ()
              )
            )
          )
          onTagExpensesRequested=(tagId => props##navigation##navigate(
            "ExpensesList",
            RParams.make(~tagId, ())
          ))
          onCreateTag=((_) => props##navigation##navigate("TagCreate", RParams.make()))
          onEditTag=(
            (tagId) =>
              props##navigation##navigate(
                "TagEdit",
                RParams.make(~tagId, ())
              )
          )
      />
    },
    "navigationOptions": {
      "title": "Categories & Tags",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  },
  "CategoryCreate": {
    "screen": CategoryCreateStack.router
  },
  "CategoryEdit": {
    "screen": CategoryEditStack.router
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "ExpensesList": {
    "screen": ExpensesListStack.router
  },
  "CategorySummary": {
    "screen": CategorySummaryStack.router
  },
  "TagCreate": {
    "screen": TagCreateStack.router
  },
  "TagEdit": {
    "screen": TagEditStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
