let router = SimpleDrawerNav.stackNav({
  "RecurringExpenses": {
    "screen": (props) => {
      <RecurringExpensesTabs
        onRecurringExpenseCreateRequested=(() => {
          props##navigation##navigate("RecurringExpenseCreate", RParams.make())
        })
        onRecurringExpenseEditRequested=(
          (recurringExpenseId) =>
            props##navigation##navigate(
              "RecurringExpenseEdit",
              RParams.make(~recurringExpenseId, ())
            )
        )
        onCreateTemplate=((_) => props##navigation##navigate(
          "TemplateCreate",
          RParams.make()
        ))
        onEditTemplate=(
          (templateId) =>
            props##navigation##navigate(
              "TemplateEdit",
              RParams.make(~templateId, ())
            )
        )
      />
    },
    "navigationOptions": {
      "title": "Recurring expenses",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  },
  "RecurringExpenseCreate": {
    screen: RecurringExpenseCreateStack.router
  },
  "RecurringExpenseEdit": {
    "screen": RecurringExpenseEditStack.router
  },
  "TemplateCreate": {
    "screen": TemplateCreateStack.router
  },
  "TemplateEdit": {
    "screen": TemplateEditStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
