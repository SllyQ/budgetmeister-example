let router = SimpleDrawerNav.stackNav({
  "TemplateCreate": {
    "screen": (props) => {
      let params: RParams.t = props##navigation##state##params;
      let optOnCreate = params##onCreateTemplate;
      <TemplateCreate
        onCreateCategory=(
          (optParentCatId) => Js.Promise.make(
            (~resolve, ~reject as _reject) =>
              props##navigation##navigate(
                "CategoryCreate",
                RParams.make(
                  ~parentCategoryId=?optParentCatId,
                  ~onCreateCategory=(optId) => [@bs] resolve(optId),
                  ()
                )
              )
          )
        )
        onTemplateCreated=(
          (optId) => {
            switch optOnCreate {
            | None => ()
            | Some(onCreate) => onCreate(optId)
            };
            props##navigation##goBack(Js.null)
          }
        )
        onCategoryPickerRequested=(
          (~allowSplit, ~excludedCategoryIds, ~onSplitSelected, ~onCategorySelected: (option(Category.id) => unit), categoryId, ()) =>
            props##navigation##navigate(
              "CategoryPicker",
              RParams.make(
                ~allowSplit,
                ~onSplitSelected?,
                ~categoryId?,
                ~excludedCategoryIds,
                ~onCategorySelected,
                ()
              )
            )
          )
          onSplitFormRequested=((optParts, onSplit) => {
            props##navigation##navigate("SpendingSplit", RParams.make(~parts=?optParts, ~onSplit, ()))
          })
      />
    },
    "navigationOptions": {
      "title": "New template"
    }
  },
  "CategoryPicker": {
    "screen": CategoryPickerStack.router
  },
  "SpendingSplit": {
    "screen": SpendingSplitStack.router
  },
  "CategoryCreate": {
    "screen": CategoryCreateStack.router
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "headerMode": "none"
});
