let router = SimpleDrawerNav.stackNav({
  "DevLog": {
    "screen": (_) => {
      <DebounceInitialRender>
        <DevLog />
      </DebounceInitialRender>
    },
    "navigationOptions": {
      "title": "DevLog",
      "gesturesEnabled": false,
      "drawerLockMode": "unlocked"
    }
  }
}, {
  "cardStyle": {
    "backgroundColor": "#fff"
  },
  "navigationOptions": {
    "gesturesEnabled": true,
    "drawerLockMode": "locked-closed",
    "header": (headerProps) => {
      <Header headerProps />
    }
  }
});
