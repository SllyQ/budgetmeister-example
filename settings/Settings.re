open ReactNative;
open RnMui;

type state = {
  showAdvancedSettings: bool,
  showExpensesDailyTotals: bool,
  mainCurrency: Currency.t,
  currencySelectVisible: bool,
  selectedTheme: Colors.themeOption,
};

type action =
  | UpdateShowAdvancedSettings(bool)
  | UpdateShowExpensesDailyTotals(bool)
  | UpdateMainCurrency(Currency.t)
  | ShowCurrencySelectModal
  | CurrencySelected(option(Currency.t))
  | UpdateTheme(Colors.themeOption)
  | UpdateHelpOnDeviceShake(bool)
  | UpdateShowProjectedRecurringExpenses(bool)
  | UpdateAutomaticSync(bool)
  | UpdateBudgetingPeriod(BudgetingPeriod.t)
  | UpdateLightBars(bool);

let component = ReasonReact.reducerComponent("Settings");

let saveSettings = (state) => {
  SettingsDb.setItem({
    ...SettingsDb.get(),
    showAdvancedSettings: state.showAdvancedSettings,
    showExpensesDailyTotals: state.showExpensesDailyTotals,
    mainCurrency: state.mainCurrency
  });
};

let dropdownData = Currency.all |> List.map(currency => {
  "value": Currency.currencyToString(currency),
  "label": Currency.symbol(currency)
}) |> Array.of_list;

let make = (_) => {
  ...component,
  initialState: () => {
    let settings = SettingsDb.get();
    let history = HistoryDb.get();
    {
      showAdvancedSettings: settings.showAdvancedSettings,
      showExpensesDailyTotals: settings.showExpensesDailyTotals,
      mainCurrency: settings.mainCurrency,
      currencySelectVisible: false,
      selectedTheme: history.selectedTheme,
    }
  },
  reducer: (action, state) =>
    switch action {
    | UpdateShowAdvancedSettings(showAdvancedSettings) =>
      ReasonReact.UpdateWithSideEffects(
        {...state, showAdvancedSettings},
        ({ state }) => saveSettings(state)
      )
    | UpdateShowExpensesDailyTotals(showExpensesDailyTotals) =>
      ReasonReact.UpdateWithSideEffects(
        {...state, showExpensesDailyTotals},
        ({ state }) => saveSettings(state)
      )
    | UpdateMainCurrency(mainCurrency) => {
      ReasonReact.UpdateWithSideEffects(
        {...state, mainCurrency },
        ({ state }) => saveSettings(state)
      )
    }
    | UpdateTheme(selectedTheme) => ReasonReact.UpdateWithSideEffects({
      ...state,
      selectedTheme
    }, (_) => {
      HistoryDb.setItem({
        ...HistoryDb.get(),
        selectedTheme
      });
      Colors.setTheme(selectedTheme);
    })
    | ShowCurrencySelectModal => ReasonReact.Update {
      ...state,
      currencySelectVisible: true
    }
    | CurrencySelected(optCurrency) => switch optCurrency {
      | None => ReasonReact.Update {
        ...state,
        currencySelectVisible: false
      }
      | Some(currency) => ReasonReact.UpdateWithSideEffects({
        ...state,
        mainCurrency: currency,
        currencySelectVisible: false
      }, ({ state }) => saveSettings(state))
    }
    | UpdateHelpOnDeviceShake(helpOnDeviceShake) => ReasonReact.SideEffects((_) => {
      SettingsDb.setItem({
        ...SettingsDb.get(),
        helpOnDeviceShake
      })
    })
    | UpdateLightBars(lightBars) => ReasonReact.SideEffects((_) => {
      SettingsDb.setItem({
        ...SettingsDb.get(),
        lightBars
      })
    })
    | UpdateShowProjectedRecurringExpenses(showProjectedRecurringExpenses) => ReasonReact.SideEffects((_) => {
      SettingsDb.setItem({
        ...SettingsDb.get(),
        showProjectedRecurringExpenses
      })
    })
    | UpdateBudgetingPeriod(budgetingPeriod) => ReasonReact.SideEffects((_) => {
      Js.log2("Updating period", budgetingPeriod);
      SettingsDb.setItem({
        ...SettingsDb.get(),
        budgetingPeriod
      })
    })
    | UpdateAutomaticSync(syncAutomatically) => ReasonReact.SideEffects((_) => {
      SettingsDb.setItem({
        ...SettingsDb.get(),
        syncAutomatically
      })
    })
    },
  render: ({state, send }) => {
    let {mainCurrency, showExpensesDailyTotals, currencySelectVisible} = state;
    <Connect_SelectedTheme render=(theme => {
      <HelpWrapper helpComponent={
        title: "Settings",
        message: `Element(<Column>
          <SubheadingText style=Style.(style([color(theme.primary), Styles.Text.robotoMedium, paddingTop(8.), paddingBottom(2.)]))
            value="Show future recurring expenses"
          />
          <BodyText value="Shows recurring expenses that are going to be created in the nearby future. Helps you see upcoming expenses." />
          <SubheadingText style=Style.(style([color(theme.primary), Styles.Text.robotoMedium, paddingTop(8.), paddingBottom(2.)]))
            value="Budgeting period"
          />
          <BodyText value="Allows you to set a budgeting period that starts on a different day of a month (other than 1st), or switch to bi-weekly budgeting. Useful if you want to synchronize your budgeting with your pay." />
          <SubheadingText style=Style.(style([color(theme.primary), Styles.Text.robotoMedium, paddingTop(8.), paddingBottom(2.)]))
            value="Show help on device shake"
          />
          <BodyText value="Enable/disable showing current screen after shaking your phone." />
          <SubheadingText style=Style.(style([color(theme.primary), Styles.Text.robotoMedium, paddingTop(8.), paddingBottom(2.)]))
            value="Main currency"
          />
          <BodyText value="The currency that will be used when summarizing your expenses. All other currencies will be converted to your main currency." />
        </Column>)
      }>
        <ScreenWrap>
          <C_Settings render=((settings) => {
            <C_History render=((history) => {
              <ListWrapper>
                <ListSubheader value="Expenses list" primary=true />
                <OptionSwitchRow label="Daily totals"
                  value=showExpensesDailyTotals
                  onValueChange=((show) => send(UpdateShowExpensesDailyTotals(show)))
                />
                <OptionSwitchRow label="Show future recurring expenses"
                  value=settings.showProjectedRecurringExpenses
                  onValueChange=((showProjectedRecurringExpenses) =>
                    send(UpdateShowProjectedRecurringExpenses(showProjectedRecurringExpenses))
                  )
                />
                <ListSubheader value="Other" primary=true withDivider=true />
                <BudgetingPeriodSelectRow value=settings.budgetingPeriod
                  onValueChanged=((budgetingPeriod) => send(UpdateBudgetingPeriod(budgetingPeriod)))
                />
                <OptionSwitchRow label="Show help on device shake"
                  value=settings.helpOnDeviceShake
                  onValueChange=((helpOnDeviceShake) => send(UpdateHelpOnDeviceShake(helpOnDeviceShake)))
                />
                <F_Themes>
                  <ListRow>
                    <Dropdown
                      label="Theme"
                      data=Colors.([|{
                        "label": IndigoRed |> themeToLabel,
                        "value": IndigoRed
                      }, {
                        "label": Original |> themeToLabel,
                        "value": Original
                      }, {
                        "label": OriginalLight |> themeToLabel,
                        "value": OriginalLight
                      }, {
                        "label": OriginalDark |> themeToLabel,
                        "value": OriginalDark
                      }, {
                        "label": DeepPurpleBlue |> themeToLabel,
                        "value": DeepPurpleBlue
                      }, {
                        "label": BluePink |> themeToLabel,
                        "value": BluePink
                      }, {
                        "label": BlueGreyIndigo |> themeToLabel,
                        "value": BlueGreyIndigo
                      }|])
                      value=(state.selectedTheme |> Colors.themeToLabel)
                      onChangeText=(((theme, _)) => send(UpdateTheme(theme)))
                    />
                  </ListRow>
                </F_Themes>
                <F_PastelColors>
                  <OptionSwitchRow label="Pastel category colors"
                    value=history.pastelColors
                    onValueChange=((pastelColors) => HistoryDb.setItem({
                      ...HistoryDb.get(),
                      pastelColors
                    }))
                  />
                </F_PastelColors>
                <ListRow numberOfLines=1 flexRow=true style=Style.(style([justifyContent(`spaceBetween)]))
                  onPress=Callback(() => send(ShowCurrencySelectModal))
                >
                  <Text value="Main currency" style=Styles.Text.listPrimary />
                  {
                    let str = Currency.currencyToString(mainCurrency);
                    let symbol = Currency.symbol(mainCurrency);
                    <MuiButton text={j|$(str) ($(symbol))|j}
                      primary=true
                      onPress=(() => send(ShowCurrencySelectModal))
                    />
                  }
                </ListRow>
                <CurrencySelectModal
                  isVisible=currencySelectVisible showRecentlyUsed=false
                  onCurrencySelected=(currency => send(CurrencySelected(currency)))
                />
                <F_AutoSync>
                  <OptionSwitchRow label="Sync automatically"
                    value=settings.syncAutomatically
                    onValueChange=((syncAutomatically) => send(UpdateAutomaticSync(syncAutomatically)))
                  />
                </F_AutoSync>
                <C_Alpha render=(() => {
                  <OptionSwitchRow label="Light bars"
                    value=settings.lightBars
                    onValueChange=((helpOnDeviceShake) => send(UpdateLightBars(helpOnDeviceShake)))
                  />
                }) />
                /* <OptionSwitchRow label="Advanced settings"
                  value=showAdvancedSettings
                  onValueChange=(reduce((show) => UpdateShowAdvancedSettings(show)))
                /> */
                <ListSubheader value="App info" primary=true withDivider=true />
                <ListRow numberOfLines=1 flexRow=true style=Style.(style([justifyContent(`spaceBetween)]))>
                  <Text value="Version" style=Styles.Text.listPrimary />
                  <Text value="1.0.4" style=Style.(combine(Styles.Text.listPrimary, style([color(Colors.grey)]))) />
                </ListRow>
              </ListWrapper>
            }) />
          }) />
        </ScreenWrap>
      </HelpWrapper>
    }) />
  }
};
