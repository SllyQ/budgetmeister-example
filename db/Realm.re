[@bs.module "../../../../src/db/realm.js"]
external internal_getCfg : string => Js.Nullable.t(string) = "getCfg";
let getCfg = (key) => internal_getCfg(key) |> Js.Nullable.toOption;

[@bs.module "../../../../src/db/realm.js"]
external setCfg: string => string => unit = "";

[@bs.module "../../../../src/db/realm.js"]
external deleteCfg: string => unit = "";
