import { InteractionManager } from "react-native"
import Rt from "reactotron-react-native"
import {
  addItem as addRealmItem,
  getItems as getRealmItems,
  getAllItems as getAllRealmItems,
} from "./realm"

export const addItem = item => {
  addRealmItem(item)
  return Promise.resolve()
}

const chunkSize = 32

export const getItems = collection => {
  const data = getRealmItems(collection)
  const ret = []

  let inc = i => {
    for (let j = i; j < i + chunkSize; j++) {
      if (j >= data.length) {
        return Promise.resolve()
      } else {
        ret.push(data[j])
      }
    }
    return InteractionManager.runAfterInteractions({
      gen: () =>
        new Promise(resolve => {
          setTimeout(() => {
            resolve(inc(i + chunkSize))
          }, 0)
        }),
    })
  }
  if (data.length === 0) {
    return Promise.resolve([])
  } else {
    return new Promise(resolve => {
      InteractionManager.runAfterInteractions({
        gen: () => inc(0),
      }).then(() => {
        resolve(ret)
      })
    })
  }
}
