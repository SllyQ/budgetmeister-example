type remoteStorageFormat = {
  .
  "entityId": Js.Nullable.t(string),
  "entryId": string,
  "data": Js.Json.t,
  "collectionKey": string,
  "createTimestamp": string,
  "generatorData": Js.Nullable.t({.
    "id": string,
    "lastUpdateTimestamp": string
  })
};

let decodeGeneratorData = (json) => Json.Decode.({
  "id": json |> field("id", string),
  "lastUpdateTimestamp": json |> field("lastUpdateTimestamp", string)
});

let decodeRemoteStorage = (json) =>
  Json.Decode.(
    {
      "entryId": json |> field("entryId", string),
      "createTimestamp": json |> field("createTimestamp", string),
      "collection": json |> field("collectionKey", string),
      "data": json |> field("data", (a) => a),
      "generatorData": json
        |> optional(field("generatorData", decodeGeneratorData))
        |> Js.Nullable.fromOption
    }
  );

external toRespData : 'a => {. "data": array(Js.Json.t)} = "%identity";

let decodeFetchResp = (resp) => {
  let obj = toRespData(resp);
  let respItems = Array.map(decodeRemoteStorage, obj##data);
  respItems
};

let store = (item) => {
  let _ = SQLite.addItem(item);
};

let fetchAll = (~collection) => SQLite.getItems(collection);

let dropAll = () => SQLite.dropAllData();


/* TODO: Need to move everything to new db format to remove these */
let storeAllRemote = Obj.magic();
/* let storeAllRemote = (~collection, entries) =>
  switch AppAuthService.jwt^ {
  | None => ()
  | Some(jwt) =>
    let remoteEntries: array(remoteStorageFormat) =
      entries
      |> Array.map(
           (entry) => {
             "entityId": entry##entityId,
             "entryId": entry##entryId,
             "data": entry##data,
             "collectionKey": collection,
             "createTimestamp": entry##createTimestamp,
             "generatorData": entry##generatorData
           }
         );
    let json =
      switch ({"items": remoteEntries} |> Js.Json.stringifyAny) {
      | Some(j) => j
      | None => ""
      };
    let reqInit =
      Bs_fetch.RequestInit.make(
        ~method_=Post,
        ~headers=
          Bs_fetch.HeadersInit.make({
            "Content-Type": "application/json",
            "Authorization": "Bearer " ++ jwt
          }),
        ~body=Bs_fetch.BodyInit.make(json),
        ()
      );
    let _ =
      Bs_fetch.fetchWithInit(Config.apiUrl ++ "/items", reqInit)
      |> Js.Promise.then_(Bs_fetch.Response.json);
    ()
  /* |> Js.Promise.then_ (fun resp => {
       Js.Promise.resolve ()
     }); */
  }; */

let fetchAllRemote = Obj.magic();
/* let fetchAllRemote = (~collection) =>
  switch AppAuthService.jwt^ {
  | None => Js.Promise.resolve([||])
  | Some(jwt) =>
    let reqInit =
      Bs_fetch.RequestInit.make(
        ~method_=Get,
        ~headers=Bs_fetch.HeadersInit.make({"Authorization": "Bearer " ++ jwt}),
        ()
      );
    Bs_fetch.fetchWithInit(Config.apiUrl ++ ("/items/" ++ collection), reqInit)
    |> Js.Promise.then_(Bs_fetch.Response.json)
    |> Js.Promise.then_(
         (respJson) => {
           let items = decodeFetchResp(respJson);
           Js.Promise.resolve(
             items
             |> Array.map(
                  (item) => {
                    "data": item##data,
                    "entryId": item##entryId,
                    "collection": item##collection,
                    "entityId": Js.Nullable.null,
                    "createTimestamp": item##createTimestamp,
                    "generatorData": item##generatorData
                  }
                )
           )
         }
       )
  }; */
