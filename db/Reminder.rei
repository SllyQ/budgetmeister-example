type id;

let key: id => string;

type t = {
  id,
  message: option(string),
  daysOfWeek: array(int),
  time: Js.Date.t
};

let getPlaceholderId: unit => id;
