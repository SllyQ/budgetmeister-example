let initCritical = (~localStorageConfig, ~remoteStorageConfig, ()) => {
  Js.Promise.all([|
    HistoryDb.init(~storageKey="history", ~localStorageConfig, ()),
    SettingsDb.init(~storageKey="settings", ~localStorageConfig, ~remoteStorageConfig, ()),
    CategoryDb.init(~storageKey="categories", ~localStorageConfig, ~remoteStorageConfig, ()),
    TagDb.init(~storageKey="tags", ~localStorageConfig, ~remoteStorageConfig, ())
  |])
  |> Js.Promise.then_((_) => {
    Js.Promise.resolve()
  })
};

let delayPromise = (delay, fn) => Js.Promise.make((~resolve, ~reject as _) => {
  Js.Global.setTimeout(() => {
    fn() |> C.Promise.consume(a => resolve(. a))
  }, delay) |> ignore
});

let init = (~localStorageConfig, ~remoteStorageConfig, ()) => {
  BudgetDb.init(~storageKey="budget", ~localStorageConfig, ~remoteStorageConfig, ())
  |> Js.Promise.then_(() => delayPromise(0, () => BudgetDb.init(~storageKey="budget", ~localStorageConfig, ~remoteStorageConfig, ())))
  |> Js.Promise.then_(() => delayPromise(0, () => BudgetDb2.init(~storageKey="budget2", ~localStorageConfig, ~remoteStorageConfig, ())))
  |> Js.Promise.then_(() => delayPromise(0, () => TemplateDb.init(~storageKey="templates", ~localStorageConfig, ())))
  |> Js.Promise.then_(() => delayPromise(0, () => RecurringExpenseDb.init(~storageKey="recurring_expenses", ~localStorageConfig, ())))
  |> Js.Promise.then_(() => delayPromise(0, () => ExpenseDb.init(~storageKey="spendings", ~localStorageConfig, ())))
  |> Js.Promise.then_(() => delayPromise(0, () => ExpenseTagDb.init(~storageKey="spending_tag", ~localStorageConfig, ~remoteStorageConfig, ())))
  |> Js.Promise.then_((_) => {
    PeriodicExpensesView.init();
    RecurringExpensesService.init();
    Js.Promise.resolve()
  })
  |> Js.Promise.catch((e) => {
    Log.log(~key="Global db init err", e);
    Js.Promise.resolve()
  })
};

let clear = () =>
  Js.Promise.all([|
    CategoryDb.clear(),
    ExpenseDb.clear(),
    TagDb.clear(),
    ExpenseTagDb.clear(),
    SettingsDb.clear(),
    BudgetDb.clear(),
    BudgetDb2.clear(),
    TemplateDb.clear(),
    RecurringExpenseDb.clear(),
    HistoryDb.clear()
  |]);
