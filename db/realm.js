import Realm from "realm"
import Rt from "reactotron-react-native"

require("es6-symbol/implement")
if (Array.prototype[Symbol.iterator] === undefined) {
  Array.prototype[Symbol.iterator] = function() {
    let i = 0
    return {
      next: () => ({
        done: i >= this.length,
        value: this[i++],
      }),
    }
  }
}

class GeneratorData extends Realm.Object {}
GeneratorData.schema = {
  name: "GeneratorData",
  properties: {
    id: "string",
    lastUpdateTimestamp: "string",
  },
}

type generatorData = {
  lastUpdateTimestamp: string,
  id: string,
}

class Doc extends Realm.Object {}
Doc.schema = {
  name: "Doc",
  primaryKey: "entryId",
  properties: {
    entityId: { type: "string", optional: true },
    entryId: "string",
    dataString: "string",
    collectionKey: "string",
    createTimestamp: "string",
    systemCreateTimestamp: "string",
    generatorData: { type: "GeneratorData", optional: true },
  },
}
Object.defineProperty(Doc.prototype, "data", {
  get: function() {
    return JSON.parse(this.dataString)
  },
})

class Config extends Realm.Object {}
Config.schema = {
  name: "Config",
  primaryKey: "key",
  properties: {
    key: "string",
    value: "string",
  },
}

const realm = new Realm({
  schema: [GeneratorData, Doc, Config],
  schemaVersion: 2,
})

export const getItems = collection => {
  return realm.objects("Doc").filtered(`collectionKey = "${collection}"`)
}

export const getAllItems = () => realm.objects("Doc")

export const addItem = item => {
  realm.write(() => {
    if (realm.objects("Doc").filtered(`entryId = "${item.entryId}"`).length === 0) {
      realm.create("Doc", {
        entityId: item.entityId,
        entryId: item.entryId,
        dataString: JSON.stringify(item.data),
        collectionKey: item.collectionKey,
        createTimestamp: item.createTimestamp,
        systemCreateTimestamp: item.systemCreateTimestamp || new Date().toISOString(),
        generatorData: item.generatorData,
      })
    }
  })
}

export const getCfg = key => {
  const ret = realm.objects("Config").filtered(`key = "${key}"`)
  const val = Array.from(ret)[0]
  return val && val.value
}

export const setCfg = (key, value) => {
  realm.write(() => {
    realm.delete(realm.objects("Config").filtered(`key = "${key}"`))
    realm.create("Config", {
      key,
      value,
    })
  })
}

export const deleteCfg = key => {
  const ret = realm.objects("Config").filtered(`key = "${key}"`)
  const val = Array.from(ret)[0]
  if (val) {
    realm.write(() => {
      realm.delete(val)
    })
  }
}
