[@bs.module "../../../../src/db/sql.js"]
external getItems : string => Js.Promise.t(array(SyncService.remoteStorageFormat)) =
  "";

[@bs.module "../../../../src/db/sql.js"]
external addItem : (SyncService.remoteStorageFormat) => Js.Promise.t(unit) =
  "";

[@bs.module "../../../../src/db/sql.js"] external dropAllData : unit => Js.Promise.t(unit) = "";

[@bs.module "../../../src/db/sql.js"] external dropAllData2 : unit => Js.Promise.t(unit) =
  "dropAllData";
