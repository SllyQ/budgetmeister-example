type id = string;

let key = id => id;

type t = {
  id,
  message: option(string),
  daysOfWeek: array(int),
  time: Js.Date.t
};

let getPlaceholderId = () => "";
