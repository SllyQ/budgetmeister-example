open ReactNative;
open SharingFormScreenComponents;
open Paper;

type splitPart = {
  amount: NumberInput.value,
  userId: string
};

type formSplit =
  | Equally(array(string))
  | Fixed(array(splitPart))
  | Percentage(array(splitPart))
  | Shares(array(splitPart))
  | Adjustment(array(splitPart));


let styles = Style.(StyleSheet.create({
  "inputSuffix": combine(Styles.Text.listPrimary, style([paddingBottom(15.), paddingLeft(5.)])),
  "inputWrap": style([width(50.)])
}));

type state = {
  totalAmount: NumberInput.value,
  currency: Currency.t,
  sharedBetween: array(User.id),
  split: formSplit,
  paidBy: ShareConfig.paidBy,
  costInputRef: ref(option(ReasonReact.reactRef))
};

type actions =
  | AmountChanged(NumberInput.value)
  | CurrencyChanged(Currency.t)
  | ChangeSplittingMethod(string)
  | ToggleUser(User.id)
  | UpdatePartAmount(User.id, NumberInput.value)
  | SubmitShareConfig
  | AddSharedBetween(User.id)
  | RemoveSharedBetween(User.id);

let component = ReasonReact.reducerComponent("SharingFormScreen");

let setCostInputRef = (nullRef, { ReasonReact.state }) => {
  state.costInputRef := nullRef |> Js.Nullable.toOption
};

let handleFocusCostInput = ((), { ReasonReact.state }) => {
  switch state.costInputRef.contents {
    | Some(ref) => CostInput.focus(ref)
    | None => ()
  }
};

let options = [|{
  "value": "Evenly",
  "label": "Evenly"
}, {
  "value": "Fixed",
  "label": "Fixed"
}, {
  "value": "Percentage",
  "label": "By percentage"
}, {
  "value": "Shares",
  "label": "By shares"
}, {
  "value": "Adjustment",
  "label": "By adjustment"
}|];

let make = (~sharing=?, ~onShareConfigSelected, _) => {
  ...component,
  initialState: () => {
    let user = AppAuthService.user^ |> Js.Option.getExn;
    switch (sharing) {
      | Some(sharing: RParams.sharing) => {
        totalAmount: sharing.totalAmount |> NumberInput.toValue,
        currency: sharing.currency,
        sharedBetween: switch (sharing.shareConfig.ratio) {
          | Equally(users) => users
          | Fixed(parts) | Percentage(parts) | Shares(parts) | Adjustment(parts) =>
            parts |> Array.map(({ ShareConfig.userId }) => userId)
        },
        split: switch (sharing.shareConfig.ratio) {
          | Equally(users) => Equally(users)
          | Fixed(parts) =>
            Fixed(parts |> Array.map(({ ShareConfig.userId, amount }) => { userId, amount: amount |> NumberInput.toValue }))
          | Percentage(parts) =>
            Percentage(parts |> Array.map(({ ShareConfig.userId, amount }) => { userId, amount: amount |> NumberInput.toValue }))
          | Shares(parts) =>
            Shares(parts |> Array.map(({ ShareConfig.userId, amount }) => { userId, amount: amount |> NumberInput.toValue }))
          | Adjustment(parts) =>
            Adjustment(parts |> Array.map(({ ShareConfig.userId, amount }) => { userId, amount: amount |> NumberInput.toValue }))
        },
        paidBy: sharing.shareConfig.paidBy,
        costInputRef: ref(None)
      }
      | None => {
        totalAmount: Empty,
        currency: SettingsDb.get().mainCurrency,
        sharedBetween: [|user.id|],
        split: Equally([|user.id|]),
        paidBy: User(user.id),
        costInputRef: ref(None)
      }
    }
  },
  didMount: ({ handle }) => {
    Js.Global.setTimeout((_) => {
      handle(handleFocusCostInput, ());
    }, 0) |> ignore;
  },
  reducer: C.flip(state => fun
    | AmountChanged(totalAmount) => ReasonReact.Update {
      ...state,
      totalAmount
    }
    | CurrencyChanged(currency) => ReasonReact.Update {
      ...state,
      currency
    }
    | ToggleUser(userId) => {
      switch state.split {
        | Equally(users) => {
          if (users |> Js.Array.includes(userId)) {
            ReasonReact.Update {
              ...state,
              split: Equally(
                users |> Js.Array.filter(u => u != userId)
              )
            }
          } else {
            ReasonReact.Update {
              ...state,
              split: Equally(
                users |> Js.Array.concat([|userId|])
              )
            }
          }
        }
        | _ => ReasonReact.NoUpdate
      }
    }
    | UpdatePartAmount(user, newAmount) => {
      switch (state.split) {
        | Equally(_) => ReasonReact.NoUpdate
        | Fixed(parts) => {
          ReasonReact.Update {
            ...state,
            split: Fixed(
              parts |> Array.map(({ userId, amount }) => user == userId ? {
                userId: userId,
                amount: newAmount
              } : {
                userId: userId,
                amount: amount
              })
            )
          }
        }
        | Percentage(parts) => {
          ReasonReact.Update {
            ...state,
            split: Percentage(
              parts |> Array.map(({ userId, amount }) => user == userId ? {
                userId: userId,
                amount: newAmount
              } : {
                userId: userId,
                amount: amount
              })
            )
          }
        }
        | Shares(parts) => {
          ReasonReact.Update {
            ...state,
            split: Shares(
              parts |> Array.map(({ userId, amount }) => user == userId ? {
                userId: userId,
                amount: newAmount
              } : {
                userId: userId,
                amount: amount
              })
            )
          }
        }
        | Adjustment(parts) => {
          ReasonReact.Update {
            ...state,
            split: Adjustment(
              parts |> Array.map(({ userId, amount }) => user == userId ? {
                userId: userId,
                amount: newAmount
              } : {
                userId: userId,
                amount: amount
              })
            )
          }
        }
      }
    }
    | ChangeSplittingMethod(strMethod) => ReasonReact.Update {
      ...state,
      split: switch (strMethod) {
        | "Evenly" => Equally(state.sharedBetween)
        | "Fixed" => Fixed(state.sharedBetween |> Array.map(userId => {
          userId: userId,
          amount: Empty
        }))
        | "Percentage" => Percentage(state.sharedBetween |> Array.map(userId => {
          userId: userId,
          amount: Empty
        }))
        | "Shares" => Shares(state.sharedBetween |> Array.map(userId => {
          userId: userId,
          amount: Empty
        }))
        | "Adjustment" => Adjustment(state.sharedBetween |> Array.map(userId => {
          userId: userId,
          amount: Empty
        }))
        | _ => Equally(state.sharedBetween)
      }
    }
    | AddSharedBetween(userId) => ReasonReact.Update {
      ...state,
      sharedBetween: Array.append(state.sharedBetween, [|userId|]),
      split: switch (state.split) {
        | Equally(users) => Equally(Array.append(users, [|userId|]))
        | Fixed(parts) => Fixed(Array.append(parts, [|{ userId, amount: Empty }|]))
        | Percentage(parts) => Percentage(Array.append(parts, [|{ userId, amount: Empty }|]))
        | Shares(parts) => Shares(Array.append(parts, [|{ userId, amount: Empty }|]))
        | Adjustment(parts) => Adjustment(Array.append(parts, [|{ userId, amount: Empty }|]))
      }
    }
    | RemoveSharedBetween(userId) => ReasonReact.Update {
      ...state,
      sharedBetween: state.sharedBetween |> Js.Array.filter(uid => uid != userId),
      split: switch (state.split) {
        | Equally(users) => Equally(users |> Js.Array.filter(user => user != userId))
        | Fixed(parts) => Fixed(parts |> Js.Array.filter(({ userId: uid }) => uid != userId))
        | Percentage(parts) => Percentage(parts |> Js.Array.filter(({ userId: uid }) => uid != userId))
        | Shares(parts) => Shares(parts |> Js.Array.filter(({ userId: uid }) => uid != userId))
        | Adjustment(parts) => Adjustment(parts |> Js.Array.filter(({ userId: uid }) => uid != userId))
      }
    }
    | SubmitShareConfig => ReasonReact.SideEffects((_) => {
      let parsedSplitting = switch(state.split) {
        | Equally(users) => Some(ShareConfig.Equally(users))
        | Fixed(parts) => {
          let parsedParts = parts |> Array.fold_left((acc, { userId, amount }) => {
            switch (acc, amount |> NumberInput.toNumber) {
              | (Some(parts), Some(num)) => Some(Array.append(parts, [|{ ShareConfig.userId, amount: num }|]))
              | _ => None
            }
          }, Some([||]));
          parsedParts |> C.Option.map(parts => ShareConfig.Fixed(parts));
        }
        | Percentage(parts) => {
          let parsedParts = parts |> Array.fold_left((acc, { userId, amount }) => {
            switch (acc, amount |> NumberInput.toNumber) {
              | (Some(parts), Some(num)) => Some(Array.append(parts, [|{ ShareConfig.userId, amount: num }|]))
              | _ => None
            }
          }, Some([||]));
          parsedParts |> C.Option.map(parts => ShareConfig.Percentage(parts));
        }
        | Shares(parts) => {
          let parsedParts = parts |> Array.fold_left((acc, { userId, amount }) => {
            switch (acc, amount |> NumberInput.toNumber) {
              | (Some(parts), Some(num)) => Some(Array.append(parts, [|{ ShareConfig.userId, amount: num }|]))
              | _ => None
            }
          }, Some([||]));
          parsedParts |> C.Option.map(parts => ShareConfig.Shares(parts));
        }
        | Adjustment(parts) => {
          let parsedParts = parts |> Array.fold_left((acc, { userId, amount }) => {
            switch (acc, amount |> NumberInput.toNumber) {
              | (Some(parts), Some(num)) => Some(Array.append(parts, [|{ ShareConfig.userId, amount: num }|]))
              | _ => None
            }
          }, Some([||]));
          parsedParts |> C.Option.map(parts => ShareConfig.Adjustment(parts));
        }
      };
      switch (state.totalAmount |> NumberInput.toNumber, parsedSplitting) {
        | (Some(num), Some(ratio)) => onShareConfigSelected({
          RParams.totalAmount: num,
          currency: state.currency,
          shareConfig: {
            ratio,
            paidBy: state.paidBy,
            groupId: None
          }
        })
        | _ => Alert.alert(~title="Error", ~message="Something has gone wrong", ())
      }
    })
  ),
  render: ({ handle, state, send }) => {
    let { totalAmount, currency, sharedBetween, split } = state;
    let totalAmountNum = totalAmount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.);
    <ScreenWrap fab=(
      <ThemeActionButton icon="check" onPress=(() => send(SubmitShareConfig)) />
    )>
      <Connect_SelectedTheme render=(theme => {
        <Connect_Friends render=(friends => {
          let friendsLeft = friends |> Js.Array.filter(friend => {
            !Js.Array.includes(friend.User.id, sharedBetween)
          });
          <ListWrapper>
            <Column>
              <ListRow>
                <CostInput amount=totalAmount currency
                  ref=handle(setCostInputRef)
                  label="Total spent"
                  onAmountChanged=(amount => send(AmountChanged(amount)))
                  onCurrencyChanged=(currency => send(CurrencyChanged(currency)))
                />
              </ListRow>
              <ListRow>
                <RnMui.Dropdown
                  label="Split"
                  data=options
                  value="Evenly"
                  onChangeText=(((ratio, _)) => send(ChangeSplittingMethod(ratio)))
                />
              </ListRow>
              (switch (split) {
                | Equally(users) => {
                  <Column>
                    (
                      sharedBetween
                      |> Array.map((userId: User.id) => {
                        let checked = (users |> Js.Array.some(uid => uid == userId));
                        <ListRow key=userId numberOfLines=1 onPress=Callback(() => send(
                          ToggleUser(userId))
                        )>
                          <Row alignItems=`center justifyContent=`spaceBetween style=Style.(style([marginLeft(-8.)]))>
                            <Row alignItems=`center>
                              <RemoveButton userId onPress=(() => send(RemoveSharedBetween(userId))) />
                              <C_User userId render=(user => {
                                <ListPrimary
                                  value=(switch (user) {
                                    | You(_) => "You"
                                    | User(user) => user.username
                                    | UnknownUser => "Unknown user"
                                  })
                                />
                              }) />
                            </Row>
                            <Checkbox checked />
                          </Row>
                        </ListRow>
                      })
                      |> ReasonReact.array
                    )
                    <ListRow>
                      <ListSecondary>
                        <Text value="Total amount per person: " />
                        <Text value=Currency.format(~currency, ~amount=totalAmountNum /. float_of_int(Array.length(users))) />
                      </ListSecondary>
                    </ListRow>
                  </Column>
                }
                | Fixed(shares) => {
                  <Column>
                    <ListRow>
                      <ListSecondary value="Enter each person's share" />
                    </ListRow>
                    (shares
                    |> Array.map(({ userId, amount }) => {
                      <ListRow key=userId style=Style.(style([marginTop(-16.)]))>
                        <Row alignItems=`flexEnd justifyContent=`spaceBetween>
                          <Row alignItems=`flexEnd>
                            <RemoveButton userId onPress=(() => send(RemoveSharedBetween(userId))) />
                            <Text value=C_User.getUsername(userId) style=Style.(combine(Styles.Text.listPrimary, style([paddingBottom(14.)]))) />
                          </Row>
                          <Row alignItems=`flexEnd>
                            <View style=styles##inputWrap>
                              <NumberInput label="" value=amount onChangeValue=((v) => send(UpdatePartAmount(userId, v))) placeholder="0.00" />
                            </View>
                            <Text style=styles##inputSuffix value=Currency.symbol(currency) />
                          </Row>
                        </Row>
                      </ListRow>
                    })
                    |> ReasonReact.array)
                    <ListRow numberOfLines=1>
                    {
                      let shareTotal = shares |> Array.map(({ amount }) => amount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.)) |> Array.fold_left((+.), 0.);
                      let totalLeft = (totalAmountNum -. shareTotal);
                      let fullySplit = C.floatEq(totalAmountNum, shareTotal);
                      <ListSecondary>
                        <Text value="Amount left: " />
                        (switch (fullySplit, totalLeft) {
                          | (true, _) => <Text value="0.00" />
                          | (false, totalLeft) when totalLeft >= 0. => <Text value=Currency.format(~currency, ~amount=totalLeft) />
                          | (false, totalLeft) => <Text style=Style.(style([color(Colors.danger)])) value=Currency.format(~currency, ~amount=totalLeft) />
                        })
                      </ListSecondary>
                    }
                    </ListRow>
                  </Column>
                }
                | Percentage(shares) => {
                  <Column>
                    (
                      shares
                      |> Array.map(({ userId, amount }) => {
                        let currentAmount = amount
                        |> NumberInput.toNumber
                        |> Js.Option.getWithDefault(0.)
                        |> ((/.)(_, 100.))
                        |> ((*.)(totalAmountNum));
                        <ListRow key=userId style=Style.(style([marginTop(-16.)]))>
                          <Row alignItems=`flexEnd justifyContent=`spaceBetween>
                            <Row alignItems=`center>
                              <RemoveButton userId onPress=(() => send(RemoveSharedBetween(userId))) />
                              <Column style=Style.(style([paddingBottom(14.)]))>
                                <ListPrimary value=C_User.getUsername(userId) />
                                <ListSecondary>
                                  <Text value="Total share: " />
                                  <Text value=Currency.format(~currency, ~amount=currentAmount) />
                                </ListSecondary>
                              </Column>
                            </Row>
                            <Row alignItems=`flexEnd>
                              <View style=styles##inputWrap>
                                <NumberInput label="" value=amount onChangeValue=((v) => send(UpdatePartAmount(userId, v))) placeholder="0.00" />
                              </View>
                              <Text style=styles##inputSuffix value="%" />
                            </Row>
                          </Row>
                        </ListRow>
                      })
                      |> ReasonReact.array
                    )
                    <ListRow numberOfLines=1>
                    {
                      let shareTotal = shares |> Array.map(({ amount }) => amount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.)) |> Array.fold_left((+.), 0.);
                      let totalLeft = (100. -. shareTotal);
                      let totalLeftStr = totalLeft |> Js.Float.toFixedWithPrecision(~digits=2);
                      let fullySplit = C.floatEq(totalLeft, 0.);
                      <ListSecondary>
                        <Text value="Amount left: " />
                        (switch (fullySplit, totalLeft) {
                          | (true, _) => <Text value="0.00%" />
                          | (false, totalLeft) when totalLeft >= 0. => <Text value=(totalLeftStr ++ "%") />
                          | (false, _) => <Text style=Style.(style([color(Colors.danger)])) value=(totalLeftStr ++ "%") />
                        })
                      </ListSecondary>
                    }
                    </ListRow>
                  </Column>
                }
                | Shares(shares) => {
                  let totalShares = shares
                  |> Array.map(part => part.amount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.))
                  |> Js.Array.filter(amount => amount >= 0.)
                  |> Array.fold_left((+.), 0.);
                  shares
                  |> Array.map(({ userId, amount }) => {
                    let userTotalAmount = amount
                    |> NumberInput.toNumber
                    |> Js.Option.getWithDefault(0.)
                    |> (totalShares == 0. ? C.const(0.) : ((/.)(_, totalShares)))
                    |> ((*.)(totalAmountNum));
                    <ListRow key=userId style=Style.(style([marginTop(-16.)]))>
                      <Row alignItems=`flexEnd justifyContent=`spaceBetween>
                        <Row alignItems=`center>
                          <RemoveButton userId onPress=(() => send(RemoveSharedBetween(userId))) />
                          <Column style=Style.(style([paddingBottom(14.)]))>
                            <ListPrimary value=C_User.getUsername(userId) />
                            <ListSecondary>
                              <Text value="Total share: " />
                              <Text value=Currency.format(~currency, ~amount=userTotalAmount) />
                            </ListSecondary>
                          </Column>
                        </Row>
                        <Row alignItems=`flexEnd>
                          <View style=styles##inputWrap>
                            <NumberInput label="" value=amount onChangeValue=((v) => send(UpdatePartAmount(userId, v))) placeholder="0" />
                          </View>
                        </Row>
                      </Row>
                    </ListRow>
                  })
                  |> ReasonReact.array
                }
                | Adjustment(shares) => {
                  let totalAdjustment = shares |> Array.fold_left((acc, part) => acc +. (part.amount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.)), 0.);
                  let amountToSplit = totalAmountNum -. totalAdjustment;
                  let basePerUser = amountToSplit /. float_of_int(Array.length(shares));
                  shares
                  |> Array.map(({ userId, amount }) => {
                    let userTotalAmount = amount
                    |> NumberInput.toNumber
                    |> Js.Option.getWithDefault(0.)
                    |> ((+.)(basePerUser));
                    <ListRow key=userId style=Style.(style([marginTop(-16.)]))>
                      <Row alignItems=`flexEnd justifyContent=`spaceBetween>
                        <Row alignItems=`center>
                          <RemoveButton userId onPress=(() => send(RemoveSharedBetween(userId))) />
                          <Column style=Style.(style([paddingBottom(14.)]))>
                            <ListPrimary value=C_User.getUsername(userId) />
                            <ListSecondary>
                              <Text value="Total share: " />
                              <Text value=Currency.format(~currency, ~amount=userTotalAmount) />
                            </ListSecondary>
                          </Column>
                        </Row>
                        <Row alignItems=`flexEnd>
                          <ListPrimary style=Style.(style([marginBottom(15.), marginRight(4.)])) value="+" />
                          <View style=styles##inputWrap>
                            <NumberInput label="" value=amount onChangeValue=((v) => send(UpdatePartAmount(userId, v))) placeholder="0.00" />
                          </View>
                          <Text style=styles##inputSuffix value=Currency.symbol(currency) />
                        </Row>
                      </Row>
                    </ListRow>
                  })
                  |> ReasonReact.array
                }
              })
              <ListSubheader value="Other friends" primary=true />
              (friendsLeft
              |> Array.map(friend => {
                <ListRow key=friend.User.id numberOfLines=1 flexRow=true onPress=Callback(() => send(AddSharedBetween(friend.id)))>
                  <IconButton icon="plus" color=theme.primary onPress=(() => send(AddSharedBetween(friend.id))) />
                  <ListPrimary value=friend.username />
                </ListRow>
              })
              |> ReasonReact.array)
            </Column>
          </ListWrapper>
        }) />
      }) />
    </ScreenWrap>
  }
}
