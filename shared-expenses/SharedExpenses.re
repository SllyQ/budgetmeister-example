open ReactNative;

let component = ReasonReact.statelessComponent("SharedExpenses");

let balance = (~currency, ~userId, expense: Expense.t) => {
  let grandTotal = Expense.getTotalAmount(~currency, expense);
  let yourTotal = Expense.getTotalAmount(~currency, ~userId, expense);
  let youPaid = switch (expense.sharing) {
    | None => assert false
    | Some(sharing) => switch (sharing.paidBy) {
      | User(uid) => uid == userId ? (grandTotal |> Js.Option.getWithDefault(0.)) : 0.
      | Amounts(_) => 0.
    }
  };
  youPaid -. (yourTotal |> Js.Option.getWithDefault(0.))
};

let make = (_) => {
  ...component,
  render: (_) => {
    <ScreenWrap>
      <C_Settings render=(settings => {
        <Connect_ActiveUser render=(user => {
          let userId = user |> C.Option.map(user => user.User.id) |> Js.Option.getExn;
          <C_Expenses render=(expenses => {
            let sharedExpenses = expenses |> Js.Array.filter(expense => switch (expense.Expense.sharing) {
              | Some(_) => true
              | None => false
            });
            let totalBalance = sharedExpenses |> Array.fold_left((acc, expense) => {
              acc +. balance(~currency=settings.mainCurrency, ~userId, expense)
            }, 0.);
            <Column>
              <Row style=Style.(style([elevation(4.), backgroundColor("#fff"), paddingHorizontal(16.), paddingVertical(12.)]))>
                <ListPrimary>
                  <Text value="Total: " />
                  <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=totalBalance)
                    style=Style.(style([totalBalance >= 0. ? marginBottom(0.) : color(Colors.danger)]))
                  />
                </ListPrimary>
              </Row>
              <ListWrapper>
                (
                  sharedExpenses
                  |> Common.iSort((e1, e2) => {
                    DateFns.compareDesc(e1.Expense.date, e2.date)
                  })
                  |> Array.map((expense: Expense.t) => {
                    let leftIcon = switch (expense.categorization) {
                      | Some(Single(part)) => {
                        let { ListRow.icon, color } = CategoryStuff.getIconParams(part.categoryId);
                        <LeftIcon icon color />
                      }
                      | Some(Split(parts)) => <SplitSpendingIcon parts />
                      | None => <UncategorizedIcon />
                    };
                    <ListRow numberOfLines=2 leftIcon>
                      <Row>
                        <Column>
                          <Row>
                            (switch expense.categorization {
                              | Some(Single(part)) => {
                                <ListPrimary
                                  value=Utils.getCategoryPathString(part.categoryId)
                                  numberOfLines=1
                                />
                              }
                              | Some(Split(parts)) => {
                                let categoriesStr = parts |> Utils.getSplitPartsTitleStr;
                                <ListPrimary
                                  value=categoriesStr
                                  numberOfLines=1
                                />
                              }
                              | None => <ListPrimary
                                value="Uncategorized"
                                numberOfLines=1
                                style=Style.(style([color(Colors.danger)]))
                              />
                            })
                            (switch expense.title {
                              | Some(title) => <ListPrimary value=(" (" ++ title ++ ")") />
                              | None => ReasonReact.null
                            })
                            <ListPrimary value=(expense.date |> DateFns.format(" YYYY-MM-Do")) />
                          </Row>
                          {
                            let tot = balance(~currency=settings.mainCurrency, ~userId, expense);
                            <ListPrimary>
                              (if (tot >= 0.) {
                                <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=tot) />
                              } else {
                                <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=tot)
                                  style=Style.(style([color(Colors.danger)]))
                                />
                              })
                            </ListPrimary>
                          }
                        </Column>
                      </Row>
                    </ListRow>
                  })
                  |> ReasonReact.array
                )
              </ListWrapper>
            </Column>
          }) />
        }) />
      }) />
    </ScreenWrap>
  }
}
