open ReactNative;

module RemoveButton = {
  let component = ReasonReact.statelessComponent("RemoveButton");

  let make = (~userId, ~onPress, _) => {
    ...component,
    render: (_) => {
      <C_User userId render=(user => {
        switch (user) {
          | You(_) => <View style=Style.(style([width(48.), height(48.)])) />
          | _ => <IconButton icon="close" color=Colors.danger onPress />
        }
      }) />
    }
  }
};
