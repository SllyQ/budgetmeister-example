open ReactNative;

open RnMui;

let component = ReasonReact.statelessComponent("DevLog");

let logout = (_, _) =>
  /* SyncService.sync() */
  /* |> Js.Promise.then_((_) => GlobalDb.clear()) */
  /* |> C.Promise.consume((_) => AppAuthService.logout()); */
  AppAuthService.logout();

let make = (_) => {
  ...component,
  render: ({ handle }) =>
    <ScreenWrap>
      <ListSubheader value="Perf times" primary=true />
      <C_DevLog
        render=(
          (messages) =>
            <ScrollView>
              (
                messages
                |> Array.of_list
                |> Array.mapi(
                     (i, message: DevLogService.t) =>
                       <ListItem
                         key=(string_of_int(i))
                         centerElement=(<Text value=message.message style=Styles.Text.body1 />)
                         rightElement=(
                           <Text
                             value=(DateFns.format("MMM Do HH:mm:ss", message.date))
                             style=Style.(combine(Styles.Text.body1, style([marginRight(12.)])))
                           />
                         )
                       />
                   )
                |> ReasonReact.array
              )
            </ScrollView>
        )
      />
      <View style=Style.(style([flex(1.)])) />
      <View style=Style.(style([padding(15.)]))>
        <MuiButton
          raised=true
          primary=true
          text="Drop local db"
          onPress=((_) => {
            GlobalDb.clear() |> C.Promise.ignore;
            SQLite.dropAllData() |> C.Promise.ignore;
            SyncService.resetSyncDates();
          })
        />
      </View>
      <ListSubheader value="Logout" primary=true />
      <ListRow>
        <MuiButton text="Logout" primary=true onPress=(handle(logout)) raised=true />
      </ListRow>
    </ScreenWrap>
};
