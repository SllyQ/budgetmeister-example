type t = {
  date: Js.Date.t,
  message: string
};

let messages = ref([]);

let subs = ref([]);

type sub =
  | Sub(list(t) => unit);

let onChange = (cb) => {
  subs := [cb, ...subs^];
  Sub(cb)
};

let unsub = (sub) =>
  switch sub {
  | Sub(cb) => subs := subs^ |> List.filter((s) => s !== cb)
  };

let dispatchUpdates = () => subs^ |> List.iter((cb) => cb(messages^));

let addMessage = (message) => {
  messages := [{message, date: Js.Date.make()}, ...messages^];
  dispatchUpdates()
};
