type t =
  | ThisWeek
  | LastWeek
  | ThisMonth
  | LastMonth
  | Last7
  | Last30
  | All;

let toRange = (str) =>
  switch str {
  | "This week" => ThisWeek
  | "Last week" => LastWeek
  | "This month" => ThisMonth
  | "Last month" => LastMonth
  | "Last 7 days" => Last7
  | "Last 30 days" => Last30
  | "All" => All
  | _ => ThisMonth
  };

let toString = (range) =>
  switch range {
  | ThisWeek => "This week"
  | LastWeek => "Last week"
  | ThisMonth => "This month"
  | LastMonth => "Last month"
  | Last7 => "Last 7 days"
  | Last30 => "Last 30 days"
  | All => "All"
  };

let filterExpenses = (range, expenses) => {
  let now = Js.Date.make();
  expenses
  |> Js.Array.filter(
       (expense: Expense.t) =>
         switch range {
         | ThisWeek => DateFns.isThisISOWeek(expense.date)
         | LastWeek => DateFns.differenceInCalendarISOWeeks(now, expense.date) === 1.
         | ThisMonth => DateFns.isThisMonth(expense.date)
         | LastMonth => DateFns.differenceInCalendarMonths(now, expense.date) === 1.
         | Last7 => DateFns.differenceInCalendarDays(now, expense.date) < 7.
         | Last30 => DateFns.differenceInCalendarDays(now, expense.date) < 30.
         | All => true
         }
     )
};
