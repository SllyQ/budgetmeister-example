let component = ReasonReact.statelessComponent("Line");

let make = (~x, ~y, ~data, ~color, _) => {
  ...component,
  render: (_) => {
    let path  = D3.Line.make(~x, ~y, data);
    <Svg.Path d=path fill="none" strokeWidth="2.5px" stroke=color />
  }
}
