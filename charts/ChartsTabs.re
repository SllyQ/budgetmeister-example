open ReactNative;

type tab =
  | LineChart
  | LineAreaChart
  | PieChart;

  type state = {selectedTab: tab};

  type actions =
    | SelectTab(tab);

let component = ReasonReact.reducerComponent("ChartsTabs");

let make = (_) => {
  ...component,
  initialState: () => {selectedTab: PieChart},
  reducer: (action, _state) =>
    switch action {
    | SelectTab(tab) => ReasonReact.Update({selectedTab: tab})
    },
  render: ({send, state: {selectedTab}}) =>
    <HelpWrapper helpComponent={
      title: "Charts",
      message: `String("Charts give you a more visual overview of your expenses. Pie chart is useful to see how your expenses are distributed across time periods. Line chart can show you how expenses for each category change over time. And line-area chart gives you both at the same time. In line-area chart the area shows how much you spent on that category, while the sum of all areas show you how much you have spent total.")
    }>
      <C_Expenses render=(expenses => {
        if (Array.length(expenses) == 0) {
          <ChartsEmpty />
        } else {
          <View style=Style.(style([flex(1.)]))>
            (
              switch selectedTab {
              | LineChart => <LineChart />
              | LineAreaChart => <LineAreaChart />
              | PieChart => <NewPie />
              }
            )
            <BottomNav.Container>
              <BottomNav.Tab
                label="Pie"
                icon="chart-donut"
                active=(selectedTab == PieChart)
                onPress=(() => send(SelectTab(PieChart)))
              />
              <BottomNav.Tab
                label="Line"
                icon="chart-line"
                active=(selectedTab == LineChart)
                onPress=(() => send(SelectTab(LineChart)))
              />
              <BottomNav.Tab
                label="Line-Area"
                icon="chart-areaspline"
                active=(selectedTab == LineAreaChart)
                onPress=(() => send(SelectTab(LineAreaChart)))
              />
            </BottomNav.Container>
          </View>
        }
      }) />
    </HelpWrapper>
}
