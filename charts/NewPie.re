open ReactNative;

let component = ReasonReact.statelessComponent("NewPie");

let make = (_) => {
  ...component,
  render: (_) => {
    <ScreenWrap>
      <Connect_ActiveUser render=(user => {
        let userId = user |> C.Option.map(user => user.User.id);
        <C_Settings render=(settings => {
          <PeriodicExpensesView.Connect render=(expensesView => {
            <C_ChildCategories parentCategoryId=None render=(categories => {

              let screenWidth = Dimensions.get(`window)##width |> float_of_int;
              let screenHeight = Dimensions.get(`window)##height |> float_of_int;
              let svgWidth = screenWidth;
              let svgHeight = svgWidth *. 0.9;
              let innerRadius = 59.;
              let outerRadius = screenWidth /. 2. -. 50.;
              let padAngle = Svg.pi /. 220.;

              let now = Js.Date.make();
              let budgetingPeriods = expensesView.periods |> Js.Dict.values |> Array.map(({ PeriodicExpensesView.period }) => period);
              let initialIndex = budgetingPeriods  |> Js.Array.findIndex(({ Utils.start, finish }) => {
                now |> DateFns.isWithinRange(start, finish)
              });
              <Column style=Style.(style([flex(1.), width(screenWidth), height(screenHeight -. 136.)]))>
              <Swiper index=initialIndex loop=false loadMinimal=true>
                (expensesView.periods
                |> Js.Dict.values
                |> Array.map(({ PeriodicExpensesView.expenses, period: { start, finish, title }}) => {
                  if (Array.length(expenses) == 0) {
                    <ListWrapper key=title>
                      <ListRow numberOfLines=1>
                        <Row justifyContent=`center>
                        <ListPrimary value={j|No expenses entered for $(title)|j} />
                        </Row>
                      </ListRow>
                    </ListWrapper>
                  } else {
                    let totals =
                      categories
                      |> Array.map(
                           (category: Category.t) => {
                             let total = Utils.getExpensesTotal((), ~userId, ~currency=settings.mainCurrency, ~categoryId=`WithChildren(category.id), ~expenses=expenses |> Array.to_list);
                             (category, total);
                           }
                         )
                      |> Js.Array.filter(((_, total)) => total > 0.)
                      |> Common.iSort(((_, totalAmount1), (_, totalAmount2)) => {
                        int_of_float(totalAmount2 -. totalAmount1)
                      });
                    let numGaps = Array.length(totals);
                    let chartAngle = 2. *. Svg.pi -. float_of_int(numGaps) *. padAngle;
                    let totalTotal = totals |> Array.fold_left((acc, (_, total)) => acc +. total, 0.);
                    let currStartAngle = ref(0.);
                    <ScrollView key=title contentContainerStyle=Style.(style([
                      paddingBottom(48.) /* For swiper dots */
                    ]))>
                      <View>
                        <Svg width=svgWidth height=svgHeight fill="#cccccc">
                          /* <Svg.Rect x=0. y=0. width=width height=width /> */
                          <Svg.G x=(svgWidth /. 2.) y=(svgHeight /. 2.)>
                            (totals
                            |> Array.map(((category, total)) => {
                              let startAngle = currStartAngle^;
                              let angle = total /. totalTotal *. chartAngle +. padAngle;
                              currStartAngle := startAngle +. angle;
                              <Arc key=Category.key(category.Category.id) innerRadius outerRadius startAngle endAngle=(startAngle +. angle) padAngle
                                fill=?(switch (category.Category.categoryType) { | Root({ color }) => Some(color |> CategoryStuff.color) | Child(_) => None })
                              />
                            })
                            |> ReasonReact.array)

                          </Svg.G>
                        </Svg>
                        {
                          let fullMonths = DateFns.getDate(start) === 1. && DateFns.getDate(finish) === DateFns.getDaysInMonth(finish);
                          if (fullMonths) {
                            <Column style=Style.(style([position(`absolute), left(svgWidth /. 2. -. 75.), top(svgHeight /. 2. -. 75.), width(150.), height(150.), alignItems(`center)]))>
                              <Text
                                style=Style.(style([Styles.Text.robotoLight, fontSize(24.), color(Colors.black), paddingTop(48.)]))
                                value=(start |> DateFns.format("YYYY"))
                              />
                              <Text
                                style=Style.(style([fontSize(17.), color(Colors.black), marginTop(-5.)]))
                                value=(start |> DateFns.format("MMMM"))
                              />
                            </Column>
                          } else {
                            <Column style=Style.(style([position(`absolute), left(svgWidth /. 2. -. 75.), top(svgHeight /. 2. -. 75.), width(150.), height(150.), alignItems(`center)]))>
                              <Text
                                style=Style.(style([Styles.Text.robotoLight, fontSize(24.), color(Colors.black), paddingTop(45.)]))
                                value=(start |> DateFns.format("YYYY"))
                              />
                              <Text
                                style=Style.(style([fontSize(14.), color(Colors.black), marginTop(-4.)]))
                                value=((start |> DateFns.format("MMM Do")))
                              />
                              <Text
                                style=Style.(style([fontSize(14.), color(Colors.black), marginTop(-3.)]))
                                value=(finish |> DateFns.format("MMM Do"))
                              />
                            </Column>
                          }
                        }
                        ({
                          currStartAngle := 0.;
                          totals
                          |> Array.map(((category, total)) => {
                            let startAngle = currStartAngle^;
                            let angle = total /. totalTotal *. chartAngle +. padAngle;
                            currStartAngle := startAngle +. angle;
                            let midAngle = Svg.pi /. 2. -. (startAngle +. angle /. 2.);
                            let midRadius = (innerRadius *. 0.44 +. outerRadius *. 0.56);
                            let iconSize = 24.;
                            let { ListRow.icon } = CategoryStuff.getIconParams(category.Category.id);
                            if (angle > 0.25) {
                              <View
                                key=Category.key(category.id)
                                style=Style.(style([
                                  position(`absolute),
                                  left(svgWidth /. 2. +. Js.Math.cos(midAngle) *. (midRadius) -. iconSize /. 2.),
                                  top(svgHeight /. 2. -. Js.Math.sin(midAngle) *. (midRadius) -. iconSize /. 2.),
                                ]))
                              >
                                (switch icon {
                                  | Icon(icon) => {
                                    <RnIcon name=icon color="white" size=iconSize />
                                  }
                                  | Letter(letter) => {
                                    <Text value=letter style=Style.(style([width(iconSize), height(iconSize +. 10.), Styles.Text.robotoMedium, fontSize(iconSize), color(Colors.white), textAlign(`center)])) />
                                  }
                                })
                              </View>
                            } else {
                              ReasonReact.null
                            }
                          })
                          |> ReasonReact.array
                        })
                      </View>
                      <Column alignItems=`center>
                        (
                          totals
                          |> Array.map(
                            ((category: Category.t, total)) => {
                              let { ListRow.icon, color } = CategoryStuff.getIconParams(category.id);
                              <View key=(Category.key(category.id)) style=Style.(style([flexDirection(`row), alignItems(`center), height(26.)]))>
                                <View style=Style.(style([paddingRight(Styles.Spacing.m), paddingTop(2.)]))>
                                  <LeftIcon icon color size=16. />
                                </View>
                                <View style=Style.(style([flexDirection(`row), justifyContent(`spaceBetween), width(215.), height(22.), alignItems(`center)]))>
                                  <Text
                                    style=Style.(
                                            combine(Styles.Text.listPrimary, style([flexShrink(1.), paddingRight(6.)]))
                                          )
                                    value=category.title
                                    numberOfLines=1
                                  />
                                  <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=total) style=Styles.Text.listPrimary />
                                </View>
                              </View>
                            })
                          |> ReasonReact.array
                        )
                      </Column>
                    </ScrollView>
                  }
                })
                |> ReasonReact.array)
              </Swiper>
              </Column>
            }) />
          }) />
        }) />
      }) />
    </ScreenWrap>
  }
}
