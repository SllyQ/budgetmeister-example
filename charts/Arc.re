let component = ReasonReact.statelessComponent("Arc");

let make = (~innerRadius, ~outerRadius, ~startAngle, ~endAngle, ~padAngle=?, ~fill=?, _) => {
  ...component,
  render: (_) => {
    let path = D3.Arc.make((), ~innerRadius, ~outerRadius, ~startAngle, ~endAngle, ~padAngle?);
    <Svg.Path d=path ?fill />
  }
};
