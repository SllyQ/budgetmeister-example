open ReactNative;
open Paper;

let component = ReasonReact.statelessComponent("CategoriesFilter");

let make = (~filteredCategories, ~onFilteredCategoriesChanged, _) => {
  ...component,
  render: (_) => {
    <C_ChildCategories parentCategoryId=None render=(categories => {
      <Row flexWrap=`wrap justifyContent=`spaceAround style=Style.(style([paddingHorizontal(Styles.Spacing.xl)]))>
        <Row alignItems=`center style=Style.(style([widthPct(50.), height(44.)]))>
          <MuiButton primary=true label="Show all"
            onPress=(() => onFilteredCategoriesChanged([]))
          />
        </Row>
        <Row alignItems=`center style=Style.(style([widthPct(50.), height(44.)]))>
          <MuiButton primary=true label="Hide all"
            onPress=(() => onFilteredCategoriesChanged(categories |> Array.map(cat => cat.Category.id) |> Array.to_list))
          />
        </Row>
        (categories
        |> CategoryUtils.sortByTitle
        |> Array.map((category: Category.t) => {
          let isFiltered = filteredCategories |> List.exists(catId => catId == category.id);
          let { ListRow.icon, color } = CategoryStuff.getIconParams(category.Category.id);
          <Touchable key=category.title style=Style.(style([widthPct(50.), height(44.)]))
            onPress=(() => if (isFiltered) {
              onFilteredCategoriesChanged(filteredCategories |> List.filter(catId => catId != category.id))
            } else {
              onFilteredCategoriesChanged([category.id, ...filteredCategories])
            })
          >
            <Row flex=1. alignItems=`center>
              <Checkbox
                checked=(!isFiltered)
                onPress=(() => if (isFiltered) {
                  onFilteredCategoriesChanged(filteredCategories |> List.filter(catId => catId != category.id))
                } else {
                  onFilteredCategoriesChanged([category.id, ...filteredCategories])
                })
              />
              <LeftIcon icon color size=16. />
              <ListPrimary value=category.title style=Style.(style([paddingLeft(Styles.Spacing.s)])) />
            </Row>
          </Touchable>
        })
        |> ReasonReact.array)
      </Row>
    }) />
  }
}
