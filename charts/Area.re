let component = ReasonReact.statelessComponent("Area");

let make = (~x, ~y, ~y0, ~data, ~color, _) => {
  ...component,
  render: (_) => {
    let path  = D3.Area.make(~x, ~y, ~y0, data);
    <Svg.Path d=path fill=color />
  }
}
