open ReactNative;

type state = {selectedRange: TimeRange.t};

type actions =
  | SelectRange(TimeRange.t);

let component = ReasonReact.reducerComponent("Charts");

let make = (_) => {
  ...component,
  initialState: () => {selectedRange: ThisMonth},
  reducer: (action, _state) =>
    switch action {
    | SelectRange(range) => ReasonReact.Update({selectedRange: range})
    },
  render: (_) =>
    /* <ScreenWrap scrollable=false> */
      <NewPie />
      /* <CategoriesPie timeRange=TimeRange.All /> */
      /* <View style=Style.(style([padding(15.)]))>
        <TimeRangePicker selectedRange onRangeSelected=(reduce((range) => SelectRange(range))) />
      </View>
      <Swiper showsButtons=true>
        <CategoriesPie timeRange=selectedRange />
        <CategoriesBar timeRange=selectedRange />
        <TimeSeries timeRange=selectedRange />
      </Swiper> */
    /* </ScreenWrap> */
};
