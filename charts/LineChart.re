open ReactNative;

type state = {
  filteredCategories: list(Category.id),
};

type actions = | UpdateFilteredCategories(list(Category.id));

let component = ReasonReact.reducerComponent("LineChart");

type dataPoint = {
  x: Utils.periodInfo,
  y: float
};

let make = (_) => {
  ...component,
  initialState: () => { filteredCategories: [] },
  reducer: C.flip((_) => fun
    | UpdateFilteredCategories(categories) => ReasonReact.Update {
      filteredCategories: categories
    }
  ),
  render: ({ send, state: { filteredCategories } }) => {
    <ScreenWrap>
      <C_Settings render=(settings => {
        <PeriodicExpensesView.Connect render=(expensesView => {
          <C_ChildCategories parentCategoryId=None render=(categories => {
            let screenWidth = Dimensions.get(`window)##width |> float_of_int;
            let svgWidth = screenWidth;
            let svgHeight = svgWidth *. 0.9;

            let stuff = [||];

            let maxTotal = ref(0.);

            let periods = expensesView.periods |> Js.Dict.values |> Js.Array.slice(~start=0, ~end_=-1);

            let shownCategories = categories
            |> Js.Array.filter(cat => !List.exists(fCatId => fCatId == cat.Category.id, filteredCategories));

            periods |> Array.iter(({ PeriodicExpensesView.period, expenses }) => {
              shownCategories |> Array.iter((category: Category.t) => {
                let total = Utils.getExpensesTotal(
                  ~userId=AppAuthService.user^ |> C.Option.map(user => user.User.id),
                  ~currency=settings.mainCurrency,
                  ~categoryId=`WithChildren(category.id),
                  ~expenses=expenses |> Array.to_list,
                  ()
                );
                if (total > maxTotal^) {
                  maxTotal := total
                };
                stuff |> Js.Array.push((
                  (category, period, total)
                )) |> ignore;
                ()
              });
            });

            let catData = shownCategories |> Array.map(category => {
              let data = stuff |> Js.Array.filter(((cat, _, _)) => {
                cat.Category.id == category.Category.id
              })
              |> Js.Array.map(((_, period, total)) => {
                x: period,
                y: total
              });
              (category, data);
            });

            let leftPadding = 40.;
            let rightPadding = 20.;
            let topPadding = 20.;
            let bottomPadding = 65.;

            let periodTitles = periods |> Array.map(({ PeriodicExpensesView.period }) => {
              period.title
            });

            let xScale = D3.Scale.makeBand()
            |> D3.Scale.rangeRound([|leftPadding, svgWidth -. rightPadding|])
            |> D3.Scale.padding(0.1)
            |> D3.Scale.stringDomain(periodTitles);

            let yScale = D3.Scale.makeLinear() |> D3.Scale.range([|svgHeight -. bottomPadding, topPadding|]) |> D3.Scale.domain([|0., (maxTotal^ *. 1.05)|]);

            let yTicks = yScale |> D3.Scale.ticks(10);

            <Column>
              <Svg height=svgHeight width=svgWidth>
                <Svg.G x=0 y=0>
                  {
                    let a = catData
                    |> Array.map(((category, data)) => {
                      let { ListRow.color } = CategoryStuff.getIconParams(category.Category.id);
                      <Line key=Category.key(category.id) data x=((d) => D3.Scale.apply(d.x.title, xScale)) y=((d) => yScale(d.y)) color />
                    });
                    a |> ReasonReact.array
                  }
                </Svg.G>
                <Svg.G>
                /* Y Axis */
                  <Svg.Line
                    x1=leftPadding
                    x2=leftPadding
                    y1=topPadding
                    y2=(svgHeight -. bottomPadding)
                    stroke=Colors.black
                    strokeWidth=1.
                  />
                  (yTicks
                  |> Array.mapi((i, tick) => {
                    <Svg.G
                      key=string_of_int(i)
                      x=leftPadding
                      y=yScale(tick)
                    >
                      <Svg.Line x1=(-5.) x2=0. y1=0. y2=0. strokeWidth=1. stroke=Colors.black />
                      <Svg.Text
                        dx=(-8.)
                        dy="0.32em"
                        textAnchor="end"
                        fill=Colors.black
                      >
                        (ReasonReact.string({j|$(tick)|j}))
                      </Svg.Text>
                    </Svg.G>
                  })
                  |> ReasonReact.array)
                </Svg.G>
                <Svg.G>
                /* X Axis */
                  <Svg.Line
                    x1=leftPadding
                    x2=(svgWidth -. rightPadding)
                    y1=(svgHeight -. bottomPadding)
                    y2=(svgHeight -. bottomPadding)
                    stroke=Colors.black
                    strokeWidth=1.
                  />
                  (periods
                  |> Array.map(({ PeriodicExpensesView.period: { start, finish, title } }) => {
                    let fullMonths = DateFns.getDate(start) === 1. && DateFns.getDate(finish) === DateFns.getDaysInMonth(finish);
                    <Svg.G
                      key=title
                      x=(D3.Scale.apply(title, xScale))
                      y=(svgHeight -. bottomPadding)
                    >
                      <Svg.Line x1=0. x2=0. y1=0. y2=5. strokeWidth=1. stroke=Colors.black />
                      (fullMonths ? (
                        <Svg.Text dx="0.42em" dy="1.10em" transform="rotate(45)" fill=Colors.black>
                          (ReasonReact.string(start |> DateFns.format("MMM")))
                        </Svg.Text>
                      ) : (
                        <Svg.Text dx="0.42em" dy="1.10em" transform="rotate(45)" fill=Colors.black>
                          (ReasonReact.string(
                            (finish |> DateFns.format("MMM Do"))
                          ))
                        </Svg.Text>
                      ))
                    </Svg.G>
                  })
                  |> ReasonReact.array)
                </Svg.G>
              </Svg>
              <CategoriesFilter filteredCategories
                onFilteredCategoriesChanged=(newFilteredCategories => send(UpdateFilteredCategories(newFilteredCategories)))
              />
            </Column>
          }) />
        }) />
      }) />
    </ScreenWrap>
  }
}
