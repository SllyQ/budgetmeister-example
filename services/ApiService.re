exception NotLoggedIn;

let fetch = (~method_, ~path, ~body=?, ()) => {
  switch (AppAuthService.jwt^) {
    | None => Js.Promise.reject(NotLoggedIn)
    | Some(jwt) => {
      let reqInit = Bs_fetch.RequestInit.make(
        ~method_,
        ~mode=CORS,
        ~headers=Bs_fetch.HeadersInit.make({
          "Content-Type": "application/json;charset=UTF-8",
          "Authorization": "Bearer " ++ jwt
        }),
        ~body=?(body |> C.Option.map(Bs_fetch.BodyInit.make)),
        ()
      );
      Bs_fetch.fetchWithInit(Config.apiUrl ++ path, reqInit)
      |> Js.Promise.then_(Bs_fetch.Response.json)
    }
  }
};
