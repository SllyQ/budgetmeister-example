type action = {
  text: string,
  handler: unit => unit
};

type t = {
  visible: bool,
  textMessage: string,
  bottom: float,
  action: option(action)
};

let snackbarDuration = 3500;

let subs = ref([]);

type sub =
  | Sub(t => unit);

let onChange = (cb) => {
  subs := [cb, ...subs^];
  Sub(cb)
};

let unsub = (sub) =>
  switch sub {
  | Sub(cb) => subs := subs^ |> List.filter((s) => s !== cb)
  };

let currState = ref({visible: false, textMessage: "", action: None, bottom: 0.});

let dispatchUpdates = () => subs^ |> List.iter((cb) => cb(currState^));

let timeout = ref(None);

let hideSnackbar = () => {
  currState := {...currState.contents, visible: false, textMessage: "", action: None};
  dispatchUpdates()
};

let clearCurrTimeout = () =>
  switch timeout^ {
  | Some(t) => Js.Global.clearTimeout(t)
  | None => ()
  };

let wrapAction = (optAction) =>
  switch optAction {
  | None => None
  | Some(action) =>
    Some({
      ...action,
      handler: () => {
        clearCurrTimeout();
        action.handler()
      }
    })
  };

let setBottom = (bottom) => {
  currState := { ...currState.contents, bottom };
};

let resetBottom = () => {
  currState := { ...currState.contents, bottom: 0. };
};

let show = (~action=?, text) =>
  if (currState^.visible) {
    clearCurrTimeout();
    hideSnackbar();
    let _ =
      Js.Global.setTimeout(
        () => {
          currState := {...currState.contents, visible: true, textMessage: text, action: wrapAction(action)};
          dispatchUpdates()
        },
        200
      );
    timeout := Some(Js.Global.setTimeout(() => hideSnackbar(), snackbarDuration + 200))
  } else {
    currState := {...currState.contents, visible: true, textMessage: text, action: wrapAction(action)};
    dispatchUpdates();
    timeout := Some(Js.Global.setTimeout(() => hideSnackbar(), snackbarDuration))
  };
