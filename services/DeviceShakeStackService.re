[@bs.module "react-native-shake-event"] external addShakeListener : string => (unit => unit) => unit = "addEventListener";
[@bs.module "react-native-shake-event"] external removeShakeListener : string => unit = "removeEventListener";

type shakeListener = {
  title: string,
  message: [ | `String(string) | `Element(ReasonReact.reactElement) ]
};

let screensStack = ref([]);
let shakeListener = ref(None);

type sub = | Sub(unit => shakeListener);

let callListeners = () => {
  if (ReactNative.AppState.currentState() == ReactNative.AppState.Active && SettingsDb.get().helpOnDeviceShake) {
    switch (screensStack.contents, shakeListener.contents) {
      | ([cb, ..._], Some(listener)) => {
        let thing = cb();
        Mixpanel.trackWithProperties(EventConsts.helpShake, {
          "title": thing.title
        });
        Mixpanel.increment(EventConsts.helpShake);
        listener(thing)
      }
      | _ => ()
    }
  }
};

addShakeListener("shake", () => {
  callListeners();
});

let addEventListener = (cb): sub => {
  screensStack := [cb, ...screensStack.contents];
  Sub(cb)
};

let removeEventListener = (sub: sub) => {
  switch sub {
    | Sub (cb) => screensStack := screensStack.contents |> List.filter((c) => c !== cb)
  };
};
