let storageService: ref(option(StorageService.t)) = ref(None);

let friends: ref(array(User.t)) = ref([||]);

let get = () => friends^;

type sub = | Sub(array(User.t) => unit);

let subs: ref(list(array(User.t) => unit)) = ref([]);

let subscribe = (cb) => {
  subs := [cb, ...subs^];
  Sub(cb);
};

let unsubscribe = fun
  | Sub(cb) => subs := subs^ |> List.filter(c => c !== cb);

let dispatchChanges = () =>
  subs^
  |> List.iter(cb => cb(friends^));


let setFriends = (~friends as f, ()) => {
  friends := f;
  dispatchChanges();
};

type fetchFriendsPayload = {.
  "data": Js.Nullable.t({.
    "friends": array({.
      "id": int,
      "username": string,
      "email": string
    })
  }),
  "error": Js.Nullable.t({.
    "devMessage": string,
    "userMessage": string
  })
};

let fetchFriends = () => {
  ApiService.fetch(~method_=Get, ~path="/friends", ())
  |> C.Promise.consume((resp) => {
    let stuff: fetchFriendsPayload = Obj.magic(resp);
    switch (stuff##data |> Js.Nullable.toOption) {
      | None => ()
      | Some(data) => {
        data##friends
        |> Array.map(fr => {
          User.id: string_of_int(fr##id),
          username: fr##username,
          email: fr##email
        })
        |> friends => setFriends(~friends, ())
      }
    }
  })
};

type addFriendResponse =
  | FriendAdded
  | EmailDoesNotExist
  | AlreadyFriends
  | UnknownError;

type addFriendPayload = {.
  "error": Js.Nullable.t({.
    "devMessage": string,
    "userMessage": string
  })
};

let addFriend = (~email, ()) => {
  let body = {
    "email": email
  } |> Js.Json.stringifyAny |> Js.Option.getExn;
  ApiService.fetch(~method_=Post, ~path="/friends", ~body, ())
  |> Js.Promise.then_(resp => {
    let stuff: addFriendPayload = Obj.magic(resp);
    switch (stuff##error |> Js.Nullable.toOption) {
      | None => {
        fetchFriends();
        Js.Promise.resolve(FriendAdded)
      }
      | Some(err) => switch err##devMessage {
        | "Already friends" => Js.Promise.resolve(AlreadyFriends)
        | "Requested user does not exist" => Js.Promise.resolve(EmailDoesNotExist)
        | _ => Js.Promise.resolve(UnknownError)
      }
    }
  })
  |> Js.Promise.catch((_) => Js.Promise.resolve(UnknownError))
};


let init = (~storageService as ss, ()) => {
  storageService := Some(ss);
  if (Js.Option.isSome(AppAuthService.user^)) {
    fetchFriends();
  }
  /* Load friends from cache */
}
