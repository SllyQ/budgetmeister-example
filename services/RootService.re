open ReactNative;

let storageService = {
  StorageService.getItem: (~key, ()) => Js.Promise.resolve(Realm.getCfg(key)),
  setItem: (~key, ~value, ()) => Js.Promise.resolve(Realm.setCfg(key, value)),
  removeItem: (~key, ()) => Js.Promise.resolve(Realm.deleteCfg(key))
};

/* let storageService = {
  StorageService.getItem: (~key, ()) => AsyncStorage.getItem(key, ()),
  setItem: (~key, ~value, ()) => AsyncStorage.setItem(key, value, ()),
  removeItem: (~key, ()) => AsyncStorage.removeItem(key, ())
}; */

let addEntries = (receivedEntries) => {
  CategoryDb.addNewEntries(receivedEntries);
  ExpenseDb.addNewEntries(receivedEntries);
  TagDb.addNewEntries(receivedEntries);
  ExpenseTagDb.addNewEntries(receivedEntries);
  SettingsDb.addNewEntries(receivedEntries);
  BudgetDb.addNewEntries(receivedEntries);
  BudgetDb2.addNewEntries(receivedEntries);
  TemplateDb.addNewEntries(receivedEntries);
  RecurringExpenseDb.addNewEntries(receivedEntries);
  Js.Promise.resolve()
};

let sliceSize = 200;

let rec slowlyAddEntries = (~start=0, receivedEntries) => {
  if (Array.length(receivedEntries) > start) {
    Js.Promise.make((~resolve, ~reject as _) => {
      Js.Global.setTimeout(() => {
        addEntries(receivedEntries |> Js.Array.slice(~start, ~end_=start + sliceSize)) |> C.Promise.ignore;
        [@bs] resolve(0);
      }, 0) |> ignore;
    }) |> Js.Promise.then_((_) => {
      slowlyAddEntries(~start=start + sliceSize, receivedEntries)
    });
  } else {
    addEntries(receivedEntries |> Js.Array.sliceFrom(start)) |> C.Promise.ignore;
    Js.Promise.resolve()
  }
};

let init = () => {
  let apiUrl = Config.apiUrl;
  SyncService.init(~storageService, ~apiUrl, ~getNewEntries=((time) => List.concat([
      CategoryDb.getNewEntries(time),
      ExpenseDb.getNewEntries(time),
      TagDb.getNewEntries(time),
      ExpenseTagDb.getNewEntries(time),
      SettingsDb.getNewEntries(time),
      BudgetDb.getNewEntries(time),
      BudgetDb2.getNewEntries(time),
      TemplateDb.getNewEntries(time),
      RecurringExpenseDb.getNewEntries(time)
    ])
  ), ~addNewEntries=addEntries, ());
  AppAuthService.init(~storageService, ~apiUrl=apiUrl, ())
  |> Js.Promise.catch(e => { Js.log(e); Js.Promise.resolve()})
  |> C.Promise.consume(() => {
    FriendsService.init(~storageService, ());
  });

  let criticalPromise = GlobalDb.initCritical((),
    ~localStorageConfig={
      Entries.store: SQLiteStorageDriver.store,
      fetchAll: SQLiteStorageDriver.fetchAll
    }, ~remoteStorageConfig={
      Entries.fetchAll: SQLiteStorageDriver.fetchAllRemote,
      storeAll: SQLiteStorageDriver.storeAllRemote
    }
  );

  criticalPromise |> C.Promise.consume(() => {
    Js.Global.setTimeout(() => {
      let start = Js.Date.make();
      GlobalDb.init(~localStorageConfig={
        Entries.store: SQLiteStorageDriver.store,
        fetchAll: SQLiteStorageDriver.fetchAll
      }, ~remoteStorageConfig={
        Entries.fetchAll: SQLiteStorageDriver.fetchAllRemote,
        storeAll: SQLiteStorageDriver.storeAllRemote
      }, ())
      |> C.Promise.consume((_) => {
        let donezo = Js.Date.make();
        let duration = Js.Date.getTime(donezo) -. Js.Date.getTime(start);
        DevLogService.addMessage({j|Db init duration $(duration)ms|j});
        SyncService.autoSync()
      });
    }, 0) |> ignore;
  });

  RemindersService.init(~storageService, ());
  criticalPromise
};
