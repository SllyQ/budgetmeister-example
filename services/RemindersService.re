let getDateWithOffset = (date) => {
  (Js.Date.toISOString(date), Js.Date.getDate(date) -. Js.Date.getUTCDate(date))
};

module Cfg = {
  let name = "Reminders";
  type state = {
    storageService: option(StorageService.t),
    reminders: array(Reminder.t),
    loading: bool
  };

  let initialState = {
    loading: false,
    reminders: [||],
    storageService: None
  };

  type action =
    | Init(StorageService.t)
    | FetchReminders
    | CreateReminder(Reminder.t)
    | UpdateReminder(Reminder.t)
    | DeleteReminder(Reminder.id)
    | ReminderCreated(Reminder.t)
    | ReminderUpdated(Reminder.t)
    | ReminderDeleted(Reminder.id)
    | RemindersFetched(array(Reminder.t));

  type fetchResp = {.
    "data": {.
      "reminders": array({.
        "daysOfWeek": array(int),
        "id": int,
        "timeOfDay": string,
        "message": Js.Nullable.t(string)
      })
    }
  };

  let update = state => (fun
    | Init(storageService) => `Update({
      ...state,
      storageService: Some(storageService)
    })
    | FetchReminders => `UpdateWithSideEffects({
      ...state,
      loading: true
    }, dispatch => {
      ApiService.fetch((), ~method_=Get, ~path="/reminders")
      |> C.Promise.consume(resp => {
        let resp: fetchResp = Obj.magic(resp);
        let reminders = resp##data##reminders |> Array.map(r => {
          Reminder.id: Obj.magic(r##id),
          daysOfWeek: r##daysOfWeek,
          time: r##timeOfDay |> Js.Date.fromString,
          message: r##message |> Js.Nullable.toOption
        });
        dispatch(RemindersFetched(reminders))
      });
    })
    | CreateReminder(reminder) => `UpdateWithSideEffects({
      ...state,
      loading: true
    }, dispatch => {
      let (time, daysOffset) = getDateWithOffset(reminder.time);
      ApiService.fetch((), ~method_=Post, ~path="/reminders", ~body={
        "timeOfDay": time,
        "daysOfWeek": reminder.daysOfWeek,
        "daysOffset": daysOffset,
        "message": reminder.message |> Js.Nullable.fromOption
      } |> Js.Json.stringifyAny |> Js.Option.getExn) |> C.Promise.ignore;
      dispatch(ReminderCreated(reminder))
    })
    | UpdateReminder(reminder) => `UpdateWithSideEffects({
      ...state,
      loading: true
    }, dispatch => {
      let (time, daysOffset) = getDateWithOffset(reminder.time);
      ApiService.fetch((), ~method_=Put, ~path="/reminders/" ++ Reminder.key(reminder.id), ~body={
        "timeOfDay": time,
        "daysOfWeek": reminder.daysOfWeek,
        "daysOffset": daysOffset,
        "message": reminder.message |> Js.Nullable.fromOption
      } |> Js.Json.stringifyAny |> Js.Option.getExn) |> C.Promise.ignore;
      dispatch(ReminderUpdated(reminder))
    })
    | DeleteReminder(reminderId) => `UpdateWithSideEffects({
      ...state,
      loading: true
    }, dispatch => {
      ApiService.fetch((), ~method_=Delete, ~path="/reminders/" ++ Reminder.key(reminderId)) |> C.Promise.ignore;
      dispatch(ReminderDeleted(reminderId))
    })
    | RemindersFetched(reminders) => `Update({
      ...state,
      loading: false,
      reminders
    })
    | ReminderCreated(reminder) => `Update({
      ...state,
      loading: false,
      reminders: Array.append(state.reminders, [| reminder |])
    })
    | ReminderUpdated(reminder) => `Update({
      ...state,
      loading: false,
      reminders: state.reminders |> Array.map(r => r.Reminder.id == reminder.id ? reminder : r)
    })
    | ReminderDeleted(reminderId) => `Update({
      ...state,
      loading: false,
      reminders: state.reminders |> Js.Array.filter(r => r.Reminder.id != reminderId)
    })
  )
};

include Store.Make(Cfg);

let init = (~storageService, ()) => {
  dispatch(Init(storageService))
};
