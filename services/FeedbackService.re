module Cfg = {
  let cacheKey = "feedback_unsent-feedback";

  type feedback = {
    email: string,
    feedback: string,
    timestamp: string
  };

  let encodeFeedback = (feedback) => Json.Encode.(object_([
    ("email", string(feedback.email)),
    ("feedback", string(feedback.feedback)),
    ("timestamp", string(feedback.timestamp))
  ]));

  let decodeFeedback = (json) => Json.Decode.{
    email: json |> field("email", string),
    feedback: json |> field("feedback", string),
    timestamp: json |> field("timestamp", string)
  };

  let encodeUnsentFeedback = (unsentFeedback) => unsentFeedback |> Array.of_list |> Json.Encode.array(encodeFeedback);

  let decodeUnsentFeedback = (json) => json |> Json.Decode.array(decodeFeedback) |> Array.to_list;

  let name = "Feedback";

  type state = {
    unsentFeedback: list(feedback)
  };

  let initialState = {
    unsentFeedback: Realm.getCfg(cacheKey) |> C.Option.chain(Json.parse) |> C.Option.map(decodeUnsentFeedback) |> Js.Option.getWithDefault([])
  };

  type action =
    | AddUnsent(feedback)
    | SendFeedback(string, string);

  let update = state => (fun
    | AddUnsent(feedback) => {
      let newUnsentFeedback = [feedback, ...state.unsentFeedback];
      `UpdateWithSideEffects({
        unsentFeedback: newUnsentFeedback
      }, (_) => {
        Realm.setCfg(cacheKey, encodeUnsentFeedback(newUnsentFeedback) |> Js.Json.stringify)
      })
    }
    | SendFeedback(email, feedback) => {
      `SideEffects((dispatch) => {
        let body = {
          "email": email,
          "feedback": feedback
        } |> Js.Json.stringifyAny |> Js.Option.getExn;
        let reqInit = Bs_fetch.RequestInit.make(
          ~method_=Post,
          ~mode=CORS,
          ~headers=Bs_fetch.HeadersInit.make(switch (AppAuthService.jwt^) {
            | None => Obj.magic({
              "Content-Type": "application/json;charset=UTF-8"
            })
            | Some(jwt) => {
              Obj.magic({
              "Content-Type": "application/json;charset=UTF-8",
              "Authorization": "Bearer " ++ jwt
            })
          }
          }),
          ~body=(body |> Bs_fetch.BodyInit.make),
          ()
        );
        Bs_fetch.fetchWithInit(Config.apiUrl ++ (Js.Option.isSome(AppAuthService.jwt^) ? "/feedback/user" : "/feedback/anonymous"), reqInit)
        |> Js.Promise.then_(Bs_fetch.Response.json)
        |> Js.Promise.catch((_) => {
          dispatch(AddUnsent({
            email,
            feedback,
            timestamp: Js.Date.make() |> Js.Date.toISOString
          }));
          Js.Promise.resolve(Obj.magic())
        })
        |> C.Promise.ignore;
      })
    }
  );
};

include Store.Make(Cfg);
