open ReactNative;

let lastCheckDateKey = "RecurringExpensesService_lastCheckDate";
let lastCheckDate: ref(option(Js.Date.t)) = ref(None);

let setLastCheckDate = (date) => {
  lastCheckDate := Some(date);
  Realm.setCfg(lastCheckDateKey, date |> Js.Date.toISOString) |> ignore
};

let handleActive = () => {
  switch lastCheckDate.contents {
    | None => {
      let lastCheckDateStr = Realm.getCfg(lastCheckDateKey);
      switch lastCheckDateStr {
        | None => {
          lastCheckDate := Some(Js.Date.makeWithYM(~year=1000., ~month=0., ()))
        }
        | Some(lastCheckDateStr) => {
          lastCheckDate := Some(Js.Date.fromString(lastCheckDateStr));
        }
      };
      RecurringExpenseDb.Generator.updateGeneratedItems();
    }
    | Some(_) => RecurringExpenseDb.Generator.updateGeneratedItems();
  }
};

let init = () => {
  if (AppState.currentState() == Active) {
    handleActive()
  };
  AppState.addEventListener("change", (state) => {
    if (state === "active") {
      handleActive();
    }
  })
};
