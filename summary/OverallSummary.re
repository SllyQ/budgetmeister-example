open ReactNative;

let component = ReasonReact.statelessComponent("OverallSummary");

let make = (_) => {
  ...component,
  render: (_) =>
    <C_Settings render=(settings => {
      <ScreenWrap>
        <View style=Style.(style([alignItems(`center), paddingTop(20.), flex(1.)]))>
          <Connect_ActiveUser render=(user => {
            let userId = user |> C.Option.map(user => user.User.id);
            <C_Expenses
              render=(
                (expenses) =>
                  if (Array.length(expenses) === 0) {
                    <Text
                      style=Styles.Text.subheading
                      value="You have not created any expenses yet"
                    />
                  } else {
                    let totalCost =
                      Utils.getExpensesTotal(~userId, ~currency=settings.mainCurrency, ~expenses=expenses |> Array.to_list, ());
                    let thisMonthCost =
                      expenses
                      |> Js.Array.filter(expense => DateFns.isThisMonth(expense.Expense.date))
                      |> expenses => Utils.getExpensesTotal(~userId, ~expenses=expenses |> Array.to_list, ~currency=settings.mainCurrency, ());
                    let lastMonthCost =
                      expenses
                      |> Js.Array.filter(expense => DateFns.differenceInCalendarMonths(Js.Date.make(), expense.Expense.date) == 1.)
                      |> expenses => Utils.getExpensesTotal(~userId, ~expenses=expenses |> Array.to_list, ~currency=settings.mainCurrency, ());
                    let last30DaysCost =
                      expenses
                      |> Js.Array.filter(expense => DateFns.differenceInCalendarDays(Js.Date.make(), expense.Expense.date) <= 30.)
                      |> expenses => Utils.getExpensesTotal(~userId, ~expenses=expenses |> Array.to_list, ~currency=settings.mainCurrency, ());
                    let lastExpenseDate =
                      expenses
                      |> Array.fold_left(
                           (acc, expense: Expense.t) =>
                             if (Js.Date.getTime(acc) > Js.Date.getTime(expense.date)) {
                               acc
                             } else {
                               expense.date
                             },
                           Js.Date.makeWithYM(~year=2015., ~month=0., ())
                         );
                    let firstExpenseDate =
                      expenses
                      |> Array.fold_left(
                           (acc, expense: Expense.t) =>
                             if (Js.Date.getTime(acc) > Js.Date.getTime(expense.date)) {
                               expense.date
                             } else {
                               acc
                             },
                           Js.Date.make()
                         );
                    let daysBetweenEdgeExpenses =
                      DateFns.differenceInCalendarDays(lastExpenseDate, firstExpenseDate);
                    let expensesCount = Array.length(expenses);
                    let summaryCostRowStyle =
                      Style.(style([flexDirection(`row), width(200.), justifyContent(`spaceBetween)]));
                    <View style=Style.(style([alignItems(`center)]))>
                      <Text
                        style=Style.(combine(Styles.Text.title, style([paddingBottom(12.)])))
                        value="Amount Spent"
                      />
                      <View style=summaryCostRowStyle>
                        <Text style=Styles.Text.subheading value="Total" />
                        <Text style=Styles.Text.subheading value=Currency.format(~currency=settings.mainCurrency, ~amount=totalCost) />
                      </View>
                      <View style=summaryCostRowStyle>
                        <Text style=Styles.Text.subheading value="This month" />
                        <Text style=Styles.Text.subheading value=Currency.format(~currency=settings.mainCurrency, ~amount=thisMonthCost) />
                      </View>
                      <View style=summaryCostRowStyle>
                        <Text style=Styles.Text.subheading value="Last month" />
                        <Text style=Styles.Text.subheading value=Currency.format(~currency=settings.mainCurrency, ~amount=lastMonthCost) />
                      </View>
                      <View style=summaryCostRowStyle>
                        <Text style=Styles.Text.subheading value="Last 30 days" />
                        <Text style=Styles.Text.subheading value=Currency.format(~currency=settings.mainCurrency, ~amount=last30DaysCost) />
                      </View>
                      <Text
                        value={j|You have created $expensesCount expenses in $daysBetweenEdgeExpenses days|j}
                        style=Style.(combine(style([marginTop(20.), paddingHorizontal(16.), textAlign(`center)]), Styles.Text.subheading))
                      />
                    </View>
                  }
              )
            />
          }) />
        </View>
      </ScreenWrap>
    }) />
};
