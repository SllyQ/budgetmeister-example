open ReactNative;

type tab =
  | OverallSummary
  | MonthlySummary
  | Charts;

type state = {selectedTab: tab};

type actions =
  | SelectTab(tab);

let component = ReasonReact.reducerComponent("SummaryTabs");

let make = (_) => {
  ...component,
  initialState: () => {selectedTab: Charts},
  reducer: (action, _state) =>
    switch action {
    | SelectTab(tab) => ReasonReact.Update({selectedTab: tab})
    },
  render: ({send, state: {selectedTab}}) =>
    <View style=Style.(style([flex(1.)]))>
      (
        switch selectedTab {
        | OverallSummary => <OverallSummary />
        | MonthlySummary => <MonthlySummary />
        | Charts => <Charts />
        }
      )
      <BottomNav.Container>
        <BottomNav.Tab
          label="Overall"
          icon="reorder-horizontal"
          active=(selectedTab == OverallSummary)
          onPress=(() => send(SelectTab(OverallSummary)))
        />
        <BottomNav.Tab
          label="Monthly"
          icon="calendar"
          active=(selectedTab == MonthlySummary)
          onPress=(() => send(SelectTab(MonthlySummary)))
        />
        <BottomNav.Tab
          label="Charts"
          icon="poll-box"
          active=(selectedTab == Charts)
          onPress=(() => send(SelectTab(Charts)))
        />
      </BottomNav.Container>
    </View>
};
