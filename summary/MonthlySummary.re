open ReactNative;

let component = ReasonReact.statelessComponent("MonthlySummary");

let make = (_) => {
  ...component,
  render: (_) =>
    <HelpWrapper helpComponent={
      title: "Monthly Summary",
      message: `String("This is an overview how did you spend your money in a given month or a budgeting period of your choice.")
    }>
      <C_Settings render=(settings => {
        <View style=Style.(style([flex(1.)]))>
          <C_Expenses
            render=(
              (expenses) => {
                if (Array.length(expenses) === 0) {
                  <PeriodicExpensesSummaryEmpty />
                } else {
                  let (firstDate, lastDate) = ExpenseUtils.getBoundsDates(expenses);
                  let budgetingPeriods = Utils.getBudgetingPeriods(~period=settings.budgetingPeriod, ~firstDate, ~lastDate, ());
                  let now = Js.Date.make();
                  let initialIndex = budgetingPeriods  |> Js.Array.findIndex(({ Utils.start, finish }) => {
                    now |> DateFns.isWithinRange(start, finish)
                  });
                  <Swiper index=initialIndex loop=false>
                    (budgetingPeriods
                    |> Array.map((period: Utils.periodInfo) => {
                      <PeriodSummary key=period.title period />
                    })
                    |> ReasonReact.array)
                  </Swiper>
                }
              }
            )
          />
        </View>
      }) />
    </HelpWrapper>
};
