let rec getIconParams = catId => {
  CategoryDb.get(~id=catId)
  |> (fun
    | Some(category) => {
      let color = CategoryUtils.getRootCategory(catId)
      |> C.Option.chain(rootCategory => switch(rootCategory.Category.categoryType) {
        | Root({ color }) => Some(color)
        | Child(_) => None
      })
      |> Js.Option.getWithDefault(Colors.blue600 |> CategoryColor.fromString);
      switch category.icon {
        | Some(icon) => {
          ListRow.icon: Icon(icon),
          color
        }
        | None => {
          switch category.categoryType {
            | Child({ parentCategoryId: id }) => {
              let { ListRow.icon, _ } = getIconParams(id);
              { icon, color }
            }
            | Root(_) => {
              ListRow.icon: Letter(category.title |> (x => String.get(x, 0)) |> Char.escaped |> String.uppercase),
              color
            }
          };
        }
      }
    }
    | None => {
      ListRow.icon: Letter("?"),
      color: "blue" |> CategoryColor.fromString
    }
  )
};

let color = (color) => {
  let pastelColors = HistoryDb.get().pastelColors;
  pastelColors ? color |> CategoryColor.toPastelColor : color |> CategoryColor.toColor
}
