open ReactNative;

let get = (selectedElevation) => {
  open Style;
  if (Platform.equals(Platform.IOS)) {
    if (selectedElevation === 0.) {
      style([shadowColor("transparent"), zIndex(0)])
    } else {
      Obj.magic({
        "shadowColor": Colors.black,
        "shadowOpacity": 0.3,
        "shadowRadius": selectedElevation /. 2.,
        "shadowOffset": {
          "height": 1.,
          "width": 0.
        },
        "zIndex": 0
      })
    }
  } else {
    style([elevation(selectedElevation)])
  }
}
