open ReactNative;

let component = ReasonReact.statelessComponent("ColorBar");

let make = (~color, ~height=30., _) => {
  ...component,
  render: (_) =>
    <View
      style=(Style.style([Style.backgroundColor(color), Style.width(height *. 2.5), Style.height(height)]))
    />
};
