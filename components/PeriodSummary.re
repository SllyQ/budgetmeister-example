open ReactNative;

let component = ReasonReact.statelessComponent("PeriodSummary");

let make = (~period: Utils.periodInfo, _) => {
  ...component,
  render: (_) => {
    <C_Expenses render=(allExpenses => {
      let expenses = allExpenses |> Js.Array.filter(expense => {
        expense.Expense.date |> DateFns.isWithinRange(period.start, period.finish)
      });
      <Connect_ActiveUser render=(user => {
        let userId = user |> C.Option.map(user => user.User.id);
        <C_Settings render=(settings => {
          <ScreenWrap>
            <View style=Style.(style([alignItems(`center), marginTop(15.)]))>
              <Text
                style=Style.(combine(Styles.Text.title, style([marginBottom(18.)])))
                value=period.title
              />
              {
                if (Array.length(expenses) == 0) {
                  <Text style=Styles.Text.subheading value="No expenses entered for this period" />
                } else {
                  let windowWidth = Dimensions.get(`window)##width;
                  let totalCost = Utils.getExpensesTotal(
                    ~userId,
                    ~expenses=expenses |> Array.to_list,
                    ~currency=settings.mainCurrency,
                    ()
                  );
                    /* |> Array.map(expense => ExpenseUtils.getTotalCost(expense.Expense.spending))
                    |> CortUtils.sumArr; */
                  let sortedTotals =
                    CategoryDb.getAll()
                    |> Js.Array.filter((category) => switch (category.Category.categoryType) { | Root(_) => true | Child(_) => false })
                    |> Array.map(
                         (category) => (
                           category,
                           Utils.getExpensesTotal(
                             ~userId,
                             ~categoryId=`WithChildren(category.Category.id),
                             ~expenses=expenses |> Array.to_list,
                             ~currency=settings.mainCurrency,
                             ()
                           )
                         )
                       )
                    |> Common.iSort(((_, total1), (_, total2)) => compare(total2, total1));
                  let rowStyle =
                    Style.(style([flexDirection(`row), justifyContent(`spaceBetween), width(215.), height(22.), alignItems(`center)]));
                    <View style=Style.(style([alignItems(`center)]))>
                      <View style=rowStyle>
                        <Text style=Styles.Text.body1 value="Total money spent" />
                        <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=totalCost) style=Styles.Text.body1 />
                      </View>
                      <Text
                        style=Style.(combine(Styles.Text.subheading, style([marginTop(12.), marginBottom(6.)])))
                        value="Most spent on"
                      />
                      (
                        sortedTotals
                        |> Js.Array.filter(((_, total)) => total > 0.)
                        |> Array.map(
                          ((category: Category.t, total)) => {
                            let { ListRow.icon, color } = CategoryStuff.getIconParams(category.id);
                            <View key=(Category.key(category.id)) style=Style.(style([flexDirection(`row), alignItems(`center)]))>
                              <View style=Style.(style([paddingRight(Styles.Spacing.s), paddingTop(2.)]))>
                                <LeftIcon icon color size=12. />
                              </View>
                              <View style=rowStyle>
                                <Text
                                  style=Style.(
                                          combine(Styles.Text.body1, style([flexShrink(1.), paddingRight(6.)]))
                                        )
                                  value=category.title
                                  numberOfLines=1
                                />
                                <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=total) style=Styles.Text.body1 />
                              </View>
                            </View>
                          })
                        |> ReasonReact.array
                      )
                      <Text
                        style=Style.(combine(Styles.Text.subheading, style([marginTop(Styles.Spacing.xxl), marginBottom(Styles.Spacing.m)])))
                        value="Expense distribution by category"
                      />
                      {
                        let rowWidth = float_of_int(windowWidth - 100);
                        <View style=Style.(style([flexDirection(`row), width(rowWidth)]))>
                          (sortedTotals
                          |> Array.map(((category, catTotal)) => {
                            let { ListRow.color: bgColor } = CategoryStuff.getIconParams(category.Category.id);
                            <View key=Category.key(category.id)
                              style=Style.(style([
                                backgroundColor(bgColor |> CategoryStuff.color),
                                width((catTotal /. totalCost) *. rowWidth),
                                height(20.)
                              ]))
                            />
                          })
                          |> ReasonReact.array)
                        </View>
                      }
                      {
                        let currencies = expenses |> Array.fold_left((acc, expense) => {
                          List.append(acc, [expense.Expense.currency])
                        }, [])
                        |> C.List.unique;
                        let totals = currencies |> List.map(currency =>
                          (
                            currency,
                            expenses
                              |> Array.map(expense =>
                                expense.Expense.currency == currency
                                  ? expense |> Expense.getTotalAmount(~userId?) |> Js.Option.getWithDefault(0.)
                                  : 0.
                              )
                              |> Array.fold_left((+.), 0.)
                          ));
                        <View style=Style.(style([alignItems(`center), paddingBottom(Styles.Spacing.xl)]))>
                          <Text
                            style=Style.(combine(Styles.Text.subheading, style([marginTop(Styles.Spacing.xxl), marginBottom(Styles.Spacing.m)])))
                            value="Currency breakdown"
                          />
                          (totals
                          |> List.map(((currency, totalCost)) => {
                            <View key=Currency.symbol(currency) style=Style.(style([flexDirection(`row), width(120.), justifyContent(`spaceBetween)]))>
                              <Text style=Styles.Text.body1 value=Currency.currencyToString(currency) />
                              <Text style=Styles.Text.body1 value=(Currency.format(~currency, ~amount=totalCost)) />
                            </View>
                          })
                          |> Array.of_list
                          |> ReasonReact.array)
                        </View>
                      }
                    </View>
                }
              }
            </View>
          </ScreenWrap>
        }) />
      }) />
    }) />
  }
}
