type t = {
  repeatFrom: Js.Date.t,
  repeatUntil: option(Js.Date.t),
  repeatConfig: RepeatConfig.t,
};

let component = ReasonReact.statelessComponent("RecurringForm");

let make = (~value, ~onValueChanged, _) => {
  ...component,
  render: (_) => {
    <Column>
      <DatePickerRow
        date=Some(value.repeatFrom)
        onDateChanged=(repeatFrom => onValueChanged({ ...value, repeatFrom }))
        label="Repeat from"
      />
      <DatePickerRow
        date=value.repeatUntil
        onDateChanged=(repeatUntil => onValueChanged({ ...value, repeatUntil: Some(repeatUntil) }))
        onDateCleared=(() => onValueChanged({ ...value, repeatUntil: None }))
        label="Repeat until"
        noneLabel="Indefinitely"
      />
      <RepeatConfigForm value=value.repeatConfig
        onValueChanged=(repeatConfig => onValueChanged({ ...value, repeatConfig }))
      />
    </Column>
  }
}
