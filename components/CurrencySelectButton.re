open ReactNative;
open RnMui;

type state = {
  modalVisible: bool
};

type actions =
  | ShowModal
  | SelectCurrency(option(Currency.t));

let component = ReasonReact.reducerComponent("CurrencySelectButton");

let make = (~buttonStyle=?, ~label, ~onNewCurrencySelected, _) => {
  ...component,
  initialState: () => { modalVisible: false },
  reducer: (action, _state) => switch action {
    | ShowModal => ReasonReact.Update {
      modalVisible: true
    }
    | SelectCurrency(optCurrency) => {
      switch optCurrency {
        | Some(currency) => ReasonReact.UpdateWithSideEffects({
          modalVisible: false
        }, (_) => onNewCurrencySelected(currency))
        | None => ReasonReact.Update {
          modalVisible: false
        }
      }
    }
  },
  render: ({ send, state: { modalVisible } }) => {
    <View style=Style.(buttonStyle |> Js.Option.getWithDefault(style([])))>
      <MuiButton text=label onPress=(() => send(ShowModal))
        primary=true uppercase=false
      />
      <CurrencySelectModal
        isVisible=modalVisible
        onCurrencySelected=(currency => send(SelectCurrency(currency)))
      />
    </View>
  }
}
