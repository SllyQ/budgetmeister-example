open ReactNative;

let component = ReasonReact.statelessComponent("IconButton");

let make = (~icon, ~onPress, ~color=Colors.black, _) => {
  ...component,
  render: (_) => {
    <Touchable onPress
      style=Style.(style([
        width(48.),
        height(48.),
        borderRadius(24.),
        alignItems(`center),
        justifyContent(`center)
      ]))
    >
      <RnIcon name=icon color />
    </Touchable>
  }
}
