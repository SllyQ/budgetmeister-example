open ReactNative;

type state = {
  keyboardOpen: bool
};

type actions =
  | KeyboardOpened
  | KeyboardClosed;

let component = ReasonReact.reducerComponent("KaMarginView");

let make = (children) => {
  {
    ...component,
    initialState: () => { keyboardOpen: false },
    reducer: (action, _state) => switch action {
      | KeyboardOpened => ReasonReact.Update { keyboardOpen: true }
      | KeyboardClosed => ReasonReact.Update { keyboardOpen: false }
    },
    subscriptions: ({ send }) => [Sub(
      () => Keyboard.addListener("keyboardWillShow", () => send(KeyboardOpened)),
      listener => listener##remove()
    ), Sub(
      () => Keyboard.addListener("keyboardWillHide", () => send(KeyboardClosed)),
      listener => listener##remove()
    )],
    render: ({ state: { keyboardOpen }}) => {
      <View style=Style.(style([marginBottom(keyboardOpen ? 226. : 0.)]))>
        ...children
      </View>
    }
  }
}
