let component = ReasonReact.statelessComponent("SharedInfoRow");

let make = (~sharing, ~showTotalCost=true, _) => {
  ...component,
  render: (_) => {
    let { RParams.totalAmount, currency, shareConfig } = sharing;
    let sharingMethod = switch (shareConfig.ratio) {
      | Equally(_) => "equally"
      | Fixed(_) => "by fixed amounts"
      | Percentage(_) => "by percentages"
      | Shares(_) => "by shares"
      | Adjustment(_) => "by adjustment"
    };
    let users = switch (shareConfig.ratio) {
      | Equally(users) => users
      | Fixed(parts) | Percentage(parts) | Shares(parts) | Adjustment(parts) => parts |> Array.map(({ ShareConfig.userId }) => userId)
    } |> Array.map(C_User.get);
    let includesYou = ref(false);
    let usersString = users |> Array.fold_left((str, user) => switch (user) {
      | C_User.You(_) => {
        includesYou := true;
        str
      }
      | UnknownUser => Js.String.length(str) == 0 ? "unknown user" : str ++ ", unknown user"
      | User(u) => Js.String.length(str) == 0 ? u.User.username : str ++ ", " ++ u.username
    }, "");
    let paidByStr = switch (shareConfig.paidBy) {
      | User(uid) => switch (C_User.get(uid)) {
        | You(_) => "you"
        | User(u) => u.username
        | UnknownUser => "unknown user"
      }
      | Amounts(_) => "multiple users"
    };
    let userId = AppAuthService.user^ |> C.Option.map(user => user.User.id) |> Js.Option.getExn;
    let userPart = ShareConfig.getUserPart(~userId, ~totalAmount, shareConfig);
    <ListRow numberOfLines=2>
      <ListPrimary value=(
        "Split "
        ++ (showTotalCost ? (
          Currency.format(~currency, ~amount=totalAmount) ++ " "
        ) : "")
        ++ sharingMethod
        ++ (includesYou^ ? " with " : " between ")
        ++ usersString
      ) />
      <ListSecondary value=("Paid by " ++ paidByStr) />
      <ListSecondary value=("Your share " ++ Currency.format(~currency, ~amount=userPart)) />
    </ListRow>
  }
}
