open ReactNative;

type sheetAction = {
  icon: option(string),
  text: string,
  onPress: unit => unit
};

type sheetCfg = {
  actions: array(sheetAction)
};

type state = {
  actionSheetContent: option(sheetCfg),
  isExpanded: bool
};

type icon = {
  icon: CategoryIcon.icon,
  color: CategoryColor.t
};

type actions =
  | ShowActionSheet(sheetCfg)
  | HideActionSheet
  | ToggleExpanded;

let component = ReasonReact.reducerComponent("ListRow");

type onPress = | Callback(unit => unit) | Actions(sheetCfg);

let make = (~onRef=?, ~leftIcon=?, ~numberOfLines=?, ~dense=false, ~flexRow=false, ~onPress: option(onPress)=?, ~onLongPress=?, ~style as additionalStyle=Style.(style([])), ~expandedContent=?, ~expandedContentStyle=?,
~wrapperStyle=Style.style([]), children) => {
  ...component,
  initialState: () => { actionSheetContent: None, isExpanded: false },
  reducer: (action, state) => switch action {
    | ShowActionSheet(actionSheetContent) => ReasonReact.Update {
      ...state,
      actionSheetContent: Some(actionSheetContent)
    }
    | HideActionSheet => ReasonReact.Update {
      ...state,
      actionSheetContent: None
    }
    | ToggleExpanded => ReasonReact.Update {
      ...state,
      isExpanded: !state.isExpanded
    }
  },
  render: ({ send, state: { actionSheetContent, isExpanded } }) => {
    let expandedContentElement = {
      (switch (isExpanded, expandedContent) {
        | (true, Some(expContent)) => {
          let defaultStyle = Style.(style([
            paddingLeft(Styles.Spacing.xl +. 44.),
            paddingRight(Styles.Spacing.xl +. 48.)
          ]));
          let style = switch expandedContentStyle {
            | None => defaultStyle
            | Some(s) => Style.combine(defaultStyle, s)
          };
          <View style>
            (expContent)
          </View>
        }
        | _ => ReasonReact.null
      })
    };

    let contentElement = {
      <View>
        <View style=Style.(style([
          switch numberOfLines {
            | None => paddingVertical(0.)
            | Some(numLines) => height(switch (numLines, dense) {
              | (1, false) => 48.
              | (2, false) => 72.
              | (3, false) => 88.
              | (1, true) => 40.
              | (2, true) => 60.
              | (3, true) => 80.
              | _ => 48.
            })
          },
          flexDirection(`row),
          alignItems(`center),
          paddingLeft(Styles.Spacing.xl),
          paddingRight(Js.Option.isSome(expandedContent) ? 0. : Styles.Spacing.xl),
        ]))>
          (switch leftIcon {
            | None => ReasonReact.null
            | Some(icon) => {
              <View
                style=Style.(style([paddingRight(Styles.Spacing.l), paddingTop(1.)]))
              >
                (icon)
              </View>
            }
          })
          <View style=Style.(combine(style([
            flex(1.),
            (flexRow ? flexDirection(`row) : paddingVertical(0.)),
            (flexRow ? alignItems(`center) : paddingVertical(0.)),
            (flexRow ? paddingVertical(0.) : justifyContent(`center)),
          ]), additionalStyle))
          >
            ...children
          </View>
          (Js.Option.isSome(expandedContent) ? {
            <Touchable onPress=(() => send(ToggleExpanded))
              style=Style.(style([width(48. +. Styles.Spacing.s), paddingRight(Styles.Spacing.s), height(48.), alignItems(`center), justifyContent(`center)]))
            >
              <RnIcon name=(isExpanded ? "chevron-up" : "chevron-down") color=Colors.grey />
            </Touchable>
          } : ReasonReact.null)
        </View>
        (expandedContentElement)
      </View>
    };

    (Js.Option.isSome(onPress) || Js.Option.isSome(onLongPress) ? (
      <View style=Style.(combine(style([flexDirection(`row), alignItems(`center)]), wrapperStyle)) ref=?onRef>
        <Touchable style=Style.(style([flex(1.)])) onPress=?(switch onPress {
          | None => None
          | Some(Callback(cb)) => Some(cb)
          | Some(Actions(cfg)) => Some(() => send(ShowActionSheet(cfg)))
        }) ?onLongPress>
          (contentElement)
        </Touchable>
        (switch actionSheetContent {
          | Some(sheetCfg) => {
            <ActionSheet isVisible=true onRequestClose=(() => send(HideActionSheet))>
              (sheetCfg.actions
                |> Array.map(({ icon, text, onPress }) => {
                  <ActionSheet.Action key=text ?icon text onPress=(() => {
                    onPress();
                    send(HideActionSheet)
                  }) />
                })
                |> ReasonReact.array
              )
            </ActionSheet>
          }
          | None => ReasonReact.null
        })
      </View>
    ) : (
      <View style=wrapperStyle ref=?onRef>
      (contentElement)
      </View>
    ))
  }
}
