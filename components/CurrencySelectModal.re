open ReactNative;
open RnMui;

module CurrencyRow = {
  let component = ReasonReact.statelessComponent("CurrencyRow");
  let make = (~currency, ~onPress, _) => {
    ...component,
    render: (_) => {
      let str = Currency.currencyToString(currency);
      let symbol = Currency.symbol(currency);
      <ListRow key=str numberOfLines=1 dense=true
        onPress=Callback(() => onPress(currency))
      >
        <Text style=Styles.Text.body1 value={j|$(str) ($(symbol))|j} />
      </ListRow>
    }
  }
};

type state = {
  searchString: string
};

type actions =
  | UpdateSearchString(string)
  | ClearSearchString;

let component = ReasonReact.reducerComponent("CurrencySelectModal");

let make = (~showRecentlyUsed=true, ~isVisible, ~onCurrencySelected, _) => {
  ...component,
  initialState: () => {
    searchString: ""
  },
  reducer: (action, _state) => switch action {
    | UpdateSearchString(searchString) => ReasonReact.Update {
      searchString: searchString
    }
    | ClearSearchString => ReasonReact.Update {
      searchString: ""
    }
  },
  render: ({ send, state: { searchString } }) => {
    let selectCurrency = (currency) => {
      onCurrencySelected(currency);
      Js.Global.setTimeout(() => {
        send(ClearSearchString)
      }, 300) |> ignore
    };
    let filterCurrency = currency => {
      if (searchString == "") {
        true
      } else {
        Currency.currencyToString(currency) |> String.lowercase |> Js.String.includes(searchString |> String.lowercase)
        || Currency.symbol(currency) |> String.lowercase |> Js.String.includes(searchString |> String.lowercase)
      }
    };
    <RnModal isVisible
      onBackdropPress=(() => selectCurrency(None))
      onBackButtonPress=(() => selectCurrency(None))
      style=Style.(style([alignItems(`center)]))
    >
      <KaScrollView style=Style.(style([backgroundColor("white"), width(250.)]))
        keyboardShouldPersistTaps="handled"
      >
        <ListRow>
          <TextField
            autoCapitalize="none"
            autoCorrect=false
            label="Search"
            value=searchString
            onChangeText=(searchString => send(UpdateSearchString(searchString)))
          />
        </ListRow>
        {showRecentlyUsed ? {
          let recentCurrencies = HistoryDb.get().recentCurrencies;
          let otherCurrencies = Currency.all |> List.filter(currency => {
            !List.exists(rCurrency => rCurrency == currency, recentCurrencies)
          });
          <View>
            (List.length(recentCurrencies) > 0 ? {
              <ListSubheader value="Recently used" withDivider=true />
            } : ReasonReact.null)
            (recentCurrencies
            |> List.filter(filterCurrency)
            |> List.map(currency => {
              <CurrencyRow key=Currency.currencyToString(currency) currency onPress=(currency => selectCurrency(Some(currency))) />
            })
            |> Array.of_list
            |> ReasonReact.array)
            <ListSubheader value="Other" withDivider=true />
            (otherCurrencies
            |> List.filter(filterCurrency)
            |> List.map(currency => {
              <CurrencyRow key=Currency.currencyToString(currency) currency onPress=(currency => selectCurrency(Some(currency))) />
            })
            |> Array.of_list
            |> ReasonReact.array)
          </View>
        } : {
          <View>
            <ListSubheader value="Currencies" withDivider=true />
            (Currency.all
            |> List.filter(filterCurrency)
            |> List.map(currency => {
              <CurrencyRow key=Currency.currencyToString(currency) currency onPress=(currency => selectCurrency(Some(currency))) />
            })
            |> Array.of_list
            |> ReasonReact.array)
          </View>
        }}
      </KaScrollView>
    </RnModal>
  }
}
