open RnMui;

open Category;

let component = ReasonReact.statelessComponent("CategoryTitleConfigDropdown");

let make = (~value=Optional, ~onValueChange, _) => {
  ...component,
  render: (_) => {
    let data =
      [|Required, Optional|]
      |> Array.map(titleConfigToStr)
      |> Array.map((config) => {"label": config, "value": config});
    <Dropdown
      label="Title config"
      data
      value=(titleConfigToStr(value))
      onChangeText=(((value, _)) => onValueChange(strToTitleConfig(value)))
    />
  }
};
