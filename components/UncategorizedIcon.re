let component = ReasonReact.statelessComponent("UncategorizedIcon");

let make = (_) => {
  ...component,
  render: (_) => {
    <LeftIcon icon=Icon("folder-remove") color=(Colors.danger |> CategoryColor.fromString) />
  }
}
