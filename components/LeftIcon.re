open ReactNative;

let component = ReasonReact.statelessComponent("LeftIcon");

let make = (~color as iconColor=?, ~icon, ~size=24., _) => {
  ...component,
  render: (_) => {
    let iconPadding = size /. 3.;
    <View
      style=Style.(
              style([
                flexDirection(`row),
                alignItems(`center),
                justifyContent(`center),
                backgroundColor(
                  iconColor |> C.Option.map(CategoryStuff.color) |> Js.Option.getWithDefault("#616161")
                ),
                width(size +. iconPadding),
                height(size +. iconPadding),
                overflow(`hidden),
                borderRadius(4.)
              ])
            )>
      <CategoryIcon icon size />
    </View>;
  }
};
