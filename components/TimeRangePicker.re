open RnMui;

let component = ReasonReact.statelessComponent("TimeRangePicker");

let make = (~selectedRange, ~onRangeSelected, _) => {
  ...component,
  render: (_) =>
    <Dropdown
      label="Time range"
      value=(TimeRange.toString(selectedRange))
      onChangeText=(((range, _)) => onRangeSelected(TimeRange.toRange(range)))
      data=[|
        {"value": "This week"},
        {"value": "Last week"},
        {"value": "Last 7 days"},
        {"value": "This month"},
        {"value": "Last month"},
        {"value": "Last 30 days"},
        {"value": "All"}
      |]
    />
};
