open ReactNative;

let component = ReasonReact.statelessComponent("Chip");

let make =
    (
      ~color as chipColor=?,
      ~withTitle,
      ~height as elementHeight=16.,
      ~style as additionalStyle=Style.style([]),
      ~icon=?,
      ~label,
      _
    ) => {
  ...component,
  render: (_) => {
    let fontScale = PixelRatio.getFontScale();
    <View
      style=Style.(
              combine(
                style([
                  flexDirection(`row),
                  alignItems(`center),
                  backgroundColor(
                    switch chipColor {
                    | Some(c) => c
                    | None => "#616161"
                    }
                  ),
                  withTitle ? minWidth(elementHeight) : width(elementHeight),
                  height(elementHeight *. fontScale),
                  borderRadius(2.)
                ]),
                additionalStyle
              )
            )>
      (
        switch icon {
        | Some(icon) =>
          <RnIcon
            name=icon
            color="white"
            size=((elementHeight -. 5.) *. fontScale)
            style=Style.(style([withTitle ? paddingLeft(3.) : padding(2.)]))
          />
        | None => ReasonReact.null
        }
      )
      (
        withTitle ?
          {
            let textSize =
              switch elementHeight {
              | h when h > 22. => 15.
              | h when h > 17. => 13.
              | _ => 11.
              };
            <Text
              value=label
              style=Style.(
                      style([
                        fontSize(textSize),
                        color("white"),
                        maxWidth(85.),
                        paddingLeft(3.),
                        paddingRight(3.),
                        paddingBottom(1.)
                      ])
                    )
              numberOfLines=1
            />;
          } :
          ReasonReact.null
      )
    </View>
  }
};
