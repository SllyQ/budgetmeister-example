open ReactNative;

let component = ReasonReact.statelessComponent("OptionSwitchRow");

let make = (~label, ~value, ~disabled=?, ~onValueChange, ~inverted=false, _) => {
  ...component,
  render: (_) => {
    let switchElement = (
      <Connect_SelectedTheme render=(theme => {
        <Switch value=value ?disabled
          thumbTintColor=?(value ? Some(theme.primary) : None)
          onTintColor=theme.primaryLight
          onValueChange=((value) => onValueChange(value))
        />
      }) />
    );
    <ListRow numberOfLines=1 onPress=Callback(() => onValueChange(!value))>
      <View style=Style.(style([
        flex(1.),
        flexDirection(`row),
        alignItems(`center),
        justifyContent(inverted ? `flexStart : `spaceBetween),
      ]))>
        (inverted ? switchElement : ReasonReact.null)
        <Text value=label
          style=Style.(combine(
            Styles.Text.listPrimary,
            style([marginLeft(inverted ? Styles.Spacing.s : 0.)])
          ))
        />
        (inverted ? ReasonReact.null : switchElement)
      </View>
    </ListRow>
  }
}
