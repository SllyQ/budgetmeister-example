type state = {
  snackbarState: SnackbarService.t,
  snackbarSub: option(SnackbarService.sub)
};

type actions =
  | UpdateSnackbarState(SnackbarService.t)
  | SetSnackbarSub(SnackbarService.sub);

let component = ReasonReact.reducerComponent("SnackbarService");

let make = (_) => {
  ...component,
  initialState: () => {snackbarState: SnackbarService.currState^, snackbarSub: None},
  didMount: ({send}) => {
    let sub = SnackbarService.onChange((snackbarState) => send(UpdateSnackbarState(snackbarState)));
    send(SetSnackbarSub(sub));
  },
  willUnmount: ({state}) =>
    switch state.snackbarSub {
    | Some(sub) => SnackbarService.unsub(sub)
    | None => ()
    },
  reducer: (action, state) =>
    switch action {
    | UpdateSnackbarState(snackbarState) => ReasonReact.Update({...state, snackbarState})
    | SetSnackbarSub(sub) => ReasonReact.Update({...state, snackbarSub: Some(sub)})
    },
  render: ({state: {snackbarState}}) => {
    let actionText =
      switch snackbarState.action {
      | None => None
      | Some(a) => Some(a.text)
      };
    let actionHandler =
      switch snackbarState.action {
      | None => None
      | Some(a) => Some(a.handler)
      };
    <RnSnackbar
      visible=snackbarState.visible
      textMessage=snackbarState.textMessage
      bottom=snackbarState.bottom
      ?actionText
      ?actionHandler
    />
  }
};
