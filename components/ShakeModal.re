open ReactNative;
open RnMui;

type state = {
  shakeListener: option(DeviceShakeStackService.shakeListener)
};

type actions =
  | ShowComponent(DeviceShakeStackService.shakeListener)
  | HideComponent;

let component = ReasonReact.reducerComponent("ShakeModal");


let make = (_) => {
  ...component,
  initialState: () => { shakeListener: None },
  didMount: ({ send }) => {
    DeviceShakeStackService.shakeListener := Some(element => send(ShowComponent(element)));
  },
  reducer: (action, _state) => switch action {
    | ShowComponent(component) => ReasonReact.Update { shakeListener: Some(component) }
    | HideComponent => ReasonReact.Update { shakeListener: None }
  },
  render: ({ state: { shakeListener }, send }) => {
    switch (shakeListener) {
      | None => ReasonReact.null
      | Some (component: DeviceShakeStackService.shakeListener) => {
        <RnModal isVisible=Js.Option.isSome(shakeListener)
          onBackdropPress=(() => send(HideComponent))
          onBackButtonPress=(() => send(HideComponent))
        >
          <View style=Style.(style([paddingHorizontal(Styles.Spacing.xxl), paddingTop(Styles.Spacing.xxl), backgroundColor("#fff")]))>
            <ScrollView>
              <View style=Style.(style([paddingBottom(20.)]))>
                <Text style=Styles.Text.title value=component.title />
              </View>
              <View style=Style.(style([paddingBottom(20.)]))>
                (switch component.message {
                  | `String(message) => <ListPrimary value=message />
                  | `Element(element) => element
                })
              </View>
            </ScrollView>
            <View style=Style.(style([padding(8.), flexDirection(`row), justifyContent(`flexEnd)]))>
              /* <MuiButton text="Send feedback" onPress=(() => send(HideComponent)) primary=true /> */
              <MuiButton text="Close" onPress=(() => send(HideComponent)) primary=true />
            </View>
          </View>
        </RnModal>
      }
    }
  }
}
