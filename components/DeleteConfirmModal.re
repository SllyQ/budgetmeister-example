open ReactNative;

open RnMui;

let component = ReasonReact.statelessComponent("DeleteConfirmModal");

let make = (~isVisible, ~onConfirm, ~onDecline, _) => {
  let _a = (_) => ();
  {
    ...component,
    render: (_) =>
      <RnModal isVisible onBackButtonPress=onDecline>
        <View style=Style.(style([flex(1.), alignItems(`center), justifyContent(`center)]))>
          <Dialog>
            <Dialog.Title> <Text value="Are you sure?" /> </Dialog.Title>
            <Dialog.Content>
              <Text value="Just double checking so you don't delete things by accident" />
            </Dialog.Content>
            <Dialog.Actions>
              <View
                style=Style.(style([flexDirection(`row), justifyContent(`flexEnd), padding(10.)]))>
                <MuiButton text="No" primary=true onPress=onDecline />
                <MuiButton text="Yes" accent=true onPress=onConfirm />
              </View>
            </Dialog.Actions>
          </Dialog>
        </View>
      </RnModal>
  }
};
