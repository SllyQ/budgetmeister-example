let component = ReasonReact.statelessComponent("TagIcon");

let make =
    (
      ~tagId,
      ~withTitle=false,
      ~height=?,
      ~style=?,
      _
    ) => {
  ...component,
  render: (_) =>
    <C_Tag
      tagId
      render=(
        (tag) =>
          switch (tag.color, tag.icon, withTitle) {
          | (None, None, false) => ReasonReact.null
          | _ =>
            <Chip
              ?height
              withTitle
              icon=?tag.icon
              label=tag.title
              ?style
              color=?(tag.color |> C.Option.map(CategoryStuff.color))
            />
          }
      )
    />
};
