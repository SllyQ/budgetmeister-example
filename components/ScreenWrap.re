open ReactNative;

type state = {
  shakeEventListener: ref(option(DeviceShakeStackService.sub))
};

let component = ReasonReact.statelessComponent("ScreenWrap");

let make = (~scrollable=true, ~contentContainerStyle=Style.(style([])), ~fab=?, children) => {
  ...component,
  render: (_) => {
    let fabElement = (switch fab {
      | None => ReasonReact.null
      | Some(fab) => {
        <SnackbarAwareWrapper>
          (Platform.equals(IOS) ? {
            <KaMarginView>
              (fab)
            </KaMarginView>
          } : {
            (fab)
          })
        </SnackbarAwareWrapper>
      }
    });
    scrollable ?
      <View style=Style.(style([flex(1.)]))>
        (ReasonReact.element(KaScrollView.make(
          ~keyboardShouldPersistTaps="handled",
          ~contentContainerStyle=Style.(
            combine(style([paddingBottom(Js.Option.isSome(fab) && fab != Some(ReasonReact.null) ? 70. : 0.)]),
            contentContainerStyle)
          ),
          children
        )))
        (fabElement)
      </View> :
      <View style=Style.(style([flex(1.)]))>
        <View style=Style.(style([flex(1.)]))>
          ...children
        </View>
        (fabElement)
      </View>
  }
};
