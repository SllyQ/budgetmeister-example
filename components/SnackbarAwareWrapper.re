open ReactNative;

type state = {
  bottom: Animated.Value.t,
  snackbarSub: option(SnackbarService.sub)
};

type actions =
  | AdjustToSnackbarVisibility(SnackbarService.t)
  | SetSnackbarSub(SnackbarService.sub);

let component = ReasonReact.reducerComponent("SnackbarAwareWrapper");

let make = (children) => {
  ...component,
  initialState: () => {bottom: Animated.Value.create(0.), snackbarSub: None},
  didMount: ({send}) => {
    let sub =
      SnackbarService.onChange((snackbarState) => send(AdjustToSnackbarVisibility(snackbarState)));
    send(SetSnackbarSub(sub));
  },
  willUnmount: ({state}) =>
    switch state.snackbarSub {
    | Some(sub) => SnackbarService.unsub(sub)
    | None => ()
    },
  reducer: (action, state) =>
    switch action {
    | AdjustToSnackbarVisibility(snackbarState) =>
      ReasonReact.SideEffects(
        (
          (_) =>
            if (snackbarState.visible) {
              AnimatedRe.CompositeAnimation.start(
                AnimatedRe.Timing.animate(
                  ~value=state.bottom,
                  ~toValue=`raw(45.),
                  ~duration=225.,
                  ~easing=(Swipeable.Easing.bezier(0.0, 0.0, 0.2, 1.))(),
                  ()
                ),
                ()
              )
            } else {
              AnimatedRe.CompositeAnimation.start(
                AnimatedRe.Timing.animate(
                  ~value=state.bottom,
                  ~toValue=`raw(0.),
                  ~duration=195.,
                  ~easing=(Swipeable.Easing.bezier(0.4, 0.0, 1., 1.))(),
                  ()
                ),
                ()
              )
            }
        )
      )
    | SetSnackbarSub(snackbarSub) => ReasonReact.Update({...state, snackbarSub: Some(snackbarSub)})
    },
  render: ({state}) =>
    ReasonReact.element(
      Swipeable.AnimatedView.make(~style={"marginBottom": state.bottom}, children)
    )
};
