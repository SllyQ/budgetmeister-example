open ReactNative;

type actions =
  | SetInputRef(Js.Nullable.t(ReasonReact.reactRef))
  | FocusInput;

type state = {
  inputRef: ref(option(ReasonReact.reactRef)),
};

let component = ReasonReact.reducerComponent("CostInput");

let focus = (ref) => {
  let jsObj = ReasonReact.refToJsObj(ref);
  let self = jsObj##self();
  self.ReasonReact.send(FocusInput);
};

let make =
    (
      ~amount,
      ~currency,
      ~onAmountChanged,
      ~onCurrencyChanged,
      ~containerStyle=?,
      ~disabled=?,
      ~inputStyle=?,
      ~onSubmitEditing=?,
      ~label=?,
      ~returnKeyType=?,
      ~onlyPositive=?,
      _
    ) => {
  {
    ...component,
    initialState: () => {
      inputRef: ref(None)
    },
    reducer: (action, state) =>
      switch action {
      | SetInputRef(ref) => ReasonReact.SideEffects((_) => {
        state.inputRef := Js.Nullable.toOption(ref)
      })
      | FocusInput =>
        ReasonReact.SideEffects(
          (
            (_) =>
              switch state.inputRef^ {
              | Some(ref) => NumberInput.focus(ref)
              | None => ()
              }
          )
        )
    },
    render: ({send}) => {
      <View style=Style.(style([flexDirection(`row), alignItems(`flexEnd)]))>
        <View style=Style.(style([flex(1.), paddingRight(Styles.Spacing.l)]))>
          <NumberInput
            style=?inputStyle
            value=amount
            onChangeValue=onAmountChanged
            ref=((ref) => send(SetInputRef(ref)))
            ?onSubmitEditing
            ?containerStyle
            ?disabled
            ?label
            ?returnKeyType
            ?onlyPositive
          />
        </View>
        <CurrencySelectButton
          buttonStyle=Style.(style([marginBottom(8.)]))
          label=Currency.symbol(currency)
          onNewCurrencySelected=onCurrencyChanged
        />
      </View>
    }
  }
};
