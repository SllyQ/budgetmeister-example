open RnMui;

let component = ReasonReact.statelessComponent("MonthsDropdown");

let monthNames = [|
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December"
|];

let make = (~label="Month", ~value, ~onValueChanged, _) => {
  ...component,
  render: (_) => {
    <Dropdown label
      data=(monthNames |> Array.map(name => { "label": name, "value": name }))
      value=monthNames[int_of_float(value)]
      onChangeText=(((_, i)) => onValueChanged(i))
    />
  }
}
