let component = ReasonReact.statelessComponent("EmptyStateWrapper");

let make = (children) => {
  ...component,
  render: (_) => {
    let dimensions = ReactNative.Dimensions.get(`window);
    ReasonReact.element(Column.make(
      ~flex=1.,
      ~alignItems=`center,
      ~justifyContent=`center,
      ~style=ReactNative.Style.(style([
        paddingHorizontal(36.),
        height((dimensions##height - (TabsService.active^ ? 222 : 150)) |> float_of_int),
        width(dimensions##width |> float_of_int)
      ])),
      children
    ))
  }
}
