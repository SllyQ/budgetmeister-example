open ReactNative;

type sheetAction = {
  icon: string,
  text: string,
  onPress: unit => unit
};

type state = {
  actionsToShow: option(array(sheetAction))
};

type actions =
  | ShowActionSheet(array(sheetAction))
  | HideActionSheet;

let component = ReasonReact.reducerComponent("ModalBox");

let onShow = ref(None);

let show = (actions) => {
  switch onShow.contents {
    | Some(cb) => cb(actions)
    | None => ()
  }
};

let make = (_) => {
  ...component,
  initialState: () => { actionsToShow: None },
  didMount: ({ send }) => {
    onShow := Some(actions => send(ShowActionSheet(actions)));
  },
  reducer: (action, _state) => switch action {
    | ShowActionSheet(actions) => ReasonReact.Update { actionsToShow: Some(actions) }
    | HideActionSheet => ReasonReact.Update { actionsToShow: None }
  },
  render: ({ send, state: { actionsToShow }}) => {
    switch actionsToShow {
      | Some(actions) => {
        <ModalBox isOpen=true position="bottom" style=Style.(style([height(120.)])) onClosed=(() => send(HideActionSheet))>
          <View style=Style.(style([paddingVertical(Styles.Spacing.m), backgroundColor("#fff")]))>
            (actions
              |> Array.map(({ icon, text, onPress }) => {
                <ActionSheet.Action key=text icon text onPress />
              })
              |> ReasonReact.array
            )
          </View>
        </ModalBox>
      }
      | None => ReasonReact.null
    }
  }
};
