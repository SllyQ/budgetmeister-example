open ReactNative;

let component = ReasonReact.statelessComponent("CategoryIcon");

type icon =
  | Icon(string)
  | Letter(string);

let make = (~icon, ~size, _) => {
  ...component,
  render: (_) => {
    <View style=Style.(style([width(size), height(size), alignItems(`center), justifyContent(`center)]))>
      (switch icon {
      | Icon(icon) =>
        <RnIcon name=icon color="white" size=(size) />
      | Letter(letter) =>
        <Text
          value=letter
          style=Style.(
            style([
              fontSize(size),
              color(Colors.white),
              fontFamily("Roboto-Medium"),
              paddingBottom(1.)
            ])
          )
        />
      })
    </View>
  }
}
