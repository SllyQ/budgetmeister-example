let component = ReasonReact.statelessComponent("RecurringExpenseChip");

let make = (~style=?, _) => {
  ...component,
  render: (_) => {
    <Chip
      ?style
      withTitle=true
      icon="repeat"
      label="Recurring"
    />
  }
}
