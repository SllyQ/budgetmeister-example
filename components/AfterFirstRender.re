open ReactNative;

type state = {
  firstRenderHappened: ref(bool),
};

type actions = | HandleFirstRender;

let component = ReasonReact.reducerComponent("AfterFirstRender");

let make = (~cb, children) => {
  ...component,
  initialState: () => { firstRenderHappened: ref(false) },
  reducer: (action, state) => switch action {
    | HandleFirstRender => ReasonReact.SideEffects((_) => {
      state.firstRenderHappened := true;
      Js.Global.setTimeout(() => {
        cb()
      }, 0) |> ignore
    })
  },
  render: ({ state: { firstRenderHappened }, send }) => {
    if (!firstRenderHappened.contents) {
      send(HandleFirstRender)
    };
    <Fragment>
      ...children
    </Fragment>
  }
}
