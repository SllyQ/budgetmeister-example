open ReactNative;
open RnMui;

type state = {
  pickerOpen: bool
};

type actions =
  | OpenPicker
  | PickDate(Js.Date.t)
  | ClosePicker;

let component = ReasonReact.reducerComponent("DatePickerRow");

let make = (~label, ~date, ~onDateChanged, ~onDateCleared=?, ~noneLabel="none", _) => {
  let formatDate = (date) => {
    switch date {
      | date when DateFns.isToday(date) => "Today"
      | date when DateFns.isYesterday(date) => "Yesterday"
      | date => date |> DateFns.format("YYYY MMMM Do")
    }
  };

  {
    ...component,
    initialState: () => { pickerOpen: false },
    reducer: C.flip(_state => fun
      | OpenPicker => ReasonReact.Update {
        pickerOpen: true
      }
      | PickDate(date) => ReasonReact.UpdateWithSideEffects({
        pickerOpen: false
      }, (_) => {
        onDateChanged(date)
      })
      | ClosePicker => ReasonReact.Update {
        pickerOpen: false
      }
    ),
    render: ({ send, state: { pickerOpen }}) => {
      <Connect_SelectedTheme render=(theme => {
        <ListRow numberOfLines=1 flexRow=true onPress=Callback(() => send(OpenPicker)) style=Style.(style([justifyContent(`spaceBetween)]))>
          <View style=Style.(style([flexDirection(`row)]))>
            <Text style=Style.(combine(Styles.Text.listPrimary, style([color(theme.primary), Styles.Text.robotoMedium]))) value=(label ++ ": ") />
            <Text style=Styles.Text.listPrimary value=(switch date {
              | Some(date) => formatDate(date)
              | None => noneLabel
            }) />
          </View>
          (switch (onDateCleared, date) {
            | (Some(cb), Some(_)) => {
              <View>
                <MuiButton text="Clear" onPress=cb primary=true />
              </View>
            }
            | _ => ReasonReact.null
          })
          <DtPicker
            ?date
            isVisible=pickerOpen
            onConfirm=(date => send(PickDate(date)))
            onCancel=(() => send(ClosePicker))
          />
        </ListRow>
      }) />
    }
  }
}
