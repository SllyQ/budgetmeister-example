open ReactNative;

let component = ReasonReact.statelessComponent("TagsSelectRow");

let styles =
  StyleSheet.create(
    Style.(
      {
        "inputRow": style([flexDirection(`row), alignItems(`center), height(45.)]),
        "selectedTagsRow": style([flexDirection(`row), alignItems(`center)]),
        "selectedTag":
          style([
            flexDirection(`row),
            alignItems(`center),
            marginLeft(5.),
            marginTop(2.),
            flexShrink(1.)
          ])
      }
    )
  );

let make = (_, ~selectedTagIds, ~onSelectedTagIdsUpdate, ~onSelectTags) => {
  let selectTags = (selectedTagIds, _) => {
    onSelectTags(selectedTagIds |> Array.to_list)
    |> Js.Promise.then_(
         (optTagIds) => {
           switch optTagIds {
           | Some(tagIds) => onSelectedTagIdsUpdate(tagIds)
           | None => ()
           };
           Js.Promise.resolve()
         }
       )
    |> ignore;
  };
  {
    ...component,
    render: ({handle}) =>
      <Connect_SelectedTheme render=(theme => {
        <C_Tags
          render=(
            (tags) => {
              let selectedTags =
                selectedTagIds
                |> List.map(
                     (tagId: Tag.id) => List.find_all((tag: Tag.t) => tag.id == tagId, Array.to_list(tags))
                   )
                |> List.flatten
                |> Array.of_list;
              let sortedSelectedTags =
                Common.iSort(
                  (t1, t2) =>
                    String.compare(String.lowercase(t1.Tag.title), String.lowercase(t2.title)),
                  selectedTags
                );
              <ListRow
                onPress=Callback(() =>
                  handle(selectTags, selectedTags |> Array.map(tag => tag.Tag.id))
                )
                numberOfLines=1
              >
                (
                  switch sortedSelectedTags {
                  | [||] => <Row>
                    <Text style=Style.(combine(Styles.Text.listPrimary, style([color(theme.primary), paddingRight(Styles.Spacing.m), Styles.Text.robotoMedium])))  value="Tags:" />
                    <Text style=Styles.Text.listPrimary value="None" />
                  </Row>
                  | tags =>
                    <View style=styles##selectedTagsRow>
                      <Text style=Style.(combine(Styles.Text.listPrimary, style([color(theme.primary), paddingRight(Styles.Spacing.m), Styles.Text.robotoMedium])))  value="Tags:" />
                      (
                        tags
                        |> Array.map(
                             (tag: Tag.t) =>
                               <View key=(Tag.key(tag.id)) style=styles##selectedTag>
                                 <TagChip tagId=tag.id withTitle=true height=16. />
                               </View>
                           )
                        |> ReasonReact.array
                      )
                    </View>
                  }
                )
              </ListRow>
            }
          )
        />
      }) />
  }
};
