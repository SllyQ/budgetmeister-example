open ReactNative;
open Paper;

type state = {
  editedValue: option(BudgetingPeriod.t),
  selectingBiweeklyInterval: bool
};

type actions =
  | OpenSelectModal
  | CancelSelection
  | SelectBiweeklyInterval
  | CancelBiweeklyIntervalSelection
  | StartOfMonthSelected(int)
  | BiweeklyIntervalSelected(int)
  | SelectMonthly
  | SelectBiweekly
  | SaveSelection
  ;

let component = ReasonReact.reducerComponent("BudgetingPeriodSelectRow");

let make = (~value, ~onValueChanged, _) => {
  ...component,
  initialState: () => { editedValue: None, selectingBiweeklyInterval: false },
  reducer: C.flip(state => fun
    | OpenSelectModal => ReasonReact.Update {
      ...state,
      editedValue: Some(value)
    }
    | CancelSelection => ReasonReact.Update {
      ...state,
      editedValue: None
    }
    | SelectBiweeklyInterval => ReasonReact.Update {
      ...state,
      selectingBiweeklyInterval: true
    }
    | CancelBiweeklyIntervalSelection => ReasonReact.Update {
      ...state,
      selectingBiweeklyInterval: false
    }
    | SelectMonthly => ReasonReact.Update {
      ...state,
      editedValue: Some(Monthly(1))
    }
    | SelectBiweekly => ReasonReact.Update {
      ...state,
      editedValue: Some(BiWeekly(0))
    }
    | StartOfMonthSelected(i) => ReasonReact.Update {
      ...state,
      editedValue: Some(Monthly(i))
    }
    | BiweeklyIntervalSelected(i) => ReasonReact.Update {
      editedValue: Some(BiWeekly(i)),
      selectingBiweeklyInterval: false
    }
    | SaveSelection => ReasonReact.UpdateWithSideEffects({
      ...state,
      editedValue: None
    }, (_) => {
      switch (state.editedValue) {
        | None => ()
        | Some(value) => onValueChanged(value)
      }
    })
  ),
  render: ({ send, state: { editedValue, selectingBiweeklyInterval } }) => {
    <View>
      (switch value {
        | BudgetingPeriod.Monthly(_) => {
          let { Utils.start } = Utils.getPeriodInfo(~date=Js.Date.make(), ~period=value, ());
          <ListRow numberOfLines=2 dense=true onPress=Callback(() => send(OpenSelectModal))>
            <Row alignItems=`center>
              <ListPrimary>
                <RowTitle value="Budgeting period: " />
                <Text value="Monthly" />
              </ListPrimary>
            </Row>
            <ListSecondary value=("Month starts at " ++ (start |> DateFns.format("Do")) ++ " day") />
          </ListRow>
        }
        | BiWeekly(_) => {
          let { Utils.start, finish } = Utils.getPeriodInfo(~date=Js.Date.make(), ~period=value, ());
          <ListRow numberOfLines=2 dense=true onPress=Callback(() => send(OpenSelectModal))>
            <Row alignItems=`center>
              <ListPrimary>
                <RowTitle value="Budgeting period: " />
                <Text value="Bi-Weekly" />
              </ListPrimary>
            </Row>
            <ListSecondary value=("Current period: " ++ (start |> DateFns.format("MMM Do")) ++ " - " ++ (finish |> DateFns.format("MMM Do"))) />
          </ListRow>
        }
      })
      (switch editedValue {
        | None => ReasonReact.null
        | Some(editedValue) => {
          let monthlySelected = switch editedValue { | Monthly(_) => true | _ => false };
          <Dialog visible=true onDismiss=(() => send(CancelSelection))>
            <Dialog.Title value="Budgeting period" />
            <Dialog.Content>
              <TouchableRipple onPress=(() => send(SelectMonthly))>
                <Row height=48. alignItems=`center>
                  <RadioButton checked=monthlySelected />
                  <Text style=Style.(combine(Styles.Text.listPrimary, style([]))) value="Monthly" />
                </Row>
              </TouchableRipple>
              <TouchableRipple onPress=(() => send(SelectBiweekly))>
                <Row height=48. alignItems=`center>
                  <RadioButton checked=(!monthlySelected) />
                  <Text style=Style.(combine(Styles.Text.listPrimary, style([]))) value="Bi-Weekly" />
                </Row>
              </TouchableRipple>
              (switch editedValue {
                | Monthly(day) => {
                  <Row>
                    <View style=Style.(style([flex(1.)]))>
                      <RnMui.Dropdown
                        label="Month starts at"
                        value=day
                        onChangeText=(((v, _)) => send(StartOfMonthSelected(v)))
                        data=(Belt.Array.range(1, 28) |> Array.map(i => {
                          "value": i,
                          "label": i
                        }))
                      />
                    </View>
                  </Row>
                }
                | BiWeekly(offset) => {
                  let periodStart = BudgetingPeriod.dateFromOffset(offset);
                  <TouchableRipple onPress=(() => send(SelectBiweeklyInterval))>
                    <Row height=48. alignItems=`center>
                      <Text style=Styles.Text.listPrimary>
                        <Text style=Style.(style([color(Colors.primary), Styles.Text.robotoMedium])) value="Period starts at: " />
                        <Text value=(periodStart |> DateFns.format("MMMM Do")) />
                      </Text>
                    </Row>
                  </TouchableRipple>
                }
              })
              <Dialog.Actions>
                <MuiButton label="Cancel" onPress=(() => send(CancelSelection)) primary=true />
                <MuiButton label="Save" onPress=(() => send(SaveSelection)) primary=true />
              </Dialog.Actions>
            </Dialog.Content>
          </Dialog>
        }
      })
      (switch editedValue {
        | Some(BiWeekly(offset)) => {
          let now = Js.Date.make();
          <DtPicker isVisible=selectingBiweeklyInterval
            date=BudgetingPeriod.dateFromOffset(offset)
            onConfirm=(date => send(BiweeklyIntervalSelected(BudgetingPeriod.offsetFromDate(date))))
            onCancel=(() => send(CancelBiweeklyIntervalSelection))
            minimumDate=(now |> DateFns.subDays(13.))
            maximumDate=now
          />
        }
        | _ => ReasonReact.null
      })
    </View>
  }
}
