open RepeatConfig;
open ReactNative;
open Paper;

let defaultDailyConfig = RepeatConfig.Daily({
  addOn: HoursOfDay([|0|])
});

let defaultWeeklyConfig = RepeatConfig.Weekly({
  addOn: DaysOfWeek([|1|])
});

let defaultMonthlyConfig = RepeatConfig.Monthly({
  addOn: DaysOfMonth([|1|])
});


let component = ReasonReact.statelessComponent("RepeatConfigForm");

let make = (~value, ~onValueChanged, _) => {
  ...component,
  render: (_) => {
    <Column>
      <ListRow flexRow=true>
        <Column flex=1.>
          <TouchableRipple onPress=(() => onValueChanged(defaultMonthlyConfig)) style=Style.(style([flex(1.)]))>
            <Row alignItems=`center style=Style.(style([paddingHorizontal(Styles.Spacing.m), paddingVertical(Styles.Spacing.s)]))>
              <Paper.RadioButton
                checked=(switch value { | Monthly(_) => true | _ => false })
              />
              <ListPrimary value="Monthly" />
            </Row>
          </TouchableRipple>
        </Column>
        <Column flex=1.>
          <TouchableRipple onPress=(() => onValueChanged(defaultWeeklyConfig))>
            <Row alignItems=`center style=Style.(style([paddingHorizontal(Styles.Spacing.m), paddingVertical(Styles.Spacing.s)]))>
              <Paper.RadioButton
                checked=(switch value { | Weekly(_) => true | _ => false })
              />
              <ListPrimary value="Weekly" />
            </Row>
          </TouchableRipple>
        </Column>
      </ListRow>
      (switch value {
        | Daily(config) => <RecurringExpenseDailyConfig
          value=config
          onValueChanged=(config => onValueChanged(Daily(config)))
        />
        | Weekly(config) => <RecurringExpenseWeeklyConfig
          value=config
          onValueChanged=(config => onValueChanged(Weekly(config)))
        />
        | Monthly(config) => <RecurringExpenseMonthlyConfig
          value=config
          onValueChanged=(config => onValueChanged(Monthly(config)))
        />
      })
    </Column>
  }
}
