open ReactNative;

module Container = {
  let component = ReasonReact.statelessComponent("BottomNav_Container");

  let make = (children) => {
    ...component,
    didMount: (_) => {
      TabsService.active := true;
    },
    willUnmount: (_) => {
      TabsService.active := false
    },
    render: (_) => {
      <C_Settings render=(settings => {
        <Connect_SelectedTheme render=(theme => {
          <Row style=Style.(style([height(56.), elevation(6.), backgroundColor(settings.lightBars ? "white" : theme.primary)]))>
            ...children
          </Row>
        }) />
      }) />
    }
  }
};

module Tab = {
  let component = ReasonReact.statelessComponent("BottomNav_Tab");

  let make = (~icon, ~label, ~active, ~onPress, _) => {
    ...component,
    render: (_) => {
      <C_Settings render=(settings => {
        <Connect_SelectedTheme render=(theme => {
          <Touchable onPress style=Style.(style([flex(1.)]))>
            <Column style=Style.(style([paddingTop(active ? 6. : 8.), paddingBottom(10.), alignItems(`center), flex(1.)]))>
              <RnIcon name=icon size=24.
                color=(settings.lightBars
                  ? (active ? theme.primary : Colors.grey)
                  : (active ? Colors.white : Colors.lightWhite))
              />
              <Text value=label
                style=Style.(style([
                  paddingHorizontal(12.),
                  color(settings.lightBars
                    ? (active ? theme.primary : Colors.grey)
                    : (active ? Colors.white : Colors.lightWhite)
                  ),
                  fontSize(active ? 14. : 12.)
                ]))
              />
            </Column>
          </Touchable>
        }) />
      }) />
    }
  };
};
