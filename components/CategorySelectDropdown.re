open ReactNative;

open RnMui;

let component = ReasonReact.statelessComponent("CategorySelectDropdown");

let styles =
  StyleSheet.create(
    Style.({"inputRow": style([flexDirection(`row), alignItems(`center), height(45.)])})
  );

let make =
    (
      ~parentCategoryId,
      ~selectedCategoryId,
      ~onCategorySelected,
      ~allowNull=false,
      ~label="Parent category",
      ~nullOptionLabel="None",
      _
    ) => {
  ...component,
  render: (_) =>
    <C_ChildCategories
      parentCategoryId
      render=(
        (categories) => {
          let sortedCategories =
            categories
            |> Common.iSort(
                 (c1, c2) =>
                   String.compare(String.lowercase(c1.Category.title), String.lowercase(c2.title))
               );
          let options =
            sortedCategories
            |> Array.map(
                 (category: Category.t) => {
                   "label": category.title,
                   "value": Category.key(category.id)
                 }
               );
          let allOptions =
            if (allowNull) {
              Array.append([|{"value": "", "label": nullOptionLabel}|], options)
            } else {
              options
            };
          let doubleSortedCategories =
            Common.iSort(
              (c1: Category.t, c2: Category.t) =>
                String.compare(String.lowercase(c1.title), String.lowercase(c2.title)),
              sortedCategories
            );
          if (Array.length(sortedCategories) === 0) {
            /* Initially its empty for first render as sending categories
               gets debounced in DB */
            ReasonReact.null
          } else {
            let selectedCategory =
              switch selectedCategoryId {
              | Some(catId: Category.id) =>
                Some(
                  List.find(
                    (cat) => cat.Category.id == catId,
                    Array.to_list(doubleSortedCategories)
                  )
                )
              | None => None
              };
            <Dropdown
              label
              data=allOptions
              value=(
                switch selectedCategory {
                | Some(cat) => cat.title
                | None => nullOptionLabel
                }
              )
              onChangeText=(
                ((value, _)) =>
                  if (allowNull) {
                    if (value === "") {
                      onCategorySelected(None)
                    } else {
                      onCategorySelected(Some(Category.fromKey(value)))
                    }
                  } else {
                    onCategorySelected(Some(Category.fromKey(value)))
                  }
              )
            />
          }
        }
      )
    />
};
