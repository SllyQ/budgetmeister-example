open ReactNative;

type state = {showComponent: bool};

type actions =
  | Show;

let component = ReasonReact.reducerComponent("DebounceInitialRender");

let make = (children) => {
  ...component,
  initialState: () => {showComponent: false},
  reducer: (action, _state) =>
    switch action {
    | Show => ReasonReact.Update({showComponent: true})
    },
  didMount: ({send}) => {
    let _ = Js.Global.setTimeout((_) => send(Show), 0);
  },
  render: ({state}) =>
    state.showComponent ?
      ReasonReact.element(View.make(~style=Style.(style([flex(1.)])), children)) :
      <ScreenActivityIndicator />
};
