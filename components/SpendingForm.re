open ReactNative;

type value = {
  categorization: option(Categorization.t),
  totalAmount: NumberInput.value,
  currency: Currency.t
};

type state = {
  costInputRef: ref(option(ReasonReact.reactRef))
};

let component = ReasonReact.reducerComponent("SpendingForm");

let setCostInputRef = (nullRef, { ReasonReact.state }) => {
  state.costInputRef := nullRef |> Js.Nullable.toOption
};

let handleFocusCostInput = ((), { ReasonReact.state }) => {
  switch state.costInputRef.contents {
    | Some(ref) => CostInput.focus(ref)
    | None => ()
  }
};

let focusCostInput = (ref) => {
  let jsObj = ReasonReact.refToJsObj(ref);
  let self = jsObj##self();
  self.ReasonReact.handle(handleFocusCostInput, ())
};

let make = (~value: value, ~onValueChanged, ~onCategoryPickerRequested, ~onSplitFormRequested, ~onSubmitEditingCost=?, _) => {
  let requestSplitForm = ((), { ReasonReact.state }) => {
    let currentParts = switch (value.categorization) {
      | Some(Single(part)) => [|{
        Categorization.categoryId: part.categoryId,
        amount: NumberInput.toNumber(value.totalAmount),
        description: None
      }|]
      | Some(Split(parts)) => parts
      | None => [||]
    };
    onSplitFormRequested(Some(currentParts), ({ categorization, currency, totalAmount }: ExpenseSplitForm.value) => {
      onValueChanged({
        categorization: Some(categorization),
        totalAmount: totalAmount |> NumberInput.toValue,
        currency
      });
      switch (value.categorization, state.costInputRef.contents) {
        | (Some(Single(_)), Some(ref)) => {
          Js.Global.setTimeout(() => {
            value.totalAmount == Empty ? CostInput.focus(ref) : ()
          }, 0) |> ignore
        }
        | _ => ()
      }
    });
  };

  let requestCategoryPicker = ((), { handle, ReasonReact.state }) => {
    Keyboard.dismiss();
    let currCategory = switch value.categorization {
      | Some(Single(part)) => Some(part.categoryId)
      | Some(Split(_)) => None
      | None => None
    };
    onCategoryPickerRequested(~allowSplit=true, ~excludedCategoryIds=[], ~onSplitSelected=Some(() => {
      handle(requestSplitForm, ());
    }), ~onCategorySelected=((optCategoryId) => {
      switch optCategoryId {
        | None => ()
        | Some(categoryId) => {
          onValueChanged({
            ...value,
            categorization: Some(Single({
              categoryId: categoryId
            }))
          })
        }
      };
      switch (value.categorization, state.costInputRef.contents) {
        | (Some(Single(_)), Some(ref)) => {
          Js.Global.setTimeout(() => {
            value.totalAmount == Empty ? CostInput.focus(ref) : ()
          }, 0) |> ignore
        }
        | _ => ()
      }
    }), currCategory, ());
  };

  {
    ...component,
    initialState: () => { costInputRef: ref(None) },
    reducer: (_action: unit, _state) => ReasonReact.NoUpdate,
    render: ({ handle }) => {
      let { categorization, totalAmount, currency } = value;
      <View>
        (switch categorization {
          | Some(Single(part)) => {
            let { ListRow.icon, color: iconColor } = CategoryStuff.getIconParams(part.categoryId);
            let label = Utils.getCategoryPathString(part.categoryId);
            <CategorySelectPicker.PickerRow
              icon
              color=(iconColor |> CategoryStuff.color)
              label
              onPress=handle(requestCategoryPicker)
            />
            /* <ListRow numberOfLines=1>
              <View
                style=Style.(style([borderWidth(2.), borderStyle(`dashed), flexDirection(`row), alignItems(`center), borderRadius(4.), height(38.), marginHorizontal(1.)]))
              />
            </ListRow> */
          }
          | Some(Split(parts)) => {
            <CategorySelectPickerRow onCategoryPickerRequested=handle(requestCategoryPicker)
              selectedCategory=Split(parts)
            />
          }
          | None =>
            <CategorySelectPickerRow onCategoryPickerRequested=handle(requestCategoryPicker)
              selectedCategory=Category(None)
            />

        })
        (switch categorization {
          | None | Some(Single(_)) => {
            <ListRow>
              <CostInput
                amount=totalAmount
                currency
                onAmountChanged=(amount => onValueChanged({
                  categorization,
                  totalAmount: amount,
                  currency
                }))
                onCurrencyChanged=(currency => onValueChanged({
                  categorization,
                  totalAmount,
                  currency
                }))
                ref=handle(setCostInputRef)
                onSubmitEditing=?onSubmitEditingCost
                label="Amount spent"
                returnKeyType="next"
              />
            </ListRow>
          }
          | Some(Split(_)) => {
            switch (totalAmount |> NumberInput.toNumber) {
              | None => ReasonReact.null
              | Some(totalAmount) => {
                <ListRow onPress=Callback(handle(requestSplitForm)) numberOfLines=1>
                  {
                    let formatedTotalCost = Currency.format(
                      ~currency=currency,
                      ~amount=totalAmount
                    );
                    <Text style=Styles.Text.listPrimary value=("Total amount spent: " ++ formatedTotalCost) />
                  }
                </ListRow>
              }
            }
          }
        })
      </View>
    }
  }
}
