open ReactNative;
open RnMui;

let component = ReasonReact.statelessComponent("MonthPickerRow");

let make = (~error=?, ~value, ~onValueChanged, ~label, _) => {
  ...component,
  render: (_) => {
    <ListRow>
      <View style=Style.(style([flexDirection(`row), alignItems(`flexEnd)]))>
        <Text style=Style.(combine(Styles.Text.listPrimary, style([paddingBottom(14.)]))) value=label />
        <View style=Style.(style([width(70.), marginHorizontal(Styles.Spacing.xl)]))>
          <Dropdown label="Year"
            data=[|{ "label": "2018", "value": "2018" }, { "label": "2017", "value": "2017" }, { "label": "2016", "value": "2016" }|]
            value=(DateFns.getYear(value) |> int_of_float |> string_of_int)
            onChangeText=(((selectedYear, _)) => onValueChanged(Js.Date.makeWithYM(~year=float_of_string(selectedYear), ~month=DateFns.getMonth(value), ())))
          />
        </View>
        <View style=Style.(style([width(120.)]))>
          <MonthsDropdown value=DateFns.getMonth(value) onValueChanged=(month => onValueChanged(Js.Date.makeWithYM(~year=DateFns.getYear(value), ~month, ()))) />
        </View>
      </View>
      (switch error {
        | None => ReasonReact.null
        | Some(error) =>
          <Text value=error
            style=Style.(combine(Styles.Text.listPrimary, style([color(Colors.danger)])))
          />
      })
    </ListRow>
  }
}
