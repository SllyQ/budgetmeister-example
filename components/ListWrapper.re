open ReactNative;

let component = ReasonReact.statelessComponent("ListWrapper");

let make = (children) => {
  ...component,
  render: (_) => {
    (ReasonReact.element(View.make(~style=Style.(style([paddingVertical(8.)])), children)))
  }
}
