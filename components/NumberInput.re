open ReactNative;
open RnMui;

type value =
  | Empty
  | Invalid(string)
  | Number(float);

let toValue = (number) => Number(number);
let optToValue = (optNumber) => switch optNumber {
  | Some(number) => toValue(number)
  | None => Empty
};
let toNumber = (value) => switch value {
  | Number(number) => Some(number)
  | _ => None
};

type actions =
  | SetInputRef(Js.Nullable.t(ReasonReact.reactRef))
  | FocusInput
  | UpdateInputValue(string);

type state = {
  inputRef: ref(option(ReasonReact.reactRef)),
  inputValue: string
};

let component = ReasonReact.reducerComponent("NumberInput");

let focus = (ref) => {
  let jsObj = ReasonReact.refToJsObj(ref);
  let self = jsObj##self();
  self.ReasonReact.send(FocusInput)
};

let make =
    (
      ~value,
      ~onChangeValue,
      ~containerStyle=?,
      ~disabled=?,
      ~style=?,
      ~onSubmitEditing=?,
      ~placeholder=?,
      ~label=?,
      ~returnKeyType=?,
      ~prefix=?,
      ~suffix=?,
      ~onlyPositive=true,
      _
    ) => {
  let valToString = (value) =>
    switch value {
    | Empty => ""
    | Invalid(v) => v
    | Number(n) => {
      let v = {j|$(n)|j};
      let split = v |> Js.String.split(".");
      if (split |> Array.length === 2) {
        if (split[1] |> String.length === 1) {
          n |> Js.Float.toFixedWithPrecision(~digits=1)
        } else {
          n |> Js.Float.toFixedWithPrecision(~digits=2)
        }
      } else {
        v
      }
    }
    };
  let stringToVal = (string) =>
    if (string === "") {
      Empty
    } else {
      try (Number(float_of_string(string |> Js.String.replace(",", ".")))) {
      | _ => Invalid(string)
      }
    };
  {
    ...component,
    initialState: () => {inputRef: ref(None), inputValue: valToString(value)},
    willReceiveProps: ({state}) =>
      if (stringToVal(state.inputValue) != value) {
        {...state, inputValue: valToString(value)}
      } else {
        state
      },
    reducer: (action, state) =>
      switch action {
      | SetInputRef(ref) => ReasonReact.SideEffects((_) => {
        state.inputRef := Js.Nullable.toOption(ref)
      })
      | FocusInput =>
        ReasonReact.SideEffects(
          (
            (_) =>
              switch state.inputRef^ {
              | Some(ref) => ReasonReact.refToJsObj(ref)##focus()
              | None => ()
              }
          )
        )
      | UpdateInputValue(inputValue) =>
        ReasonReact.UpdateWithSideEffects(
          {...state, inputValue},
          ((_) => onChangeValue(stringToVal(inputValue)))
        )
      },
    render: ({send, state: {inputValue}}) =>
      <View ?style>
        <TextField
          value=inputValue
          autoCorrect=false
          onChangeText=((text) => send(UpdateInputValue(text)))
          error=?(switch (disabled, value) {
            | (Some(true), _) => None
            /* | (_, Number(x)) when x < 0. && onlyPositive =>
              Some("Only positive values allowed") */
            | (_, Empty) | (_, Number(_)) => None
            | _ => Some("Not a valid number")
          })
          ref=((ref) => send(SetInputRef(ref)))
          ?onSubmitEditing
          keyboardType="numeric"
          ?containerStyle
          ?placeholder
          ?disabled
          ?label
          ?prefix
          ?suffix
          returnKeyType=?(switch (returnKeyType, Platform.os) {
            | (Some(_), IOS) => Some("done") /* iOS only supports "done" for numeric inputs at the moment */
            | (Some(keyType), _) => Some(keyType)
            | (None, _) => None
          })
        />
      </View>
  }
};
