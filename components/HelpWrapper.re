type state = {
  shakeEventListener: ref(option(DeviceShakeStackService.sub))
};

let component = ReasonReact.reducerComponent("HelpWrapper");

let make = (~helpComponent, children) => {
  ...component,
  initialState: () => { shakeEventListener: ref(None) },
  reducer: (_action: unit, _state) => ReasonReact.NoUpdate,
  didMount: ({ state }) => {
    state.shakeEventListener := Some(DeviceShakeStackService.addEventListener(() => helpComponent));
  },
  willUnmount: ({ state }) => {
    switch state.shakeEventListener.contents {
      | Some(sub) => DeviceShakeStackService.removeEventListener(sub)
      | None => ()
    }
  },
  render: (_) => {
    <Fragment>
      ...children
    </Fragment>
  }
}
