open ReactNative;

let component = ReasonReact.statelessComponent("SplitSpendingIcon");

let make = (~parts: array(Categorization.part), ~size=24., _) => {
  ...component,
  render: (_) => {
    let iconPadding = size /. 3.;
    let numParts = Array.length(parts);
    let fullSize = size +. iconPadding;
    let iconSize = size /. 2.;
    let sortedParts = parts |> Common.iSort((p1, p2) => {
      compare(p1.Categorization.amount, p2.amount) * -1
    });
    switch (sortedParts |> Array.to_list) {
      | [p1, p2] => {
        let { ListRow.color: c1, icon: i1 } = CategoryStuff.getIconParams(p1.categoryId);
        let { ListRow.color: c2, icon: i2 } = CategoryStuff.getIconParams(p2.categoryId);
        <View style=Style.(style([width(fullSize), height(fullSize)]))>
          <View style=Style.(style([width(fullSize), height(fullSize /. 2.), flexDirection(`row), borderTopLeftRadius(4.), borderTopRightRadius(4.), backgroundColor(c1 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
            <CategoryIcon icon=i1 size=iconSize />
          </View>
          <View style=Style.(style([width(fullSize), height(fullSize /. 2.), flexDirection(`row), borderBottomLeftRadius(4.), borderBottomRightRadius(4.), backgroundColor(c2 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
            <CategoryIcon icon=i2 size=iconSize />
          </View>
        </View>
      }
      | [p1, p2, p3] => {
        let { ListRow.color: c1, icon: i1 } = CategoryStuff.getIconParams(p1.categoryId);
        let { ListRow.color: c2, icon: i2 } = CategoryStuff.getIconParams(p2.categoryId);
        let { ListRow.color: c3, icon: i3 } = CategoryStuff.getIconParams(p3.categoryId);
        <View style=Style.(style([width(fullSize), height(fullSize)]))>
          <View style=Style.(style([width(fullSize), height(fullSize /. 2.), flexDirection(`row), borderTopRightRadius(4.), borderTopLeftRadius(4.), backgroundColor(c1 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
            <CategoryIcon icon=i1 size=iconSize />
          </View>
          <View style=Style.(style([width(fullSize), height(fullSize /. 2.), flexDirection(`row)]))>
            <View style=Style.(style([width(fullSize /. 2.), height(fullSize /. 2.), borderBottomLeftRadius(4.), backgroundColor(c2 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
              <CategoryIcon icon=i2 size=iconSize />
            </View>
            <View style=Style.(style([width(fullSize /. 2.), height(fullSize /. 2.), borderBottomRightRadius(4.), backgroundColor(c3 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
              <CategoryIcon icon=i3 size=iconSize />
            </View>
          </View>
        </View>
      }
      | [p1, p2, p3, p4, ..._] => {
        let { ListRow.color: c1, icon: i1 } = CategoryStuff.getIconParams(p1.categoryId);
        let { ListRow.color: c2, icon: i2 } = CategoryStuff.getIconParams(p2.categoryId);
        let { ListRow.color: c3, icon: i3 } = CategoryStuff.getIconParams(p3.categoryId);
        let { ListRow.color: c4, icon: i4 } = CategoryStuff.getIconParams(p4.categoryId);
        <View style=Style.(style([width(fullSize), height(fullSize)]))>
          <View style=Style.(style([width(fullSize), height(fullSize /. 2.), flexDirection(`row)]))>
            <View style=Style.(style([width(fullSize /. 2.), height(fullSize /. 2.), borderTopLeftRadius(4.), backgroundColor(c1 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
              <CategoryIcon icon=i1 size=iconSize />
            </View>
            <View style=Style.(style([width(fullSize /. 2.), height(fullSize /. 2.), borderTopRightRadius(4.), backgroundColor(c2 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
              <CategoryIcon icon=i2 size=iconSize />
            </View>
          </View>
          <View style=Style.(style([width(fullSize), height(fullSize /. 2.), flexDirection(`row)]))>
            <View style=Style.(style([width(fullSize /. 2.), height(fullSize /. 2.), borderBottomLeftRadius(4.), backgroundColor(c3 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
              <CategoryIcon icon=i3 size=iconSize />
            </View>
            <View style=Style.(style([width(fullSize /. 2.), height(fullSize /. 2.), borderBottomRightRadius(4.), backgroundColor(c4 |> CategoryStuff.color), alignItems(`center), justifyContent(`center)]))>
              <CategoryIcon icon=i4 size=iconSize />
            </View>
          </View>
        </View>
      }
      | [] | [_] => {
        <LeftIcon size color=("#0091EA" |> CategoryColor.fromString) icon=Letter("S") />
      }
    };
    /* <View
      style=Style.(
              style([
                flexDirection(`row),
                alignItems(`center),
                justifyContent(`center),
                backgroundColor(
                  iconColor |> Js.Option.getWithDefault("#616161")
                ),
                width(fullSize +. iconPadding),
                height(fullSize +. iconPadding),
                borderRadius(4.)
              ])
            )>
      (
        switch icon {
        | Some(Icon(icon)) =>
          <RnIcon name=icon color="white" fullSize=(int_of_float(fullSize)) />
        | Some(Letter(letter)) =>
          <Text
            value=letter
            style=Style.(
                    style([
                      fontSize(fullSize),
                      color(Colors.white),
                      fontFamily("Roboto-Medium"),
                      paddingBottom(1.)
                    ])
                  )
          />
        | None => ReasonReact.null
        }
      )
    </View>; */
  }
}
