open ReactNative;

module Action = {
  let component = ReasonReact.statelessComponent("Action");

  let make = (~icon=?, ~text, ~onPress, _) => {
    ...component,
    render: (_) => {
      <Touchable onPress>
        <View style=Style.(style([flexDirection(`row), alignItems(`center), height(48.)]))>
          <View style=Style.(style([paddingHorizontal(16.), width(16. *. 2. +. 24.)]))>
            (switch icon {
              | None => ReasonReact.null
              | Some(icon) => <RnIcon name=icon color=Colors.grey />
            })
          </View>
          <View style=Style.(style([paddingHorizontal(16.)]))>
            <Text style=Styles.Text.listPrimary value=text numberOfLines=1 />
          </View>
        </View>
      </Touchable>
    }
  }
};

let component = ReasonReact.statelessComponent("ActionSheet");

let make = (~isVisible, ~onRequestClose, children) => {
  ...component,
  render: (_) => {
    <RnModal isVisible
      style=Style.(style([justifyContent(`flexEnd), margin(0.)]))
      onBackdropPress=onRequestClose onBackButtonPress=onRequestClose
    >
      <View style=Style.(style([paddingVertical(Styles.Spacing.m), backgroundColor("#fff")]))>
        ...children
      </View>
    </RnModal>
    /* <ModalBox isOpen=isVisible
    >
      <View style=Style.(style([paddingVertical(Styles.Spacing.m), backgroundColor("#fff")]))>
        ...children
      </View>
    </ModalBox> */
  }
}
