open ReactNative;

let iconOptions = [|
  "book",
  "book-open-variant",
  "dice-5",
  "heart",
  "star",
  "home",
  "cart",
  "domain",
  "email-open",
  "currency-usd",
  "currency-eur",
  "currency-gbp",
  "currency-btc",
  "currency-eth",
  "beach",
  "baby-buggy",
  "beer",
  "calendar",
  "cake-variant",
  "cannabis",
  "car",
  "cards",
  "cash",
  "coffee",
  "compass",
  "creation",
  "delete-empty",
  "dumbbell",
  "feather",
  "ferry",
  "gift",
  "gavel",
  "glass-tulip",
  "keyboard",
  "leaf",
  "pine-tree",
  "ruler",
  "tshirt-crew",
  "umbrella",
  "gas-station",
  "stocking",
  "snowflake",
  "paw",
  "food",
  "food-apple",
  "food-croissant",
  "food-fork-drink",
  "wrench",
  "smoking",
  "brush",
  "flower",
  "face"
|];

type state = {
  selectModalVisible: bool
};

type actions =
  | ShowSelectModal
  | HideSelectModal;

let component = ReasonReact.reducerComponent("IconSelectRow");

let make = (~selectedIcon, ~onIconSelected, ~noneText=?, _) => {
  ...component,
  initialState: () => { selectModalVisible: false },
  reducer: (action, _state) => switch action {
    | ShowSelectModal => ReasonReact.Update { selectModalVisible: true }
    | HideSelectModal => ReasonReact.Update { selectModalVisible: false }
  },
  render: ({ send, state: { selectModalVisible } }) =>
    <View>
      <ListRow numberOfLines=1 flexRow=true onPress=Callback(() => send(ShowSelectModal))>
        <ListPrimary>
          <RowTitle value="Icon: " />
          (
            switch selectedIcon {
            | None => <Text
              value=("None" ++ switch (noneText) { | None => "" | Some(text) => " (" ++ text ++ ")"})
            />
            | Some(i) => <RnIcon name=i />
            }
          )
        </ListPrimary>
      </ListRow>
      <RnModal isVisible=selectModalVisible onBackdropPress=(() => send(HideSelectModal)) onBackButtonPress=(() => send(HideSelectModal))>
        <View style=Style.(style([backgroundColor("#fff")]))>
          <ScrollView>
            <View style=Style.(style([flexDirection(`row), flexWrap(`wrap), justifyContent(`center), padding(Styles.Spacing.xl)]))>
              <Touchable onPress=(() => {
                onIconSelected(None);
                send(HideSelectModal);
              })>
                <View
                  style=Style.(style([width(68.), height(30.), margin(Styles.Spacing.m), alignItems(`center), justifyContent(`center)]))
                >
                  <Text style=Styles.Text.listPrimary
                    value="No icon"
                  />
                </View>
              </Touchable>
              (iconOptions |> Array.map(icon => {
                <Touchable key=icon
                  onPress=(() => {
                    onIconSelected(Some(icon));
                    send(HideSelectModal);
                  })
                  style=Style.(style([padding(Styles.Spacing.m)]))
                >
                  <RnIcon name=icon size=30. />
                </Touchable>
              })
              |> ReasonReact.array)
            </View>
            <View style=Style.(style([alignSelf(`flexEnd), padding(12.)]))>
              <RnMui.MuiButton primary=true text="Cancel" onPress=(() => send(HideSelectModal)) />
            </View>
          </ScrollView>
        </View>
      </RnModal>
    </View>
};
