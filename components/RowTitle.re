open ReactNative;
open Style;

let component = ReasonReact.statelessComponent("ListPrimary");

let make = (~value=?, ~style as s=style([]), children) => {
  ...component,
  render: (_) => {
    <Connect_SelectedTheme render=(theme => {
      <Text style=combine(style([Styles.Text.robotoMedium, color(theme.primary)]), s) ?value>
        ...children
      </Text>
    }) />
  }
}
