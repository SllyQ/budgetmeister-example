open ReactNative;

let component = ReasonReact.statelessComponent("ScreenActivityIndicator");

let make = (_) => {
  ...component,
  render: (_) => {
    <Connect_SelectedTheme render=(theme => {
      <View style=Style.(style([flex(1.)]))>
        <ActivityIndicator color=theme.primary style=Style.(style([marginTop(40.)])) size=(Platform.equals(IOS) ? `large : `exact(40)) />
      </View>
    }) />
  }
}
