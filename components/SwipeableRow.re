open ReactNative;

open RnMui;

let styles =
  StyleSheet.create(
    Style.(
      {
        "editIconWrap":
          style([
            width(50.),
            alignItems(`center),
            justifyContent(`center),
            backgroundColor(Colors.theme^.primary)
          ]),
        "deleteIconWrap":
          style([
            width(50.),
            alignItems(`center),
            justifyContent(`center),
            backgroundColor(Colors.danger)
          ])
      }
    )
  );

type state = {
  swipeEnabled: bool,
  initializeTimeoutId: ref(option(Js.Global.timeoutId)),
  showActionSheet: bool
};

type actions =
  | ShowActionSheet
  | HideActionSheet
  | Edit
  | Delete;

let component = ReasonReact.reducerComponent("Swipeable_Row");

let make = (~onEdit, ~onDelete, ~onSwipeableOpened=?, children) => {
  ...component,
  initialState: () => {swipeEnabled: false, initializeTimeoutId: ref(None), showActionSheet: false},
  reducer: (action, state) =>
    switch action {
    | Edit =>
      ReasonReact.Update({
        ...state,
        showActionSheet: false
      })
    | Delete =>
      ReasonReact.UpdateWithSideEffects({
        ...state,
        showActionSheet: false
      }, (_) => onDelete())
    | ShowActionSheet => ReasonReact.Update {...state, showActionSheet: true}
    | HideActionSheet => ReasonReact.Update {...state, showActionSheet: false}
    },
  render: ({send, state: { showActionSheet }}) => {
    <View>
      (
        ReasonReact.element(
          Swipeable.make(
            <Touchable onPress=(() => send(ShowActionSheet))>
              (ReasonReact.element(View.make(children)))
            </Touchable>,
            ~rightButtonContainerStyle=Style.(style([flexDirection(`row)])),
            ~rightButtonWidth=100.,
            ~rightButtons=[|
              (
                <Touchable
                  key="edit" style=styles##editIconWrap onPress=((_) => send(Edit))>
                  <Icon name="edit" color="white" />
                </Touchable>,
                <Touchable key="delete" style=styles##deleteIconWrap onPress=onDelete>
                  <Icon name="delete" color="white" />
                </Touchable>
              )
            |]
          )
        )
      )
      <RnModal isVisible=showActionSheet
        style=Style.(style([justifyContent(`flexEnd), margin(0.)]))
        onBackdropPress=(() => send(HideActionSheet))
        onBackButtonPress=(() => send(HideActionSheet))
      >
        <View style=Style.(style([paddingVertical(Styles.Spacing.m), backgroundColor("#fff")]))>
          <Touchable onPress=(() => send(Edit))>
            <View style=Style.(style([flexDirection(`row), alignItems(`center), height(48.)]))>
              <View style=Style.(style([paddingHorizontal(16.)]))>
                <Icon name="edit" color=Colors.grey />
              </View>
              <View style=Style.(style([paddingHorizontal(16.)]))>
                <Text style=Styles.Text.listPrimary value="Edit" />
              </View>
            </View>
          </Touchable>
          <Touchable onPress=(() => send(Delete))>
            <View style=Style.(style([flexDirection(`row), alignItems(`center), height(48.)]))>
              <View style=Style.(style([paddingHorizontal(16.)]))>
                <Icon name="delete" color=Colors.grey />
              </View>
              <View style=Style.(style([paddingHorizontal(16.)]))>
                <Text style=Styles.Text.listPrimary value="Delete" />
              </View>
            </View>
          </Touchable>
        </View>
      </RnModal>
    </View>
  }
};
