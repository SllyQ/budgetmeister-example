open ReactNative;

let getColorOptions = () => {
  CategoryColor.([|
    Red,
    Pink,
    Purple,
    DeepPurple,
    Indigo,
    Blue,
    LightBlue,
    Cyan,
    Teal,
    Green,
    LightGreen,
    Lime,
    Amber,
    Orange,
    DeepOrange,
    Brown,
    Grey,
    BlueGrey
    /* red600,
    pink600,
    purple600,
    deepPurple600,
    indigo600,
    blue600,
    lightBlue600,
    cyan600,
    teal600,
    green600,
    lightGreen600,
    lime600,
    amber600,
    orange600,
    deepOrange600,
    brown600,
    grey600,
    blueGrey600, */
    /* red200,
    pink200,
    purple200,
    deepPurple200,
    indigo200,
    blue200,
    lightBlue200,
    cyan200,
    teal200,
    green200,
    lightGreen200,
    lime200,
    amber200,
    orange200,
    deepOrange200 */
  |])
};

type state = {
  selectModalVisible: bool
};

type actions =
  | ShowSelectModal
  | HideSelectModal;

let component = ReasonReact.reducerComponent("ColorSelect");

let colorSelectItemStyle = Style.(style([
  minHeight(24.),
  minWidth(84.),
  alignItems(`center),
  justifyContent(`center),
  padding(Styles.Spacing.m)
]));

let make = (~selectedColor: option(CategoryColor.t), ~onColorSelected, ~error=?, _) => {
  ...component,
  initialState: () => { selectModalVisible: false },
  reducer: (action, _state) => switch action {
    | ShowSelectModal => ReasonReact.Update { selectModalVisible: true }
    | HideSelectModal => ReasonReact.Update { selectModalVisible: false }
  },
  render: ({ send, state: { selectModalVisible } }) =>
    <View>
      <ListRow numberOfLines=1 onPress=Callback(() => send(ShowSelectModal))>
        <View style=Style.(style([flexDirection(`row), alignItems(`center)]))>
        (switch selectedColor {
        | None => {
          <ListPrimary>
            <RowTitle value="Color*: " />
            <Text value="None" style=Styles.Text.listPrimary />
          </ListPrimary>
        }
        | Some(c) => {
          <Row alignItems=`center>
            <ListPrimary>
              <RowTitle value="Color*: " />
            </ListPrimary>
            <View style=Style.(style([marginLeft(8.)]))>
              <ColorBar color=(c |> CategoryStuff.color) height=24. />
            </View>
          </Row>
        }
        })
        </View>
        (switch error {
          | None => ReasonReact.null
          | Some(err) => {
            <Text value=err style=Style.(combine(Styles.Text.listSecondary, style([color(Colors.danger)]))) />
          }
        })
      </ListRow>
      <RnModal isVisible=selectModalVisible onBackdropPress=(() => send(HideSelectModal)) onBackButtonPress=(() => send(HideSelectModal))>
        <View style=Style.(style([backgroundColor("#fff")]))>
          <ScrollView>
            <View style=Style.(style([flexDirection(`row), flexWrap(`wrap), padding(Styles.Spacing.l), paddingVertical(Styles.Spacing.m), alignItems(`center), justifyContent(`flexStart)]))>
              <Touchable onPress=(() => onColorSelected(None))>
                <View style=colorSelectItemStyle>
                  <Text value="None" style=Styles.Text.listPrimary />
                </View>
              </Touchable>
              (getColorOptions() |> Array.map(color => {
                <Touchable key=(color |> CategoryStuff.color) onPress=(() => {
                  onColorSelected(Some(color));
                  send(HideSelectModal);
                })>
                  <View style=colorSelectItemStyle>
                    <ColorBar color=(color |> CategoryStuff.color) height=26. />
                  </View>
                </Touchable>
              })
              |> ReasonReact.array)
            </View>
            <View style=Style.(style([alignSelf(`flexEnd), padding(12.)]))>
              <RnMui.MuiButton primary=true text="Cancel" onPress=(() => send(HideSelectModal)) />
            </View>
          </ScrollView>
        </View>
      </RnModal>
    </View>
};
