open ReactNative;
open RnMui;

type state = {
  email: string,
  feedback: string,
  emailRef: ref(option(ReasonReact.reactRef)),
  feedbackRef: ref(option(ReasonReact.reactRef))
};

type actions =
  | UpdateEmail(string)
  | UpdateFeedback(string)
  | SendFeedback;

let component = ReasonReact.reducerComponent("Feedback");

let make = (~onFeedbackSubmitted, _) => {
  ...component,
  initialState: () => {
    email: AppAuthService.user^ |> C.Option.map(user => user.User.email) |> Js.Option.getWithDefault(""),
    feedback: "",
    emailRef: ref(None),
    feedbackRef: ref(None)
  },
  reducer: C.flip(state => fun
    | UpdateEmail(email) => ReasonReact.Update {
      ...state,
      email
    }
    | UpdateFeedback(feedback) => ReasonReact.Update {
      ...state,
      feedback
    }
    | SendFeedback => ReasonReact.SideEffects((_) => {
      FeedbackService.dispatch(SendFeedback(state.email, state.feedback));
      SnackbarService.show("Your feedback has been sent, thank you");
      onFeedbackSubmitted()
    })
  ),
  render: ({ state, send }) => {
    let { email, feedback } = state;
    <ScreenWrap fab=(
      <ThemeActionButton icon="send" onPress=(() => send(SendFeedback)) />
    )>
      <ListWrapper>
        <ListRow>
          <TextField label="Your email"  value=email autoCorrect=false autoCapitalize="none"
            onChangeText=(newEmail => send(UpdateEmail(newEmail)))
            returnKeyType="next"
          />
        </ListRow>
        <View style=Style.(style([flex(1.)]))>
          <ListRow>
            <TextField label="Feedback" multiline=true autoCorrect=true autoCapitalize="sentences" value=feedback
              onChangeText=(newFeedback => send(UpdateFeedback(newFeedback)))
            />
          </ListRow>
        </View>
      </ListWrapper>
    </ScreenWrap>
  }
};

let jsComponent =
  ReasonReact.wrapReasonForJs(
    ~component,
    (jsProps) => make(~onFeedbackSubmitted=jsProps##onFeedbackSubmitted, [||])
  );
