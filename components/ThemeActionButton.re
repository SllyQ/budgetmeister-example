open RnMui;

let component = ReasonReact.statelessComponent("ThemeActionButton");

let make = (~icon=?, ~onPress, _) => {
  ...component,
  render: (_) => {
    <Connect_SelectedTheme render=(theme => {
      <ActionButton
        ?icon
        onPress
        style={
          "container": {
            "backgroundColor": theme.maybeAccent
          }
        }
      />
    }) />
  }
}
