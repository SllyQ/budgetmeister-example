open ReactNative;

type quote = {
  text: string,
  author: string
};

let quotes = [|{
  text: {j|Many people take no care of their money till they come nearly to the end of it, and others do just the same with their time.|j},
  author: "Johann Wolfgang von Goethe"
}, {
  text: {j|A penny saved is a penny earned.|j},
  author: "Benjamin Franklin"
}, {
  text: {j|It’s not how much money you make, but how much money you keep, how hard it works for you, and how many generations you keep it for.|j},
  author: "Robert Kiyosaki"
}, {
  text: {j|The way to build your savings is by spending less each month.|j},
  author: "Suze Orman"
}, {
  text: {j|Too many people spend money they earned to buy things they don't want to impress people that they don't like.|j},
  author: "Will Rogers"
}, {
  text: {j|Annual income twenty pounds, annual expenditure nineteen six, result happiness. Annual income twenty pounds, annual expenditure twenty pound ought and six, result misery.|j},
  author: "Charles Dickens"
}, {
  text: {j|Financial peace isn't the acquisition of stuff. It's learning to live on less than you make, so you can give money back and have money to invest. You can't win until you do this.|j},
  author: "Dave Ramsey"
}, {
  text: {j|It is not the man who has too little, but the man who craves more, that is poor.|j},
  author: "Seneca"
}, {
  text: {j|You must gain control over your money or the lack of it will forever control you.|j},
  author: "Dave Ramsey"
}, {
  text: {j|The habit of saving is itself an education; it fosters every virtue, teaches self-denial, cultivates the sense of order, trains to forethought, and so broadens the mind.|j},
  author: "T.T. Munger"
}, {
  text: {j|If we command our wealth, we shall be rich and free. If our wealth commands us, we are poor indeed.|j},
  author: "Edmund Burke"
}, {
  text: {j|It's not your salary that makes you rich, it's your spending habits.|j},
  author: "Charles A. Jaffe"
}, {
  text: {j|Do not save what is left after spending, but spend what is left after saving.|j},
  author: "Warren Buffett"
}, {
  text: {j|You've got to tell your money what to do or it will leave.|j},
  author: "Dave Ramsey"
}, {
  text: {j|It’s only when the tide goes out that you discover who’s been swimming naked|j},
  author: "Warren Buffett"
}, {
  text: {j|A simple fact that is hard to learn is that the time to save money is when you have some.|j},
  author: "Joe Moore"
}, {
  text: {j|A goal without a plan is just a wish.|j},
  author: {j|Antoine de Saint Exupéry|j}
}, {
  text: {j|If you don't get serious about your money, you will never have serious money.|j},
  author: "Grant Cardone"
}, {
  text: {j|A budget is telling your money where to go instead of wondering where it went.|j},
  author: "John Maxwell"
}, {
  text: {j|Beware of little expenses. A small leak will sink a great ship.|j},
  author: "Benjamin Franklin"
}, {
  text: {j|If you would be wealthy, think of saving as well as getting.|j},
  author: "Benjamin Franklin"
}, {
  text: {j|Money is multiplied in practical value depending on the number of W’s you control in your life: what you do, when you do it, where you do it, and with whom you do it.|j},
  author: "Timothy Ferriss"
}, {
  text: {j|The art of living easily as to money is to pitch your scale of living one degree below your means.|j},
  author: "Sir Henry Taylor"
}|];

let component = ReasonReact.statelessComponent("Drawer");

let make = (~activeItemKey, ~onItemPress, ~items as _items, _) => {
  ...component,
  render: (_) => {
    let currDay = DateFns.differenceInDays(Js.Date.make(), Js.Date.fromFloat(0.)) |> int_of_float;
    let quote = quotes[(currDay + 1) mod Array.length(quotes)];
    let quoteText = quote.text;
    let quoteAuthor = "- " ++ quote.author;
    let goToRoute = (routeName) => {
      Mixpanel.trackWithProperties("Drawer route", {
        "route": routeName
      });
      onItemPress({
        "focused": routeName === activeItemKey,
        "route": {"key": routeName, "routeName": routeName}
      });
    };
    <Connect_SelectedTheme render=((_) => {
      <View style=Style.(style([flex(1.)]))>
        /* Height here must be 9/16 of drawer width (to respect proportions) */
        <View style=Style.(style([height(187.5), widthPct(100.)]))>
          <View
            style=Style.(
                    style([
                      flex(1.),
                      justifyContent(`center),
                      paddingHorizontal(20.),
                      backgroundColor(Colors.theme^.primary)
                    ])
                  )>
            <BodyText
              style=Style.(style([color("white"), fontFamily("Kalam-Light"), fontSize(15.)]))
              value={j|“$(quoteText)”|j}
            />
            <BodyText
              style=Style.(style([color("white"), opacity(0.7), fontFamily("Kalam-Light"), textAlign(`right), paddingTop(4.), paddingRight(4.), fontSize(15.)]))
              value=quoteAuthor
            />
          </View>
        </View>
        <ScrollView contentContainerStyle=Style.(style([paddingTop(Styles.Spacing.s)]))>
          <DrawerItem
            activeItemKey
            route="Home"
            icon="home"
            onPress=((_) => goToRoute("Home"))
          />
          <DrawerItem
            activeItemKey
            route="Charts"
            icon="chart-arc"
            onPress=((_) => goToRoute("Charts"))
          />
          /* <DrawerItem
            activeItemKey
            route="Summary"
            icon="clipboard-text"
            onPress=((_) => goToRoute("Summary"))
          /> */
          <DrawerItem
            activeItemKey
            route="Budget"
            icon="calculator"
            onPress=((_) => goToRoute("Budget"))
          />
          <DrawerItem
            activeItemKey
            route="Categories"
            label="Categories & Tags"
            icon="folder"
            onPress=((_) => goToRoute("Categories"))
          />
          /* <DrawerItem activeItemKey route="Tags" icon="tag" onPress=((_) => goToRoute("Tags")) /> */
          /* <DrawerItem
            activeItemKey
            route="Templates"
            icon="file-document-box"
            onPress=((_) => goToRoute("Templates"))
          /> */
          <DrawerItem
            activeItemKey
            route="RecurringExpenses"
            label="Recurring Expenses"
            icon="repeat"
            onPress=((_) => goToRoute("RecurringExpenses"))
          />
          <C_Alpha render=(() => {
            <C_Settings render=(settings => {
              let enabled = settings.tripMode == TripMode.empty ? false : settings.tripModeActive;
              <Touchable style=(DrawerItem.styles##itemWrapper) onPress=((_) => goToRoute("TripMode"))>
                <Row flex=1. alignItems=`center justifyContent=`spaceBetween>
                  <Row>
                    <RnIcon name="airplane-takeoff" color=(Colors.grey) />
                    <Text value="Travel Mode" style=(DrawerItem.styles##itemText) />
                  </Row>
                  <Switch style=Style.(style([marginRight(Styles.Spacing.m)]))
                    value=enabled
                    onValueChange=(tripModeActive => {
                      if (!enabled && settings.tripMode == TripMode.empty) {
                        goToRoute("TripMode")
                      } else {
                        SettingsDb.setItem({
                          ...settings,
                          tripModeActive
                        })
                      }
                    })
                  />
                </Row>
              </Touchable>
            }) />
          }) />
          <C_Alpha render=(() => {
            <DrawerItem
              activeItemKey
              route="SharedExpenses"
              icon="account-multiple"
              onPress=((_) => goToRoute("SharedExpenses"))
            />
          }) />
          <C_Alpha render=(() => {
            <DrawerItem
              activeItemKey
              route="Reminders"
              icon="clock"
              onPress=((_) => goToRoute("Reminders"))
            />
          }) />
          <DrawerItem
            activeItemKey
            route="Settings"
            icon="settings"
            onPress=((_) => goToRoute("Settings"))
          />
          <DrawerItem
            activeItemKey
            route="Account"
            icon="account"
            onPress=((_) => goToRoute("Account"))
          />
          <Touchable
            style=DrawerItem.styles##itemWrapper
            onPress=(() => goToRoute("Feedback"))
          >
            <View style=DrawerItem.styles##drawerItemRow>
              <RnIcon name="send" color=Colors.grey />
              <Text value="Send feedback" style=DrawerItem.styles##itemText />
            </View>
          </Touchable>
          <View style=Style.(style([flex(1.)])) />
          <C_Alpha render=(() => {
            <TouchableWithoutFeedback onPress=((_) => goToRoute("DevLog"))>
              <View style=Style.(style([widthPct(100.), height(25.)])) />
            </TouchableWithoutFeedback>
          }) />
        </ScrollView>
      </View>
    }) />
  }
};

let jsComponent =
  ReasonReact.wrapReasonForJs(
    ~component,
    (jsProps) =>
      make(
        ~activeItemKey=jsProps##activeItemKey,
        ~onItemPress=jsProps##onItemPress,
        ~items=jsProps##items,
        [||]
      )
  );
