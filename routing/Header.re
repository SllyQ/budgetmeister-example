open RnMui;
open ReactNative;

type state = {
  notificationsOpen: bool,
  notificationIconRef: ref(option(ReasonReact.reactRef))
};

type actions =
  | Sync
  | OpenNotifications
  | CloseNotifications
  | SetNotificationIconRef(Js.Nullable.t(ReasonReact.reactRef));

let component = ReasonReact.reducerComponent("Header");

let make = (~headerProps, ~alwaysShowBack=false, ~noElements=false, _) => {
  ...component,
  initialState: () => { notificationsOpen: false, notificationIconRef: ref(None) },
  reducer: (action, state) =>
    switch action {
    | Sync => ReasonReact.SideEffects((_) => {
      if (AppAuthService.isAunthenticated()) {
        SyncService.forceSyncPartial()
        |> Js.Promise.then_(
             (_) => {
               Mixpanel.track("Sync success");
               SnackbarService.show("Successully synchronized");
               Js.Promise.resolve()
             }
           )
        |> Js.Promise.catch(
             (_) => {
               Mixpanel.track("Sync error");
               ReactNative.Alert.alert(~title="Error", ~message="Synchronization failed for unknown reason, please check your internet connection and try again.", ());
               Js.Promise.resolve()
             }
           )
        |> ignore
      } else {
        Mixpanel.track("Must authenticate to sync alert");
        ReactNative.Alert.alert(~title="Synchronization not available", ~message="To synchronize your data you first need log in or register, you can do that in account section in the side menu.", ())
      }
    })
    | OpenNotifications => ReasonReact.SideEffects((_) => {
      DeviceShakeStackService.callListeners();
      switch (state.notificationIconRef^) {
        | None => ()
        | Some(ref) => ReasonReact.refToJsObj(ref)##swing(800)
      }
    })
    /* | OpenNotifications => ReasonReact.Update {
      notificationsOpen: true
    } */
    | CloseNotifications => ReasonReact.Update {
      ...state,
      notificationsOpen: false
    }
    | SetNotificationIconRef(nullableRef) => ReasonReact.SideEffects((_) => {
      state.notificationIconRef := nullableRef |> Js.Nullable.toOption
    })
    },
  render: ({send, state: { notificationsOpen }}) => {
    let isChildScene = headerProps##scene##index > 0;
    let details = headerProps##getScreenDetails(headerProps##scene);
    let title = details##options##title;
    <View>
      <C_Settings render=(settings => {
        <Connect_SelectedTheme
          render=(theme => {
            <RnMui.ThemeProvider
              uiTheme={
                "palette": {
                  "primaryColor": theme.primary,
                  "accentColor": theme.accent,
                  "primaryTextColor": Colors.black,
                  "secondaryTextColor": Colors.grey,
                  "alternateTextColor": Colors.white,
                  "canvasColor": Colors.white
                },
                "bottomNavigationAction": {
                  "icon": {
                    "color": Colors.lightWhite
                  },
                  "label": {
                    "color": Colors.lightWhite
                  },
                  "iconActive": {
                    "color": Colors.white
                  },
                  "labelActive": {
                    "color": Colors.white,
                    "fontSize": 14
                  }
                }
              }
            >
              <Toolbar
                style=?(settings.lightBars ? Some({
                  "container": {
                    "backgroundColor": "white"
                  },
                  "leftElement": {
                    "color": Colors.grey
                  },
                  "titleText": {
                    "color": Colors.black
                  }
                }) : None)
                leftElement=?(noElements ? None : Some(alwaysShowBack || isChildScene ? "arrow-back" : "menu"))
                onLeftElementPress=(alwaysShowBack || isChildScene ? () => headerProps##navigation##goBack(Js.null) : () => {
                  /* Sometimes due to interactions with swipe it might think it's open when it's closed,
                    so need to close it first so the librarie's state is correct */
                  Obj.magic(headerProps##navigation)##navigate("DrawerClose", Js.null) |> ignore;
                  Js.Global.setTimeout(() => {
                    Obj.magic(headerProps##navigation)##navigate("DrawerOpen", Js.null)
                  }, 0) |> ignore
                })
                centerElement=title
                rightElement=?(noElements ? None : Some(
                  <View style=Style.(style([flexDirection(`row)]))>
                    /* <C_Alpha render=(() => {
                      <Touchable onPress=(() => send(OpenNotifications))
                        style=ReactNative.Style.(style([
                          width(48.),
                          height(48.),
                          borderRadius(24.),
                          alignItems(`center),
                          justifyContent(`center)
                        ]))
                      >
                        <Badge text="1" style={
                          "container": {
                            "backgroundColor": theme.accent,
                            "top": -6,
                            "right": -6,
                            "paddingBottom": 1,
                            "paddingRight": 1
                          }
                        }>
                          <Animatable.View ref=(ref => send(SetNotificationIconRef(ref)))>
                            <RnIcon name="bell" color=(settings.lightBars ? Colors.grey : "white") />
                          </Animatable.View>
                        </Badge>
                      </Touchable>
                    }) /> */
                    <Touchable onPress=(() => send(Sync))
                      style=ReactNative.Style.(style([
                        width(48.),
                        height(48.),
                        borderRadius(24.),
                        alignItems(`center),
                        justifyContent(`center)
                      ]))
                    >
                      <SyncService.Connect render=(syncState => {
                        (switch syncState.status {
                          | Unsynced => {
                            <Badge size=12 style={
                              "container": {
                                "backgroundColor": Colors.danger,
                                "top": -2,
                                "right": -2
                              }
                            } icon={
                              "name": "close",
                              "color": Colors.white
                            }>
                              <RnIcon name="sync" color=(settings.lightBars ? Colors.grey : "white") />
                            </Badge>
                          }
                          | Syncing => {
                            <Animatable.View animation="rotate" duration=1600 iterationCount=`Infinite easing="linear" direction="reverse">
                              <RnIcon name="sync" color=(settings.lightBars ? Colors.grey : "white") />
                            </Animatable.View>
                          }
                          | Synced => {
                            /* <Badge size=14 style={
                              "container": {
                                "backgroundColor": Colors.success,
                                "top": -4,
                                "right": -4
                              }
                            } icon={
                              "name": "check",
                              "color": Colors.white
                            }> */
                              <RnIcon name="sync" color=(settings.lightBars ? Colors.grey : "white") />
                            /* </Badge> */
                          }
                        })
                      }) />
                    </Touchable>
                  </View>
                ))
              />
            </RnMui.ThemeProvider>
          }
        ) />
      }) />
      <Modal visible=notificationsOpen onRequestClose=(() => send(CloseNotifications)) transparent=true>
        <Notifications onRequestClose=(() => send(CloseNotifications)) />
      </Modal>
    </View>
  }
};
