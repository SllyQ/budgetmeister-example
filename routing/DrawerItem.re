open ReactNative;

let styles =
  StyleSheet.create(
    Style.(
      {
        "activeItemWrapper":
          style([
            widthPct(100.),
            height(50.),
            paddingLeft(16.),
            backgroundColor(Colors.lightGrey)
          ]),
        "itemWrapper":
          style([
            widthPct(100.),
            height(50.),
            paddingLeft(16.)
          ]),
        "activeItemText":
          combine(
            Styles.Text.body1,
            style([fontFamily("Roboto-Medium"), paddingLeft(16.), paddingBottom(1.5), color(Colors.theme^.primary)])
          ),
        "itemText":
          combine(
            Styles.Text.body1,
            style([fontFamily("Roboto-Medium"), paddingLeft(16.), paddingBottom(1.5)])
          ),
        "drawerItemRow": style([
          flex(1.),
          flexDirection(`row),
          alignItems(`center),
        ])
      }
    )
  );

let component = ReasonReact.statelessComponent("DrawerItem");

let make = (~activeItemKey, ~route, ~label=route, ~icon, ~onPress, _) => {
  ...component,
  render: (_) => {
    let isActive = activeItemKey === route;
    <Touchable style=(isActive ? styles##activeItemWrapper : styles##itemWrapper) onPress>
      <View style=styles##drawerItemRow>
        <RnIcon name=icon color=(isActive ? Colors.theme^.primary : Colors.grey) />
        <Text value=label style=(isActive ? styles##activeItemText : styles##itemText) />
      </View>
    </Touchable>
  }
};
