type budgetCategoryFilter =
  | All
  | Category(Category.id)
  | Other;

type budgetFilter ={
  budgetId: Budget.id,
  category: budgetCategoryFilter
};

type sharing = {
  totalAmount: float,
  currency: Currency.t,
  shareConfig: ShareConfig.t,
};

type t = {
  .
  "allowSplit": option(bool),
  "allowParentSelection": option(bool),
  "budgetFilter": option(budgetFilter),
  "date": option(Js.Date.t),
  "onSplitSelected": option((unit) => unit),
  "expenseId": option(Expense.id),
  "categoryId": option(Category.id),
  "tagId": option(Tag.id),
  "templateId": option(Template.id),
  "reminderId": option(Reminder.id),
  "budgetId": option(Budget.id),
  "recurringExpenseId": option(RecurringExpense.id),
  "excludedCategoryIds": option(list(Category.id)),
  "onCreateCategory": option((option(Category.id) => unit)),
  "onCreateTemplate": option((option(Template.id) => unit)),
  "onCreateReminder": option(unit => unit),
  "onCreateTag": option((option(Tag.id) => unit)),
  "selectedTags": option(list(Tag.id)),
  "onSelectTags": option((option(list(Tag.id)) => unit)),
  "onCategorySelected": option((option(Category.id) => unit)),
  "parentCategoryId": option(Category.id),
  "timeRange": option(TimeRange.t),
  "onExpenseFiltersSelected": option(((option(Category.id), option(Tag.id), TimeRange.t) => unit)),
  "parts": option(array(Categorization.part)),
  "onSplit": option((ExpenseSplitForm.value) => unit),
  "onSharingSelected": option((sharing => unit)),
  "sharing": option(sharing),
  "period": option(Utils.periodInfo)
};

let make =
    (
      ~allowSplit=?,
      ~allowParentSelection=?,
      ~budgetFilter=?,
      ~date=?,
      ~onSplitSelected=?,
      ~expenseId=?,
      ~categoryId: option(Category.id)=?,
      ~tagId=?,
      ~recurringExpenseId=?,
      ~excludedCategoryIds=?,
      ~onCreateCategory=?,
      ~onCreateTag=?,
      ~selectedTags=?,
      ~onSelectTags=?,
      ~onCategorySelected=?,
      ~parentCategoryId=?,
      ~timeRange=?,
      ~onExpenseFiltersSelected=?,
      ~templateId=?,
      ~reminderId=?,
      ~budgetId=?,
      ~onCreateTemplate=?,
      ~onCreateReminder=?,
      ~period=?,
      ~parts=?,
      ~onSplit=?,
      ~sharing=?,
      ~onSharingSelected=?,
      ()
    ): t => {
  "allowSplit": allowSplit,
  "allowParentSelection": allowParentSelection,
  "budgetFilter": budgetFilter,
  "date": date,
  "onSplitSelected": onSplitSelected,
  "expenseId": expenseId,
  "excludedCategoryIds": excludedCategoryIds,
  "onCreateCategory": onCreateCategory,
  "onCreateTemplate": onCreateTemplate,
  "onCreateReminder": onCreateReminder,
  "onCreateTag": onCreateTag,
  "selectedTags": selectedTags,
  "onSelectTags": onSelectTags,
  "onCategorySelected": onCategorySelected,
  "parentCategoryId": parentCategoryId,
  "categoryId": categoryId,
  "tagId": tagId,
  "templateId": templateId,
  "reminderId": reminderId,
  "budgetId": budgetId,
  "recurringExpenseId": recurringExpenseId,
  "timeRange": timeRange,
  "onExpenseFiltersSelected": onExpenseFiltersSelected,
  "parts": parts,
  "onSplit": onSplit,
  "sharing": sharing,
  "onSharingSelected": onSharingSelected,
  "period": period
};

let null = make();
