open ReactNative;

let component = ReasonReact.statelessComponent("BudgetPeriodSelect");

let make = (~disabled, ~period: Budget.budgetPeriod, ~onPeriodChange, _) => {
  ...component,
  render: (_) => {
    let { Utils.start, finish } = Utils.getPeriodInfo(~date=period.date, ~period=period.budgetingPeriod, ());
    <Connect_SelectedTheme render=(theme => {
      <Row alignItems=`center justifyContent=`spaceBetween paddingHorizontal=16.>
        <ListPrimary value="Budget period:" style=Style.(style([disabled ? color(Colors.grey) : marginBottom(0.), flexShrink(1.)])) numberOfLines=1 />
        <Row alignItems=`center>
          <IconButton icon="chevron-left" onPress=(() => disabled ? Alert.alert((), ~title="Error", ~message="Can not change budget's period because it was created with a different budgeting period than the currently selected one.") : onPeriodChange(switch (period.budgetingPeriod) {
            | Monthly(_) => {
              ...period,
              date: start |> DateFns.subMonths(1.),
            }
            | BiWeekly(_) => {
              ...period,
              date: start |> DateFns.subDays(14.),
            }
          })) color=(disabled ? Colors.grey : theme.primary) />
          (switch period.budgetingPeriod {
            | Monthly(1) => {
              <ListPrimary value=(period.date |> DateFns.format("YYYY MMMM")) style=Style.(style([minWidth(100.), textAlign(`center), disabled ? color(Colors.grey) : marginBottom(0.)])) />
            }
            | _ => {
              let str = (start |> DateFns.format("MMM Do")) ++ " - " ++ (finish |> DateFns.format("MMM Do"));
              <ListPrimary value=str style=Style.(style([minWidth(150.), textAlign(`center), disabled ? color(Colors.grey) : marginBottom(0.)])) />
            }
          })
          <IconButton icon="chevron-right" onPress=(() => disabled ? Alert.alert((), ~title="Error", ~message="Can not change budget's period because it was created with a different budgeting period than the currently selected one.") : onPeriodChange(switch (period.budgetingPeriod) {
            | Monthly(_) => {
              ...period,
              date: start |> DateFns.addMonths(1.),
            }
            | BiWeekly(_) => {
              ...period,
              date: start |> DateFns.addDays(14.),
            }
          })) color=(disabled ? Colors.grey : theme.primary) />
        </Row>
      </Row>
    }) />
  }
}
