open ReactNative;
open RnMui;

type state = {
  copyDate: Budget.budgetPeriod,
  formValue: BudgetForm.t
};

type actions =
  | UpdateFormValue(BudgetForm.t)
  | UpdateCopyDate(Budget.budgetPeriod)
  | CreateBudget
  | CopyBudget;

let component = ReasonReact.reducerComponent("BudgetCreate");

let make = (~onCategoryPickerRequested, ~onBudgetCreated, _) => {
  ...component,
  initialState: () => {
    let budgets = BudgetDb2.getAll()
      |> Common.iSort((b1, b2) => DateFns.compareDesc(b1.Budget.period.date, b2.period.date));

    {
      formValue: BudgetForm.initialState(),
      copyDate: Array.length(budgets) > 0 ? Array.get(budgets, 0).period : {
        date: Js.Date.make(),
        budgetingPeriod: Monthly(0)
      }
    }
  },
  reducer: (action, state) => switch action {
    | UpdateFormValue(formValue) => ReasonReact.Update {
      ...state,
      formValue: formValue
    }
    | CreateBudget => ReasonReact.SideEffects((_) => {
      let { BudgetForm.period, currency, totalAmount, parts } = state.formValue;
      let budget = A_Budget.create(
        ~period,
        ~budgetType=Monthly,
        ~currency,
        ~totalAmount=totalAmount |> NumberInput.toNumber |> Js.Option.getWithDefault(BudgetForm.getTotalPartsCost(parts)),
        ~parts=(parts |> Array.map(BudgetForm.toBudgetPart)),
        ()
      );
      Mixpanel.increment(EventConsts.budgetCreated);
      onBudgetCreated(budget.id)
    })
    | UpdateCopyDate(copyDate) => ReasonReact.Update {
      ...state,
      copyDate
    }
    | CopyBudget => {
      let optBudget = BudgetDb2.getAll()
        |> Js.Array.find(budget => budget.Budget.period == state.copyDate);
      let copiedBudget = optBudget
      |> C.Option.map(BudgetForm.getInitialState)
      |> Js.Option.getWithDefault(BudgetForm.initialState());
      ReasonReact.Update {
        ...state,
        formValue: {
          ...copiedBudget,
          period: state.formValue.period
        }
      }
    }
  },
  render: ({ state, send }) => {
    <ScreenWrap fab=(
      <ThemeActionButton icon="save" onPress=(() => send(CreateBudget)) />
    )>
      <Connect_Budgets render=(budgets => {
        if (Array.length(budgets) == 0) {
          ReasonReact.null
        } else {
          <ListRow flexRow=true style=Style.(style([alignItems(`flexEnd)]))>
            <View style=Style.(style([flex(1.)]))>
              {
                let data = budgets
                  |> Common.iSort((b1, b2) => DateFns.compareAsc(b1.Budget.period.date, b2.period.date))
                  |> Array.map(budget => {
                    "label": budget.Budget.period |> Budget.formatPeriod,
                    "value": budget.Budget.period
                  });
                <Dropdown label="Copy from previous budget" value=(state.copyDate)
                  onChangeText=(((newPeriod, _)) => send(UpdateCopyDate(newPeriod)))
                  data
                />
              }
            </View>
            <View style=Style.(style([marginBottom(8.)]))>
              <MuiButton text="Copy" onPress=(() => send(CopyBudget)) primary=true />
            </View>
          </ListRow>
        }
      }) />
      <BudgetForm
        onSubmitRquested=(() => send(CreateBudget))
        createFlow=true
        value=state.formValue onCategoryPickerRequested
        onValueChanged=((formValue) => send(UpdateFormValue(formValue)))
      />
    </ScreenWrap>
  }
}
