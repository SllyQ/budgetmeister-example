open ReactNative;
open RnMui;

type formPart = {
  categoryId: Category.id,
  amount: NumberInput.value
};

let toBudgetPart = (formPart: formPart): Budget.part => {
  categoryId: formPart.categoryId,
  amount: NumberInput.toNumber(formPart.amount) |> Js.Option.getWithDefault(0.)
};

let toFormPart = (budgetPart: Budget.part): formPart => {
  categoryId: budgetPart.categoryId,
  amount: NumberInput.toValue(budgetPart.amount)
};

type t = {
  period: Budget.budgetPeriod,
  currency: Currency.t,
  totalAmount: NumberInput.value,
  parts: array(formPart)
};

let initialState = () => {
  let settings = SettingsDb.get();
  {
    period: {
      date: Js.Date.make(),
      budgetingPeriod: settings.budgetingPeriod
    },
    currency: SettingsDb.get().mainCurrency,
    totalAmount: Empty,
    parts: [||]
  }
};

let getInitialState = (budget: Budget.t) => {
  period: budget.period,
  currency: budget.currency,
  totalAmount: budget.totalAmount |> NumberInput.toValue,
  parts: budget.parts |> Array.map(toFormPart)
};

type state = {
  costInputRefs: Js.Dict.t(option(ReasonReact.reactRef)),
  totalCostInputRef: ref(option(ReasonReact.reactRef))
};

let component = ReasonReact.reducerComponent("BudgetForm");

let setCostInputRef = ((categoryId, ref), { ReasonReact.state }) => {
  Js.Dict.set(state.costInputRefs, Category.key(categoryId), Js.Nullable.toOption(ref))
};

let focusCostInput = (categoryId, { ReasonReact.state }) =>
  switch (Js.Dict.get(state.costInputRefs, Category.key(categoryId))) {
    | Some(Some(ref)) => NumberInput.focus(ref)
    | _ => ()
  };

let setTotalCostInputRef = (ref, { ReasonReact.state }) => {
  state.totalCostInputRef := Js.Nullable.toOption(ref)
};

let focusTotalCostInput = (_, { ReasonReact.state }) => {
  switch state.totalCostInputRef.contents {
    | Some(ref) => CostInput.focus(ref)
    | None => ()
  };
};

let getTotalCostNum = (totalCost) => totalCost |> NumberInput.toNumber |> Js.Option.getWithDefault(0.);
let getTotalPartsCost = (parts) => parts
  |> Array.map(part => part.amount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.))
  |> Array.fold_left((+.), 0.);

let make = (~budgetId=?, ~createFlow, ~onSubmitRquested, ~value, ~onValueChanged, ~onCategoryPickerRequested, _) => {
  let addNewCategory = ((), { ReasonReact.handle }) => {
    let addedCategories = value.parts |> Array.map(part => part.categoryId) |> Array.to_list;
    onCategoryPickerRequested(
      ~allowSplit=false,
      ~allowParentSelection=true,
      ~excludedCategoryIds=addedCategories,
      ~onSplitSelected=None,
      ~onCategorySelected=(fun
        | None => ()
        | Some(categoryId) => {
          onValueChanged({
            ...value,
            parts: Array.append(value.parts, [|{ categoryId, amount: Empty }|])
          });
          Js.Global.setTimeout(() => {
            handle(focusCostInput, categoryId)
          }, 0) |> ignore;
        }
      ),
      None,
      ()
    )
  };

  let updatePartCost = ((categoryId, amount), _) => {
    onValueChanged({
      ...value,
      parts: value.parts |> Array.map(part => {
        if (part.categoryId === categoryId) {
          {
            ...part,
            amount
          }
        } else {
          part
        }
      })
    })
  };

  let removePart = (categoryId, _) => {
    onValueChanged({
      ...value,
      parts: value.parts |> Js.Array.filter(part => part.categoryId !== categoryId)
    })
  };

  let recalculateTotalCost = ((), _) => {
    onValueChanged({
      ...value,
      totalAmount: getTotalPartsCost(value.parts) |> NumberInput.toValue
    })
  };

  {
    ...component,
    initialState: () => {
      costInputRefs: Js.Dict.empty(),
      totalCostInputRef: ref(None)
    },
    didMount: ({ handle }) => {
      if (createFlow) {
        Js.Global.setTimeout(handle(focusTotalCostInput), 0) |> ignore
      };
    },
    reducer: (_action: unit, _state) => ReasonReact.NoUpdate,
    render: ({ handle }) => {
      let totalPartsCost = getTotalPartsCost(value.parts);
      let costDiff = (value.totalAmount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.))
        -. totalPartsCost;
      let formatedCostDiff = Currency.format(~amount=costDiff, ~currency=value.currency);
      let allBudgets = BudgetDb2.getAll();
      let existingThisMonthBudget = allBudgets |> Js.Array.find((budget: Budget.t) => {
        switch (budgetId |> C.Option.chain(id => BudgetDb2.get(~id))) {
          | None => DateFns.isSameMonth(budget.period.date, value.period.date)
          | Some(currentBudget) => DateFns.isSameMonth(budget.period.date, value.period.date) && budget.id !== currentBudget.id
        }
      });
      <C_Settings render=(settings => {
        <View style=Style.(style([flex(1.)]))>
          <View
            style=Style.(combine(PlatformElevation.get(1.), style([backgroundColor("#fff"), paddingTop(8.)])))
          >
            <BudgetPeriodSelect
              disabled=(settings.budgetingPeriod != value.period.budgetingPeriod)
              period=value.period
              onPeriodChange=(period => onValueChanged({ ...value, period }))
            />
            /* <MonthPickerRow value=value.date onValueChanged=(date => onValueChanged({ ...value, date }))
              label="Budget period"
              error=?(Js.Option.isSome(existingThisMonthBudget) ? Some("Budget for this month already exists") : None)
            /> */
            <ListRow style=Style.(style([paddingVertical(8.), alignItems(`flexEnd)])) flexRow=true>
              <View style=Style.(style([flex(1.)]))>
                <CostInput label="Total budget for month"
                  amount=value.totalAmount
                  currency=value.currency
                  onAmountChanged=(amount => onValueChanged({
                    ...value,
                    totalAmount: amount
                  }))
                  onCurrencyChanged=(currency => onValueChanged({
                    ...value,
                    currency: currency
                  }))
                  ref=handle(setTotalCostInputRef)
                  returnKeyType=(createFlow ? "next" : "done")
                  onSubmitEditing=?(switch createFlow {
                    | false => None
                    | true => if (Array.length(value.parts) === 0) {
                      Some(handle(addNewCategory))
                    } else {
                      None
                    }
                  })
                />
              </View>
              <View style=Style.(style([paddingBottom(Styles.Spacing.m), paddingLeft(Styles.Spacing.s)]))>
                <MuiButton text="Recalculate" onPress=handle(recalculateTotalCost) primary=true style=Style.(style([paddingBottom(Styles.Spacing.m)]))/>
              </View>
            </ListRow>
            <ListSubheader value="Budget for categories" rightValue=Text({j|$(formatedCostDiff) remaining|j})
              style=Style.(style([paddingTop(Styles.Spacing.l)]))
            />
          </View>
          <KaScrollView contentContainerStyle=Style.(style([paddingBottom(35.)]))>
            <ListWrapper>
              (value.parts
                |> Array.map(({ categoryId, amount }) => {
                  <C_Category key=Category.key(categoryId) categoryId=categoryId render=(category => {
                    <ListRow flexRow=true>
                      <View style=Style.(style([flex(1.)]))>
                        <NumberInput value=amount label=category.title
                          onChangeValue=(amount => handle(updatePartCost, (categoryId, amount)))
                          ref=(ref => handle(setCostInputRef, (categoryId, ref)))
                          returnKeyType=(createFlow ? "next" : "done")
                          onSubmitEditing=?(switch createFlow {
                            | false => None
                            | true => if (costDiff !== 0.) {
                              Some(handle(addNewCategory))
                            } else {
                              Some(onSubmitRquested)
                            }
                          })
                        />
                      </View>
                      <IconButton icon="close" color=Colors.danger
                        onPress=(() => handle(removePart, categoryId))
                      />
                    </ListRow>
                  }) />
                })
                |> ReasonReact.array
              )
              <ListRow numberOfLines=1>
                <MuiButton primary=true text="Add budget for category" onPress=handle(addNewCategory) />
              </ListRow>
            </ListWrapper>
          </KaScrollView>
        </View>
      }) />
    }
  }
}
