open ReactNative;
open RnMui;

type t = | Create | Edit(Category.id) | Hidden;

type state = {
  budgetAmount: NumberInput.value,
  budgetAmountInputRef: ref(option(ReasonReact.reactRef))
};

type actions =
  | UpdateBudget(NumberInput.value);

let component = ReasonReact.reducerComponent("BudgetFormModal");

let make = (~categoryId, ~onRequestClose, ~onBudgetSaveRequested, _) => {
  let focusBudgetInput = (_, { ReasonReact.state }) => {
    switch state.budgetAmountInputRef.contents {
      | Some(ref) => NumberInput.focus(ref)
      | None => ()
    }
  };

  let setBudgetInputRef = (ref, { ReasonReact.state }) => {
    state.budgetAmountInputRef := Js.Nullable.toOption(ref);
  };

  {
    ...component,
    initialState: () => {
      let currentBudgetAmount = switch (Js.Dict.get(BudgetDb.get().categoryMonthlyBudgets, Category.key(categoryId))) {
        | Some(Some(b)) => NumberInput.Number(b)
        | _ => Empty
      };
      {
        budgetAmount: currentBudgetAmount,
        budgetAmountInputRef: ref(None)
      }
    },
    didMount: ({ handle }) => {
      Js.Global.setTimeout(() => {
        handle(focusBudgetInput, ());
      }, 0) |> ignore;
    },
    reducer: (action, state) => switch action {
      | UpdateBudget(budgetAmount) => ReasonReact.Update {
        ...state,
        budgetAmount: budgetAmount
      }
    },
    render: ({ state, send, handle }) => {
      <RnModal isVisible=true
        onBackButtonPress=onRequestClose onBackdropPress=onRequestClose
      >
        <View style=Style.(style([padding(Styles.Spacing.xxl), backgroundColor("white")]))>
          <C_Category categoryId render=((category) => {
            <Text style=Styles.Text.title value=("Budget for " ++ category.title) />
          }) />
          <View style=Style.(style([paddingTop(20.), paddingBottom(24.)]))>
            <NumberInput ref=handle(setBudgetInputRef) label="Budget" value=state.budgetAmount onChangeValue=((budgetAmount) => send(UpdateBudget(budgetAmount))) />
          </View>
          <View style=Style.(style([flexDirection(`row), justifyContent(`flexEnd), paddingVertical(8.)]))>
            <View style=Style.(style([marginRight(8.)]))>
              <MuiButton primary=true text="Cancel" onPress=onRequestClose />
            </View>
            <View style=Style.(style([marginRight(8.)]))>
              (switch state.budgetAmount {
                | NumberInput.Number(budgetAmount) => <MuiButton primary=true text="Save" onPress=(() => onBudgetSaveRequested((categoryId, budgetAmount))) />
                | _ => <MuiButton primary=true text="Save" disabled=true onPress=(() => ()) />
              })

            </View>
          </View>
        </View>
      </RnModal>
    }
  }
}
