type state = {
  formValue: BudgetForm.t
};

type actions =
  | UpdateFormValue(BudgetForm.t)
  | UpdateBudget;

let component = ReasonReact.reducerComponent("BudgetEdit");

let make = (~budgetId, ~onCategoryPickerRequested, ~onBudgetUpdated, _) => {
  ...component,
  initialState: () => {
    formValue: BudgetDb2.get(~id=budgetId)
      |> C.Option.map(BudgetForm.getInitialState)
      |> Js.Option.getWithDefault(BudgetForm.initialState())
  },
  reducer: (action, state) => switch action {
    | UpdateFormValue(formValue) => ReasonReact.Update {
      formValue: formValue
    }
    | UpdateBudget => ReasonReact.SideEffects((_) => {
      let { BudgetForm.period, currency, totalAmount, parts } = state.formValue;
      A_Budget.update(
        budgetId,
        ~period,
        ~currency,
        ~totalAmount=totalAmount |> NumberInput.toNumber |> Js.Option.getWithDefault(BudgetForm.getTotalPartsCost(parts)),
        ~parts=(parts |> Array.map(BudgetForm.toBudgetPart)),
        ()
      );
      onBudgetUpdated()
    })
  },
  render: ({ state, send }) => {
    <ScreenWrap fab=(
      <ThemeActionButton icon="save" onPress=(() => send(UpdateBudget)) />
    )>
      <BudgetForm
        budgetId
        onCategoryPickerRequested
        createFlow=false
        onSubmitRquested=(() => send(UpdateBudget))
        value=state.formValue
        onValueChanged=((formValue) => send(UpdateFormValue(formValue)))
      />
    </ScreenWrap>
  }
}
