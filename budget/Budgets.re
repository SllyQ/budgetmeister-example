open ReactNative;
open RnMui;

let component = ReasonReact.statelessComponent ("Budgets");

let deleteBudget = (budgetId, _) => {
  let optBudget = BudgetDb2.get(~id=budgetId);
  switch optBudget {
  | None => ()
  | Some(budget) =>
    let formatedDate = budget.period.date |> DateFns.format("MMMM YYYY");
    let snackbarText = {j|Budget for $(formatedDate) deleted|j};
    let undoDelete = A_Budget.delete(budgetId);
    SnackbarService.show(
      snackbarText,
      ~action={
        text: "Undo",
        handler: () => {
          undoDelete();
          SnackbarService.hideSnackbar()
        }
      }
    )
  }
};

let make = (~onBudgetCreateRequested, ~onBudgetEditRequested, _) => {
  {
    ...component,
    render: ({ handle }) => {
      <HelpWrapper helpComponent={
        title: "Budgets",
        message: `String("While tracking your expenses is a great first step to take better care of your finances, many experts recommend planning in advance where you are going to spend your money. Budgets are there exactly for that! Using budgets you can plan your expenses for upcoming months or budgeting periods of your choice. Once you have created a budget you can track your progress in the \"Budget\" tab at home screen.")
      }>
        <ScreenWrap fab=(
          <ThemeActionButton onPress=onBudgetCreateRequested />
        )>
          <Connect_Budgets render=(budgets => {
            if (Array.length(budgets) == 0) {
              <BudgetsEmpty />
            } else {
              let sortedBudgets = budgets |> Common.iSort((b1, b2) => {
                DateFns.compareAsc(b1.Budget.period.date, b2.period.date)
              });
              <ListWrapper>
                (sortedBudgets
                  |> Array.map((budget: Budget.t) => {
                    <ListRow key=Budget.key(budget.id) numberOfLines=1
                      onPress=Actions({
                        ListRow.actions: [|{
                          text: "Edit",
                          icon: Some("pencil"),
                          onPress: () => onBudgetEditRequested(budget.id)
                        }, {
                          text: "Delete",
                          icon: Some("delete"),
                          onPress: () => handle(deleteBudget, budget.id)
                        }|]
                      })
                    >
                      <ListPrimary value=Budget.formatPeriod(budget.period) />
                    </ListRow>
                  })
                  |> ReasonReact.array)
              </ListWrapper>
            }
          }) />
        </ScreenWrap>
      </HelpWrapper>
    }
  }
}
