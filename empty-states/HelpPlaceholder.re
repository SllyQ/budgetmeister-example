open ReactNative;

let component = ReasonReact.statelessComponent("HelpPlaceholder");

let make = (_) => {
  ...component,
  render: (_) => {
    ReasonReact.null
  }
}
