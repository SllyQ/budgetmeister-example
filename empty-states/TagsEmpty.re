open ReactNative;

let component = ReasonReact.statelessComponent("TagsEmpty");

let make = (_) => {
  ...component,
  render: (_) => {
    <EmptyStateWrapper>
      <TitleText textAlign=`center value="Categories" />
      <BodyText textAlign=`center paddingTop=16.
        value="This is where you will be able to see and manage your tags."
      />
      <BodyText textAlign=`center paddingTop=16.
        value="To create your first tags press \"+\" button on the bottom right"
      />
      <CaptionText textAlign=`center paddingTop=16.
        value="Hint: You can also shake your device to see more information about this or any other screen."
      />
    </EmptyStateWrapper>
  }
}
