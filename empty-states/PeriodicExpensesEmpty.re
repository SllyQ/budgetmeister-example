open ReactNative;

let component = ReasonReact.statelessComponent("PeriodicExpensesEmpty");

let make = (_) => {
  ...component,
  render: (_) => {
    <EmptyStateWrapper>
      /* <TitleText textAlign=`center value="Expenses" />
      <BodyText textAlign=`center paddingTop=16.
        value="This is where you will see the list of all your expenses grouped by month (or a budgeting period of your choice)."
      />
      <BodyText textAlign=`center paddingTop=16.
        value="You can create your first expense by pressing the \"+\" button on the bottom left."
      /> */
      <BodyText textAlign=`center paddingTop=16.
        value="You do not have any expenses created yet. To create your first expense press the \"+\" button on the bottom left."
      />
      <CaptionText textAlign=`center paddingTop=16.
        value="Hint: You can also shake your device to see more information about this or any other screen."
      />
    </EmptyStateWrapper>
  }
}
