open ReactNative;

let component = ReasonReact.statelessComponent("PeriodicExpensesSummaryEmpty");

let make = (_) => {
  ...component,
  render: (_) => {
    <EmptyStateWrapper>
      <TitleText textAlign=`center value="Expenses Summary" />
      <BodyText textAlign=`center paddingTop=16.
        value="This is where you will see the summary of all your expenses for each month (or a budgeting period of your choice)."
      />
      <BodyText textAlign=`center paddingTop=16.
        value="To create your first expense go to the \"Expenses\" tab and click the \"+\" button on the bottom left"
      />
      <CaptionText textAlign=`center paddingTop=16.
        value="Hint: You can also shake your device to see more information about this or any other screen."
      />
    </EmptyStateWrapper>
  }
}
