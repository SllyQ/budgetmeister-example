open ReactNative;

let component = ReasonReact.statelessComponent("BudgetSummaryEmpty");

let make = (_) => {
  ...component,
  render: (_) => {
    <EmptyStateWrapper>
      <TitleText textAlign=`center value="Budget Summary" />
      <BodyText textAlign=`center paddingTop=16.
        value="This is where you will see the summary of all your budgets once you create your first budget."
      />
      <BodyText textAlign=`center paddingTop=16.
        value="You can create your first budget by going to budgets via side menu."
      />
      <CaptionText textAlign=`center paddingTop=16.
        value="Hint: You can also shake your device to see more information about this or any other screen."
      />
    </EmptyStateWrapper>
  }
}
