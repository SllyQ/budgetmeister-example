open ReactNative;

let component = ReasonReact.statelessComponent("ChartsEmpty");

let make = (_) => {
  ...component,
  render: (_) => {
    <EmptyStateWrapper>
      <TitleText textAlign=`center value="Charts" />
      <BodyText textAlign=`center paddingTop=16.
        value="This is where you will be able to explore your expenses in more depth with the help of charts once you create some expenses."
      />
      <BodyText textAlign=`center paddingTop=16.
        value="To create your first expense go to \"Home\" in the side menu and press \"+\" button on the bottom right"
      />
      <CaptionText textAlign=`center paddingTop=16.
        value="Hint: You can also shake your device to see more information about this or any other screen."
      />
    </EmptyStateWrapper>
  }
}
