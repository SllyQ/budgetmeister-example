open ReactNative;

type state = {
  currencyModalVisible: bool,
  currency: option(Currency.t),
  tagIds: list(Tag.id),
  tripModeActive: bool
};

type actions =
  | OpenCurrencyModal
  | CurrencySelected(option(Currency.t))
  | ClearCurrency
  | UpdateTripModeActive(bool)
  | UpdateTripModeTagIds(list(Tag.id))
  | SaveChanges;

let component = ReasonReact.reducerComponent("TravelModeScreen");

let make = (~onGoBack, ~onSelectTags, _) => {
  ...component,
  initialState: () => {
    let settings = SettingsDb.get();
    {
      currencyModalVisible: false,
      currency: settings.tripMode.currency,
      tagIds: settings.tripMode.tagIds |> Array.to_list,
      tripModeActive: settings.tripModeActive,
    }
  },
  reducer: C.flip(state => fun
    | OpenCurrencyModal => ReasonReact.Update {
      ...state,
      currencyModalVisible: true
    }
    | CurrencySelected(currency) => ReasonReact.Update {
      ...state,
      currencyModalVisible: false,
      currency
    }
    | ClearCurrency => ReasonReact.Update {
      ...state,
      currency: None
    }
    | UpdateTripModeActive(tripModeActive) => ReasonReact.Update {
      ...state,
      tripModeActive
    }
    | UpdateTripModeTagIds(tagIds) => ReasonReact.Update {
      ...state,
      tagIds
    }
    | SaveChanges => ReasonReact.SideEffects((_) => {
      let settings = SettingsDb.get();
      SettingsDb.setItem({
        ...settings,
        tripMode: {
          tagIds: state.tagIds |> Array.of_list,
          currency: state.currency
        },
        tripModeActive: state.tripModeActive
      });
      onGoBack();
    })
  ),
  render: ({ send, state }) => {
    let { currencyModalVisible, currency, tripModeActive, tagIds } = state;
    <ScreenWrap fab=(
      <ThemeActionButton icon="check" onPress=(() => send(SaveChanges)) />
    )>
      <ListWrapper>
        <ListRow numberOfLines=1 onPress=Callback(() => send(OpenCurrencyModal)) >
          <Row alignItems=`center justifyContent=`spaceBetween>
            <ListPrimary>
              <RowTitle value="Default currency: " />
              <Text value={
                switch currency {
                  | Some(currency) => {
                    let str = Currency.currencyToString(currency);
                    let symbol = Currency.symbol(currency);
                    {j|$(str) ($(symbol))|j}
                  }
                  | None => "None selected"
                }
              } />
            </ListPrimary>
            <Paper.MuiButton label="clear" onPress=(() => send(ClearCurrency)) primary=true />
          </Row>
          <CurrencySelectModal
            isVisible=currencyModalVisible
            onCurrencySelected=((currency) => send(CurrencySelected(currency)))
          />
        </ListRow>
        <TagsSelectRow onSelectTags selectedTagIds=tagIds
          onSelectedTagIdsUpdate=((tagIds) => send(UpdateTripModeTagIds(tagIds)))
        />
        <OptionSwitchRow
          label="Enabled"
          value=tripModeActive
          onValueChange=(active => send(UpdateTripModeActive(active)))
        />
      </ListWrapper>
    </ScreenWrap>
  }
};
