open ReactNative;
open RnMui;

type state = {
  descriptionInputRef: ref(option(ReasonReact.reactRef)),
  description: string
};

let setDescriptionInputRef = (ref, { ReasonReact.state }) => {
  state.descriptionInputRef := Js.Nullable.toOption(ref)
};

let focusDescriptionInput = (_, { ReasonReact.state }) => {
  switch state.descriptionInputRef.contents {
    | Some(ref) => ReasonReact.refToJsObj(ref)##focus()
    | None => ()
  };
};

type actions =
  | UpdateDescription(string);

let component = ReasonReact.reducerComponent("PartDescriptionEditModal");

let make = (~initialDescription, ~onEditCanceled, ~onEditSubmitted, _) => {
  ...component,
  initialState: () => {
    descriptionInputRef: ref(None),
    description: initialDescription
  },
  didMount: ({ handle}) => {
    Js.Global.setTimeout(handle(focusDescriptionInput), 0) |> ignore;
  },
  reducer: (action, state) => switch action {
    | UpdateDescription(description) => ReasonReact.Update {
      ...state,
      description
    }
  },
  render: ({ handle, send, state }) => {
    <RnModal isVisible=true
      onBackButtonPress=onEditCanceled onBackdropPress=onEditCanceled
    >
      <View style=Style.(style([padding(Styles.Spacing.xxl), backgroundColor("white")]))>
        <TextField
          ref=handle(setDescriptionInputRef)
          label="Description"
          autoCapitalize="sentences"
          autoCorrect=false
          onChangeText=(text => send(UpdateDescription(text)))
          value=state.description
          onSubmitEditing=(() => onEditSubmitted(state.description))
        />
        <View style=Style.(style([flexDirection(`row), justifyContent(`flexEnd), paddingVertical(8.)]))>
          <View style=Style.(style([marginRight(8.)]))>
            <MuiButton primary=true text="Cancel" onPress=onEditCanceled />
          </View>
          <View style=Style.(style([marginRight(8.)]))>
            <MuiButton primary=true text="Save" onPress=(() => onEditSubmitted(state.description)) />
          </View>
        </View>
      </View>
    </RnModal>
  }
}
