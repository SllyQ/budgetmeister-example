open ReactNative;
open RnMui;

let component = ReasonReact.statelessComponent("TemplatePicker");

let make = (~onTemplateSelected, ~onTemplateCreateRequested, _) => {
  ...component,
  render: (_) =>
    <ScreenWrap contentContainerStyle=Style.(style([justifyContent(`flexEnd), minHeightPct(90.)]))>
      <C_Templates
        render=(
          (templates) =>
            <ListWrapper>
              <Text
                style=Style.(
                        combine(Styles.Text.title, style([textAlign(`center)]))
                      )
                value="Select a template"
              />
              (
                templates
                |> Common.iSort(
                     (t1, t2) =>
                       String.compare(
                         t1.Template.title |> String.lowercase,
                         t2.Template.title |> String.lowercase
                       )
                   )
                |> Array.map(
                     (template: Template.t) =>
                       <ListRow numberOfLines=1 key=(DbTemplate.key(template.id))>
                         <MuiButton
                           text=template.title
                           primary=true
                           upperCase=false
                           raised=true
                           onPress=(() => onTemplateSelected(template.id))
                           style={"text": {"fontSize": 16}}
                         />
                       </ListRow>
                   )
                |> ReasonReact.array
              )
              <ListRow numberOfLines=1>
                <MuiButton
                  text="Add New"
                  icon="add"
                  primary=true
                  raised=true
                  upperCase=false
                  onPress=onTemplateCreateRequested
                  style={"text": {"fontSize": 16}}
                />
              </ListRow>
            </ListWrapper>
        )
      />
    </ScreenWrap>
};
