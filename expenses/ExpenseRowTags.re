open ReactNative;

let component = ReasonReact.statelessComponent("ExpenseRowTags");

let styles =
  StyleSheet.create(
    Style.(
      {
        "tagsRow":
          style([
            flexDirection(`row),
            alignItems(`center),
            marginTop(5.),
          ])
      }
    )
  );

let make = (~tagIds, ~recurring=false, _) => {
  ...component,
  render: (_) => {
    if (Array.length(tagIds) === 0 && !recurring) {
      ReasonReact.null
    } else {
      <View style=styles##tagsRow>
        (if (recurring) {
          <RecurringExpenseChip
            style=Style.(style([marginRight(2.)]))
          />
        } else {
          ReasonReact.null
        })
        (
          tagIds
          |> Common.iSort(
               (t1, t2) => {
                 let optTag1 = TagDb.get(~id=t1);
                 let optTag2 = TagDb.get(~id=t2);
                 switch (optTag1, optTag2) {
                 | (Some(tag1), Some(tag2)) =>
                   String.compare(
                     String.lowercase(tag1.Tag.title),
                     String.lowercase(tag2.title)
                   )
                 | _ => 0
                 }
               }
             )
          |> Array.map(
               (tagId) =>
                 <TagChip
                   key=(Tag.key(tagId))
                   tagId
                   height=16.
                   withTitle=true
                   style=Style.(style([marginRight(2.)]))
                 />
             )
          |> ReasonReact.array
        )
      </View>
    }
  }
}
