let component = ReasonReact.statelessComponent("ExpensesListScreen");

let make = (
  ~onEditExpense,
  ~onEditRecurringExpense,
  ~budgetFilter: option(RParams.budgetFilter)=?,
  ~categoryId=?,
  ~tagId=?,
  ~timeRange=TimeRange.All,
  ~onCategoryPickerRequested,
  _
) => {
  ...component,
  render: (_) => {
    <Connect_ActiveUser render=(user => {
      <C_Expenses render=(expenses => {
        let filteredExpenses = switch (budgetFilter, categoryId) {
          | (Some(filter), _) => {
            let budget = BudgetDb2.get(~id=filter.budgetId) |> Js.Option.getExn;
            let { Utils.start, finish } = Utils.getPeriodInfo(~period=budget.period.budgetingPeriod, ~date=budget.period.date, ());
            expenses |> Js.Array.filter((expense) => {
              expense.Expense.date |> DateFns.isWithinRange(start, finish)
              && switch (filter.category) {
                | RParams.All => true
                | RParams.Category(catId) => switch (expense.categorization) {
                  | Some(Single({ categoryId })) => catId == categoryId
                  | Some(Split(parts)) => parts |> Js.Array.some(part => part.Categorization.categoryId == catId)
                  | None => false
                }
                | RParams.Other => switch (expense.categorization) {
                  | None => true
                  | Some(Single({ categoryId })) => {
                    !(budget.parts |> Js.Array.some(budgetPart => budgetPart.Budget.categoryId == categoryId))
                  }
                  | Some(Split(parts)) => {
                    parts |> Js.Array.every(part =>
                      budget.parts
                      |> Js.Array.some(budgetPart => budgetPart.Budget.categoryId == part.Categorization.categoryId)
                    )
                  }
                }
              }
            })
          }
          | (_, Some(cid)) => expenses |> Js.Array.filter(
            Utils.belongsToCategory(~categoryId=`WithChildren(cid))
          )
          | (None, None) => expenses
        }
        |> Js.Array.filter(
           (expense) =>
             switch tagId {
             | None => true
             | Some(tid) =>
               let tagIds = ExpenseTagDb.getExpenseTags(expense.Expense.id);
               Js.Array.includes(tid, tagIds)
             }
         )
        |> TimeRange.filterExpenses(timeRange);
        <ExpensesList
          user
          onEditExpense
          onEditRecurringExpense
          expenses=filteredExpenses
          headerHeight=0
          header=ReasonReact.null
          onCategoryPickerRequested
        />
      }) />
    }) />
  }
};
