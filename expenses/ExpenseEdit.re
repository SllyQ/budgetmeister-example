open ReactNative;

let styles =
  StyleSheet.create(Style.({"screenWrap": style([flex(1.0), widthPct(100.), padding(15.)])}));

type state = {
  formValue: ExpenseForm.t,
};

type actions =
  | SplitCost
  | SplitSelected(ExpenseSplitForm.value)
  | UpdateExpense
  | UpdateFormValue(ExpenseForm.t);

let component = ReasonReact.reducerComponent("ExpenseEdit");

let make = (_, ~onExpenseUpdated, ~id, ~onSelectTags, ~onCategoryPickerRequested, ~onSplitFormRequested) => {
  ...component,
  initialState: () => {
    let optExpense = ExpenseDb.Data.get(id);
    switch optExpense {
    | None => {
      onExpenseUpdated(); /* Shouldn't ever happen */
      Obj.magic()
    }
    | Some(expense) => {
      Js.log(expense);
      let tagIds = ExpenseTagDb.getExpenseTags(expense.id);
      {
        formValue: {
          ...ExpenseForm.initialFormState(
            ~date=expense.date,
            ~categorization=expense.categorization,
            ~totalAmount=?expense.totalAmount,
            ~currency=expense.currency,
            ()
          ),
          title: switch expense.title {
            | None => ""
            | Some(t) => t
          },
          unpaid: expense.unpaid,
          date: expense.date,
          tagIds: Array.to_list(tagIds)
        }
      }
    }
    }
  },
  reducer: (action, state) =>
    switch action {
    | SplitCost => ReasonReact.SideEffects(({ send }) => {
      let currentParts = switch (state.formValue.spending.categorization) {
        | Some(Single(part)) => [|{ Categorization.categoryId: part.categoryId, amount: state.formValue.spending.totalAmount |> NumberInput.toNumber, description: None }|]
        | Some(Split(parts)) => parts
        | None => [||]
      };
      onSplitFormRequested(Some(currentParts), (parts: ExpenseSplitForm.value) => send(SplitSelected(parts)))
    })
    | SplitSelected({ totalAmount, currency, categorization }) => {
      ReasonReact.Update {
        formValue: {
          ...state.formValue,
          spending: {
            totalAmount: totalAmount |> NumberInput.toValue,
            currency,
            categorization: Some(categorization)
          }
        }
      }
    }
    | UpdateFormValue(formValue) => ReasonReact.Update { formValue: formValue }
    | UpdateExpense =>
      let { ExpenseForm.title, tagIds, date, unpaid, spending } = state.formValue;
      ReasonReact.SideEffects(
        (
          (_) => {
            switch (spending.totalAmount, spending.categorization) {
            | (NumberInput.Invalid(_), _) => Alert.alert(~title="Error", ~message="Total amount is invalid", ())
            | (_, None) => Alert.alert(~title="Error", ~message="Category is required", ())
            | (_, Some(categorization)) => {
              let parsedTitle = title == "" ? None : Some(String.trim(title));
              A_Expense.update((), ~id, ~title=parsedTitle, ~date, ~tagIds=tagIds |> Array.of_list,
                ~totalAmount=(spending.totalAmount |> NumberInput.toNumber), ~categorization, ~currency=spending.currency, ~unpaid)
              |> (fun
                | NotFound | ValidationError(_) => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
                | Updated(_) => {
                  HistoryDb.currencyUsed(spending.currency);
                  onExpenseUpdated()
                }
              );
            }
            };
          }
        )
      )
    },
  render: ({state, send}) => {
    <ScreenWrap fab=(
      <ThemeActionButton icon="save" onPress=((_) => send(UpdateExpense)) />
    )>
      <ListWrapper>
        <ExpenseForm
          expenseId=id
          onSplitFormRequested
          onCategoryPickerRequested
          onSelectTags
          value=state.formValue
          onValueChange=((formValue) => send(UpdateFormValue(formValue)))
        />
      </ListWrapper>
    </ScreenWrap>
  }
};
