open ReactNative;
open RnMui;

type part = {
  categoryId: Category.id,
  amount: NumberInput.value,
  description: string,
};

type value = {
  categorization: Categorization.t,
  totalAmount: float,
  currency: Currency.t
};

type state = {
  totalAmount: NumberInput.value,
  currency: Currency.t,
  parts: array(part),
  costInputRefs: Js.Dict.t(option(ReasonReact.reactRef)),
  totalCostInputRef: ref(option(ReasonReact.reactRef)),
  descriptionEdited: option(part),
};

type actions =
  | AddRest(Category.id)
  | UpdateTotalAmount(NumberInput.value)
  | UpdateCurrency(Currency.t)
  | UpdatePartAmount(Category.id, NumberInput.value)
  | UpdatePartDescription(Category.id, string)
  | RecalculateTotalCost
  | AddCategory(Category.id)
  | FocusCategoryCostInput(Category.id)
  | AddNewCategory
  | RemovePart(Category.id)
  | EditPartDescription(part)
  | CancelPartDescriptionEdit
  | SubmitParts;

let component = ReasonReact.reducerComponent("ExpenseSplitForm");

let toCategorizationPart = (part): Categorization.part => {
  categoryId: part.categoryId,
  amount: part.amount |> NumberInput.toNumber,
  description: part.description === "" ? None : Some(part.description)
};

let toFormPart = (part: Categorization.part) => {
  categoryId: part.categoryId,
  amount: part.amount |> NumberInput.optToValue,
  description: part.description |> Js.Option.getWithDefault("")
};

let getTotalCostNum = (totalCost) => totalCost |> NumberInput.toNumber |> Js.Option.getWithDefault(0.);
let getTotalPartsCost = (parts) => parts
  |> Array.map(part => part.amount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.))
  |> Array.fold_left((+.), 0.);

let setCostInputRef = ((categoryId, ref), { ReasonReact.state }) => {
  Js.Dict.set(state.costInputRefs, Category.key(categoryId), Js.Nullable.toOption(ref))
};

let setTotalCostInputRef = (ref, { ReasonReact.state }) => {
  state.totalCostInputRef := Js.Nullable.toOption(ref)
};

let focusTotalCostInput = (_, { ReasonReact.state }) => {
  switch state.totalCostInputRef.contents {
    | Some(ref) => CostInput.focus(ref)
    | None => ()
  };
};

let make = (~parts=?, ~totalAmount=?, ~currency=?, ~onCategoryPickerRequested, ~onPartsSubmitted, _) => {
  ...component,
  initialState: () => {
    currency: currency |> Js.Option.getWithDefault(SettingsDb.get().mainCurrency),
    totalAmount: totalAmount |> C.Option.map(NumberInput.toValue) |> Js.Option.getWithDefault(NumberInput.Empty),
    parts: parts |> C.Option.map(Array.map(toFormPart)) |> Js.Option.getWithDefault([||]),
    costInputRefs: Js.Dict.empty(),
    totalCostInputRef: ref(None),
    descriptionEdited: None,
  },
  didMount: ({ handle }) => {
    switch parts {
      | Some(_) => ()
      | None => Js.Global.setTimeout(handle(focusTotalCostInput), 0) |> ignore
    };
  },
  reducer: (action, state) => switch action {
    | AddRest(categoryId) => {
      let totalPartsCost = getTotalPartsCost(state.parts);
      let costDiff = (state.totalAmount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.))
        -. totalPartsCost;
      let parts = state.parts |> Js.Array.map(part => if (part.categoryId === categoryId) {
        {
          ...part,
          amount: part.amount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.) |> ((c) => c +. costDiff) |> NumberInput.toValue
        }
      } else {
        part
      });
      ReasonReact.Update {
        ...state,
        parts
      }
    }
    | UpdateTotalAmount(totalAmount) => ReasonReact.Update { ...state, totalAmount }
    | UpdateCurrency(currency) => ReasonReact.Update { ...state, currency }
    | UpdatePartAmount(categoryId, amount) => ReasonReact.Update {
      ...state,
      parts: state.parts |> Array.map(part => {
        part.categoryId !== categoryId ? part : {
          ...part,
          amount
        }
      })
    }
    | UpdatePartDescription(categoryId, description) => ReasonReact.Update {
      ...state,
      parts: state.parts |> Array.map(part => {
        part.categoryId !== categoryId ? part : {
          ...part,
          description
        }
      }),
      descriptionEdited: None
    }
    | RemovePart(categoryId) => ReasonReact.Update {
      ...state,
      parts: state.parts |> Js.Array.filter(part => part.categoryId !== categoryId)
    }
    | RecalculateTotalCost => ReasonReact.Update {
      ...state,
      totalAmount: getTotalPartsCost(state.parts) |> NumberInput.toValue
    }
    | AddCategory(categoryId) => ReasonReact.UpdateWithSideEffects({
      ...state,
      parts: Array.append(state.parts, [|{ categoryId, amount: Empty, description: "" }|])
    }, ({ send }) => Js.Global.setTimeout(() => {
      send(FocusCategoryCostInput(categoryId))
    }, 0) |> ignore)
    | FocusCategoryCostInput(categoryId) => {
      switch (Js.Dict.get(state.costInputRefs, Category.key(categoryId))) {
        | Some(Some(ref)) => NumberInput.focus(ref)
        | _ => ()
      };
      ReasonReact.NoUpdate
    }
    | AddNewCategory => ReasonReact.SideEffects(({ send }) => {
      let currentlySelectedCategories = state.parts |> Array.map((part => part.categoryId));
      Keyboard.dismiss();
      onCategoryPickerRequested(~allowSplit=false, ~excludedCategoryIds=(currentlySelectedCategories |> Array.to_list), ~onSplitSelected=None, ~onCategorySelected=((categoryId) => {
        switch categoryId {
          | None => ()
          | Some(catId) => send(AddCategory(catId))
        };
      }), None, ())
    })
    | SubmitParts => ReasonReact.SideEffects((_) => {
      switch (state.totalAmount |> NumberInput.toNumber) {
        | None => Alert.alert((), ~title="Error", ~message="Total amount must be a valid number")
        | Some(totalAmount) => {
          onPartsSubmitted({
            totalAmount: totalAmount,
            currency: state.currency,
            categorization: Split(state.parts |> Array.map(toCategorizationPart))
          })
        }
      }
    })
    | EditPartDescription(part) => ReasonReact.Update {
      ...state,
      descriptionEdited: Some(part)
    }
    | CancelPartDescriptionEdit => ReasonReact.Update {
      ...state,
      descriptionEdited: None
    }
  },
  render: ({ state, handle, send }) => {
    let { totalAmount, currency, descriptionEdited } = state;
    let totalPartsCost = getTotalPartsCost(state.parts);
    let totalCostFloat = (totalAmount |> NumberInput.toNumber |> Js.Option.getWithDefault(0.));
    let costDiff = totalCostFloat -. totalPartsCost;
    let costFullySplit = CostUtils.areAmountsEqual(totalCostFloat, totalPartsCost);
    let formatedCostDiff = Currency.format(~amount=costDiff, ~currency);
    <ScreenWrap
      fab=(
        <ThemeActionButton icon="check" onPress=(() => {
          if (costFullySplit) {
            send(SubmitParts)
          }
        }) />
      )
      scrollable=false
    >
      <View
        style=Style.(combine(PlatformElevation.get(1.), style([backgroundColor("#fff")])))
      >
        <ListRow style=Style.(style([flexDirection(`row), alignItems(`flexEnd), justifyContent(`spaceBetween)]))>
          <View style=Style.(style([flex(1.)]))>
            <CostInput amount=totalAmount currency
              onAmountChanged=(amount => send(UpdateTotalAmount(amount)))
              onCurrencyChanged=(currency => send(UpdateCurrency(currency)))
              ref=handle(setTotalCostInputRef)
              label="Total cost"
              returnKeyType=(Js.Option.isSome(parts) ? "done" : "next")
              onSubmitEditing=?(switch parts {
                | Some(_) => None
                | None => if (Array.length(state.parts) === 0) {
                  Some(() => send(AddNewCategory))
                } else {
                  None
                }
              })
            />
          </View>
          <View style=Style.(style([paddingBottom(Styles.Spacing.m), paddingLeft(Styles.Spacing.s)]))>
            <MuiButton text="Recalculate" onPress=(() => send(RecalculateTotalCost)) primary=true />
          </View>
        </ListRow>
        <ListSubheader value={j|Split categories|j} rightValue=Text({j|$(formatedCostDiff) remaining|j})
          rightValueColor=?(totalPartsCost != 0. && costFullySplit ? Some(Colors.success) : None)
          style=Style.(style([paddingTop(Styles.Spacing.l)]))
        />
      </View>
      <View style=Style.(style([flex(1.)]))>
        <KaScrollView contentContainerStyle=Style.(style([paddingBottom(35.)])) keyboardShouldPersistTaps="handled">
          (state.parts
            |> Array.map(({ categoryId, amount, description } as part) => {
              <C_Category key=Category.key(categoryId) categoryId render=(category => {
                <ListRow flexRow=true style=Style.(style([alignItems(`flexEnd)]))>
                  <View style=Style.(style([flex(1.)]))>
                    <NumberInput value=amount label=category.title onChangeValue=((newCost) => send(UpdatePartAmount(categoryId, newCost)))
                      ref=(ref => handle(setCostInputRef, (categoryId, ref)))
                      returnKeyType=(Js.Option.isNone(parts) ? "next" : "done")
                      onSubmitEditing=?(switch parts {
                        | Some(_) => None
                        | None => if (!costFullySplit) {
                          Some(() => send(AddNewCategory))
                        } else {
                          Some(() => send(SubmitParts))
                        }
                      })
                    />
                  </View>
                  <View style=Style.(style([padding(5.)]))>
                    <MuiButton text="Add remaining" primary=true onPress=(() => send(AddRest(categoryId)))/>
                  </View>
                  <IconButton icon="note"
                    onPress=(() => send(EditPartDescription(part)))
                    color=(description === "" ? Colors.grey : Colors.theme^.primary)
                  />
                  <IconButton icon="close"
                    onPress=(() => send(RemovePart(categoryId)))
                    color=Colors.danger
                  />
                </ListRow>
              }) />
            })
            |> ReasonReact.array
          )
          <ListRow numberOfLines=1>
            <MuiButton primary=true text="Add category" onPress=(() => send(AddNewCategory)) />
          </ListRow>
        </KaScrollView>
      </View>
      (switch descriptionEdited {
        | None => ReasonReact.null
        | Some(part) => {
          <PartDescriptionEditModal initialDescription=part.description
            onEditCanceled=(() => send(CancelPartDescriptionEdit))
            onEditSubmitted=((description) => send(UpdatePartDescription(part.categoryId, description)))
          />
        }
      })
    </ScreenWrap>
  }
}
