open ReactNative;

type state = {
  categoryFilter: option(Category.id),
  tagFilter: option(Tag.id),
  timeRangeFilter: TimeRange.t,
};

type actions =
  | ChangeFilters
  | UpdateFilters(option(Category.id), option(Tag.id), TimeRange.t)
  | ResetFilters
  | CreateNewExpense;

let component = ReasonReact.reducerComponent("Expenses");

let recenterSwipeable = (swipeableRef) =>
  switch swipeableRef {
  | Some(ref) =>
    let _ = ReasonReact.refToJsObj(ref)##recenter();
    ()
  | None => ()
  };

let make =
    (
      ~onCreateNewExpense,
      ~onEditExpense,
      ~onEditRecurringExpense,
      ~onChangeFilters,
      ~categoryFilter=None,
      ~timeRangeFilter=TimeRange.All,
      ~onCategoryPickerRequested,
      _
    ) => {
  ...component,
  initialState: () => {categoryFilter, tagFilter: None, timeRangeFilter},
  reducer: (action, state) =>
    switch action {
    | ChangeFilters =>
      ReasonReact.SideEffects(
        (
          ({send}) => {
            let _ =
              onChangeFilters(state.categoryFilter, state.tagFilter, state.timeRangeFilter)
              |> Js.Promise.then_(
                   ((categoryFilter, tagFilter, timeRangeFilter)) => {
                     send(UpdateFilters(categoryFilter, tagFilter, timeRangeFilter));
                     Js.Promise.resolve()
                   }
                 );
            ()
          }
        )
      )
    | UpdateFilters(categoryFilter, tagFilter, timeRangeFilter) =>
      ReasonReact.Update({categoryFilter, tagFilter, timeRangeFilter})
    | ResetFilters => ReasonReact.NoUpdate
    | CreateNewExpense =>
      ReasonReact.SideEffects(
        (
          (_) => {
            onCreateNewExpense();
          }
        )
      )
    },
  render: ({state: {categoryFilter, tagFilter, timeRangeFilter}}) =>
    <Connect_ActiveUser render=(user => {
      <C_Settings render=(settings => {
        <ScreenWrap scrollable=false fab=(
          <ThemeActionButton onPress=onCreateNewExpense />
        )>
          <C_Expenses
            render=(
              (expenses) => {
                let filteredExpenses = switch categoryFilter {
                  | None => expenses
                  | Some(cid) => expenses |> Js.Array.filter(Utils.belongsToCategory(~categoryId=`WithChildren(cid)))
                }
                |> Js.Array.filter(
                   (expense) =>
                     switch tagFilter {
                     | None => true
                     | Some(tid) =>
                       let tagIds = ExpenseTagDb.getExpenseTags(expense.Expense.id);
                       Js.Array.includes(tid, tagIds)
                     }
                 )
                |> TimeRange.filterExpenses(timeRangeFilter);
                let expensesWithoutCost =
                  expenses
                  |> Js.Array.filter((expense) => Js.Option.isNone(expense.Expense.totalAmount))
                  |> Array.length;
                <ExpensesList
                  user
                  headerHeight=(expensesWithoutCost > 0 ? 48 + 49 : 48)
                  expenses=filteredExpenses
                  onEditExpense
                  onEditRecurringExpense
                  onCategoryPickerRequested
                  header=
                    <View>
                      <ListRow numberOfLines=1>
                        <C_Budget
                          render=(
                            (budget) => {
                              let expectedExpenses =
                                switch budget.totalMonthlyBudget {
                                | None => None
                                | Some(mb) =>
                                  let now = Js.Date.make();
                                  let currDate = Js.Date.getDate(now);
                                  let daysInMonth = DateFns.getDaysInMonth(now);
                                  let partPassed = currDate /. daysInMonth;
                                  Some(mb *. partPassed)
                                };
                              let thisMonthCost =
                                expenses
                                |> Js.Array.filter(
                                  (expense) => DateFns.isThisMonth(expense.Expense.date)
                                )
                                |> Js.Array.map(Utils.getExpenseBudgetCost(~currency=settings.mainCurrency, ~userId=user |> C.Option.map(user => user.User.id)))
                                |> Array.fold_left((+.), 0.);
                              <View
                                style=Style.(
                                        style([
                                          flex(1.),
                                          flexDirection(`row),
                                          justifyContent(`spaceBetween),
                                          alignItems(`center)
                                        ])
                                      )>
                                <Text value="This month" style=Styles.Text.subheading />
                                <View style=Style.(style([flexDirection(`row)]))>
                                  <Text value=Currency.format(~currency=settings.mainCurrency,~amount=thisMonthCost) style=Styles.Text.subheading />
                                  (
                                    switch expectedExpenses {
                                    | None => ReasonReact.null
                                    | Some(expectedExpenses) =>
                                      let renderRatio = Js.Float.toFixed(thisMonthCost /. expectedExpenses *. 100.);
                                      <Text
                                        value={j| ($(renderRatio)% of target)|j}
                                        style=Styles.Text.subheading
                                      />
                                    }
                                  )
                                </View>
                              </View>
                            }
                          )
                        />
                      </ListRow>
                      (
                        if (expensesWithoutCost > 0) {
                          <View>
                            <Divider />
                            <ListRow numberOfLines=1>
                              <Text
                                style=Style.(
                                        combine(Styles.Text.listPrimary, style([color(Colors.danger)]))
                                      )
                                value={j|$expensesWithoutCost expenses do not have cost assigned to them!|j}
                              />
                            </ListRow>
                          </View>
                        } else {
                          ReasonReact.null
                        }
                      )
                      {
                        let numUnpaidExpenses = expenses |> Js.Array.filter ((expense) => expense.Expense.unpaid) |> Array.length;
                        if (numUnpaidExpenses > 0) {
                          <View>
                            <Divider />
                            <ListRow numberOfLines=1>
                              <Text
                                style=Style.(
                                        combine(Styles.Text.listPrimary, style([color(Colors.danger)]))
                                      )
                                value={j|$numUnpaidExpenses expenses are unpaid!|j}
                              />
                            </ListRow>
                          </View>
                        } else {
                          ReasonReact.null
                        }
                      }
                      <BudgetNotifications />
                    </View>
                />
                /* <Touchable
                     style=Style.(
                             style [
                               padding 15.,
                               borderBottomWidth 1.,
                               borderBottomColor "rgba(0, 0, 0, 0.87)",
                               paddingTop 12.,
                               paddingBottom 12.,
                               flexDirection `row,
                               justifyContent `spaceBetween,
                               alignItems `center
                             ]
                           )
                     onPress=(reduce (fun _ => ChangeFilters))>
                     (
                       switch (categoryFilter, tagFilter, timeRangeFilter) {
                       | (None, None, All) =>
                         <Text value="Showing all expenses" style=Styles.Text.body2 />
                       | (Some cid, _, _) =>
                         <C_Category
                           key=(Category.key cid)
                           categoryId=cid
                           render=(
                             fun category => {
                               let title = category.Category.title;
                               <Text value={j|Showing expenses for "$title"|j} />
                             }
                           )
                         />
                       /* How the fuck previous match is not exhaustive */
                       | _ => ReasonReact.null
                       }
                     )
                     <Text value="Change" style=Style.(style [fontWeight `bold, color "black"]) />
                   </Touchable> */
              }
            )
          />
        </ScreenWrap>
      }) />
    }) />
};
