open ReactNative;

type filter =
  | Unpaid
  | NoCost
  | NoFilter;

type state = {
  filter: filter
};

type actions =
  | FilterUnpaid
  | FilterNoCost
  | ClearFilter;

let component = ReasonReact.reducerComponent("MonthlyExpensesSummary");

let loggedFirstRender = ref(false);

let make = (~onPeriodSummaryRequested, ~onEditExpense, ~onEditRecurringExpense, ~onCreateNewExpense, ~onCategoryPickerRequested, _) => {
  ...component,
  initialState: () => { filter: NoFilter },
  reducer: C.flip((_) => fun
    | FilterUnpaid => ReasonReact.Update { filter: Unpaid }
    | FilterNoCost => ReasonReact.Update { filter: NoCost }
    | ClearFilter => ReasonReact.Update { filter: NoFilter }
  ),
  render: ({ state, send }) => {
    if (!loggedFirstRender.contents) {
      loggedFirstRender := true;
      let start = Js.Date.make();
      Js.Global.setTimeout((_) => {
        let donezo = Js.Date.make();
        let duration = Js.Date.getTime(donezo) -. Js.Date.getTime(start);
        DevLogService.addMessage({j|First render duration $(duration)ms|j});
      }, 0) |> ignore;
    };
    <C_Settings render=(settings => {
      <Connect_ActiveUser render=(user => {
        let userId = user |> C.Option.map(user => user.User.id);
        <HelpWrapper helpComponent={
          title: "Expenses",
          message: `String(
            "When tracking your expenses you can choose not to enter their cost. Expenses without cost entered are highlighted which can act as a reminder.\n\nYou can also mark any expense as unpaid to highlight it (for example to remind you to pay your bills)."
          )
        }>
          <ScreenWrap scrollable=false fab=(
            <ThemeActionButton onPress=onCreateNewExpense />
          )>
            <PeriodicExpensesView.Connect render=(expensesView => {
              let periods = expensesView.periods;
              let hasExpenses = periods |> Js.Dict.values |> Array.fold_left((acc, { PeriodicExpensesView.expenses }) => {
                max(acc, Array.length(expenses))
              }, 0) > 0;
              let now = Js.Date.make();
              let budgetingPeriods = expensesView.periods |> Js.Dict.values |> Array.map(({ PeriodicExpensesView.period }) => period);
              let initialIndex = budgetingPeriods  |> Js.Array.findIndex(({ Utils.start, finish }) => {
                now |> DateFns.isWithinRange(start, finish)
              });
              if (!hasExpenses) {
                <PeriodicExpensesEmpty />
              } else {
                <Swiper index=initialIndex loop=false loadMinimal=true>
                  {
                    periods
                    |> Js.Dict.values
                    |> Array.map(({ PeriodicExpensesView.expenses: group, period: { start, finish, title } as budgetingPeriod}) => {
                    /* budgetingPeriods
                    |> Array.map(({ Utils.start, finish, title } as budgetingPeriod) => {
                      let group = expenses |> Js.Array.filter(expense =>
                        expense.Expense.date |> DateFns.isWithinRange(start, finish)
                      ); */
                      let now = Js.Date.make();
                      let preview = if (now |> DateFns.isAfter(finish)) {
                        None
                      } else if (now |> DateFns.isAfter(start)) {
                        Some((now, finish))
                      } else {
                        Some((start, finish))
                      };
                      let shouldShowPreview = settings.showProjectedRecurringExpenses;
                      let previewStuff = shouldShowPreview
                        ? preview |> C.Option.map(((startDate, endDate)) => {
                          RecurringExpenseDb.Generator.previewItems(startDate, endDate)
                        })
                        : None;
                      let totalGroupCost = Utils.getExpensesTotal((), ~userId, ~currency=settings.mainCurrency, ~expenses=group |> Array.to_list);
                      let numExpensesWithoutCost =
                        group
                        |> Js.Array.filter((expense) => Js.Option.isNone(expense.Expense.totalAmount))
                        |> Array.length;
                      let numUnpaidExpenses = group |> Js.Array.filter ((expense) => expense.Expense.unpaid) |> Array.length;
                      let header = {
                        <View>
                          <ListRow numberOfLines=1 flexRow=true style=Style.(style([justifyContent(`spaceBetween)]))
                            onPress=Callback(() => onPeriodSummaryRequested(budgetingPeriod))
                          >
                            <Text value={j|$(title)|j} style=Styles.Text.listPrimary />
                            <View style=Style.(style([flexDirection(`row), paddingLeft(12.)]))>
                              <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=totalGroupCost) style=Styles.Text.listPrimary />
                              {
                                switch (previewStuff) {
                                  | Some(preview) => {
                                    let expenses = preview |> List.map(((expense, _ ,_)) => expense);
                                    let previewTotalCost = Utils.getExpensesTotal((), ~userId, ~currency=settings.mainCurrency, ~expenses);
                                    if (previewTotalCost == 0.) {
                                      ReasonReact.null
                                    } else {
                                      let cost = Currency.format(~amount=previewTotalCost, ~currency=settings.mainCurrency);
                                      <Text value={j| + $(cost)|j} style=Style.(combine(Styles.Text.listPrimary, style([opacity(0.65)]))) />
                                    }
                                  }
                                  | None => ReasonReact.null
                                }
                              }
                            </View>
                          </ListRow>
                          (switch (state.filter) {
                            | Unpaid => {
                              <Column>
                                <Divider />
                                <ListRow numberOfLines=1 onPress=Callback(() => send(ClearFilter)) flexRow=true style=Style.(style([justifyContent(`spaceBetween)]))>
                                  <ListPrimary style=Style.(style([Styles.Text.robotoMedium])) value="Showing unpaid expenses" />
                                  <ListPrimary>
                                    <RowTitle value="Clear" />
                                  </ListPrimary>
                                </ListRow>
                              </Column>
                            }
                            | NoCost => {
                              <Column>
                                <Divider />
                                <ListRow numberOfLines=1 onPress=Callback(() => send(ClearFilter)) flexRow=true style=Style.(style([justifyContent(`spaceBetween)]))>
                                  <ListPrimary style=Style.(style([Styles.Text.robotoMedium])) value="Showing expenses without cost" />
                                  <ListPrimary>
                                    <RowTitle value="Clear" />
                                  </ListPrimary>
                                </ListRow>
                              </Column>
                            }
                            | NoFilter => {
                              <Fragment>
                              {
                                if (numExpensesWithoutCost > 0) {
                                  let expenses = numExpensesWithoutCost == 1 ? "expense does" : "expenses do";
                                  <View>
                                    <Divider />
                                    <ListRow numberOfLines=1 onPress=Callback(() => send(FilterNoCost))>
                                      <Text
                                        style=Style.(
                                                combine(Styles.Text.listPrimary, style([color(Colors.danger)]))
                                              )
                                        value={j|$numExpensesWithoutCost $expenses not have cost assigned to them!|j}
                                      />
                                    </ListRow>
                                  </View>
                                } else {
                                  ReasonReact.null
                                }
                              }
                              {
                                if (numUnpaidExpenses > 0) {
                                  let expenses = numUnpaidExpenses == 1 ? "expense" : "expenses";
                                  <View>
                                    <Divider />
                                    <ListRow numberOfLines=1 onPress=Callback(() => send(FilterUnpaid))>
                                      <Text
                                        style=Style.(
                                                combine(Styles.Text.listPrimary, style([color(Colors.danger)]))
                                              )
                                        value={j|$numUnpaidExpenses $expenses are unpaid!|j}
                                      />
                                    </ListRow>
                                  </View>
                                } else {
                                  ReasonReact.null
                                }
                              }
                              </Fragment>
                            }
                          })
                        </View>
                      };
                      switch ((group, previewStuff)) {
                        | ([||], Some([])) | ([||], None) => {
                          <View key=title>
                            (header)
                            <ListSubheader value="No expenses" withBackground=true />
                          </View>
                        }
                        | _ => {
                          <ExpensesList
                            user
                            key=title
                            header
                            headerHeight=(48 + switch (state.filter) {
                              | NoFilter => (numExpensesWithoutCost > 0 ? 48 : 0) + (numUnpaidExpenses > 0 ? 48 : 0)
                              | NoCost => 48
                              | Unpaid => 48
                            })
                            onEditExpense
                            onEditRecurringExpense
                            expenses=(switch (state.filter) {
                              | NoFilter => group
                              | NoCost => group |> Js.Array.filter(expense => Belt.Option.isNone(expense.Expense.totalAmount))
                              | Unpaid => group |> Js.Array.filter(expense => expense.Expense.unpaid)
                            })
                            ?previewStuff
                            onCategoryPickerRequested
                          />
                        }
                      }
                    })
                  }
                </Swiper>
              }
            }) />
            /* <ExpenseDb.InitListener.Connect render=(initialized => {
              if (!initialized) {
                <ScreenActivityIndicator />
              } else {
                <C_Expenses render=(expenses => {
                  if (Array.length(expenses) === 0) {
                    <Text
                      style=Style.(
                              combine(
                                Styles.Text.subheading,
                                style([paddingTop(20.), textAlign(`center)])
                              )
                            )
                      value="You have not created any expenses yet"
                    />
                  } else {
                    let (firstDate, lastDate) = ExpenseUtils.getBoundsDates(expenses);
                    let budgetingPeriods = Utils.getBudgetingPeriods(~period=settings.budgetingPeriod, ~firstDate, ~lastDate, ());
                    let now = Js.Date.make();
                    let initialIndex = budgetingPeriods  |> Js.Array.findIndex(({ Utils.start, finish }) => {
                      now |> DateFns.isWithinRange(start, finish)
                    });
                    <Swiper index=initialIndex loop=false loadMinimal=true>
                      {
                        budgetingPeriods
                        |> Array.map(({ Utils.start, finish, title } as budgetingPeriod) => {
                          let group = expenses |> Js.Array.filter(expense =>
                            expense.Expense.date |> DateFns.isWithinRange(start, finish)
                          );
                          let now = Js.Date.make();
                          let preview = if (now |> DateFns.isAfter(finish)) {
                            None
                          } else if (now |> DateFns.isAfter(start)) {
                            Some((now, finish))
                          } else {
                            Some((start, finish))
                          };
                          let shouldShowPreview = settings.showProjectedRecurringExpenses;
                          let previewStuff = shouldShowPreview
                            ? preview |> C.Option.map(((startDate, endDate)) => {
                              RecurringExpenseDb.Generator.previewItems(startDate, endDate)
                            })
                            : None;
                          let totalGroupCost = Utils.getExpensesTotal((), ~userId, ~currency=settings.mainCurrency, ~expenses=group |> Array.to_list);
                          let numExpensesWithoutCost =
                            group
                            |> Js.Array.filter((expense) => Js.Option.isNone(expense.Expense.totalAmount))
                            |> Array.length;
                          let numUnpaidExpenses = group |> Js.Array.filter ((expense) => expense.Expense.unpaid) |> Array.length;
                          let header = {
                            <View>
                              <ListRow numberOfLines=1 flexRow=true style=Style.(style([justifyContent(`spaceBetween)]))
                                onPress=Callback(() => onPeriodSummaryRequested(budgetingPeriod))
                              >
                                <Text value={j|$(title)|j} style=Styles.Text.listPrimary />
                                <View style=Style.(style([flexDirection(`row), paddingLeft(12.)]))>
                                  <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=totalGroupCost) style=Styles.Text.listPrimary />
                                  {
                                    switch (previewStuff) {
                                      | Some(preview) => {
                                        let expenses = preview |> List.map(((expense, _ ,_)) => expense);
                                        let previewTotalCost = Utils.getExpensesTotal((), ~userId, ~currency=settings.mainCurrency, ~expenses);
                                        if (previewTotalCost == 0.) {
                                          ReasonReact.null
                                        } else {
                                          let cost = Currency.format(~amount=previewTotalCost, ~currency=settings.mainCurrency);
                                          <Text value={j| + $(cost)|j} style=Style.(combine(Styles.Text.listPrimary, style([opacity(0.65)]))) />
                                        }
                                      }
                                      | None => ReasonReact.null
                                    }
                                  }
                                </View>
                              </ListRow>
                              {
                                if (numExpensesWithoutCost > 0) {
                                  let expenses = numExpensesWithoutCost == 1 ? "expense does" : "expenses do";
                                  <View>
                                    <Divider />
                                    <ListRow numberOfLines=1>
                                      <Text
                                        style=Style.(
                                                combine(Styles.Text.listPrimary, style([color(Colors.danger)]))
                                              )
                                        value={j|$numExpensesWithoutCost $expenses not have cost assigned to them!|j}
                                      />
                                    </ListRow>
                                  </View>
                                } else {
                                  ReasonReact.null
                                }
                              }
                              {
                                if (numUnpaidExpenses > 0) {
                                  let expenses = numUnpaidExpenses == 1 ? "expense" : "expenses";
                                  <View>
                                    <Divider />
                                    <ListRow numberOfLines=1>
                                      <Text
                                        style=Style.(
                                                combine(Styles.Text.listPrimary, style([color(Colors.danger)]))
                                              )
                                        value={j|$numUnpaidExpenses $expenses are unpaid!|j}
                                      />
                                    </ListRow>
                                  </View>
                                } else {
                                  ReasonReact.null
                                }
                              }
                            </View>
                          };
                          switch ((group, previewStuff)) {
                            | ([||], Some([])) | ([||], None) => {
                              <View key=title>
                                (header)
                                <ListSubheader value="No expenses entered for this period" withBackground=true />
                              </View>
                            }
                            | _ => {
                              <ExpensesList
                                user
                                key=title
                                header
                                headerHeight=(48 + (numExpensesWithoutCost > 0 ? 48 : 0) + (numUnpaidExpenses > 0 ? 48 : 0))
                                onEditExpense
                                onEditRecurringExpense
                                expenses=group
                                ?previewStuff
                                onCategoryPickerRequested
                              />
                            }
                          }
                        })
                      }
                    </Swiper>
                  }
                }) />
              }
            }) /> */
          </ScreenWrap>
        </HelpWrapper>
      }) />
    }) />
  }
}
