open ReactNative;
open RnMui;

type t = {
  title: string,
  titleError: option(string),
  spending: SpendingForm.value,
  tagIds: list(Tag.id),
  date: Js.Date.t,
  unpaid: bool,
};

let initialFormState = (~date=?, ~categorization, ~totalAmount=?, ~currency=?, ()) => {
  title: "",
  titleError: None,
  spending: {
    categorization: categorization,
    totalAmount: totalAmount |> C.Option.map(NumberInput.toValue) |> Js.Option.getWithDefault(NumberInput.Empty),
    currency: currency |> Js.Option.getWithDefault(SettingsDb.get().mainCurrency)
  },
  tagIds: [],
  date: date |> Js.Option.getWithDefault(Js.Date.make()),
  unpaid: false,
};

let empty = {
  title: "",
  titleError: None,
  spending: {
    categorization: None,
    totalAmount: NumberInput.Empty,
    currency: SettingsDb.get().mainCurrency
  },
  tagIds: [],
  date: Js.Date.make(),
  unpaid: false,
};

type state = {
  costInputRef: ref(option(ReasonReact.reactRef)),
  descriptionInputRef: ref(option(ReasonReact.reactRef)),
  spendingFormRef: ref(option(ReasonReact.reactRef))
};

type actions =
  | FocusCostInputIfEmpty
  | FocusDescriptionInput
  | FocusDescriptionInputIfEmpty
  | SetCostInputRef(Js.Nullable.t(ReasonReact.reactRef))
  | SetDescriptionInputRef(Js.Nullable.t(ReasonReact.reactRef));

let component = ReasonReact.reducerComponent("ExpenseForm");

let setSpendingFormRef = (nullableRef, { ReasonReact.state }) => {
  state.spendingFormRef := nullableRef |> Js.Nullable.toOption
};

let make = (~expenseId=?, ~onCategoryPickerRequested, ~value, ~onValueChange, ~onSelectTags, ~onSplitFormRequested, _) => {
  ...component,
  initialState: () => {
    costInputRef: ref(None),
    descriptionInputRef: ref(None),
    spendingFormRef: ref(None)
  },
  didMount: ({ send }) => {
    Js.Global.setTimeout(() => {
      switch (value.spending.categorization) {
        | None | Some(Single(_)) => {
          send(FocusCostInputIfEmpty)
        }
        | Some(Split(_)) => {
          send(FocusDescriptionInputIfEmpty)
        }
      };
    }, 0) |> ignore;
  },
  reducer: (action, state) => switch action {
    | SetCostInputRef(ref) => ReasonReact.SideEffects(({ state }) => state.costInputRef := Js.Nullable.toOption(ref))
    | SetDescriptionInputRef(ref) => ReasonReact.SideEffects(({ state }) => state.descriptionInputRef := Js.Nullable.toOption(ref))
    | FocusCostInputIfEmpty => {
      switch (state.spendingFormRef.contents, value.spending.totalAmount) {
        | (Some(ref), Empty) => {
          Js.Global.setTimeout(() => {
            SpendingForm.focusCostInput(ref)
          }, 0) |> ignore;
        }
        | _ => ()
      };
      ReasonReact.NoUpdate
    }
    | FocusDescriptionInput =>
      ReasonReact.SideEffects(
        (
          (_) =>
            switch state.descriptionInputRef.contents {
            | None => ()
            | Some(ref) => {
              Js.Global.setTimeout(() => {
                ReasonReact.refToJsObj(ref)##focus()
              }, 0) |> ignore;
            }
            }
        )
      )
    | FocusDescriptionInputIfEmpty => {
      ReasonReact.SideEffects(
        (
          (_) =>
            switch state.descriptionInputRef.contents {
            | None => ()
            | Some(ref) => if(value.title === "") ReasonReact.refToJsObj(ref)##focus()
            }
        )
      )
    }
  },
  render: ({ handle, send }) => {
    <View>
      <SpendingForm
        ref=handle(setSpendingFormRef)
        value=value.spending
        onValueChanged=(spending => onValueChange({
          ...value,
          spending
        }))
        onCategoryPickerRequested
        onSplitFormRequested
        onSubmitEditingCost=(() => send(FocusDescriptionInput))
      />
      <ListRow>
        <TextField
          value=value.title
          onChangeText=((title) => {
            let history = HistoryDb.get();
            if (title == "feat  autosync") {
              HistoryDb.setItem({
                ...history,
                flagAutoSync: true
              });
              SnackbarService.show("Auto sync added");
            } else if (title == "feat  pastelcolors") {
              HistoryDb.setItem({
                ...history,
                flagPastelColors: true
              });
              SnackbarService.show("Pastel colors added");
            } else if (title == "feat  sharedexpenses") {
              HistoryDb.setItem({
                ...history,
                flagSharedExpenses: true
              });
              SnackbarService.show("Shared expenses added");
            } else if (title == "feat  themes") {
              HistoryDb.setItem({
                ...history,
                flagThemes: true
              });
              SnackbarService.show("Themes added");
            };
            onValueChange({
              ...value,
              title
            })
          })
          ref=((ref) => send(SetDescriptionInputRef(ref)))
          label="Description"
          multiline=true
          autoCapitalize="sentences"
          autoCorrect=true
          error=?value.titleError
        />
      </ListRow>
      <DatePickerRow label="Date" date=Some(value.date) onDateChanged=((date) => onValueChange({
        ...value,
        date
      })) />
      <TagsSelectRow
        selectedTagIds=value.tagIds
        onSelectTags
        onSelectedTagIdsUpdate=(
          (tagIds) => onValueChange({
            ...value,
            tagIds
          })
        )
      />
      <ListRow numberOfLines=1 onPress=Callback(() => onValueChange({
        ...value,
        unpaid: !value.unpaid
      }))>
        <Row alignItems=`center style=Style.(style([marginLeft(-8.)]))>
          <Paper.Checkbox
            checked=value.unpaid
          />
          <Text style=Styles.Text.listPrimary value="Unpaid" />
        </Row>
      </ListRow>
    </View>
  }
}
