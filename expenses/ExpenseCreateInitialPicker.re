/* open ReactNative;
open RnMui;

type state = {
  shouldDisappear: bool,
  opacity: Animated.Value.t,
  coords: option((Category.id, float, float)),
  selectedY: Animated.Value.t
};

type actions =
  | Disappear
  | SetStuff((Category.id, float, float));

let component = ReasonReact.reducerComponent("ExpenseCreateInitialPicker");

let make = (_) => {
  ...component,
  initialState: () => { shouldDisappear: false, opacity: Animated.Value.create(1.), coords: None, selectedY: Animated.Value.create(0.) },
  reducer: C.flip(state => fun
    | Disappear => ReasonReact.UpdateWithSideEffects({
      ...state,
      shouldDisappear: true
    }, (_) => {
      AnimatedRe.CompositeAnimation.start(
        AnimatedRe.Timing.animate(
          ~value=state.opacity,
          ~toValue=`raw(0.),
          ~duration=500.,
          ()
        ),
        ()
      )
    })
    | SetStuff((catId, x, y)) => {
      let selectedY = Animated.Value.create(y -. 60.5);
      ReasonReact.UpdateWithSideEffects({
        ...state,
        coords: Some((catId, x, y)),
        selectedY
      }, (_) => {
        AnimatedRe.CompositeAnimation.start(
          AnimatedRe.Timing.animate(
            ~value=state.opacity,
            ~toValue=`raw(0.),
            ~duration=150.,
            ()
          ),
          ()
        );
        AnimatedRe.CompositeAnimation.start(
          AnimatedRe.Timing.animate(
            ~value=selectedY,
            ~toValue=`raw(6.5),
            ~duration=350.,
            ~easing=Easing.inOut(Easing.ease),
            ()
          ),
          ()
        )
      })
    }
  ),
  render: ({ send, state: { shouldDisappear, opacity, coords, selectedY } }) => {
    <View style=Style.(style([position(`absolute), top(0.), left(0.)]))>
      <Swipeable.AnimatedView style={
        /* "position": "absolute",
        "zIndex": 1,
        "top": 0,
        "bottom": 0,
        "left": 0,
        "right": 0, */
        "minHeight": 603.5,
        "minWidth": 412,
        "opacity": opacity,
        "backgroundColor": "white",
        "flex": 1.
      }>
        /* <View style=Style.(style([position(`absolute), zIndex(1), top(0.), bottom(0.), left(0.), right(0.), minHeight(603.5), flex(1.), backgroundColor(Colors.white)]))> */
          <View style=Style.(style([flex(1.)]))>
            <CategorySelectPicker
              allowSplit=true onCreateCategory=((_) => Obj.magic()) onSplitSelected=((_) => ())
              onCategorySelected=((_) => ())
              onCategorySelectedWithRef=((catId, catRef) => {
                switch (catRef) {
                  | None => ()
                  | Some(catRef) => {
                    ReasonReact.refToJsObj(catRef)##measure((x, y, width, height, pageX, pageY) => {
                      /* Js.logMany([|"x", x, "y", y, "width", width, "height", height, "pageX", pageX, "pageY", pageY|]); */
                      send(SetStuff((catId, pageX, pageY)))
                    })
                  }
                }
              })
              selectedCategoryId=None
            />
          </View>
          <BottomNavigation active="Categories">
            <BottomNavigation.Action
              key="Categories"
              icon="folder"
              label="Categories"
              onPress=(() => ())
            />
            <BottomNavigation.Action
              key="Templates"
              icon="library-books"
              label="Templates"
              onPress=(() => ())
            />
            <BottomNavigation.Action
              key="Placeholder"
              icon="face"
              label="Placeholder"
              onPress=(() => ())
            />
          </BottomNavigation>
        /* </View> */
      </Swipeable.AnimatedView>
      (switch coords {
        | None => ReasonReact.null
        | Some((categoryId, _, _)) => {
          <Swipeable.AnimatedView style={
            "position": "absolute",
            "left": -0.5,
            "top": selectedY,
            "width": 412.,
            "height": 42.
          }>
            <C_Category categoryId render=(category => {
              let { ListRow.icon, color: iconColor } = CategoryStuff.getIconParams(category.id);
              let label = Utils.getCategoryPathString(~withOther=true, category.id);
              <CategorySelectPicker.PickerRow key=(Category.key(category.id))
                onPress=((_) => ())
                icon color=iconColor label
              />
            }) />
          </Swipeable.AnimatedView>
        }
      })
    </View>
  }
} */
