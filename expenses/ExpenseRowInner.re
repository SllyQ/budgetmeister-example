open ReactNative;

type retainedProps = {
  expense: Expense.t,
  tagIds: array(Tag.id),
  projected: bool
};

let component = ReasonReact.statelessComponentWithRetainedProps("ExpenseRowInner");

let styles =
  StyleSheet.create(
    Style.(
      {
        "costText": style([paddingRight(12.)])
      }
    )
  );


let make = (~expense: Expense.t, ~recurringExpense=?, ~onShowMissingCostModal=?, ~onEditExpense=?, ~onDeleteExpense=?, ~onMarkAsPaid=?, ~onEditRecurringExpense, ~tagIds, ~onCategoryPickerRequested, ~user, ~projected=false, _) => {
  ...component,
  retainedProps: {
    expense: expense,
    tagIds: tagIds,
    projected: projected
  },
  shouldUpdate: ({oldSelf}) => {
    oldSelf.retainedProps.expense != expense
    || oldSelf.retainedProps.tagIds != tagIds
    || oldSelf.retainedProps.projected != projected
  },
  render: (_) => {
    let (numberOfLines, dense) = switch (expense.title, (Array.length(tagIds) == 0 && Js.Option.isNone(recurringExpense))) {
      | (Some(_), true) => (2, true)
      | (None, true)  => (1, false)
      | (None, _) => (2, true)
      | (Some(_), _) => (2, false)
    };
    <ListRow
      wrapperStyle=?(projected ? Some(Style.(style([
        opacity(0.65)
      ]))) : None)
      leftIcon=(switch expense.categorization {
        | Some(Single(part)) => {
          let { ListRow.icon, color } = CategoryStuff.getIconParams(part.categoryId);
          <LeftIcon icon color />
        }
        | Some(Split(parts)) => {
          <SplitSpendingIcon parts />
        }
        | None => <UncategorizedIcon />
      })
      expandedContent=?(switch expense.categorization {
        | None => None
        | Some(Single(_)) => None
        | Some(Split(parts)) => Some({
          <SplitExpenseExpandedContent parts expenseCurrency=expense.currency />
        })
      })
      onPress=Actions({
        ListRow.actions: [||]
        |> (switch expense.categorization {
          | None => {
            C.flip(Array.append)([|{
              ListRow.text: "Assign a category",
              icon: Some("folder-plus"),
              onPress: () => {
                onCategoryPickerRequested(~allowSplit=true, ~excludedCategoryIds=[], ~onSplitSelected=None, ~onCategorySelected=(fun
                  | None => ()
                  | Some(categoryId) => {
                    A_Expense.updateCategorization((), ~id=expense.id, ~categorization=Single({ categoryId: categoryId }));
                  }
                ), None, ());
              }
            }|])
          }
          | Some(_) => C.id
        })
        |> (switch (Js.Option.isNone(expense.totalAmount), onShowMissingCostModal) {
          | (true, Some(onShowMissingCostModal)) => {
            C.flip(Array.append)([|{
              ListRow.text: "Enter missing cost",
              icon: None,
              onPress: onShowMissingCostModal
            }|])
          }
          | _ => C.id
        })
        |> (switch (expense.unpaid, onMarkAsPaid) {
          | (true, Some(onMarkAsPaid)) => {
            C.flip(Array.append)([|{
                        ListRow.text: "Mark as paid",
                        icon: None,
                        onPress: onMarkAsPaid
                      }|])
          }
          | _ => C.id
        })
        |> (switch recurringExpense {
          | Some(re) => {
            C.flip(Array.append)([|{
              ListRow.text: "Edit recurring expense",
              icon: None,
              onPress: () => onEditRecurringExpense(re.RecurringExpense.id)
            }|])
          }
          | None => C.id
        })
        |> (switch onEditExpense {
          | Some(onEditExpense) => {
            C.flip(Array.append)([|{
              ListRow.text: "Edit",
              icon: Some("pencil"),
              onPress: onEditExpense
            }|])
          }
          | None => C.id
        })
        |> (switch onDeleteExpense {
          | Some(onDeleteExpense) => {
            C.flip(Array.append)([|{
              ListRow.text: "Delete",
              icon: Some("delete"),
              onPress: onDeleteExpense
            }|])
          }
          | None => C.id
        })
      })
      numberOfLines
      dense
    >
      <View style=Style.(style([flex(1.), paddingVertical(Styles.Spacing.m)]))>
        <View style=Style.(style([flex(1.), flexDirection(`row), alignItems(`center)]))>
          <View style=Style.(style([flex(1.), justifyContent(`center)]))>
            <View
              style=Style.(
                      style([flexDirection(`row), alignItems(`center)])
                    )>
              (switch expense.categorization {
                | Some(Single(part)) => {
                  <Text
                    value=Utils.getCategoryPathString(part.categoryId)
                    numberOfLines=1
                    style=Style.(combine(Styles.Text.listPrimary, style([flexShrink(1.)])))
                  />
                }
                | Some(Split(parts)) => {
                  let categoriesStr = parts |> Utils.getSplitPartsTitleStr;
                  <Text
                    value=categoriesStr
                    numberOfLines=1
                    style=Style.(combine(Styles.Text.listPrimary, style([flexShrink(1.)])))
                  />
                }
                | None => <Text
                  value="Uncategorized"
                  numberOfLines=1
                  style=Style.(combine(Styles.Text.listPrimary, style([flexShrink(1.), color(Colors.danger)])))
                />
              })
            </View>
            (
              switch expense.title {
              | None => ReasonReact.null
              | Some(title) =>
                <Text
                  style=Styles.Text.listSecondary
                  value=title
                  numberOfLines=1
                />
              }
            )
            <ExpenseRowTags tagIds recurring=Js.Option.isSome(recurringExpense) />
          </View>
          <ExpenseRowCost user expense />
        </View>
      </View>
    </ListRow>
  }
}
