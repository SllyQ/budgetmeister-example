open ReactNative;

let component = ReasonReact.statelessComponent("BudgetNotifications");

let make = (_) => {
  ...component,
  render: (_) => {
    <C_Settings render=(settings => {
      <C_Expenses render=((expenses) => {
        <C_Budget render=((budget) => {
          <Connect_ActiveUser render=(user => {
            let userId = user |> C.Option.map(user => user.User.id);
            let rez = Js.Dict.entries(budget.categoryMonthlyBudgets)
            |> C.Array.mapFilter(((catId, budget)) => {
              switch budget {
                | Some(b) => Some((catId, b))
                | None => None
              }
            })
            |> C.Array.mapFilter(((catId, budget)) => {
              let catCost = expenses
              |> Array.map(Utils.getExpenseBudgetCost(~categoryId=catId, ~currency=settings.mainCurrency, ~userId))
              |> Array.fold_left((+.), 0.);
              catCost > budget ? Some((catId, catCost, budget)) : None;
            });
            <View>
              (rez
              |> Array.map(((categoryId, catCost, budget)) => {
                let catCostAmount = catCost |> Cost.format;
                <C_Category key=(Category.key(categoryId)) categoryId render=((category) => {
                  let title = category.title;
                  <View>
                    <Divider />
                    <ListRow numberOfLines=1 flexRow=true>
                      <Text style=Styles.Text.listPrimary value={j|$(title) is over budget for this month ($(catCostAmount)/$(budget))|j} />
                    </ListRow>
                  </View>
                }) />
              })
              |> ReasonReact.array)
            </View>
          }) />
        }) />
      })/>
    }) />
  }
}
