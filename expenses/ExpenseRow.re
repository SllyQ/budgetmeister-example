open ReactNative;

type retainedProps = {expense: Expense.t};

type state = {
  showMissingCostEnterModal: bool
};

type actions =
  | ShowMissingCostEnterModal
  | HideMissingCostEnterModal
  | EnterMissingCosts(float, Currency.t);

let component = ReasonReact.reducerComponentWithRetainedProps("ExpenseRow");

let make = (~expense: Expense.t, ~onEditExpense, ~onEditRecurringExpense, ~onCategoryPickerRequested, ~user, _) => {
  let deleteExpense = (_, _) => {
    A_Expense.delete(expense.id)
  };
  let editExpense = (_, _) => onEditExpense(expense.id);
  let markAsPaid = (_, _) => {
    A_Expense.update(~id=expense.id, ~unpaid=false, ()) |> ignore
  };
  let editRecurringExpense = (id, _) => onEditRecurringExpense(id);

  {
    ...component,
    initialState: () => { showMissingCostEnterModal: false },
    reducer: (action, _state) => switch action {
      | ShowMissingCostEnterModal => ReasonReact.Update {
        showMissingCostEnterModal: true
      }
      | HideMissingCostEnterModal => ReasonReact.Update {
        showMissingCostEnterModal: false
      }
      | EnterMissingCosts(totalAmount, currency) => ReasonReact.UpdateWithSideEffects({
        showMissingCostEnterModal: false
      }, (_) => {
        A_Expense.update(~id=expense.id, ~totalAmount=Some(totalAmount), ~currency, ()) |> ignore
      })
    },
    retainedProps: {expense: expense},
    shouldUpdate: ({oldSelf, newSelf}) => {
      oldSelf.retainedProps.expense !== expense
      || oldSelf.state.showMissingCostEnterModal !== newSelf.state.showMissingCostEnterModal;
    },
    render: ({handle, send, state: { showMissingCostEnterModal }}) => {
      <C_ExpenseGenerator expenseId=expense.id render=(generatorData => {
        <C_RecurringExpense recurringExpenseId=?(generatorData |> C.Option.map(gd => gd.Entries.id)) render=(recurringExpense => {
          <View>
            <ExpenseMissingCostsModal
              onCloseRequested=(() => send(HideMissingCostEnterModal))
              onCostEntered=((totalAmount, currency) => send(EnterMissingCosts(totalAmount, currency)))
              isVisible=showMissingCostEnterModal
            />
            <C_ExpenseTagIds expenseId=expense.id render=(tagIds => {
              <ExpenseRowInner
                user
                expense
                tagIds
                ?recurringExpense
                onEditExpense=handle(editExpense)
                onDeleteExpense=handle(deleteExpense)
                onShowMissingCostModal=(() => send(ShowMissingCostEnterModal))
                onMarkAsPaid=handle(markAsPaid)
                onEditRecurringExpense=(reid => handle(editRecurringExpense, reid))
                onCategoryPickerRequested
              />
            }) />
          </View>
        }) />
      }) />
    }
  }
};
