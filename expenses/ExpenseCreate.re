open ReactNative;
open Paper;

[@bs.module "../../../../src/renderHeader.js"] external renderHeader : ('a, 'b) => 'c =
  "renderHeader";

type tab =
  | Categories
  | Templates;

type expenseType =
  | Simple
  | Recurring;

type sharingType =
  | Personal
  | Shared;


type state = {
  expenseType,
  formValue: ExpenseForm.t,
  initialCategorySelected: bool,
  selectedTab: tab,
  recurringConfig: RecurringForm.t,
  shareConfig: option(ShareConfig.t)
};

let getSharing = (state) => switch (state.shareConfig, state.formValue.spending.totalAmount) {
  | (Some(shareConfig), Number(totalAmount)) => {
    Some({
      RParams.totalAmount,
      currency: state.formValue.spending.currency,
      shareConfig: shareConfig
    })
  }
  | _ => None
};

type actions =
  | SelectExpenseType(expenseType)
  | SplitCost
  | SplitSelected(ExpenseSplitForm.value)
  | CreateExpense
  | CreateTemplate
  | OpenSharingForm
  | UpdateSharing(RParams.sharing)
  | SelectInitialCategory(Category.id)
  | SelectTemplate(Template.id)
  | UpdateFormValue(ExpenseForm.t)
  | UpdateRecurringConfig(RecurringForm.t)
  | SelectTab(tab);

let component = ReasonReact.reducerComponent("ExpenseCreate");

let make =
    (
      ~onExpenseCreated,
      ~onCreateCategory,
      ~onCreateTemplate,
      ~onCategoryPickerRequested,
      ~onSharingFormRequested,
      ~onSelectTags,
      ~onSplitFormRequested,
      ~initialExpenseDate=?,
      _
    ) => {
  ...component,
  initialState: () => {
    let settings = SettingsDb.get();
    let selectedTagIds = settings.tripModeActive ? settings.tripMode.tagIds |> Array.to_list : [];
    let mainCurrency = settings.mainCurrency;
    let currency = (settings.tripModeActive ? settings.tripMode.currency : None) |> Js.Option.getWithDefault(mainCurrency);
    {
      expenseType: Simple,
      formValue: {
        ...ExpenseForm.empty,
        spending: {
          ...ExpenseForm.empty.spending,
          currency
        },
        date: initialExpenseDate |> Js.Option.getWithDefault(ExpenseForm.empty.date),
        tagIds: selectedTagIds
      },
      initialCategorySelected: false,
      selectedTab: Categories,
      recurringConfig: {
        repeatFrom: Js.Date.make(),
        repeatUntil: None,
        repeatConfig: RepeatConfigForm.defaultMonthlyConfig
      },
      shareConfig: None
    }
  },
  didMount: (_) => {
    Mixpanel.track(EventConsts.expenseCreationStarted);
    Mixpanel.increment(EventConsts.expenseCreationStarted);
  },
  reducer: (action, state) =>
    switch action {
    | SelectExpenseType(expenseType) => ReasonReact.Update {
      ...state,
      expenseType,
    }
    | SplitCost => ReasonReact.SideEffects(({ send }) => {
      let currentParts = switch (state.formValue.ExpenseForm.spending.categorization) {
        | Some(Single(part)) => [|{ Categorization.categoryId: part.categoryId, amount: state.formValue.spending.totalAmount |> NumberInput.toNumber, description: None }|]
        | Some(Split(parts)) => parts
        | None => [||]
      };
      onSplitFormRequested(Some(currentParts), parts => send(SplitSelected(parts)))
    })
    | SplitSelected({ totalAmount, currency, categorization }) => {
      ReasonReact.Update {
        ...state,
        formValue: {
          ...state.formValue,
          spending: {
            totalAmount: totalAmount |> NumberInput.toValue,
            currency,
            categorization: Some(categorization)
          },
        }
      }
    }
    | UpdateFormValue(formValue) => ReasonReact.Update {
      ...state,
      formValue
    }
    | UpdateRecurringConfig(recurringConfig) => ReasonReact.Update {
      ...state,
      recurringConfig
    }
    | CreateExpense =>
      switch state.shareConfig {
        | None => Js.Result.Ok(None)
        | Some(shareConfig) => {
          Js.Result.Ok(Some(shareConfig))
        }
      } |> C.Result.map((sharingConfig) => {
        switch state.expenseType {
          | Simple => {
            ReasonReact.SideEffects((_) => {
              let { ExpenseForm.tagIds, title, spending, date, unpaid } = state.formValue;
              switch (spending.totalAmount, spending.categorization) {
              | (NumberInput.Invalid(_), _) => Alert.alert(~title="Error", ~message="Total amount is invalid", ())
              | (_, None) => Alert.alert(~title="Error", ~message="Category is required", ())
              | (_, Some(categorization)) => {
                let fixedTitle = title == "" ? None : Some(title |> String.trim);
                A_Expense.create((), ~title=?fixedTitle, ~date, ~tagIds=tagIds |> Array.of_list,
                  ~totalAmount=(spending.totalAmount |> NumberInput.toNumber), ~categorization, ~currency=spending.currency, ~unpaid, ~sharing=?sharingConfig)
                |> (fun
                  | ValidationError(_) => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
                  | Created(_) => {
                    HistoryDb.currencyUsed(spending.currency);
                    Mixpanel.track(EventConsts.expenseCreated);
                    Mixpanel.increment(EventConsts.expenseCreated);
                    onExpenseCreated()
                  }
                );
              }
              };
            })
          }
          | Recurring => {
            ReasonReact.SideEffects((_) => {
              let { ExpenseForm.tagIds, title, spending, unpaid } = state.formValue;
              let { RecurringForm.repeatFrom, repeatUntil, repeatConfig } = state.recurringConfig;
              switch (title, spending.totalAmount, spending.categorization) {
              | ("", _, _) => Alert.alert(~title="Error", ~message="Description is required for recurring expense", ())
              | (_, NumberInput.Invalid(_), _) => Alert.alert(~title="Error", ~message="Total amount is invalid", ())
              | (_, _, None) => Alert.alert(~title="Error", ~message="Category is required", ())
              | (_, _, Some(categorization)) => {
                A_RecurringExpense.create((), ~title=title |> String.trim, ~repeatFrom, ~repeatUntil, ~tagIds=tagIds |> Array.of_list,
                  ~repeatConfig, ~totalAmount=(spending.totalAmount |> NumberInput.toNumber), ~categorization, ~currency=spending.currency, ~unpaid)
                |> (fun
                  | ValidationError(_) => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
                  | Created(_) => {
                    Mixpanel.increment(EventConsts.recurringExpenseCreated);
                    onExpenseCreated()
                  }
                );
              }
              }
            })
          }
        }
      }) |> (fun
        | Ok(a) => a
        | Error(e) => {
          ReasonReact.SideEffects((_) => {
            Alert.alert(~title="Error", ~message=e, ());
          })
        }
      );
    | OpenSharingForm => ReasonReact.SideEffects(({ send }) => {
      let sharing = getSharing(state);
      onSharingFormRequested(sharing, (sharing) => {
        send(UpdateSharing(sharing))
      });
    })
    | UpdateSharing(sharing) => ReasonReact.Update {
      ...state,
      shareConfig: Some(sharing.shareConfig),
      formValue: {
        ...state.formValue,
        spending: {
          ...state.formValue.spending,
          currency: sharing.currency,
          totalAmount: sharing.totalAmount |> NumberInput.toValue
        }
      }
    }
    | CreateTemplate =>
      ReasonReact.SideEffects(
        (
          ({send}) => {
            let _ =
              onCreateTemplate()
              |> Js.Promise.then_(
                   (optTemplateId) => {
                     switch optTemplateId {
                     | None => ()
                     | Some(tid) => send(SelectTemplate(tid))
                     };
                     Js.Promise.resolve()
                   }
                 );
            ()
          }
        )
      )
    | SelectInitialCategory(categoryId) => ReasonReact.Update {
      ...state,
      initialCategorySelected: true,
      formValue: {
        ...state.formValue,
        spending: {
          ...state.formValue.spending,
          categorization: Some(Single({ categoryId: categoryId }))
        }
      }
    }
    | SelectTemplate(templateId) => {
      let optTemplate = TemplateDb.Data.get(templateId);
      switch optTemplate {
      | None => ReasonReact.NoUpdate
      | Some(template) => {
        let {Template.autoCreate, totalAmount, currency, categorization, defaultTitle} = template;
        let settings = SettingsDb.get();
        let selectedTagIds = settings.tripModeActive ? settings.tripMode.tagIds |> Array.to_list : [];
        if (autoCreate) {
          ReasonReact.SideEffects(
            (
              (_) => {
                A_Expense.create(
                  ~date=Js.Date.make(),
                  ~totalAmount,
                  ~currency,
                  ~categorization?,
                  ~tagIds=selectedTagIds |> Array.of_list,
                  ~title=?defaultTitle,
                  ()
                ) |> (fun
                  | Created(_) => {
                    Mixpanel.track(EventConsts.expenseCreated);
                    Mixpanel.increment(EventConsts.expenseCreated);
                    onExpenseCreated()
                  }
                  | _ => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
                );
              }
            )
          )
        } else {
          ReasonReact.Update {
            ...state,
            initialCategorySelected: true,
            formValue: {
              ...state.formValue,
              spending: {
                totalAmount: totalAmount |> NumberInput.optToValue,
                categorization,
                currency
              },
              title: defaultTitle |> Js.Option.getWithDefault(""),
            }
          }
        }
      }}
    }
    | SelectTab(tab) => ReasonReact.Update({ ...state, selectedTab: tab })
    },
  render: ({state, send}) => {
    let { expenseType, formValue, recurringConfig, initialCategorySelected } = state;
    <Connect_ActiveUser render=(user => {
      let alpha = user |> C.Option.map(User.isAlpha) |> Js.Option.getWithDefault(false);
      if (!initialCategorySelected) {
        <ScreenWrap
          key="initial select"
          scrollable=false
        >
          (switch state.selectedTab {
            | Categories =>
              <CategorySelectPicker
                sharing=?getSharing(state)
                onSharedSelected=?(alpha ? Some((() => send(OpenSharingForm))) : None)
                allowSplit=true onCreateCategory onSplitSelected=(() => send(SplitCost))
                onCategorySelected=(categoryId => send(SelectInitialCategory(categoryId))) selectedCategoryId=None
                onGoBack=(() => onExpenseCreated())
              />
            | Templates =>
              <TemplatePicker
                onTemplateSelected=((templateId) => send(SelectTemplate(templateId)))
                onTemplateCreateRequested=((_) => send(CreateTemplate))
              />
          })
          <BottomNav.Container>
            <BottomNav.Tab
              label="Categories"
              icon="folder"
              active=(state.selectedTab == Categories)
              onPress=(() => send(SelectTab(Categories)))
            />
            <BottomNav.Tab
              label="Templates"
              icon="library-books"
              active=(state.selectedTab == Templates)
              onPress=(() => send(SelectTab(Templates)))
            />
          </BottomNav.Container>
        </ScreenWrap>
      } else {
        /* Keys required for different help components */
        <ScreenWrap
          contentContainerStyle=Style.(style([minHeight(603.5)]))
          key="create form"
          fab=(
            <ThemeActionButton icon="save" onPress=((_) => send(CreateExpense)) />
          )
        >
          <ListWrapper>
            <ExpenseForm
              onSplitFormRequested
              onCategoryPickerRequested
              onSelectTags
              value=formValue
              onValueChange=((formValue) => send(UpdateFormValue(formValue)))
            />
            <ListSubheader primary=true value="Expense type" withDivider=true />
            <Column>
              <ListRow numberOfLines=1>
                <Row justifyContent=`spaceAround>
                  <Column flex=1.>
                    <TouchableRipple onPress=(() => send(SelectExpenseType(Simple)))>
                      <Row alignItems=`center style=Style.(style([paddingHorizontal(Styles.Spacing.m), paddingVertical(Styles.Spacing.s)]))>
                        <Paper.RadioButton
                          value="Test"
                          checked=(expenseType == Simple)
                        />
                        <Text style=Styles.Text.listPrimary value="Simple" />
                      </Row>
                    </TouchableRipple>
                  </Column>
                  <Column flex=1.>
                    <TouchableRipple onPress=(() => send(SelectExpenseType(Recurring)))>
                      <Row alignItems=`center style=Style.(style([paddingHorizontal(Styles.Spacing.m), paddingVertical(Styles.Spacing.s)]))>
                        <Paper.RadioButton
                          value="Test"
                          checked=(state.expenseType == Recurring)
                        />
                        <Text style=Styles.Text.listPrimary value="Recurring" />
                      </Row>
                    </TouchableRipple>
                  </Column>
                </Row>
              </ListRow>
            </Column>
          </ListWrapper>
          (switch expenseType {
            | Recurring => {
              <Column>
                <ListSubheader value="Recurring options" primary=true withDivider=true />
                <RecurringForm value=recurringConfig onValueChanged=(recurringConfig => send(UpdateRecurringConfig(recurringConfig))) />
              </Column>
            }
            | Simple => ReasonReact.null
          })
          /* <ExpenseCreateInitialPicker /> */
        </ScreenWrap>
      }
    }) />

  }
};
