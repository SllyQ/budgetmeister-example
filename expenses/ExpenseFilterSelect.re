open RnMui;

type state = {
  categoryFilter: option(Category.id),
  tagFilter: option(Tag.id),
  timeRangeFilter: TimeRange.t
};

type actions =
  | UpdateCategoryFilter(option(Category.id))
  | UpdateTagFilter(option(Tag.id))
  | UpdateTimeRangeFilter(TimeRange.t)
  | UpdateFilters;

let component = ReasonReact.reducerComponent("ExpensesFilterSelect");

let make = (_, ~categoryFilter, ~tagFilter, ~timeRangeFilter, ~onFiltersSelected) => {
  ...component,
  initialState: () => {categoryFilter, tagFilter, timeRangeFilter},
  reducer: (action, state) =>
    switch action {
    | UpdateCategoryFilter(categoryFilter) => ReasonReact.Update({...state, categoryFilter})
    | UpdateTagFilter(tagFilter) => ReasonReact.Update({...state, tagFilter})
    | UpdateTimeRangeFilter(timeRangeFilter) => ReasonReact.Update({...state, timeRangeFilter})
    | UpdateFilters =>
      ReasonReact.SideEffects(
        ((_) => onFiltersSelected(state.categoryFilter, state.tagFilter, state.timeRangeFilter))
      )
    },
  render: ({send, state: {categoryFilter}}) =>
    <ScreenWrap scrollable=true>
      <CategorySelectDropdown
        allowNull=true
        parentCategoryId=None
        selectedCategoryId=categoryFilter
        onCategorySelected=((cid) => send(UpdateCategoryFilter(cid)))
        label="Show category"
        nullOptionLabel="All"
      />
      /* <TimeRangePicker selectedRange=timeRangeFilter
           onRangeSelected=(reduce (fun range => UpdateTimeRangeFilter range))
         /> */
      <MuiButton text="Select" primary=true raised=true onPress=((_) => send(UpdateFilters)) />
    </ScreenWrap>
};
