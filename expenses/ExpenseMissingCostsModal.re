open ReactNative;
open RnMui;

type state = {
  amount: NumberInput.value,
  currency: Currency.t,
  costInputRef: ref(option(ReasonReact.reactRef))
};

type actions =
  | UpdateAmount(NumberInput.value)
  | UpdateCurrency(Currency.t)
  | SubmitCost(float, Currency.t);

type retainedProps = {
  isVisible: bool
};

let component = ReasonReact.reducerComponentWithRetainedProps("ExpenseMissingCostsModal");

let setCostInputRef = (ref, { ReasonReact.state }) => {
  state.costInputRef := ref |> Js.Nullable.toOption
};

let focustCostInputRef = ((), { ReasonReact.state }) => {
  switch (state.costInputRef.contents) {
    | Some(ref) => CostInput.focus(ref)
    | None => ()
  }
};

let make = (~isVisible, ~onCloseRequested, ~onCostEntered, _) => {
  ...component,
  retainedProps: {
    isVisible: isVisible
  },
  initialState: () => {
    currency: SettingsDb.get().mainCurrency,
    amount: Empty,
    costInputRef: ref(None)
  },
  willReceiveProps: ({ handle, retainedProps, state }) => {
    if (!retainedProps.isVisible && isVisible) {
      C.doubleDebounce(handle(focustCostInputRef))
    };
    state
  },
  reducer: (action, state) => switch action {
    | UpdateAmount(amount) => ReasonReact.Update { ...state, amount }
    | UpdateCurrency(currency) => ReasonReact.Update { ...state, currency }
    | SubmitCost(amount, currency) => ReasonReact.UpdateWithSideEffects({
      ...state,
      currency: SettingsDb.get().mainCurrency,
      amount: Empty
    }, (_) => {
      onCostEntered(amount, currency)
    })
  },
  render: ({ handle, state: { amount, currency }, send }) => {
    let amountNum = NumberInput.toNumber(amount);
    <RnModal isVisible>
      <View style=Style.(style([padding(Styles.Spacing.xl), backgroundColor("white")]))>
        <CostInput ref=handle(setCostInputRef) label="Cost"
          amount currency
          onAmountChanged=(amount => send(UpdateAmount(amount)))
          onCurrencyChanged=(currency => send(UpdateCurrency(currency)))
          onSubmitEditing=(() => switch amountNum {
            | Some(amountNum) => send(SubmitCost(amountNum, currency))
            | None => ()
          })
        />
        <View style=Style.(style([flexDirection(`row), justifyContent(`flexEnd)]))>
          <MuiButton text="Cancel" onPress=onCloseRequested />
          (switch amountNum {
            | None => <MuiButton primary=true text="Submit" disabled=true onPress=(() => ()) />
            | Some(amountNum) => <MuiButton primary=true text="Submit" onPress=(() => send(SubmitCost(amountNum, currency))) />
          })
        </View>
      </View>
    </RnModal>
  }
}
