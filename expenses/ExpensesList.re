open ReactNative;

let component = ReasonReact.statelessComponent("ExpensesList");

type exp =
  | Entered(Expense.t)
  | Projected((Expense.t, RecurringExpense.t, array(Tag.id)));

let gd = fun
  | Entered(exp) => exp.date
  | Projected((exp, _, _)) => exp.date;

let ge = fun
  | Entered(exp) => exp
  | Projected((exp, _, _)) => exp;


let groupExpensesByDay = (expenses) => {
  let sortedExpenses =
    expenses |> Common.iSort((sp1, sp2) => DateFns.compareDesc(gd(sp1), gd(sp2)));
  let groupedExpenses = [||];
  let lastExpense: ref(option(Expense.t)) = ref(None);
  /* currIndex will be increased to 0 on first iteration */
  let currIndex = ref((-1));
  sortedExpenses
  |> Array.iter(
       (expense: exp) => {
         switch lastExpense^ {
         | Some(s) when Js.Date.getDate(s.date) === Js.Date.getDate(gd(expense)) =>
           groupedExpenses[currIndex^] |> Js.Array.push(expense) |> ignore
         | Some(_)
         | None =>
           currIndex := currIndex^ + 1;
           groupedExpenses |> Js.Array.push([|expense|]) |> ignore
         };
         lastExpense := Some(ge(expense))
       }
     );
  groupedExpenses
};


let make = (~expenses, ~previewStuff=[], ~header, ~headerHeight, ~onEditExpense, ~onEditRecurringExpense, ~onCategoryPickerRequested, ~user, _) => {
  ...component,
  render: (_) => {
    let allExpenses = expenses |> Array.map(exp => Entered(exp))
      |> Array.append(previewStuff |> List.map(exp => Projected(exp)) |> Array.of_list);
    let formatDate = (date) => {
      let dateString = DateFns.format("MMM Do", date);
      switch date {
      | d when DateFns.isToday(d) => {j|Today ($dateString)|j}
      | d when DateFns.isYesterday(d) => {j|Yesterday ($(dateString))|j}
      | d when DateFns.isThisISOWeek(d) =>
        DateFns.format("dddd", d) ++ " (" ++ dateString ++ ")"
      | _ => dateString
      }
    };

    let sections =
      groupExpensesByDay(allExpenses)
      |> Array.map(
           (group) =>
             SectionList.section(~data=group, ~key=formatDate(gd(group[0])), ())
         )
      |> SectionList.sections;
    <Connect_ActiveUser render=(user => {
      let userId = user |> C.Option.map(user => user.User.id);
      <C_Settings render=(settings => {
        <SectionList
          keyboardShouldPersistTaps=`handled
          listHeaderComponent=header
          listFooterComponent=(<View style=Style.(style([height(80.)])) />)
          initialNumToRender=12
          windowSize=9.
          sections
          getItemLayout=(
            (data, index) => {
              let currInd = ref(0);
              let offset = ref(headerHeight);
              let lastElement = ref(`Footer);
              let sectionHeaderHeight = 40;
              let dividerHeight = 1;
              let sectionFooterHeight = 0;
              let twoLineItemHeight = 60;
              let oneLineItemHeight = 48;
              let currItemHeight = ref(0);
              let element = ref(None);
              data
              |> Array.iter(
                   (dd) => {
                     if (currInd^ < index) {
                       offset := offset^ + sectionHeaderHeight
                     } else if (currInd^ === index) {
                       lastElement := `Header;
                     };
                     currInd := currInd^ + 1;
                     dd##data
                     |> Array.iteri(
                          (ind, item: Expense.t) => {
                            let extraHeight = if (Array.length(dd##data) !== ind + 1) {
                              dividerHeight
                            } else 0;
                            let itemHeight = if (Js.Option.isNone(item.title)) {
                              oneLineItemHeight + extraHeight
                            } else {
                              twoLineItemHeight + extraHeight
                            };
                            if (currInd^ < index) {
                              offset := offset.contents + itemHeight;
                            } else if (currInd^ === index) {
                              lastElement := `Element;
                              currItemHeight := itemHeight;
                              element := Some(item);
                            };
                            currInd := currInd^ + 1
                          }
                        );
                      if (currInd.contents !== index) {
                        offset := offset.contents + sectionFooterHeight;
                        currInd := currInd.contents + 1;
                      }
                   }
                 );
              let length = switch lastElement.contents {
                | `Header => sectionHeaderHeight
                | `Element => currItemHeight.contents
                | `Footer => sectionFooterHeight
              };
              {"length": length, "offset": offset^, "index": index}
            }
          )
          updateCellsBatchingPeriod=400
          maxToRenderPerBatch=6
          renderSectionHeader=(
            (param) => {
              let expenses: array(exp) = Obj.magic(param##section)##data;
              let dayCost = expenses
              |> Array.map(ge)
              |> Array.to_list
              |> expenses => Utils.getExpensesTotal((), ~userId, ~expenses, ~currency=settings.mainCurrency);
              <C_Settings render=((settings) => {
                <ListSubheader value=Obj.magic(param##section)##key withBackground=true
                  rightValue=?(settings.showExpensesDailyTotals
                    ? Some(Text(Currency.format(~currency=settings.mainCurrency, ~amount=dayCost)))
                    : None
                  )
                />
              }) />
            }
          )
          itemSeparatorComponent=SectionList.separatorComponent((_) => <Divider light=true />)
          renderItem=(
            SectionList.renderItem(
              ({item: (exp: exp)}) => {
                switch exp {
                  | Entered(expense) => {
                    <ExpenseRow
                      expense
                      onEditExpense
                      onEditRecurringExpense
                      user
                      onCategoryPickerRequested
                    />
                  }
                  | Projected((expense, recurringExpense, tagIds)) => {
                    <ExpenseRowInner
                      expense
                      recurringExpense
                      onEditRecurringExpense
                      tagIds
                      projected=true
                      user
                      onCategoryPickerRequested
                    />
                  }
                }
              }
            )
          )
          keyExtractor=((expense, _i) => DbExpense.key(ge(expense).id))
        />
      }) />
    }) />
  }
}
