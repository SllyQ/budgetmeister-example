open ReactNative;

let component = ReasonReact.statelessComponent("SplitExpenseExpandedContent");

let make = (~parts, ~expenseCurrency, _) => {
  ...component,
  render: (_) => {
    <View style=Style.(style([paddingBottom(6.)]))>
      (parts
        |> Common.iSort((p1, p2) => compare(p1, p2) * -1)
        |> Array.map((part: Categorization.part) => {
          let category = CategoryDb.get(~id=part.categoryId);
          switch category {
            | None => ReasonReact.null
            | Some(cat) => {
              let { ListRow.icon, color: iconColor } = CategoryStuff.getIconParams(part.categoryId);
              <View key=Category.key(cat.id) style=Style.(style([flexDirection(`row), alignItems(`center)]))>
                <View style=Style.(style([paddingRight(Styles.Spacing.s), paddingTop(1.)]))>
                  <LeftIcon icon color=iconColor size=12. />
                </View>
                <Text style=Styles.Text.body1
                  value=(
                    part.amount
                    |> C.Option.map(amount => Currency.format(~amount=amount, ~currency=expenseCurrency))
                    |> Js.Option.getWithDefault("Cost missing")
                  )
                />
                (switch part.description {
                  | None => ReasonReact.null
                  | Some(description) => {
                    <Text style=Style.(combine(Styles.Text.body1, style([color(Colors.grey)]))) value={j| ($(description))|j} numberOfLines=1 />
                  }
                })
              </View>
            }
          }
        })
        |> ReasonReact.array
      )
    </View>
  }
}
