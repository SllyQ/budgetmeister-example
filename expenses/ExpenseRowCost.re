open ReactNative;

let component = ReasonReact.statelessComponent("ExpenseRow");

let make = (~expense: Expense.t, ~user, _) => {
  ...component,
  render: (_) => {
    <C_Settings render=(settings => {
      (switch (expense |> Expense.getTotalAmount(~userId=?user |> C.Option.map(user => user.User.id), ~currency=settings.mainCurrency)) {
        | None => {
          <View style=Style.(style([alignItems(`center)]))>
            <Text value="Cost"
              style=Style.(combine(Styles.Text.subheading, style([color(Colors.danger)])))
            />
            <Text value="missing"
              style=Style.(combine(Styles.Text.subheading, style([marginTop(-6.), color(Colors.danger)])))
            />
          </View>
        }
        | Some(totalAmount) => {
            <View style=Style.(style([alignItems(`flexEnd), paddingLeft(Styles.Spacing.m)]))>
              <Text
                style=Style.(combine(
                  Styles.Text.listPrimary,
                  expense.unpaid ? style([color(Colors.danger)]) : style([])
                ))
                value=Currency.format(~currency=settings.mainCurrency, ~amount=totalAmount)
              />
              {
                let currency = expense.currency;
                if (settings.mainCurrency != currency) {
                  <Text style=Styles.Text.listSecondary
                    value=(
                      Currency.convert(~from=settings.mainCurrency, ~to_=expense.currency, totalAmount)
                      |> amount => Currency.format(~currency=expense.currency, ~amount)
                    )
                  />
                } else {
                  ReasonReact.null
                }
              }
            </View>
        }
      })
    }) />
  }
}
