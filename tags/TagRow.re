open ReactNative;

let component = ReasonReact.statelessComponent("TagRow");

let styles =
  StyleSheet.create(
    Style.(
      {
        "tagRow":
          style([
            flexDirection(`row),
            alignItems(`center),
            widthPct(100.),
            paddingTop(4.),
            paddingBottom(4.),
            minHeight(35.),
            paddingLeft(15.),
            paddingRight(15.)
          ])
      }
    )
  );

let make = (~tagId, ~onEditTag, ~onDeleteTag, ~onTagExpensesRequested, _) => {
  let deleteTag = (_, _) => onDeleteTag(tagId);
  {
    ...component,
    render: ({handle}) =>
      <C_Tag
        tagId
        render=(
          (tag) => {
            let hasIcon = (switch (tag.color, tag.icon) {
              | (None, None) => false
              | _ => true
            });
            <ListRow numberOfLines=1
              onPress=(Actions({
                actions: [|{
                  text: "Show expenses",
                  icon: Some("view-list"),
                  onPress: () => onTagExpensesRequested(tag.id)
                }, {
                  text: "Edit",
                  icon: Some("pencil"),
                  onPress: () => onEditTag(tagId)
                }, {
                  text: "Delete",
                  icon: Some("delete"),
                  onPress: handle(deleteTag)
                }|]
              }))
            >
              <View style=Style.(style([
                flex(1.),
                widthPct(100.),
                flexDirection(`row),
                alignItems(`center)
              ]))>
                (switch (tag.color, tag.icon) {
                  | (None, None) => ReasonReact.null
                  | _ => <TagChip tagId=tag.id height=32. />
                })
                <Text
                  value=tag.title
                  style=Style.(combine(Styles.Text.listPrimary, style([paddingRight(12.), maxWidth(240.), paddingLeft(hasIcon ? 24. : 57.)])))
                  numberOfLines=1
                />
                <View style=Style.(style([flex(1.)])) />
                <C_Settings render=(settings => {
                  <Connect_ActiveUser render=(user => {
                    let userId = user |> C.Option.map(user => user.User.id);
                    <C_TagExpenses
                      tagId=tag.id
                      render=(
                        (expenses) => {
                          let tagCost = Utils.getExpensesTotal((), ~userId, ~currency=settings.mainCurrency, ~expenses=expenses |> Array.to_list);
                          <Text
                            style=Style.(combine(style([paddingRight(12.), minWidth(20.)]), Styles.Text.listPrimary))
                            value=Currency.format(~currency=settings.mainCurrency, ~amount=tagCost)
                          />
                        }
                      )
                    />
                  }) />
                }) />
              </View>
            </ListRow>
          }
        )
      />
  }
};
