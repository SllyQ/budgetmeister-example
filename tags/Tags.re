open ReactNative;

open RnMui;

let component = ReasonReact.statelessComponent("Tags");

let make = (_, ~onCreateTag, ~onEditTag, ~onTagExpensesRequested) => {
  let deleteTag = (id, _) => {
    let optTag = TagDb.get(~id);
    switch optTag {
    | None => ()
    | Some(tag) =>
      let undoDelete = A_Tag.delete(id);
      SnackbarService.show(
        "Tag \"" ++ (tag.title ++ "\" deleted"),
        ~action={
          text: "Undo",
          handler: () => {
            undoDelete();
            SnackbarService.hideSnackbar()
          }
        }
      )
    }
  };
  {
    ...component,
    render: ({handle}) =>
      <HelpWrapper helpComponent={
        title: "Tags",
        message: `String("Categories are the primary way to group your expenses, however categories have a strict hierarchy and that doesn't fit all situations. This is where tags come in. Every expense can have as many tags as you want.")
      }>
        <ScreenWrap fab=(
          <ThemeActionButton onPress=onCreateTag />
        )>
          <ListWrapper>
            <C_Tags
              render=(
                (tags) => {
                  let sortedTags =
                    tags
                    |> Common.iSort(
                         (t1, t2) =>
                           String.compare(
                             String.lowercase(t1.Tag.title),
                             String.lowercase(t2.Tag.title)
                           )
                       );
                  if (Array.length(tags) === 0) {
                    <TagsEmpty />
                  } else {
                    <View>
                      (
                        sortedTags
                        |> Array.map(
                             (tag: Tag.t) =>
                               <TagRow
                                 tagId=tag.id
                                 key=(Tag.key(tag.id))
                                 onEditTag
                                 onDeleteTag=(handle(deleteTag))
                                 onTagExpensesRequested
                               />
                           )
                        |> ReasonReact.array
                      )
                    </View>
                  }
                }
              )
            />
          </ListWrapper>
        </ScreenWrap>
      </HelpWrapper>
  }
};
