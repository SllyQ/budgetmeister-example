open ReactNative;

open RnMui;

type state = {selectedTags: list(Tag.id)};

type actions =
  | TagChecked((bool, Tag.id))
  | SubmitSelection
  | CreateTag;

let component = ReasonReact.reducerComponent("TagsSelectScreen");

let make = (_, ~selectedTagIds, ~onTagsSelected, ~onCreateTag) => {
  let handleBack = () => {
    onTagsSelected(None);
    true
  };
  {
    ...component,
    initialState: () => {selectedTags: selectedTagIds},
    didMount: (_) => {
      BackHandler.addEventListener("hardwareBackPress", handleBack);
    },
    willUnmount: (_) => BackHandler.removeEventListener("hardwareBackPress", handleBack),
    reducer: (action, state) =>
      switch action {
      | TagChecked((isChecked, tagId)) =>
        if (isChecked) {
          ReasonReact.Update({selectedTags: [tagId, ...state.selectedTags]})
        } else {
          ReasonReact.Update({
            selectedTags: List.filter((tid) => tid !== tagId, state.selectedTags)
          })
        }
      | SubmitSelection =>
        onTagsSelected(Some(state.selectedTags));
        ReasonReact.NoUpdate
      | CreateTag =>
        ReasonReact.SideEffects(
          (
            ({send}) => {
              let _ =
                onCreateTag()
                |> Js.Promise.then_(
                     (optTagId) => {
                       switch optTagId {
                       | Some(tagId) => ((_) => send(TagChecked((true, tagId))))()
                       | None => ()
                       };
                       Js.Promise.resolve()
                     }
                   );
              ()
            }
          )
        )
      },
    render: ({send, state: {selectedTags}}) =>
      <ScreenWrap fab=(
        <ThemeActionButton icon="check" onPress=(() => send(SubmitSelection)) />
      )>
        <C_Tags
          render=(
            (tags) => {
              let sortedTags =
                tags
                |> Common.iSort(
                     (t1, t2) =>
                       String.compare(String.lowercase(t1.Tag.title), String.lowercase(t2.title))
                   );
                <ListWrapper>
                  <Text value="Select a tag to add"
                    style=Style.(combine( Styles.Text.title, style([textAlign(`center)])))
                  />
                  (
                    ReasonReact.array(
                      Array.map(
                        (tag: Tag.t) => {
                          let tagChecked = List.exists((tagId) => tagId === tag.id, selectedTags);
                          <ListRow flexRow=true numberOfLines=1
                            key=(Tag.key(tag.id))
                            onPress=Callback(() => send(TagChecked((!tagChecked, tag.id))))
                          >
                            <Icon color=(tagChecked ? Colors.theme^.primary : Colors.black)
                              name=(tagChecked ? "check-box" : "check-box-outline-blank")
                            />
                            (
                              Js.Option.isSome(tag.icon) ?
                                <View style=Style.(style([paddingLeft(Styles.Spacing.l)]))>
                                <TagChip tagId=tag.id height=22. />
                                </View>
                                : ReasonReact.null
                            )
                            <View style=Style.(style([flex(1.)]))>
                              <Text value=tag.title numberOfLines=1
                                style=Style.(Js.Option.isSome(tag.icon)
                                  ? combine(Styles.Text.listPrimary, style([paddingLeft(Styles.Spacing.m)]))
                                  : combine(Styles.Text.listPrimary, style([paddingLeft(Styles.Spacing.l)]))
                                )
                              />
                            </View>
                          </ListRow>
                        },
                        sortedTags
                      )
                    )
                  )
                  <View style=Style.(style([marginTop(3.), height(45.)]))>
                    <MuiButton
                      text="New Tag"
                      icon="add"
                      onPress=((_) => send(CreateTag))
                      primary=true
                    />
                  </View>
              </ListWrapper>
            }
          )
        />
      </ScreenWrap>
  }
};
