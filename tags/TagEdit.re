open RnMui;

type state = {
  title: string,
  icon: option(string),
  color: option(CategoryColor.t),
  excludeFromBudget: bool,
};

type actions =
  | UpdateTitle(string)
  | UpdateTag
  | SelectColor(option(CategoryColor.t))
  | SelectIcon(option(string))
  | UpdateExcludeFromBudget(bool);

let component = ReasonReact.reducerComponent("TagEdit");

let make = (~onTagUpdated, ~tagId, _) => {
  ...component,
  initialState: () => {
    let optTag = TagDb.get(~id=tagId);
    switch optTag {
      | None => { title: "", icon: None, color: None, excludeFromBudget: false }
      | Some(tag) => {
        title: tag.title,
        color: tag.color,
        icon: tag.icon,
        excludeFromBudget: tag.excludeFromBudget
      }
    }
  },
  reducer: (action, state) =>
    switch action {
    | UpdateTitle(title) => ReasonReact.Update({...state, title})
    | UpdateTag => {
      let {title, color, icon, excludeFromBudget} = state;
      if (title === "") {
        ReasonReact.SideEffects((_) => ReactNative.Alert.alert(~title="Error", ~message="Title is required", ()))
      } else {
        ReasonReact.SideEffects(
          (
            (_) => {
              A_Tag.update(~id=tagId, ~title=String.trim(title), ~color, ~icon, ~excludeFromBudget, ());
              onTagUpdated()
            }
          )
        )
      }
    }
    | SelectColor(color) => ReasonReact.Update({...state, color})
    | SelectIcon(icon) => ReasonReact.Update({...state, icon})
    | UpdateExcludeFromBudget(excludeFromBudget) => ReasonReact.Update({...state, excludeFromBudget})
    },
  render: ({send, state: {title, color, icon}}) =>
    <ScreenWrap fab=(
      <ThemeActionButton icon="save" onPress=((_) => send(UpdateTag)) />
    )>
      <C_Tags
        render=(
          (tags) => {
            let titleExists =
              tags
              |> Js.Array.some(
                   (tag) =>
                     String.lowercase(tag.Tag.title) === String.lowercase(title)
                     && tag.id !== tagId
                 );
            <ListRow>
              <TextField
                autoCapitalize="sentences"
                autoCorrect=true
                label="Title"
                value=title
                onChangeText=((title) => send(UpdateTitle(title)))
                error=?(titleExists ? Some("A tag with such title already exists") : None)
              />
            </ListRow>
          }
        )
      />
      <IconSelectRow selectedIcon=icon onIconSelected=((icon) => send(SelectIcon(icon))) />
      <ColorSelectRow
        selectedColor=color
        onColorSelected=((color) => send(SelectColor(color)))
      />
      /* <OptionSwitchRow label="Exclude from budget"
        value=excludeFromBudget
        onValueChange=(reduce((excludeFromBudget) => UpdateExcludeFromBudget(excludeFromBudget)))
      /> */
    </ScreenWrap>
};
