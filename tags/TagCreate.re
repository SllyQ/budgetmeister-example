open ReactNative;
open RnMui;

type state = {
  title: string,
  icon: option(string),
  color: option(CategoryColor.t),
  excludeFromBudget: bool,
  titleRef: ref(option(ReasonReact.reactRef)),
  tagInitiallyFocused: ref(bool)
};

type actions =
  | UpdateTitle(string)
  | CreateTag
  | SelectColor(option(CategoryColor.t))
  | SelectIcon(option(string))
  | UpdateExcludeFromBudget(bool);

let component = ReasonReact.reducerComponent("TagCreate");

let setTitleRef = (titleRef, { ReasonReact.state }) => {
  let optRef = Js.Nullable.toOption(titleRef);
  let tagInitiallyFocused =
    switch optRef {
    | None => state.tagInitiallyFocused^
    | Some(r) =>
      if (!state.tagInitiallyFocused^) {
        let _ = ReasonReact.refToJsObj(r)##focus();
        true
      } else {
        state.tagInitiallyFocused^
      }
    };
  state.titleRef := optRef;
  state.tagInitiallyFocused := tagInitiallyFocused;
};

let make = (_, ~onTagCreate) => {
  let handleBack = () => {
    onTagCreate(None);
    true
  };
  {
    ...component,
    initialState: () => {
      title: "",
      titleRef: ref(None),
      tagInitiallyFocused: ref(false),
      color: None,
      icon: None,
      excludeFromBudget: false,
    },
    didMount: (_) => {
      BackHandler.addEventListener("hardwareBackPress", handleBack);
    },
    willUnmount: (_) => BackHandler.removeEventListener("hardwareBackPress", handleBack),
    reducer: (action, state) =>
      switch action {
      | UpdateTitle(title) => ReasonReact.Update({...state, title})
      | CreateTag => {
        if (state.title === "") {
          ReasonReact.SideEffects((_) => ReactNative.Alert.alert(~title="Error", ~message="Title is required", ()))
        } else {
          ReasonReact.SideEffects(
            (
              (_) => {
                let tag =
                  A_Tag.create(
                    ~title=String.trim(state.title),
                    ~color=?(state.color |> C.Option.map(CategoryStuff.color)),
                    ~icon=?state.icon,
                    ~excludeFromBudget=state.excludeFromBudget,
                    ()
                  );
                Mixpanel.increment(EventConsts.tagCreated);
                onTagCreate(Some(tag.id))
              }
            )
          )
        }
      }
      | SelectColor(color) => ReasonReact.Update({...state, color})
      | SelectIcon(icon) => ReasonReact.Update({...state, icon})
      | UpdateExcludeFromBudget(excludeFromBudget) => ReasonReact.Update({...state, excludeFromBudget})
      },
    render: ({handle, send, state: {title, color, icon}}) =>
      <ScreenWrap fab=(
        <ThemeActionButton icon="save" onPress=((_) => send(CreateTag)) />
      )>
        <C_Tags
          render=(
            (tags) => {
              let titleExists =
                tags
                |> Js.Array.some(
                     (tag) => String.lowercase(tag.Tag.title) === String.lowercase(title)
                   );
              <ListRow>
                <TextField
                  ref=handle(setTitleRef)
                  autoCapitalize="sentences"
                  autoCorrect=true
                  label="Title"
                  value=title
                  onChangeText=((title) => send(UpdateTitle(title)))
                  error=?(titleExists ? Some("A tag with such title already exists") : None)
                />
              </ListRow>
            }
          )
        />
        <IconSelectRow selectedIcon=icon onIconSelected=((icon) => send(SelectIcon(icon))) />
        <ColorSelectRow
          selectedColor=color
          onColorSelected=((color) => send(SelectColor(color)))
        />
        /* <OptionSwitchRow label="Exclude from budget"
          value=excludeFromBudget
          onValueChange=(reduce((excludeFromBudget) => UpdateExcludeFromBudget(excludeFromBudget)))
        /> */
      </ScreenWrap>
  }
};
