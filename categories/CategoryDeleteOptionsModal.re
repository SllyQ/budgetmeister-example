open ReactNative;
open RnMui;

let component = ReasonReact.statelessComponent("CategoryDeleteOptionsModal");

let make = (~category: Category.t, ~onCategoryPickerRequested, ~isVisible, ~onCloseRequested, _) => {

  let deleteWithAllExpenses = ((), _) => {
    A_Category.deleteCategoryExpenses(category.id);
    A_Category.delete(category.id) |> ignore
  };

  let deleteAndMoveExpenses = (newCategoryId, _) => {
    A_Category.moveCategoryExpenses(~oldCategoryId=category.id, ~newCategoryId);
    A_Category.delete(category.id) |> ignore
  };

  {
    ...component,
    render: ({ handle }) => {
      <RnModal
        isVisible
        onBackButtonPress=onCloseRequested
        onBackdropPress=onCloseRequested
      >
        <View
          style=Style.(
                  style([
                    backgroundColor("white"),
                    margin(30.),
                    padding(Styles.Spacing.m)
                  ])
                )>
          <ListWrapper>
            <ListRow numberOfLines=3 flexRow=true>
              <Text
                style=Styles.Text.body1
                value="This category has expenses assigned to it. Please select a way to handle these expenses to delete the category."
              />
            </ListRow>
            <ListRow numberOfLines=1>
              <MuiButton
                text="Delete all expenses"
                onPress=handle(() => {
                  onCloseRequested();
                  deleteWithAllExpenses()
                })
                primary=true
                raised=true
              />
            </ListRow>
            (
              switch category.categoryType {
              | Root(_) => ReasonReact.null
              | Child({ parentCategoryId }) =>
                <ListRow numberOfLines=1>
                  <MuiButton
                    text="Move to parent category"
                    onPress= handle(() => {
                      onCloseRequested();
                      deleteAndMoveExpenses(parentCategoryId)
                    })
                    primary=true
                    raised=true
                  />
                </ListRow>
              }
            )
            <ListRow numberOfLines=1>
              <MuiButton
                text="Select a category to move to"
                onPress=(
                  () => {
                    onCloseRequested();
                    onCategoryPickerRequested(
                      ~allowSplit=false,
                      ~allowParentSelection=true,
                      ~excludedCategoryIds=[category.id],
                      ~onSplitSelected=None,
                      ~onCategorySelected=
                        categoryId =>
                          switch categoryId {
                          | Some(catId) =>
                            handle(() => deleteAndMoveExpenses(catId), ())
                          | None => ()
                          },
                      None,
                      ()
                    );
                  }
                )
                primary=true
                raised=true
              />
            </ListRow>
            <ListRow numberOfLines=1>
              <MuiButton
                text="Cancel"
                onPress=onCloseRequested
                primary=true
                raised=true
              />
            </ListRow>
          </ListWrapper>
        </View>
      </RnModal>
    }
  }
}
