open ReactNative;

type state = {
  showNonEmptyCategoryDeleteModal: bool
};

type deleteAction =
  | Cancel
  | DeleteAllExpenses
  | MoveToCategory(Category.id);

type actions =
  | DeleteCategory
  | CloseCategoryDeleteOptionModal;

let component = ReasonReact.reducerComponent("CategoryRow");

let deleteCategory = (id) => {
  let optCategory = CategoryDb.get(~id);
  switch optCategory {
  | None => ()
  | Some(category) =>
    let undoDelete = A_Category.delete(id);
    SnackbarService.show(
      "Category \"" ++ (category.title ++ "\" deleted"),
      ~action={
        text: "Undo",
        handler: () => {
          undoDelete();
          SnackbarService.hideSnackbar()
        }
      }
    )
  }
};

let rec make =
        (
          ~categoryId: Category.id,
          ~onCategoryPickerRequested,
          ~onEditCategory,
          ~onCategoryExpensesRequested,
          ~onCategorySummaryRequested,
          ~isChild=false,
          _
        ) => {
  ...component,
  initialState: () => {showNonEmptyCategoryDeleteModal: false},
  reducer: (action, _state) =>
    switch action {
    | CloseCategoryDeleteOptionModal =>
      ReasonReact.Update({showNonEmptyCategoryDeleteModal: false})
    | DeleteCategory =>
      let hasChildCategories = CategoryDb.getAll()
        |> Js.Array.some((cat: Category.t) => switch (cat.categoryType) { | Root(_) => false | Child({ parentCategoryId }) => parentCategoryId == categoryId });
      if (hasChildCategories) {
        ReasonReact.SideEffects(
          (
            (_) =>
              Alert.alert(
                ~title="Can not delete selected category",
                ~message="Deleting categories which have child categories are not allowed, please delete the child categories first",
                ()
              )
          )
        )
      } else {
        let categoryExpenses = ExpenseDb.Data.getAll() |> List.filter(
          Utils.belongsToCategory(~categoryId=`WithChildren(categoryId))
        );
        if (List.length(categoryExpenses) === 0) {
          ReasonReact.SideEffects(((_) => deleteCategory(categoryId)))
        } else {
          ReasonReact.Update({showNonEmptyCategoryDeleteModal: true})
        }
      }
    },
  render: ({send, state: {showNonEmptyCategoryDeleteModal}}) => {
    let textStyle = isChild ? Styles.Text.denseListPrimary : Styles.Text.listPrimary;
    let getExpandedContent = (canHaveExpandedContent, cat: Category.t) => {
      !canHaveExpandedContent
      ? None
      : Some({
        <C_ChildCategories
          parentCategoryId=(Some(cat.id))
          render=(
            (childCategories) => {
              if (Array.length(childCategories) === 0) {
                ReasonReact.null
              } else {
                let sortedChildCategories = childCategories
                  |> CategoryUtils.sortByTitle;
                <View>
                  (
                    sortedChildCategories
                    |> Array.map(
                         (childCat: Category.t) =>
                           ReasonReact.element(
                             ~key=Category.key(childCat.id),
                             make(
                               ~onCategoryPickerRequested,
                               ~onCategoryExpensesRequested,
                               ~onCategorySummaryRequested,
                               ~onEditCategory,
                               ~categoryId=childCat.id,
                               ~isChild=true,
                               [||]
                             )
                           )
                       )
                    |> ReasonReact.array
                  )
                </View>
              }
            }
          )
        />
      })
    };
    <C_Category
      categoryId
      render=(
        (cat) => {
          <View>
            <CategoryDeleteOptionsModal
              category=cat
              isVisible=showNonEmptyCategoryDeleteModal
              onCategoryPickerRequested
              onCloseRequested=(() => send(CloseCategoryDeleteOptionModal))
            />
            <C_ChildCategories parentCategoryId=Some(cat.id) render=(categories => {
              <ListRow
                flexRow=true
                numberOfLines=1
                dense=isChild
                style=Style.(style([justifyContent(`spaceBetween)]))
                expandedContent=?getExpandedContent(Array.length(categories) > 0, cat)
                expandedContentStyle=Style.(style([
                  paddingLeft(Styles.Spacing.xl),
                  paddingRight(0.)
                ]))
                leftIcon={
                  let { ListRow.icon, color } = CategoryStuff.getIconParams(cat.id);
                  <LeftIcon icon color size=(isChild ? 20. : 24.) />
                }
                onPress=Actions({
                  actions: [|{
                    text: "Summary",
                    icon: Some("chart-areaspline"),
                    onPress: () => onCategorySummaryRequested(cat.id)
                  }, {
                    text: "Show expenses",
                    icon: Some("view-list"),
                    onPress: () => onCategoryExpensesRequested(cat.id)
                  }, {
                    text: "Edit",
                    icon: Some("pencil"),
                    onPress: () => onEditCategory(cat.id)
                  }, {
                    text: "Delete",
                    icon: Some("delete"),
                    onPress: (() => send(DeleteCategory))
                  }|]
                })
              >
                <View style=Style.(style([flexDirection(`row), flex(1.)]))>
                  <Text
                    value=cat.title
                    numberOfLines=1
                    style=textStyle
                  />
                </View>
              </ListRow>
            }) />
          </View>
        }
      )
    />
  }
};
