open ReactNative;

open RnMui;

let component = ReasonReact.statelessComponent("Categories");

let make = (_, ~onCreateCategory, ~onEditCategory, ~onCategoryPickerRequested, ~onCategoryExpensesRequested, ~onCategorySummaryRequested) => {
  {
    ...component,
    render: (_) =>
      <HelpWrapper helpComponent={
        title: "Categories",
        message: `Element(<Column>
          <BodyText value="Categories are used to group your expenses. Every expense needs to belong to one of the categories (or can be split across multiple categories). You can later use categories to get a better understanding how are you spending your money, via various summaries and charts which show you how your expenses are distributed across categories." />
          <BodyText style=Style.(style([paddingTop(8.)])) value="The way you create your categories will be specific to your lifestyle but you should try to think in advance how you will want to analyze your expenses. Popular categories are: groceries, transportation, rent & utilities, entertainment." />
          <BodyText style=Style.(style([paddingTop(8.)])) value="Keep in mind that categories can also have subcategories (and subcategories too can have subcategories)." />
        </Column>)
      }>
        <ScreenWrap fab=(
          <ThemeActionButton onPress=onCreateCategory />
        )>
          <ListWrapper>
            <C_ChildCategories
              parentCategoryId=None
              render=(
                (categories) => {
                  let sortedCategories = categories |> CategoryUtils.sortByTitle;
                  if (Array.length(categories) === 0) {
                    <CategoriesEmpty />
                  } else {
                    <View>
                      (
                        sortedCategories
                        |> Array.map(
                             (cat: Category.t) =>
                               <CategoryRow
                                 key=(Category.key(cat.id))
                                 onCategoryPickerRequested
                                 onEditCategory
                                 onCategoryExpensesRequested
                                 onCategorySummaryRequested
                                 categoryId=cat.id
                               />
                           )
                        |> ReasonReact.array
                      )
                    </View>
                  }
                }
              )
            />
          </ListWrapper>
        </ScreenWrap>
      </HelpWrapper>
  }
};
