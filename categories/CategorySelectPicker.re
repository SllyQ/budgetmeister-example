open ReactNative;

module PickerRow = {
  let component = ReasonReact.statelessComponent("PickerRow");

  let make = (~onPress, ~icon, ~color as bgColor, ~label, ~onRef=?, _) => {
    ...component,
    render: (_) => {
      <ListRow flexRow=true>
        <Touchable onPress style=Style.(style([flex(1.), height(50.), justifyContent(`center)]))>
          <View ref=?onRef
            style=Style.(style([backgroundColor(bgColor), flexDirection(`row), alignItems(`center), borderRadius(4.), height(41.)]))
          >
            <View style=Style.(style([marginHorizontal(9.)]))>
              <CategoryIcon icon size=24. />
            </View>
            <Text style=Style.(combine(Styles.Text.listPrimary, style([color("white"), Platform.equals(IOS) ? fontWeight(`_500) : fontFamily("Roboto-Medium"), paddingRight(Styles.Spacing.l)])))
              numberOfLines=1
              ellipsizeMode=`head
              value=label
            />
          </View>
        </Touchable>
      </ListRow>
    }
  }
};

type state = {
  categoriesPath: list(Category.id),
  categoryRefs: Js.Dict.t(option(ReasonReact.reactRef)),
  splitRef: ref(option(ReasonReact.reactRef)),
  androidBackListener: option(unit => bool)
};

type actions =
  | SetAndroidBackListener(unit => bool)
  | HandleBackButton
  | SelectCurrentCategory
  | SelectCategory(Category.id)
  | ForceSelectCategory(Category.id)
  | DiscardLastCategory
  | SetPickerRef(Category.id, Js.Nullable.t(ReasonReact.reactRef))
  | SetSplitRef(Js.Nullable.t(ReasonReact.reactRef))
  | CreateCategory
  | SelectSplit;

let component = ReasonReact.reducerComponent("CategorySelectPicker");

let make = (~selectedCategoryId, ~onCategorySelected, ~sharing=?, ~onSharedSelected=?, ~onCreateCategory=?, ~allowSplit=false, ~allowParentSelection=false, ~onSplitSelected=?, ~excludedCategoryIds=[], ~onCategorySelectedWithRef=?, ~onGoBack, _) => {
  ...component,
  didMount: ({ send }) => {
    let listener = () => {
      send(HandleBackButton);
      true
    };
    BackHandler.addEventListener("hardwareBackPress", listener);
    send(SetAndroidBackListener(listener));
  },
  willUnmount: ({ state }) => {
    switch state.androidBackListener {
      | Some(cb) => BackHandler.removeEventListener("hardwareBackPress", cb)
      | None => ()
    }
  },
  initialState: () => {categoriesPath: switch selectedCategoryId {
    | Some(cid) => {
      let path = Utils.getCategoryPath(cid);
      let initPath = Js.Array.slice(~start=0, ~end_=Array.length(path) - (Utils.categoryHasChildren(cid) ? 0 : 1), path);
      initPath |> Array.map(cat => cat.Category.id) |> Array.to_list |> List.rev
    }
    | None => []
  }, categoryRefs: Js.Dict.empty(), splitRef: ref(None), androidBackListener: None },
  reducer: (action, state) =>
    switch action {
    | HandleBackButton => {
      switch (state.categoriesPath) {
        | [_, ...tl] => ReasonReact.Update {
          ...state,
          categoriesPath: tl
        }
        | [] => ReasonReact.SideEffects((_) => {
          onGoBack()
        })
      }
    }
    | SetAndroidBackListener(listener) => ReasonReact.Update {
      ...state,
      androidBackListener: Some(listener)
    }
    | SelectCurrentCategory =>
      let categoryId = List.hd(state.categoriesPath);
      onCategorySelected(categoryId);
      Mixpanel.track("Category selected");
      switch onCategorySelectedWithRef {
        | None => ()
        | Some(cb) => cb(categoryId, Js.Dict.get(state.categoryRefs, Category.key(categoryId)) |> (fun
          | Some(Some(a)) => Some(a)
          | _ => None
        ))
      };
      ReasonReact.NoUpdate
    | SelectCategory(categoryId) =>
      let childrenCategories = CategoryDb.getAll()
        |> Js.Array.filter((cat) => switch (cat.Category.categoryType) {
          | Root(_) => false
          | Child({ parentCategoryId }) => parentCategoryId == categoryId
        });
      if (Array.length(childrenCategories) == 0) {
        /* ReasonReact.SideEffects((_) => { */
          onCategorySelected(categoryId);
          Mixpanel.track("Category selected");
          switch onCategorySelectedWithRef {
            | None => ()
            | Some(cb) => cb(categoryId, Js.Dict.get(state.categoryRefs, Category.key(categoryId)) |> (fun
              | Some(Some(a)) => Some(a)
              | _ => None
            ))
          };
          ReasonReact.NoUpdate
        /* }) */
      } else {
        ReasonReact.Update({...state, categoriesPath: [categoryId, ...state.categoriesPath]})
      }
    | ForceSelectCategory(categoryId) =>
      onCategorySelected(categoryId);
      Mixpanel.track("Category selected");
      switch onCategorySelectedWithRef {
        | None => ()
        | Some(cb) => cb(categoryId, Js.Dict.get(state.categoryRefs, Category.key(categoryId)) |> (fun
          | Some(Some(a)) => Some(a)
          | _ => None
        ))
      };
      ReasonReact.NoUpdate
    | DiscardLastCategory => ReasonReact.Update({...state, categoriesPath: List.tl(state.categoriesPath)})
    | SelectSplit => ReasonReact.SideEffects((_) => {
      switch onSplitSelected {
        | Some(cb) => cb()
        | None => ()
      }
    })
    | CreateCategory =>
      ReasonReact.SideEffects(
        (
          (_) => {
            switch onCreateCategory {
              | None => ()
              | Some(onCreate) => {
                Mixpanel.track("Picker category creation started");
                onCreate(
                  switch state.categoriesPath {
                  | [] => None
                  | [catId, ..._] => Some(catId)
                  }
                )
                |> Js.Promise.then_((_optCatId) => {
                    /* switch optCatId {
                      | None => ()
                      | Some(cid) => reduce((_) => ForceSelectCategory(cid), ())
                    }; */
                    Js.Promise.resolve()
                  }
                )
                |> ignore;
              }
            }
          }
        )
      )
    | SetPickerRef(categoryId, ref) => {
      ReasonReact.SideEffects((_) => {
        Js.Dict.set(state.categoryRefs, Category.key(categoryId), Js.Nullable.toOption(ref));
      })
    }
    | SetSplitRef(ref) => {
      ReasonReact.SideEffects((_) => {
        state.splitRef := Js.Nullable.toOption(ref)
      })
    }
    },
  render: ({send, state: {categoriesPath}}) => {
    let currParentCategory =
      switch categoriesPath {
      | [] => None
      | [catId, ..._] => Some(catId)
      };
    <ScreenWrap contentContainerStyle=Style.(style([justifyContent(`flexEnd), minHeightPct(90.)]))>
      <Connect_SelectedTheme render=(theme => {
        <C_ChildCategories
          parentCategoryId=currParentCategory
          render=(
            (categories) => {
              let sortedCategories =
                Common.iSort(
                  (c1: Category.t, c2: Category.t) =>
                    String.compare(String.lowercase(c1.title), String.lowercase(c2.title)),
                  categories
                ) |> Js.Array.filter(category => !Js.Array.includes(category.Category.id, Array.of_list(excludedCategoryIds)));
              <ListWrapper>
                <C_Alpha render=(() => {
                  (switch (onSharedSelected) {
                    | None => ReasonReact.null
                    | Some(onSharedSelected) => switch(sharing) {
                      | Some(sharing) => {
                        <SharedInfoRow sharing />
                      }
                      | None => {
                        <Column>
                          <PickerRow icon=Icon("account-multiple") color=theme.primary
                            onPress=(onSharedSelected)
                            label="Shared with friends/family"
                          />
                          <View style=Style.(style([backgroundColor("rgba(0, 0, 0, 0.4)"), minHeight(1.5), minWidth(50.), margin(10.)])) />
                        </Column>
                      }
                    }
                  })
                }) />
                (
                  switch categoriesPath {
                  | [] =>
                    Array.length(sortedCategories) == 0 ? (
                      <BodyText value="All expenses must belong to a category. Click on the button bellow to create one."
                        style=Style.(style([paddingHorizontal(32.), paddingBottom(8.), textAlign(`center)]))
                      />
                    ) : (
                      <Text
                        style=Style.(combine(Styles.Text.title, style([textAlign(`center), marginBottom(Styles.Spacing.m), marginTop(Styles.Spacing.s)])))
                        value="Select a category"
                      />
                    )
                  | [catId, ..._] =>
                    <View>
                      <C_Category
                        key=(Category.key(catId))
                        categoryId=catId
                        render=(
                          (category) =>
                            <Text
                              style=Style.(combine(Styles.Text.title, style([textAlign(`center), marginBottom(Styles.Spacing.m), marginTop(Styles.Spacing.s)])))
                              value=("Select a category (" ++ (Utils.getCategoryPathString(~withOther=false, category.id) ++ ")"))
                            />
                        )
                      />
                      (allowParentSelection ? {
                        let { ListRow.icon, color: iconColor } = CategoryStuff.getIconParams(catId);
                        let label = Utils.getCategoryPathString(~withOther=false, catId);
                        <PickerRow
                          onRef=(ref => send(SetPickerRef(catId, ref)))
                          onPress=((_) => send(SelectCurrentCategory))
                          icon color=(iconColor |> CategoryStuff.color) label
                        />
                      } : ReasonReact.null)
                    </View>
                  }
                )
                (Array.length(sortedCategories) == 0 ? (
                  ReasonReact.null
                ) : (
                  sortedCategories
                  |> Array.map(
                       (category: Category.t) => {
                          let { ListRow.icon, color: iconColor } = CategoryStuff.getIconParams(category.id);
                          let label = Utils.getCategoryPathString(~withOther=false, category.id);
                          <PickerRow key=(Category.key(category.id))
                            onRef=(ref => send(SetPickerRef(category.id, ref)))
                            onPress=((_) => send(SelectCategory(category.id)))
                            icon color=(iconColor |> CategoryStuff.color) label
                          />
                       }
                     )
                  |> ReasonReact.array
                ))
                (
                  switch categoriesPath {
                    | [lastCatId, ..._] => {
                      let { ListRow.icon, color: iconColor } = CategoryStuff.getIconParams(lastCatId);
                      let label = Utils.getCategoryPathString(lastCatId);
                      (allowParentSelection ? ReasonReact.null : {
                        /* "> Other" selection row */
                        <PickerRow
                          onRef=(ref => send(SetPickerRef(lastCatId, ref)))
                          onPress=((_) => send(SelectCurrentCategory))
                          icon color=(iconColor |> CategoryStuff.color) label
                        />
                      })
                    }
                    | _ => ReasonReact.null
                  }
                )
                <View style=Style.(style([backgroundColor("rgba(0, 0, 0, 0.4)"), minHeight(1.5), minWidth(50.), margin(10.)])) />
                (
                  switch categoriesPath {
                  | [] => {
                    if (allowSplit && Array.length(categories) > 1) {
                      <PickerRow
                        onRef=(ref => send(SetSplitRef(ref)))
                        onPress=((_) => send(SelectSplit))
                        icon=Icon("folder-multiple-outline") color=theme.accent label="Multiple categories"
                      />
                    } else ReasonReact.null
                  }
                  | [_, ..._] => {
                    <View>
                      <PickerRow
                        onPress=((_) => send(DiscardLastCategory))
                        icon=Icon("arrow-left") color=Colors.grey label="Up a category level"
                      />
                    </View>
                  }
                  }
                )
                (switch onCreateCategory {
                  | None => ReasonReact.null
                  | Some (_) => {
                    <PickerRow
                      onPress=((_) => send(CreateCategory))
                      icon=Icon("plus") color=theme.primary label="New category"
                    />
                  }
                })
              </ListWrapper>
            }
          )
        />
      }) />
    </ScreenWrap>
  }
};
