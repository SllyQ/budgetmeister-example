open ReactNative;
open RnMui;

type selectedCategory = | Category(option(Category.id)) | Split(array(Categorization.part));

let component = ReasonReact.statelessComponent("CategorySelectPickerRow");

let make = (~label="Category", ~selectedCategory, ~onCategoryPickerRequested, ~onNoneSelected=?, ~hideOther=false, _) => {
  ...component,
  render: (_) => {
    <ListRow numberOfLines=1 onPress=Callback(onCategoryPickerRequested) flexRow=true
      style=Style.(style([justifyContent(`spaceBetween)]))
    >
      <ListPrimary style=Style.(style([flex(1.), paddingRight(Styles.Spacing.l)]))>
        <RowTitle value=(label ++ ": ") />
        <Text value=(switch (selectedCategory) {
          | Category(Some(categoryId)) => Utils.getCategoryPathString(~withOther=!hideOther, categoryId)
          | Category(None) => "None"
          | Split(parts) => Utils.getSplitPartsTitleStr(parts)
        }) />
      </ListPrimary>
      (switch (onNoneSelected, selectedCategory) {
        | (Some(cb), Category(Some(_))) => <MuiButton text="Clear" primary=true onPress=cb />
        | _ => ReasonReact.null
      })
    </ListRow>
  }
}
