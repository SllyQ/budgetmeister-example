open ReactNative;
open RnMui;

type t = {
  title: string,
  excludeFromBudget: bool,
  parentCategoryId: option(Category.id),
  titleConfig: Category.titleConfig,
  icon: option(string),
  color: option(CategoryColor.t)
};

let initialValue = {
  title: "",
  excludeFromBudget: false,
  parentCategoryId: None,
  titleConfig: Optional,
  icon: None,
  color: None
};

type actions =
  | FocusTitleInputIfEmpty;

type state = {
  titleRef: ref(option(ReasonReact.reactRef))
};

let component = ReasonReact.reducerComponent("CategoryForm");

let make = (~categoryId=?, ~value, ~onValueChanged, ~onCategoryPickerRequested, _) => {
  let setTitleRef = (ref, { ReasonReact.state }) => {
    state.titleRef := Js.Nullable.toOption(ref)
  };

  let changeSelectedCategory = (_, _) => {
    onCategoryPickerRequested(~allowSplit=false, ~allowParentSelection=true, ~excludedCategoryIds=[], ~onSplitSelected=None, ~onCategorySelected=((categoryId) => {
      onValueChanged({
        ...value,
        parentCategoryId: categoryId
      })
    }), value.parentCategoryId, ());
  };

  {
    ...component,
    initialState: (): state => {
      titleRef: ref(None)
    },
    reducer: (action, state) => switch action {
      | FocusTitleInputIfEmpty => ReasonReact.SideEffects((_) => switch state.titleRef.contents {
        | Some(ref) => ReasonReact.refToJsObj(ref)##focus()
        | None => ()
      })
    },
    didMount: ({ send }) => {
      Js.Global.setTimeout(() => {
        send(FocusTitleInputIfEmpty)
      }, 0) |> ignore;
    },
    render: ({ handle }) => {
      let { title, parentCategoryId, icon, color } = value;
      <View>
        <C_Categories
          render=(
            (categories) => {
              let titleExists =
                categories
                |> Js.Array.some(
                     (category) =>
                       String.lowercase(category.Category.title) === String.lowercase(title)
                       && switch categoryId { | None => true | Some(cid) => cid !== category.id }
                   );
              <ListRow>
                <TextField
                  autoCapitalize="sentences"
                  autoCorrect=true
                  ref=handle(setTitleRef)
                  label="Title*"
                  value=title
                  onChangeText=((title) => onValueChanged({...value, title}))
                  error=?(titleExists ? Some("A category with such title already exists") : None)
                />
              </ListRow>
            }
          )
        />
        <CategorySelectPickerRow onCategoryPickerRequested=handle(changeSelectedCategory)
          label="Sub-category of"
          selectedCategory=Category(parentCategoryId) onNoneSelected=(() => onValueChanged({ ...value, parentCategoryId: None }))
          hideOther=true
        />
        <IconSelectRow selectedIcon=icon onIconSelected=(icon => onValueChanged({ ...value, icon }))
          noneText=?(parentCategoryId |> C.Option.map((_) => "will use icon of main category"))
        />
        (switch parentCategoryId {
          | Some(parentCategoryId) => {
            let { ListRow.color } = CategoryStuff.getIconParams(parentCategoryId);
            <ListRow numberOfLines=2>
              <Row alignItems=`center>
                <Text value="Color*: " style=Styles.Text.listPrimary />
                <View style=Style.(style([marginLeft(8.)]))>
                  <ColorBar color=(color |> CategoryStuff.color) height=24. />
                </View>
              </Row>
              <Text style=Styles.Text.listSecondary value="Sub-categories use main category's color" />
            </ListRow>
          }
          | None => {
            <C_Categories render=(categories => {
              let category =
                categories
                |> Js.Array.find(
                     (category) =>
                       switch (category.Category.categoryType, color) {
                         | (Root({ color }), Some(c2)) => color === c2
                         | _ => false
                       }
                       && switch categoryId { | None => true | Some(cid) => cid !== category.id }
                   );
              <ColorSelectRow
                selectedColor=color
                onColorSelected=(color => onValueChanged({ ...value, color }))
                error=?(category |> C.Option.map((cat: Category.t) => {
                  "This color is already used for \"" ++ cat.title ++ "\""
                }))
              />
            }) />
          }
        })
        <C_Settings
          render=(
            (settings) =>
              if (! settings.showAdvancedSettings) {
                ReasonReact.null
              } else {
                <View style=Style.(style([marginTop(Styles.Spacing.xl)]))>
                  /* <ListSubheader value="Advanced Options" withDivider=true primary=true />
                  /* <ListRow>
                    <CategoryTitleConfigDropdown
                      value=titleConfig
                      onValueChange=((titleConfig) => onValueChanged({...value, titleConfig}))
                    />
                  </ListRow> */
                  <OptionSwitchRow label="Exclude from budget"
                    value=excludeFromBudget
                    onValueChange=((excludeFromBudget) => onValueChanged({...value, excludeFromBudget}))
                  /> */
                </View>
              }
          )
        />
      </View>
    }
  }
}
