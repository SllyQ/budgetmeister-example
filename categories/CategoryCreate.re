open ReactNative;

open RnMui;

type state = {formValue: CategoryForm.t};

type actions =
  | UpdateFormValue(CategoryForm.t)
  | CreateCategory;

let component = ReasonReact.reducerComponent("CategoryCreate");

let defaultColors = CategoryColor.([|
  Blue,
  Cyan,
  Green,
  Orange,
  BlueGrey,
  Red,
  DeepPurple,
  LightBlue,
  Teal,
  Lime,
  DeepOrange,
  Brown,
  Purple,
  Indigo,
  LightGreen,
  Amber,
  Grey,
  Pink
|]);

let make = (~initialParentCategory=?, ~onCategoryCreate, ~onCategoryPickerRequested, _) => {
  let handleBack = () => {
    onCategoryCreate(None);
    true
  };
  let existingColors = CategoryDb.getAll()
  |. Belt.Array.keepMap(category => {
    switch (category.categoryType) {
      | Root({ color }) => Some(color)
      | _ => None
    }
  });
  let colorsLeft = defaultColors |> Js.Array.filter(color => !Js.Array.includes(color, existingColors));
  let defaultColor = if (Array.length(colorsLeft) == 0) {
    None
  } else {
    Some(colorsLeft[0])
  };
  {
    ...component,
    initialState: () => {
      formValue: {
        ...CategoryForm.initialValue,
        parentCategoryId: initialParentCategory,
        color: defaultColor
      }
    },
    reducer: (action, state) =>
      switch action {
      | UpdateFormValue(formValue) => ReasonReact.Update({formValue: formValue})
      | CreateCategory => {
        if (state.formValue.title === "") {
          Mixpanel.trackWithProperties("Category create error", {
            "error": "Title required"
          });
          ReasonReact.SideEffects((_) => Alert.alert(~title="Error", ~message="Title is required", ()))
        } else if ({
          let categories = CategoryDb.getAll();
          categories
          |> Js.Array.some(
               (category) =>
                 switch (category.Category.categoryType, state.formValue.color) {
                   | (Root({ color }), Some(c2)) => color === c2
                   | _ => false
                 }
             );
        }) {
          Mixpanel.trackWithProperties("Category color exists", {
            "error": "No title"
          });
          ReasonReact.SideEffects((_) => Alert.alert(~title="Error", ~message="Category with such color already exists", ()))
        } else {
          ReasonReact.SideEffects(
            (
              (_) => {
                let {CategoryForm.title, parentCategoryId, titleConfig, excludeFromBudget, icon, color} =
                  state.formValue;
                let categoryType = switch (parentCategoryId, color) {
                  | (Some(parentCategoryId), _) => Some(Category.Child({ parentCategoryId: parentCategoryId }))
                  | (_, Some(color)) => Some(Root({ color: color }))
                  | _ => None
                };
                switch (categoryType) {
                  | None => {
                    Mixpanel.trackWithProperties("Category color exists", {
                      "error": "Category color required for root categories"
                    });
                    Alert.alert(~title="Please select a color", ~message="You must select a color for categories without a parent", ())
                  }
                  | Some(categoryType) => {
                    let cat =
                      A_Category.create(
                        ~title=String.trim(title),
                        ~categoryType,
                        ~titleConfig,
                        ~excludeFromBudget,
                        ~icon?,
                        ()
                      );
                    Mixpanel.increment(EventConsts.categoryCreated);
                    Mixpanel.trackWithProperties(EventConsts.categoryCreated, {
                      "type": switch (categoryType) {
                        | Child(_) => "Sub-category"
                        | Root(_) => "Root category"
                      }
                    });
                    onCategoryCreate(Some(cat.id))
                  }
                }
              }
            )
          )
        }
      }
      },
    didMount: (_) => {
      BackHandler.addEventListener("hardwareBackPress", handleBack);
    },
    willUnmount: (_) => BackHandler.removeEventListener("hardwareBackPress", handleBack),
    render: ({send, state: {formValue}}) =>
      <ScreenWrap fab=(
        <ThemeActionButton icon="save" onPress=((_) => send(CreateCategory)) />
      )>
        <ListWrapper>
          <CategoryForm
            onCategoryPickerRequested value=formValue
            onValueChanged=((formValue) => send(UpdateFormValue(formValue)))
          />
        </ListWrapper>
      </ScreenWrap>
  }
};
