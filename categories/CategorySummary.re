open ReactNative;

type dataPoint = {
  x: Utils.periodInfo,
  y: float,
  y0: float
};

let component = ReasonReact.statelessComponent("CategorySummary");

type chartCategory =
  | Child(Category.t)
  | Base(Category.t);

let make = (~id, _) => {
  ...component,
  render: (_) => {
    <ScreenWrap>
      <C_Settings render=(settings => {
        <PeriodicExpensesView.Connect render=(expensesView => {
          <C_ChildCategories parentCategoryId=Some(id) render=(childCategories => {
            let getCategoryTotal = (~expenses=?, category) => Utils.getExpensesTotal(
              ~userId=AppAuthService.user^ |> C.Option.map(user => user.User.id),
              ~currency=settings.mainCurrency,
              ~categoryId=(switch category {
                | Base(category) => `Exact(category.id)
                | Child(category) => `WithChildren(category.id)
              }),
              ~expenses?,
              ()
            );

            let baseCategory = CategoryDb.get(~id) |> Js.Option.getExn;
            let categories = childCategories |> Array.map(category => Child(category)) |> Array.append([|Base(baseCategory)|]);
            let screenWidth = Dimensions.get(`window)##width |> float_of_int;
            let svgWidth = screenWidth;
            let svgHeight = svgWidth *. 0.9;

            let stuff = [||];

            let periods = expensesView.periods |> Js.Dict.values |> Js.Array.sliceFrom(1);

            periods |> Array.iter(({ PeriodicExpensesView.period, expenses }) => {
              categories |> Array.iter((category: chartCategory) => {
                let total = getCategoryTotal(~expenses=expenses |> Array.to_list, category);
                stuff |> Js.Array.push((
                  (category, period, total)
                )) |> ignore;
                ()
              });
            });

            let lastTotals = ref(periods |> Array.map((_) => 0.));

            let maxTotal = ref(0.);

            let catData = categories
            |> Common.iSort((cat1, cat2) => {
              let t1 = getCategoryTotal(cat1);
              let t2 = getCategoryTotal(cat2);
              t1 > t2 ? -1 : 1
            })
            |> Array.map(wrappedCategory => {
              let category = switch (wrappedCategory) {
                | Child(category) => category
                | Base(category) => category
              };
              let data = stuff |> Js.Array.filter(((wrappedCat, _, _)) => {
                let cat = switch (wrappedCat) {
                  | Child(cat) => cat
                  | Base(cat) => cat
                };
                cat.Category.id == category.Category.id
              })
              |> Js.Array.mapi(((_, period, total), i) => {
                let newTotal = total +. lastTotals^[i];
                if (newTotal > maxTotal^) {
                  maxTotal := newTotal
                };
                {
                  x: period,
                  y: newTotal,
                  y0: lastTotals^[i]
                }
              });
              lastTotals := data |> Array.map(d => d.y);
              (wrappedCategory, data);
            });

            let leftPadding = 40.;
            let rightPadding = 20.;
            let topPadding = 0.;
            let bottomPadding = 65.;

            let periodTitles = periods |> Array.map(({ PeriodicExpensesView.period }) => {
              period.title
            });

            let xScale = D3.Scale.makeBand()
            |> D3.Scale.rangeRound([|leftPadding, svgWidth -. rightPadding|])
            |> D3.Scale.padding(0.1)
            |> D3.Scale.stringDomain(periodTitles);

            let yScale = D3.Scale.makeLinear() |> D3.Scale.range([|svgHeight -. bottomPadding, topPadding|]) |> D3.Scale.domain([|0., (maxTotal^ *. 1.05)|]);

            let yTicks = yScale |> D3.Scale.ticks(10);

            <Column>
              {
                let { ListRow.color, icon } = CategoryStuff.getIconParams(id);
                <Row justifyContent=`center alignItems=`center style=Style.(style([paddingTop(24.)]))>
                  <LeftIcon color icon />
                  <TitleText value=baseCategory.title style=Style.(style([paddingLeft(8.)])) />
                </Row>
              }
              <Svg height=svgHeight width=svgWidth>
                <Svg.G x=0 y=0>
                  {
                    catData
                    |> Array.mapi((i, (_, data)) => {
                      let color = Colors.colors600[i];
                      <Area data x=((d) => D3.Scale.apply(d.x.title, xScale)) y=((d) => yScale(d.y)) y0=((d) => yScale(d.y0)) color />
                    })
                    |> ReasonReact.array
                  }
                </Svg.G>
                <Svg.G>
                /* Y Axis */
                  <Svg.Line
                    x1=leftPadding
                    x2=leftPadding
                    y1=topPadding
                    y2=(svgHeight -. bottomPadding)
                    stroke=Colors.black
                    strokeWidth=1.
                  />
                  (yTicks
                  |> Array.map(tick => {
                    <Svg.G
                      x=leftPadding
                      y=yScale(tick)
                    >
                      <Svg.Line x1=(-5.) x2=0. y1=0. y2=0. strokeWidth=1. stroke=Colors.black />
                      <Svg.Text
                        dx=(-8.)
                        dy="0.32em"
                        textAnchor="end"
                        fill=Colors.black
                      >
                        (ReasonReact.string({j|$(tick)|j}))
                      </Svg.Text>
                    </Svg.G>
                  })
                  |> ReasonReact.array)
                </Svg.G>
                <Svg.G>
                /* X Axis */
                  <Svg.Line
                    x1=leftPadding
                    x2=(svgWidth -. rightPadding)
                    y1=(svgHeight -. bottomPadding)
                    y2=(svgHeight -. bottomPadding)
                    stroke=Colors.black
                    strokeWidth=1.
                  />
                  (periods
                  |> Array.map(({ PeriodicExpensesView.period: { start, finish, title } }) => {
                    let fullMonths = DateFns.getDate(start) === 1. && DateFns.getDate(finish) === DateFns.getDaysInMonth(finish);
                    <Svg.G
                      x=(D3.Scale.apply(title, xScale))
                      y=(svgHeight -. bottomPadding)
                    >
                      <Svg.Line x1=0. x2=0. y1=0. y2=5. strokeWidth=1. stroke=Colors.black />
                      (fullMonths ? (
                        <Svg.Text dx="0.42em" dy="1.10em" transform="rotate(45)" fill=Colors.black>
                          (ReasonReact.string(start |> DateFns.format("MMM")))
                        </Svg.Text>
                      ) : (
                        <Svg.Text dx="0.42em" dy="1.10em" transform="rotate(45)" fill=Colors.black>
                          (ReasonReact.string(
                            (finish |> DateFns.format("MMM Do"))
                          ))
                        </Svg.Text>
                      ))
                    </Svg.G>
                  })
                  |> ReasonReact.array)
                </Svg.G>
              </Svg>
              <Column>
              (
                catData
                |> Array.mapi((i, (wrappedCategory, data)) => {
                  let squareColor = Colors.colors600[i];
                  <ListRow numberOfLines=1>
                    <Row alignItems=`center>
                      <View style=Style.(style([width(24.), height(24.), backgroundColor(squareColor), marginRight(8.)])) />
                      <ListPrimary>
                        <Text value=(switch wrappedCategory {
                          | Base(category) => Array.length(childCategories) == 0 ? category.title : "Other"
                          | Child(category) => category.title
                        }) />
                        <Text value=" " />
                        {
                          let total = data |> Array.fold_left((acc, d) => acc +. d.y -. d.y0, 0.);
                          <Text value=Currency.format(~currency=settings.mainCurrency, ~amount=total) />
                        }
                      </ListPrimary>
                    </Row>
                  </ListRow>
                })
                |> ReasonReact.array
              )
              </Column>
            </Column>
          }) />
        }) />
      }) />
    </ScreenWrap>
  }
}
