open RnMui;
open ReactNative;

type state = {
  formValue: CategoryForm.t,
};

type actions =
  | UpdateFormValue(CategoryForm.t)
  | UpdateCategory;

let component = ReasonReact.reducerComponent("CategoryEdit");

let make = (~onCategoryUpdated, ~categoryId, ~onCategoryPickerRequested, _) => {
  ...component,
  initialState: () => {
    let optCategory = CategoryDb.get(~id=categoryId);
    switch optCategory {
      | None => {
        formValue: CategoryForm.initialValue
      }
      | Some(category) => {
        let (parentCategoryId, color) = switch (category.categoryType) {
          | Root({ color }) => (None, Some(color))
          | Child({ parentCategoryId }) => (Some(parentCategoryId), None)
        };
        {
          formValue: {
            title: category.title,
            excludeFromBudget: category.excludeFromBudget,
            parentCategoryId,
            titleConfig: category.titleConfig,
            icon: category.icon,
            color,
          }
        }
      }
    }
  },
  reducer: (action, state) =>
    switch action {
    | UpdateFormValue(formValue) => ReasonReact.Update {
      formValue: formValue
    }
    | UpdateCategory =>
      let {CategoryForm.title, excludeFromBudget, parentCategoryId, titleConfig, icon, color} = state.formValue;
      if (title === "") {
        ReasonReact.SideEffects((_) => Alert.alert(~title="Error", ~message="Title is required", ()))
      } else if ({
        let categories = CategoryDb.getAll();
        categories
        |> Js.Array.some(
             (category) =>
               switch (category.Category.categoryType, state.formValue.color) {
                 | (Root({ color }), Some(c2)) => color === c2
                 | _ => false
               }
               && categoryId !== category.id
           );
      }) {
        ReasonReact.SideEffects((_) => Alert.alert(~title="Error", ~message="Category with such color already exists", ()))
      } else {
        ReasonReact.SideEffects(
          (
            (_) => {
              let categoryType = switch (parentCategoryId, color) {
                | (Some(parentCategoryId), _) => Some(Category.Child({ parentCategoryId: parentCategoryId }))
                | (_, Some(color)) => Some(Root({ color: color }))
                | _ => None
              };
              switch (categoryType) {
                | None => {
                  Alert.alert(~title="Please select a color", ~message="You must select a color for categories without a parent", ())
                }
                | Some(categoryType) => {
                  A_Category.update(
                    categoryId,
                    ~title=String.trim(title),
                    ~excludeFromBudget,
                    ~categoryType,
                    ~titleConfig,
                    ~icon,
                    ()
                  );
                  onCategoryUpdated()
                }
              }
            }
          )
        )
      }
    },
  render: ({send, state: { formValue }}) =>
    <ScreenWrap fab=(
      <ThemeActionButton icon="save" onPress=((_) => send(UpdateCategory)) />
    )>
      <ListWrapper>
        <CategoryForm
          categoryId onCategoryPickerRequested
          value=formValue
          onValueChanged=(formValue => send(UpdateFormValue(formValue)))
        />
      </ListWrapper>
    </ScreenWrap>
};
