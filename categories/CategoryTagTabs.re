open ReactNative;

type tab =
  | Categories
  | Tags;

type state = {
  activeTab: tab
};

type actions =
  | ChangeTab(tab);

let component = ReasonReact.reducerComponent("CategoryTagTabs");

let make = (
  ~onCreateCategory,
  ~onEditCategory,
  ~onCategoryPickerRequested,
  ~onCategoryExpensesRequested,
  ~onCategorySummaryRequested,
  ~onCreateTag,
  ~onEditTag,
  ~onTagExpensesRequested,
  _
) => {
  ...component,
  initialState: () => { activeTab: Categories },
  reducer: C.flip(_state => fun
    | ChangeTab(tab) => ReasonReact.Update {
      activeTab: tab
    }
  ),
  render: ({ send, state: { activeTab } }) => {
    <Column style=Style.(style([flex(1.)]))>
      (switch activeTab {
        | Categories => {
          <DebounceInitialRender>
            <Categories
              onCreateCategory
              onEditCategory
              onCategoryPickerRequested
              onCategoryExpensesRequested
              onCategorySummaryRequested
            />
          </DebounceInitialRender>
        }
        | Tags => {
          <DebounceInitialRender>
            <Tags
              onCreateTag
              onEditTag
              onTagExpensesRequested
            />
          </DebounceInitialRender>
        }
      })
      <BottomNav.Container>
        <BottomNav.Tab
          label="Categories"
          icon="folder"
          active=(activeTab == Categories)
          onPress=(() => send(ChangeTab(Categories)))
        />
        <BottomNav.Tab
          label="Tags"
          icon="tag"
          active=(activeTab == Tags)
          onPress=(() => send(ChangeTab(Tags)))
        />
      </BottomNav.Container>
    </Column>
  }
}
