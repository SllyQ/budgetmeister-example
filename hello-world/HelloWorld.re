open ReactNative;
open Paper;

type state = {
  mainCurrency: Currency.t,
  currencySelectModalVisible: bool
};

type actions =
  | SelectCurrency(Currency.t)
  | SubmitSelection
  | OpenCurrencySelectModal
  | CloseCurrencySelectModal;

let component = ReasonReact.reducerComponent("HelloWorld");

let make = (~onSetupSubmitted, _) => {
  ...component,
  initialState: () => { mainCurrency: EUR, currencySelectModalVisible: false },
  reducer: C.flip(state => fun
    | OpenCurrencySelectModal => ReasonReact.Update {
      ...state,
      currencySelectModalVisible: true
    }
    | CloseCurrencySelectModal => ReasonReact.Update {
      ...state,
      currencySelectModalVisible: false
    }
    | SelectCurrency(mainCurrency) => ReasonReact.Update {
      currencySelectModalVisible: false,
      mainCurrency: mainCurrency
    }
    | SubmitSelection => ReasonReact.SideEffects((_) => {
      let history = HistoryDb.get();
      HistoryDb.setItem({
        ...history,
        initialMainCurrencySelected: true
      });
      SettingsDb.setItem({
        ...SettingsDb.get(),
        mainCurrency: state.mainCurrency
      });
      Mixpanel.trackWithProperties(EventConsts.initialMainCurrencySelected, {
        "currency": Currency.currencyToString(state.mainCurrency)
      });
      onSetupSubmitted();
    })
  ),
  render: ({ send, state: { mainCurrency, currencySelectModalVisible }}) => {
    let str = Currency.currencyToString(mainCurrency);
    let symbol = Currency.symbol(mainCurrency);
    let currencyStr = {j|$(str) ($(symbol))|j};
    let smallImage = Dimensions.get(`window)##height < 500;
    <ScreenWrap scrollable=false>
      <Column alignItems=`center justifyContent=`center flex=1. style=Style.(style([paddingHorizontal(32.)]))>
        <Image source=Required(Packager.require("../../../../base_assets/icon.png"))
          style=Style.(style([width(smallImage ? 150. : 250.), height(smallImage ? 150. : 250.), marginTop(-48.), marginBottom(24.)]))
        />
        <BodyText textAlign=`center value="To get started please select your main currency" />
        <Touchable onPress=(() => send(OpenCurrencySelectModal))>
          <Row alignItems=`center >
            <ListPrimary value=currencyStr />
            <MuiButton primary=true label="Change" onPress=(() => send(OpenCurrencySelectModal)) />
          </Row>
        </Touchable>
        <CaptionText textAlign=`center value="You will be able to enter expenses in different currencies, you will also be able to change your main currency later." />
        <MuiButton style=Style.(style([marginTop(Styles.Spacing.xl)])) raised=true primary=true label="Continue" onPress=(() => send(SubmitSelection)) />
      </Column>
      <CurrencySelectModal isVisible=currencySelectModalVisible
        onCurrencySelected=(fun
          | None => send(CloseCurrencySelectModal)
          | Some(currency) => send(SelectCurrency(currency))
        )
      />
    </ScreenWrap>
  }
}
