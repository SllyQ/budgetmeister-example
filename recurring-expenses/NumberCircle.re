open ReactNative;

let component = ReasonReact.statelessComponent("NumberCircle");

let make = (~isSelected, ~value, ~onPress, _) => {
  ...component,
  render: (_) => {
    <Touchable
      style=Style.(style([
        width(36.),
        height(36.),
        padding(4.),
      ]))
      onPress=onPress
    >
      <View style=Style.(style([
        height(28.),
        width(28.),
        borderRadius(14.),
        backgroundColor(isSelected ? Colors.theme^.primary : Colors.white),
        justifyContent(`center),
        alignItems(`center)
      ]))>
        <Text value
          style=Style.(style([
            color(isSelected ? Colors.white : Colors.black)
          ]))
        />
      </View>
    </Touchable>
  }
}
