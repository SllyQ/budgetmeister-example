open ReactNative;

let component = ReasonReact.statelessComponent("RecurringExpenseDailyConfig");

let make = (~value: RepeatConfig.dailyConfig, ~onValueChanged: RepeatConfig.dailyConfig => unit, _) => {
  ...component,
  render: (_) => {
    switch value.addOn {
      | RepeatConfig.HoursOfDay(hours) => {
        <View style=Style.(style([widthPct(100.), alignItems(`center)]))>
          <Text style=Style.(combine(Styles.Text.subheading, style([paddingBottom(12.)]))) value="Select hours of day" />
          (Array.init(4, x => x)
            |> Array.map(i => {
              <View key=string_of_int(i) style=Style.(style([flexDirection(`row)]))>
                (Array.init(6, x => x)
                  |> Array.map(j => {
                    let hourNum = i * 6 + j;
                    let isSelected = hours |> Js.Array.includes(hourNum);
                    <NumberCircle key=string_of_int(hourNum) isSelected value={j|$(hourNum)|j}
                      onPress=(() => {
                        if (isSelected) {
                          onValueChanged({
                            addOn: HoursOfDay(hours |> Js.Array.filter(h => h !== hourNum))
                          })
                        } else {
                          onValueChanged({
                            addOn: HoursOfDay(hours |> Js.Array.filter(h => h !== hourNum) |> Array.append([|hourNum|]))
                          })
                        }
                      })
                    />
                  })
                  |> ReasonReact.array)
              </View>
              }) |> ReasonReact.array
          )
        </View>
      }
    }
  }
}
