open ReactNative;
open RnMui;

let component = ReasonReact.statelessComponent("RecurringExpenses");

let make = (~onRecurringExpenseCreateRequested, ~onRecurringExpenseEditRequested, _) => {
  {
    ...component,
    render: (_) => {
      <HelpWrapper helpComponent={
        title: "Recurring Expenses",
        message: `String("Recurring expenses feature helps you automate the expenses that happen every month/week/day. It's very useful for various bills. Also you can mark your recurring expense as unpaid or have it with missing cost, this way it can act also act as a reminder so you don't forget to pay your bills.")
      }>
        <ScreenWrap fab=(
          <ThemeActionButton onPress=onRecurringExpenseCreateRequested />
        )>
          <ListWrapper>
            <Connect_RecurringExpenses render=(recurringExpenses => {
              if (Array.length(recurringExpenses) == 0) {
                <RecurringExpensesEmpty />
              } else {
                <View>
                  (recurringExpenses
                  |> Array.map((recurringExpense: RecurringExpense.t) => {
                    <SwipeableRow key=DbRecurringExpense.key(recurringExpense.id)
                      onEdit=(() => onRecurringExpenseEditRequested(recurringExpense.id))
                      onDelete=(() => A_RecurringExpense.delete(recurringExpense.id))
                    >
                      <ListRow numberOfLines=1>
                        <Text style=Styles.Text.listPrimary value=recurringExpense.title />
                      </ListRow>
                    </SwipeableRow>
                  })
                  |> ReasonReact.array)
                </View>
              }
            }) />
          </ListWrapper>
        </ScreenWrap>
      </HelpWrapper>
    }
  }
};
