open ReactNative;

type state = {formValue: RecurringExpenseForm.value};

type actions =
  | UpdateFormValue(RecurringExpenseForm.value)
  | UpdateRecurringExpense;

let component = ReasonReact.reducerComponent("RecurringExpenseEdit");

let make = (~recurringExpenseId, ~onRecurringExpenseUpdated, ~onCategoryPickerRequested, ~onSplitFormRequested, ~onSelectTags, _) => {
  ...component,
  initialState: () => {
    formValue: RecurringExpenseDb.Data.get(recurringExpenseId)
      |> Js.Option.getExn
      |> RecurringExpenseForm.getValue
  },
  reducer: (action, state) =>
    switch action {
    | UpdateFormValue(formValue) => ReasonReact.Update({formValue: formValue})
    | UpdateRecurringExpense =>
      ReasonReact.SideEffects(
        (_) => {
          let { RecurringExpenseForm.title, repeatFrom, repeatUntil, tagIds, repeatConfig, spending, unpaid } = state.formValue;
          switch (title, spending.totalAmount) {
          | ("", _) => Alert.alert(~title="Error", ~message="Title is required", ())
          | (_, NumberInput.Invalid(_)) => Alert.alert(~title="Error", ~message="Total amount is invalid", ())
          | _ =>
            A_RecurringExpense.update(
              ~id=recurringExpenseId,
              ~title,
              ~repeatFrom,
              ~repeatUntil,
              ~totalAmount=NumberInput.toNumber(spending.totalAmount),
              ~currency=spending.currency,
              ~categorization=?spending.categorization,
              ~tagIds,
              ~repeatConfig,
              ~unpaid,
              ()
            ) |> (fun
              | NotFound | ValidationError(_) => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
              | Updated(_) => onRecurringExpenseUpdated()
            );
          }
        }
      )
    },
  render: ({ send, state: { formValue } }) => {
    <ScreenWrap fab=(
      <ThemeActionButton icon="save" onPress=((_) => send(UpdateRecurringExpense)) />
    )>
      <RecurringExpenseForm
        value=formValue
        onSplitFormRequested
        onValueChanged=((fs) => send(UpdateFormValue(fs)))
        onCategoryPickerRequested
        onSelectTags
      />
    </ScreenWrap>
  }
}
