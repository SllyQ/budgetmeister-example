open ReactNative;

let component = ReasonReact.statelessComponent("RecurringExpenseMonthlyConfig");

let make = (~value: RepeatConfig.monthlyConfig, ~onValueChanged: RepeatConfig.monthlyConfig => unit, _) => {
  ...component,
  render: (_) => {
    switch value.addOn {
      | RepeatConfig.DaysOfMonth(days) => {
        <View style=Style.(style([widthPct(100.), alignItems(`center)]))>
          <Text style=Style.(combine(Styles.Text.subheading, style([paddingBottom(12.)]))) value="Select days of month" />
          (Array.init(4, x => x)
            |> Array.map(i => {
              <View key=string_of_int(i) style=Style.(style([flexDirection(`row)]))>
              (
                Array.init(7, x => x)
                  |> Array.map(j => {
                    let dayNum = i * 7 + j + 1;
                    let isSelected = days |> Js.Array.includes(dayNum);
                    <NumberCircle key=string_of_int(j) isSelected value={j|$(dayNum)|j} onPress=(() => {
                      if (isSelected) {
                        onValueChanged({
                          addOn: DaysOfMonth(days |> Js.Array.filter(d => d !== dayNum))
                        })
                      } else {
                        onValueChanged({
                          addOn: DaysOfMonth(days |> Js.Array.filter(d => d !== dayNum) |> Array.append([|dayNum|]))
                        })
                      }
                    }) />
                  })
                  |> ReasonReact.array
              )
                </View>
              }) |> ReasonReact.array
          )
        </View>
      }
      | _ => ReasonReact.null
    }
  }
}
