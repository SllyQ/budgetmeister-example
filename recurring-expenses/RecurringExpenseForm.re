open ReactNative;
open RnMui;
open RepeatConfig;

let defaultDailyConfig = Daily({
  addOn: HoursOfDay([|0|])
});

let defaultWeeklyConfig = Weekly({
  addOn: DaysOfWeek([|1|])
});

let defaultMonthlyConfig = Monthly({
  addOn: DaysOfMonth([|1|])
});

type value = {
  title: string,
  repeatFrom: Js.Date.t,
  repeatUntil: option(Js.Date.t),
  tagIds: array(Tag.id),
  spending: SpendingForm.value,
  unpaid: bool,
  repeatConfig: RepeatConfig.t
};

let empty = (~categorization, ~currency=?, ~totalAmount=?, ()) => {
  title: "",
  repeatFrom: Js.Date.make(),
  repeatUntil: None,
  repeatConfig: defaultMonthlyConfig,
  tagIds: [||],
  unpaid: false,
  spending: {
    totalAmount: totalAmount |> C.Option.map(NumberInput.toValue) |> Js.Option.getWithDefault(NumberInput.Empty),
    currency: currency |> Js.Option.getWithDefault(SettingsDb.get().mainCurrency),
    categorization
  },
};

let getValue = (recurringExpense: RecurringExpense.t) => {
  title: recurringExpense.title,
  repeatFrom: recurringExpense.repeatFrom,
  repeatUntil: recurringExpense.repeatUntil,
  repeatConfig: recurringExpense.repeatConfig,
  tagIds: recurringExpense.tagIds,
  unpaid: recurringExpense.unpaid,
  spending: {
    totalAmount: recurringExpense.totalAmount |> NumberInput.optToValue,
    currency: recurringExpense.currency,
    categorization: recurringExpense.categorization
    /* recurringExpense.spending |> SpendingForm.spendingToValue */
  }
};

let component = ReasonReact.statelessComponent("RecurringExpenseForm");

let make = (~value, ~onValueChanged, ~onCategoryPickerRequested, ~onSplitFormRequested, ~onSelectTags, _) => {
  ...component,
  render: (_) => {
    <View>
      <ListRow>
        <TextField
          label="Title"
          value=value.title
          onChangeText=(title => onValueChanged({ ...value, title }))
          autoCapitalize="sentences"
          autoCorrect=true
        />
      </ListRow>
      <ListRow>
        <Dropdown
          label="Period"
          data=[|{"value": "Monthly"}, {"value": "Weekly"}, {"value": "Daily"}|]
          value=(switch value.repeatConfig {
            | Daily(_) => "Daily"
            | Weekly(_) => "Weekly"
            | Monthly(_) => "Monthly"
          })
          onChangeText=(((text, _)) => onValueChanged({
            ...value,
            repeatConfig: (switch text {
              | "Daily" => defaultDailyConfig
              | "Weekly" => defaultWeeklyConfig
              | "Monthly" => defaultMonthlyConfig
              | _ => assert(false)
            })
          }))
        />
      </ListRow>
      <SpendingForm
        value=value.spending
        onValueChanged=(spending => onValueChanged({ ...value, spending }))
        onCategoryPickerRequested
        onSplitFormRequested
      />
      <TagsSelectRow
        selectedTagIds=(value.tagIds |> Array.to_list)
        onSelectTags
        onSelectedTagIdsUpdate=(
          (tagIds) => onValueChanged({
            ...value,
            tagIds: tagIds |> Array.of_list
          })
        )
      />
      <OptionSwitchRow value=value.unpaid
        onValueChange=(unpaid => onValueChanged({ ...value, unpaid }))
        inverted=true label="Unpaid"
      />
      <DatePickerRow
        date=Some(value.repeatFrom)
        onDateChanged=(repeatFrom => onValueChanged({ ...value, repeatFrom }))
        label="Repeat from"
      />
      <DatePickerRow
        date=value.repeatUntil
        onDateChanged=(repeatUntil => onValueChanged({ ...value, repeatUntil: Some(repeatUntil) }))
        onDateCleared=(() => onValueChanged({ ...value, repeatUntil: None }))
        label="Repeat until"
        noneLabel="forever"
      />
      <RepeatConfigForm value=value.repeatConfig onValueChanged=(repeatConfig => onValueChanged({ ...value, repeatConfig })) />
    </View>
  }
}
