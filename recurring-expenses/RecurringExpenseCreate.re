open ReactNative;

type state = {
  formValue: option(RecurringExpenseForm.value)
};

type actions =
  | SplitCost
  | SplitSelected(ExpenseSplitForm.value)
  | SelectInitialCategory(Category.id)
  | UpdateFormValue(RecurringExpenseForm.value)
  | Create;

let component = ReasonReact.reducerComponent("RecurringExpenseCreate");

let make = (~onRecurringExpenseCreated, ~onCategoryPickerRequested, ~onSplitFormRequested, ~onCreateCategory, ~onSelectTags, _) => {
  {
    ...component,
    initialState: () => {
      formValue: None
    },
    reducer: (action, state) => switch action {
      | SplitCost => ReasonReact.SideEffects(({ send }) => {
        let currentParts = state.formValue |> C.Option.map(formValue => switch (formValue.RecurringExpenseForm.spending.categorization) {
          | Some(Single(part)) => [|{
            Categorization.categoryId: part.categoryId,
            amount: formValue.spending.totalAmount |> NumberInput.toNumber,
            description: None
          }|]
          | Some(Split(parts)) => parts
          | None => [||]
        });
        onSplitFormRequested(currentParts, parts => send(SplitSelected(parts)))
      })
      | SplitSelected({ categorization, totalAmount, currency }) => {
        ReasonReact.Update {
          formValue: switch state.formValue {
            | None => Some(RecurringExpenseForm.empty((), ~categorization=Some(categorization), ~totalAmount, ~currency))
            | Some(form) => Some({
              ...form,
              spending: {
                totalAmount: totalAmount |> NumberInput.toValue,
                categorization: Some(categorization),
                currency
              }
            })
          }
        }
      }
      | SelectInitialCategory(categoryId) => {
        ReasonReact.Update({
          formValue: Some(RecurringExpenseForm.empty(
            (),
            ~categorization=Some(Single({
              Categorization.categoryId: categoryId
            }))
          ))
        })
      }
      | UpdateFormValue(formValue) => ReasonReact.Update {
        formValue: Some(formValue)
      }
      | Create => ReasonReact.SideEffects((_) => {
        switch state.formValue {
          | None => ()
          | Some(formValue) => {
            let { RecurringExpenseForm.title, repeatFrom, repeatUntil, tagIds, repeatConfig, spending, unpaid } = formValue;
            switch (title, spending.totalAmount, spending.categorization) {
            | ("", _, _) => Alert.alert(~title="Error", ~message="Title is required", ())
            | (_, NumberInput.Invalid(_), _) => Alert.alert(~title="Error", ~message="Total amount is invalid", ())
            | (_, _, None) => Alert.alert(~title="Error", ~message="Category is required", ())
            | (_, _, Some(categorization)) => {
              A_RecurringExpense.create((), ~title=title |> String.trim, ~repeatFrom, ~repeatUntil, ~tagIds,
                ~repeatConfig, ~totalAmount=(spending.totalAmount |> NumberInput.toNumber), ~categorization, ~currency=spending.currency, ~unpaid)
              |> (fun
                | ValidationError(_) => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
                | Created(_) => {
                  Mixpanel.track(EventConsts.recurringExpenseCreated);
                  onRecurringExpenseCreated()
                }
              );
            }
            }
          }
        };
      })
    },
    render: ({ state, send }) => {
      switch (state.formValue) {
        | None => <CategorySelectPicker
            onGoBack=onRecurringExpenseCreated
            allowSplit=true onCreateCategory onSplitSelected=(() => send(SplitCost))
            onCategorySelected=((cid) => send(SelectInitialCategory(cid))) selectedCategoryId=None
          />
        | Some(formValue) => {
          <ScreenWrap fab=(
            <ThemeActionButton icon="save" onPress=(() => send(Create)) />
          )>
            <ListWrapper>
              <RecurringExpenseForm
                value=formValue
                onValueChanged=(formValue => send(UpdateFormValue(formValue)))
                onCategoryPickerRequested
                onSplitFormRequested
                onSelectTags
              />
            </ListWrapper>
          </ScreenWrap>
        }
      }
    }
  }
}
