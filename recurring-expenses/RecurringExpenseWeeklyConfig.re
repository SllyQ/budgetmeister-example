open ReactNative;

let component = ReasonReact.statelessComponent("RecurringExpenseWeeklyConfig");

let make = (~value: RepeatConfig.weeklyConfig, ~onValueChanged: RepeatConfig.weeklyConfig => unit, _) => {
  ...component,
  render: (_) => {
    switch value.addOn {
      | RepeatConfig.DaysOfWeek(hours) => {
        <View style=Style.(style([widthPct(100.), alignItems(`center)]))>
          <Text style=Style.(combine(Styles.Text.subheading, style([paddingBottom(12.)]))) value="Select days of week" />
          <View style=Style.(style([flexDirection(`row)]))>
            (Array.init(7, x => x + 1)
              |> Array.map(dayNum => {
                let isSelected = hours |> Js.Array.includes(dayNum);
                <NumberCircle key=string_of_int(dayNum) isSelected value={j|$(dayNum)|j} onPress=(() => {
                  if (isSelected) {
                    onValueChanged({
                      addOn: DaysOfWeek(hours |> Js.Array.filter(h => h !== dayNum))
                    })
                  } else {
                    onValueChanged({
                      addOn: DaysOfWeek(hours |> Js.Array.filter(h => h !== dayNum) |> Array.append([|dayNum|]))
                    })
                  }
                }) />
                }) |> ReasonReact.array
            )
          </View>
        </View>
      }
    }
  }
}
