open ReactNative;

type tab =
  | RecurringExpenses
  | Templates;

type state = {
  activeTab: tab
};

type actions =
  | ChangeTab(tab);

let component = ReasonReact.reducerComponent("RecurringExpensesTabs");

let make = (
  ~onRecurringExpenseCreateRequested,
  ~onRecurringExpenseEditRequested,
  ~onCreateTemplate,
  ~onEditTemplate,
  _
) => {
  ...component,
  initialState: () => { activeTab: RecurringExpenses },
  reducer: C.flip(_state => fun
    | ChangeTab(tab) => ReasonReact.Update {
      activeTab: tab
    }
  ),
  render: ({ send, state: { activeTab } }) => {
    <Column style=Style.(style([flex(1.)]))>
      (switch activeTab {
        | RecurringExpenses => {
          <DebounceInitialRender>
            <RecurringExpenses
              onRecurringExpenseCreateRequested
              onRecurringExpenseEditRequested
            />
          </DebounceInitialRender>
        }
        | Templates => {
          <DebounceInitialRender>
            <Templates
              onCreateTemplate
              onEditTemplate
            />
          </DebounceInitialRender>
        }
      })
      <BottomNav.Container>
        <BottomNav.Tab
          label="Recurring expenses"
          icon="repeat"
          active=(activeTab == RecurringExpenses)
          onPress=(() => send(ChangeTab(RecurringExpenses)))
        />
        <BottomNav.Tab
          label="Templates"
          icon="file-document-box"
          active=(activeTab == Templates)
          onPress=(() => send(ChangeTab(Templates)))
        />
      </BottomNav.Container>
    </Column>
  }
}
