open ReactNative;
open RnMui;

type value = {
  title: string,
  autoCreate: bool,
  spending: SpendingForm.value,
  defaultTitle: string,
  unpaid: bool
};

type state = {
  titleRef: ref(option(ReasonReact.reactRef)),
  titleInitiallyFocused: ref(bool)
};

let component = ReasonReact.reducerComponent("TemplateForm");

let empty = (spending) => {
  title: "",
  autoCreate: false,
  spending: spending,
  defaultTitle: "",
  unpaid: false,
};

let getValue = (template: Template.t) => {
  unpaid: template.unpaid,
  title: template.title,
  autoCreate: template.autoCreate,
  spending: {
    totalAmount: template.totalAmount |> NumberInput.optToValue,
    currency: template.currency,
    categorization: template.categorization
  },
  defaultTitle: Js.Option.getWithDefault("", template.defaultTitle)
};

let make = (~templateId=?, ~template, ~onTemplateChanged, ~initiallyFocusTitle=false, ~onCategoryPickerRequested, ~onSplitFormRequested, _) => {

  let setTitleRef = (nullableRef, { ReasonReact.state }) => {
    let titleInitiallyFocused =
      switch (Js.Nullable.toOption(nullableRef)) {
      | None => state.titleInitiallyFocused.contents
      | Some(r) =>
        if (initiallyFocusTitle && ! state.titleInitiallyFocused.contents) {
          let _ = ReasonReact.refToJsObj(r)##focus();
          true
        } else {
          state.titleInitiallyFocused.contents
        }
      };
    state.titleInitiallyFocused := titleInitiallyFocused;
    state.titleRef := Js.Nullable.toOption(nullableRef);
  };

  {
    ...component,
    initialState: () => { titleRef: ref(None), titleInitiallyFocused: ref(false) },
    reducer: (_action: unit, _state) => ReasonReact.NoUpdate,
    render: ({handle}) => {
      let {title, autoCreate, defaultTitle, spending, unpaid} = template;
      <View>
        <C_Templates
          render=(
            (templates) => {
              /* let titleExists = templates |> Js.Array.some (fun template => String.lowercase template.Template.title === String.lowercase title); */
              let titleExists =
                templates
                |> Js.Array.some(
                     (template) =>
                       String.lowercase(template.Template.title) === String.lowercase(title)
                       && (
                         switch templateId {
                         | Some(tid) => template.id !== tid
                         | None => true
                         }
                       )
                   );
              <ListRow>
                <TextField
                  ref=handle(setTitleRef)
                  autoCapitalize="sentences"
                  autoCorrect=true
                  label="Title"
                  value=title
                  onChangeText=((title) => onTemplateChanged({...template, title}))
                  error=?(titleExists ? Some("A template with such title already exists") : None)
                />
              </ListRow>
            }
          )
        />
        <SpendingForm
          value=spending
          onValueChanged=(spending => onTemplateChanged({...template, spending}))
          onCategoryPickerRequested
          onSplitFormRequested
        />
        <OptionSwitchRow
          label="Auto create"
          value=autoCreate
          onValueChange=((autoCreate) => onTemplateChanged({...template, autoCreate}))
        />
        <ListRow>
          <TextField
            autoCapitalize="sentences"
            autoCorrect=false
            label="Expense title"
            value=defaultTitle
            onChangeText=((defaultTitle) => onTemplateChanged({...template, defaultTitle}))
          />
        </ListRow>
        <OptionSwitchRow label="Unpaid" inverted=true value=unpaid onValueChange=(unpaid => onTemplateChanged({...template, unpaid})) />
      </View>
    }
  }
};
