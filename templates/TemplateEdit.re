open ReactNative;

type state = {formValue: TemplateForm.value};

type actions =
  | UpdateFormState(TemplateForm.value)
  | UpdateTemplate;

let component = ReasonReact.reducerComponent("TemplateEdit");

let make = (~templateId, ~onTemplateUpdated, ~onCategoryPickerRequested, ~onSplitFormRequested, _) => {
  ...component,
  initialState: () => {formValue: TemplateDb.Data.get(templateId) |> Js.Option.getExn |> TemplateForm.getValue},
  reducer: (action, state) =>
    switch action {
    | UpdateFormState(formValue) => ReasonReact.Update({formValue: formValue})
    | UpdateTemplate =>
      ReasonReact.SideEffects(
        (
          (_) => {
            let {TemplateForm.title, autoCreate, spending, defaultTitle, unpaid} = state.formValue;
            switch (title, spending.totalAmount, spending.categorization) {
            | ("", _ , _) => Alert.alert(~title="Error", ~message="Title should not be empty", ())
            | (_, NumberInput.Invalid(_), _) => Alert.alert(~title="Error", ~message="Total amount is invalid", ())
            | (_, _, None) => Alert.alert(~title="Error", ~message="Category is required", ())
            | (_, _, Some(categorization)) => {
              A_Template.update(
                ~id=templateId,
                ~title,
                ~autoCreate,
                ~unpaid,
                ~categorization,
                ~totalAmount=spending.totalAmount |> NumberInput.toNumber,
                ~currency=spending.currency,
                ~defaultTitle=switch defaultTitle {
                | "" => None
                | t => Some(t)
                },
                ()
              ) |> (fun
                | NotFound | ValidationError(_) => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
                | Updated(_) => {
                  HistoryDb.currencyUsed(spending.currency);
                  onTemplateUpdated()
                }
              );
            }
            }
          }
        )
      )
    },
  render: ({send, state: { formValue }}) =>
    <ScreenWrap fab=(
      <ThemeActionButton icon="save" onPress=((_) => send(UpdateTemplate)) />
    )>
      <TemplateForm
        template=formValue
        onSplitFormRequested
        onTemplateChanged=((fs) => send(UpdateFormState(fs)))
        onCategoryPickerRequested
      />
    </ScreenWrap>
};
