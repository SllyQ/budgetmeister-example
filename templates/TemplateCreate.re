open ReactNative;

type state = {formValue: option(TemplateForm.value)};

type actions =
  | SplitCost
  | SplitSelected(ExpenseSplitForm.value)
  | SelectInitialCategory(Category.id)
  | UpdateFormState(TemplateForm.value)
  | CreateTemplate;

let component = ReasonReact.reducerComponent("TemplateCreate");

let make = (~onTemplateCreated, ~onCategoryPickerRequested, ~onSplitFormRequested, ~onCreateCategory, _) => {
  let handleBack = (_) => { onTemplateCreated(None);
    true
  };
  {
    ...component,
    initialState: () => {formValue: None},
    reducer: (action, state) =>
      switch action {
      | SplitCost => ReasonReact.SideEffects(({ send }) => {
        let currentParts = state.formValue |> C.Option.map(formValue => switch (formValue.TemplateForm.spending.categorization) {
          | Some(Single(part)) => [|{ Categorization.categoryId: part.categoryId, amount: formValue.spending.totalAmount |> NumberInput.toNumber, description: None }|]
          | Some(Split(parts)) => parts
          | None => [||]
        });
        onSplitFormRequested(currentParts, parts => send(SplitSelected(parts)))
      })
      | SplitSelected({ totalAmount, currency, categorization }) => {
        ReasonReact.Update {
          formValue: switch state.formValue {
            | None => Some(TemplateForm.empty({
              currency: currency,
              totalAmount: totalAmount |> NumberInput.toValue,
              categorization: Some(categorization)
            }))
            | Some(form) => Some({
              ...form,
              spending: {
                currency: currency,
                totalAmount: totalAmount |> NumberInput.toValue,
                categorization: Some(categorization)
              }
            })
          }
        }
      }
      | SelectInitialCategory(categoryId) => {
        ReasonReact.Update({
          formValue: Some(TemplateForm.empty({
            totalAmount: Empty,
            currency: SettingsDb.get().mainCurrency,
            categorization: Some(Single({ categoryId: categoryId }))
          }))
        })
      }
      | UpdateFormState(fs) => ReasonReact.Update({formValue: Some(fs)})
      | CreateTemplate =>
        ReasonReact.SideEffects(
          (
            (_) => {
              switch (state.formValue) {
                | None => ()
                | Some(formValue) => {
                  let {TemplateForm.title, autoCreate, spending, defaultTitle, unpaid} =
                    formValue;
                  switch (title, spending.totalAmount, spending.categorization) {
                  | ("", _ , _) => Alert.alert(~title="Error", ~message="Title should not be empty", ())
                  | (_, NumberInput.Invalid(_), _) => Alert.alert(~title="Error", ~message="Total amount is invalid", ())
                  | (_, _, None) => Alert.alert(~title="Error", ~message="Category is required", ())
                  | (_, _, Some(categorization)) => {
                    A_Template.create(
                      ~title,
                      ~autoCreate,
                      ~unpaid,
                      ~categorization,
                      ~totalAmount=spending.totalAmount |> NumberInput.toNumber,
                      ~currency=spending.currency,
                      ~defaultTitle=?
                        switch defaultTitle {
                        | "" => None
                        | t => Some(t)
                        },
                      ()
                    ) |> (fun
                      | ValidationError(_) => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
                      | Created(template) => {
                        Mixpanel.increment(EventConsts.templateCreated);
                        HistoryDb.currencyUsed(spending.currency);
                        onTemplateCreated(Some(template.id))
                      }
                    );
                  }
                  }
                }
              }
            }
          )
        )
      },
    didMount: (_) => {
      BackHandler.addEventListener("hardwareBackPress", handleBack);
    },
    willUnmount: (_) => BackHandler.removeEventListener("hardwareBackPress", handleBack),
    render: ({send, state: { formValue }}) =>
      switch formValue {
        | None => <CategorySelectPicker
            allowSplit=true onCreateCategory onSplitSelected=(() => send(SplitCost))
            onCategorySelected=((cid) => send(SelectInitialCategory(cid))) selectedCategoryId=None
            onGoBack=(() => onTemplateCreated(None))
          />
        | Some(formValue) => {
          <ScreenWrap fab=(
            <ThemeActionButton icon="save" onPress=((_) => send(CreateTemplate)) />
          )>
            <TemplateForm
              template=formValue
              onTemplateChanged=(fv => send(UpdateFormState(fv)))
              onSplitFormRequested
              initiallyFocusTitle=true
              onCategoryPickerRequested
            />
          </ScreenWrap>
        }
      }
  }
};
