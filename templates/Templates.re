open ReactNative;

open RnMui;

let component = ReasonReact.statelessComponent("Templates");

let make = (~onCreateTemplate, ~onEditTemplate, _) => {
  ...component,
  render: (_) =>
    <HelpWrapper helpComponent={
      title: "Templates",
      message: `String("Templates are for expenses which are very frequent yet not recurring. They can help you make the process of creating an expense faster by letting you fill more details like cost and description in advance and you can even choose the \"auto create\" option, so you can skip the expense creation screen to make the process of tracking expenses even faster!")
    }>
      <ScreenWrap fab=(
        <ThemeActionButton onPress=onCreateTemplate />
      )>
        <ListWrapper>
          <C_Templates
            render=(
              (templates) => {
                if (Array.length(templates) == 0) {
                  <TemplatesEmpty />
                } else {
                  let sortedTemplates = templates
                  |> Common.iSort(
                       (t1, t2) =>
                         String.compare(
                           String.lowercase(t1.Template.title),
                           String.lowercase(t2.title)
                         )
                     );
                  <View>
                    (
                      sortedTemplates
                      |> Array.map(
                           (template: Template.t) =>
                             <SwipeableRow
                               key=(DbTemplate.key(template.id))
                               onEdit=((_) => onEditTemplate(template.id))
                               onDelete=((_) => A_Template.delete(template.id) |> ignore)>
                               <ListItem
                                 centerElement=
                                   <Text value=template.title style=Styles.Text.listPrimary />
                               />
                             </SwipeableRow>
                         )
                      |> ReasonReact.array
                    )
                  </View>
                }
              }
            )
          />
        </ListWrapper>
      </ScreenWrap>
    </HelpWrapper>
};
