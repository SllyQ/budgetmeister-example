let component = ReasonReact.statelessComponent("C_User");

type user =
  | You(User.t)
  | User(User.t)
  | UnknownUser;

let get = (userId) => {
  let yourUser = AppAuthService.user^;
  let friends = FriendsService.get();
  switch (yourUser) {
    | Some(user) when user.id == userId => {
      You(user)
    }
    | _ => {
      let friend = friends |> Js.Array.find(friend => friend.User.id == userId);
      switch friend {
        | Some(friend) => User(friend)
        | None => UnknownUser
      }
    }
  }
};

let getUsername = (userId) => switch (get(userId)) {
  | You(_) => "You"
  | User(user) => user.username
  | UnknownUser => "Unknown user"
};

let make = (~userId, ~render, _) => {
  ...component,
  render: (_) => {
    render(get(userId));
  }
}
