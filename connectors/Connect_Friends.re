type state = {
  friends: array(User.t)
};

type actions =
  | UpdateFriends(array(User.t));

let component = ReasonReact.reducerComponent("Connect_Friends");

let make = (_, ~render) => {
  ...component,
  subscriptions: ({ send }) => [Sub(
    () => FriendsService.subscribe(
      (friends) => send(UpdateFriends(friends))
    ),
    FriendsService.unsubscribe
  )],
  initialState: () => {friends: FriendsService.get()},
  reducer: (action, _state) => switch action {
    | UpdateFriends(friends) => ReasonReact.Update({friends: friends})
  },
  render: ({state: { friends }}) => render(friends)
};
