open ReactNative;

let component = ReasonReact.statelessComponent("C_Alpha");

let make = (~render, _) => {
  ...component,
  render: (_) => {
    <Connect_ActiveUser render=(user => {
      switch user {
        | Some(user) when User.isAlpha(user) => render()
        | _ => ReasonReact.null
      }
    }) />
  }
}
