type state = {
  logMessages: list(DevLogService.t),
  devLogSub: option(DevLogService.sub)
};

type actions =
  | SetSub(DevLogService.sub)
  | UpdateLogMessages(list(DevLogService.t));

let component = ReasonReact.reducerComponent("C_DevLog");

let make = (_, ~render) => {
  ...component,
  initialState: () => {logMessages: DevLogService.messages^, devLogSub: None},
  reducer: (action, state) =>
    switch action {
    | SetSub(sub) => ReasonReact.Update({...state, devLogSub: Some(sub)})
    | UpdateLogMessages(logMessages) => ReasonReact.Update({...state, logMessages})
    },
  didMount: ({send}) => {
    let sub = DevLogService.onChange((messages) => send(UpdateLogMessages(messages)));
    send(SetSub(sub));
  },
  willUnmount: ({state}) =>
    switch state.devLogSub {
    | None => ()
    | Some(sub) => DevLogService.unsub(sub)
    },
  render: ({state: {logMessages}}) => render(logMessages)
};
