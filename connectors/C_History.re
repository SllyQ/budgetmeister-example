type state = {
  history: HistoryDb.history
};

type actions =
  | UpdateHistory(HistoryDb.history);

let component = ReasonReact.reducerComponent("C_History");

let make = (~render, _) => {
  ...component,
  initialState: () => { history: HistoryDb.get() },
  didMount: ({ onUnmount, send }) => {
    let sub = HistoryDb.subscribe(history => send(UpdateHistory(history)));
    onUnmount(() => HistoryDb.unsub(sub));
  },
  reducer: C.flip((_) => fun
    | UpdateHistory(history) => ReasonReact.Update { history: history }
  ),
  render: ({ state: { history }}) => {
    render(history)
  }
}
