let component = ReasonReact.statelessComponent("F_AutoSync");

let get = () => {
  let history = HistoryDb.get();
  AppAuthService.user^ |> C.Option.map(User.isAlpha) |> Js.Option.getWithDefault(false)
  || history.flagAutoSync
};

let make = (children) => {
  ...component,
  render: (_) => {
    <C_History render=((_) => {
      if (get()) {
        <Fragment>
          ...children
        </Fragment>
      } else {
        ReasonReact.null
      }
    }) />
  }
};
