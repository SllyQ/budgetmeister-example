let component = ReasonReact.statelessComponent("F_SharedExpenses");

let get = () => {
  let history = HistoryDb.get();
  AppAuthService.user^ |> C.Option.map(User.isAlpha) |> Js.Option.getWithDefault(false)
  || history.flagSharedExpenses
};

let make = (children) => {
  ...component,
  render: (_) => {
    <C_History render=((_) => {
      if (get()) {
        <Fragment>
          ...children
        </Fragment>
      } else {
        ReasonReact.null
      }
    }) />
  }
};
