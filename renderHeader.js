import React from 'react'
import { TabBar } from 'react-native-tab-view'

export const renderHeader = (props, extraProps) => {
  return <TabBar {...props} {...extraProps} />
}
