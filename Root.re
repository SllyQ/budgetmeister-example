open ReactNative;
let initPromise = RootService.init();

[@bs.module "react-native-splash-screen"] external rnSplashScreen : 'a = "default";

type state = {dataInitialized: bool};

type actions =
  | DataInitialized;

let component = ReasonReact.reducerComponent("Root");

let make = (_) => {
  ...component,
  initialState: () => {dataInitialized: false},
  didMount: ({send}) => {
    initPromise |> C.Promise.consume(() => send(DataInitialized));
  },
  reducer: (action, _state) =>
    switch action {
    | DataInitialized =>
      ReasonReact.UpdateWithSideEffects(
        {dataInitialized: true},
        (
          (_) => {
            let start = Js.Date.make();
            let _ =
              Js.Global.setTimeout(
                (_) => {
                  let donezo = Js.Date.make();
                  let duration =
                    Js.Date.getTime(donezo) -. Js.Date.getTime(start);
                  DevLogService.addMessage(
                    {j|Total init duration $(duration)ms|j}
                  );
                  rnSplashScreen##hide();
                },
                50
              );
            ();
          }
        )
      )
    },
  render: ({state: {dataInitialized}}) => {
    if (dataInitialized) {
      <Connect_SelectedTheme
        render=(
          theme =>
            <RnMui.ThemeProvider
              uiTheme={
                "palette": {
                  "primaryColor": theme.primary,
                  "accentColor": theme.accent,
                  "primaryTextColor": Colors.black,
                  "secondaryTextColor": Colors.grey,
                  "alternateTextColor": Colors.white,
                  "canvasColor": Colors.white
                },
                "bottomNavigationAction": {
                  "icon": {
                    "color": Colors.lightWhite
                  },
                  "label": {
                    "color": Colors.lightWhite
                  },
                  "iconActive": {
                    "color": Colors.white
                  },
                  "labelActive": {
                    "color": Colors.white,
                    "fontSize": 14
                  }
                }
              }>
              <Paper.Provider theme={
                "colors": {
                  "primary": theme.primary,
                  "primaryLight": theme.primaryLight,
                  "primaryDark": theme.primaryDark,
                  "accent": theme.accent,
                  "accentLight": theme.accentLight,
                  "accentDark": theme.accentDark,
                  "background": "#FAFAFA",
                  "paper": "#FFFFFF",
                  "text": "rgba(0, 0, 0, 0.87)",
                  "secondaryText": "rgba(0, 0, 0, 0.54)",
                  "disabled": "rgba(0, 0, 0, 0.2)",
                  "placeholder": "rgba(0, 0, 0, 0.2)"
                }
              }>
                <Menu.Context style=Style.(style([flex(1.)]))>
                  <View style=Style.(style([flex(1.)]))>
                    <View
                      style=Style.(
                              style([
                                paddingTop(Platform.equals(IOS) ? 20. : 0.),
                                backgroundColor(Colors.theme^.primaryDark)
                              ])
                            )>
                      <C_Settings render=(settings => {
                        <StatusBar
                          backgroundColor=(settings.lightBars ? Colors.grey : theme.primaryDark)
                          barStyle=`lightContent
                        />
                      }) />
                    </View>
                    /* <DrawerRouter /> */
                    <GlobalStack />
                    <SnackbarComp />
                    <ModalBoxPortal />
                    <ShakeModal />
                  </View>
                </Menu.Context>
              </Paper.Provider>
            </RnMui.ThemeProvider>
        )
      />;
    } else {
      <View />;
    }
  }
};
