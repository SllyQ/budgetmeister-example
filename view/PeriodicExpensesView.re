[@bs.val] external parseInt: string => int => int = "";

let storeKey = "MV_Periodic-expenses";
type periodInfo = {
  expenses: array(Expense.t),
  period: Utils.periodInfo
};

let encodePeriodInfo = periodInfo => Json.Encode.(object_([
  ("expenses", periodInfo.expenses |> array(Expense.encodeJson)),
  ("period", Utils.encodePeriodInfo(periodInfo.period))
]));

let decodePeriodInfo = json => Json.Decode.{
  expenses: json |> field("expenses", array(Expense.decodeJson)),
  period: json |> field("period", Utils.decodePeriodInfo)
};

module ViewCfg = {
  let name = "PeriodicExpensesView";

  type state = {
    periods: Js.Dict.t(periodInfo),
    expensesCache: array(Expense.t),
    budgetingPeriod: option(BudgetingPeriod.t)
  };

  let encodeState = state => Json.Encode.(object_([
    ("periods", state.periods |> Js.Dict.map((. period) => period |> encodePeriodInfo) |> dict)
  ]));

  let decodeState = json => Json.Decode.{
    periods: json |> field("periods", dict(decodePeriodInfo)),
    expensesCache: [||],
    budgetingPeriod: None
  };

  let initialState = Realm.getCfg(storeKey)
  |> C.Option.chain(Json.parse)
  |> C.Option.chain(json => try { Some(decodeState(json)) } { | _ => None })
  |> Js.Option.getWithDefault({
    periods: Js.Dict.empty(),
    expensesCache: [||],
    budgetingPeriod: None
  });

  type action =
    | ExpensesUpdated(array(Expense.t))
    | BudgetingPeriodChanged(BudgetingPeriod.t);

  let update = state => (fun
    | ExpensesUpdated(expenses) => {
      let budgetingPeriod = state.budgetingPeriod |> Js.Option.getExn;

      let (firstDate, lastDate) = ExpenseUtils.getBoundsDates(expenses);
      let budgetingPeriods = Utils.getBudgetingPeriods(~period=budgetingPeriod, ~firstDate, ~lastDate, ());
      let periods = budgetingPeriods |> Array.map((({ Utils.start, finish, title } as period)) => {
        let periodExpenses = expenses |> Js.Array.filter(expense =>
          expense.Expense.date |> DateFns.isWithinRange(start, finish)
        );
        (title, {
          expenses: periodExpenses,
          period
        })
      }) |> Js.Dict.fromArray;

      let newState = {
        ...state,
        periods,
        expensesCache: expenses
      };
      `UpdateWithSideEffects(newState, (_) => {
        Realm.setCfg(storeKey, newState |> encodeState |> Js.Json.stringify)
      })
    }
    | BudgetingPeriodChanged(budgetingPeriod) => {
      let expenses = state.expensesCache;
      if (Array.length(expenses) > 0) {
        let (firstDate, lastDate) = ExpenseUtils.getBoundsDates(expenses);
        let budgetingPeriods = Utils.getBudgetingPeriods(~period=budgetingPeriod, ~firstDate, ~lastDate, ());
        let periods = budgetingPeriods |> Array.map((({ Utils.start, finish, title } as period)) => {
          let periodExpenses = expenses |> Js.Array.filter(expense =>
            expense.Expense.date |> DateFns.isWithinRange(start, finish)
          );
          (title, {
            expenses: periodExpenses,
            period
          })
        }) |> Js.Dict.fromArray;

        let newState = {
          ...state,
          periods,
          budgetingPeriod: Some(budgetingPeriod)
        };
        `UpdateWithSideEffects(newState, (_) => {
          Realm.setCfg(storeKey, newState |> encodeState |> Js.Json.stringify)
        })
      } else {
        `Update({
          ...state,
          budgetingPeriod: Some(budgetingPeriod)
        })
      }
    }
  );
};

include Store.Make(ViewCfg);

let init = () => {
  let _sub = SettingsDb.subscribe(settings => dispatch(BudgetingPeriodChanged(settings.budgetingPeriod)));
  let budgetingPeriod = SettingsDb.get().budgetingPeriod;
  dispatch(BudgetingPeriodChanged(budgetingPeriod));
  let (_sub, expenses) = ExpenseDb.Data.subscribeAll(expenses => dispatch(ExpensesUpdated(expenses |> Array.of_list)));
  dispatch(ExpensesUpdated(expenses |> Array.of_list));
};
