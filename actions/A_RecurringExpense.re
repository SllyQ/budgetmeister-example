include RecurringExpenseDb.Actions;

let create = (~title, ~repeatFrom, ~repeatUntil, ~tagIds, ~repeatConfig, ~totalAmount, ~currency, ~unpaid=?, ~sharing=?, ~categorization, ()) => {
  let rez = create((), ~title, ~repeatFrom, ~repeatUntil, ~tagIds, ~repeatConfig, ~totalAmount, ~currency, ~unpaid?, ~sharing?, ~categorization);
  switch rez {
    | Created(_) => RecurringExpenseDb.Generator.updateGeneratedItems();
    | _ => ()
  };
  rez
};

let delete = (id) => {
  let optRecurringExpense = RecurringExpenseDb.Data.get(id);
  switch optRecurringExpense {
  | None => ()
  | Some(recurringExpense) =>
    let snackbarText = "Recurring expense \"" ++ (recurringExpense.title ++ "\" deleted");
    delete(id);

    SnackbarService.show(
      snackbarText,
      ~action={
        text: "Undo",
        handler: () => {
          undoDelete(id);
          SnackbarService.hideSnackbar()
        }
      }
    )
  }
}
