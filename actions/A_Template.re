include TemplateDb.Actions;

let delete = (id) => {
  let optTemplate = TemplateDb.Data.get(id);
  switch optTemplate {
  | None => ()
  | Some(template) =>
    let snackbarText = "Template \"" ++ (template.title ++ "\" deleted");
    delete(id);

    SnackbarService.show(
      snackbarText,
      ~action={
        text: "Undo",
        handler: () => {
          TemplateDb.Actions.undoDelete(id);
          SnackbarService.hideSnackbar()
        }
      }
    )
  }
};
