open ReactNative;
include ExpenseDb.Actions;

let delete = (id) => {
  let optExpense = ExpenseDb.Data.get(id);
  switch optExpense {
  | None => ()
  | Some(expense) =>
    let snackbarText =
      switch expense.title {
      | None => "Expense deleted"
      | Some(title) => "Expense \"" ++ (title ++ "\" deleted")
      };
    delete(id);
    SnackbarService.show(
      snackbarText,
      ~action={
        text: "Undo",
        handler: () => {
          undoDelete(id);
          SnackbarService.hideSnackbar()
        }
      }
    )
  }
};

let handleValidationError = (fun
  | _ => Alert.alert(~title="Error", ~message="Unknown error has occured", ())
);

type validatedOutput = {
  title: option(string),
  totalAmount: option(float),
  currency: Currency.t,
  tagIds: array(Tag.id),
  date: Js.Date.t,
  categorization: option(Categorization.t),
  unpaid: bool,
  sharing: option(ShareConfig.t)
};

let validateForm = (formValue: ExpenseForm.t) => {
  let fixedTitle = formValue.title == "" ? None : Some(formValue.title |> String.trim);
  switch (formValue.spending.totalAmount) {
    | Invalid(_) => {
      Alert.alert(~title="Error", ~message="Amount spent is not a valid number", ());
      None
    }
    | _ => {
      Some({
        title: fixedTitle,
        totalAmount: formValue.spending.totalAmount |> NumberInput.toNumber,
        currency: formValue.spending.currency,
        tagIds: formValue.tagIds |> Array.of_list,
        date: formValue.date,
        categorization: formValue.spending.categorization,
        unpaid: formValue.unpaid,
        sharing: None
      })
    }
  }
};

let createFromForm = (formValue) => {
  formValue |> validateForm |> (fun
    | Some({ title, totalAmount, currency, tagIds, date, categorization, unpaid, sharing }) => {
      create(
        ~title?,
        ~date,
        ~totalAmount,
        ~currency,
        ~tagIds,
        ~categorization?,
        ~unpaid,
        ~sharing?,
        ()
      ) |> (fun
        | Created(_) => true
        | _ => {
          Alert.alert(~title="Error", ~message="Unknown error has occured", ());
          false
        }
      )
    }
    | None => {
      false
    }
  )
};

let updateFromForm = (id, formValue) => {
  formValue |> validateForm |> (fun
    | Some({ title, totalAmount, currency, tagIds, date, categorization, unpaid, sharing }) => {
      update(
        ~id,
        ~title,
        ~date,
        ~totalAmount,
        ~currency,
        ~tagIds,
        ~categorization?,
        ~unpaid,
        ~sharing,
        ()
      ) |> (fun
        | Updated(_) => true
        | _ => {
          Alert.alert(~title="Error", ~message="Unknown error has occured", ());
          false
        }
      )
    }
    | None => {
      false
    }
  )
};
