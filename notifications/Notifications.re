open ReactNative;
open RnMui;

let component = ReasonReact.statelessComponent("Notifications");

let make = (~onRequestClose, _) => {
  ...component,
  render: (_) => {
    <View style=Style.(style([flex(1.), backgroundColor("white")]))>
      <Toolbar
        leftElement="close"
        onLeftElementPress=onRequestClose
        centerElement="Notifications"
      />
      <ScreenWrap>
        <View style=Style.(style([marginTop(Styles.Spacing.l)]))>
          <ListWrapper>
            <ListRow numberOfLines=1>
              <Text style=Styles.Text.listPrimary value="Notifications will be here one day. And they will be the best. Trust me, they will be better than all other notifications. Much better." />
            </ListRow>
          </ListWrapper>
        </View>
      </ScreenWrap>
    </View>
  }
}
