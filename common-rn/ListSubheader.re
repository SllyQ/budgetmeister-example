open ReactNative;
open RnMui;
open Styles.Spacing;

let component = ReasonReact.statelessComponent("ListSubheader");

type action = {
  icon: string,
  label: string,
  onPress: unit => unit
};

type rightValue =
  | Text(string)
  | Action(action);

let make = (~style as extraStyle=?, ~value, ~rightValue: option(rightValue)=?, ~rightValueColor=?, ~withDivider=false, ~withBackground=false, ~primary=false, _) => {
  ...component,
  render: (_) =>
    <View style=?extraStyle>
      (withDivider ? <Divider /> : ReasonReact.null)
      <View
        style=Style.(
          style([
            paddingLeft(xl),
            paddingRight(switch rightValue { | Some(Action(_)) => m | _ => xl }),
            height(40.),
            flexDirection(`row),
            justifyContent(`spaceBetween),
            alignItems(`center),
            withBackground ? backgroundColor("rgb(240, 240, 240)") : backgroundColor("transparent")
          ])
      )>
        <Text
          style=Style.(combine(Styles.Text.listSubheader, style([primary ? color(Colors.theme^.primary) : marginBottom(1.), marginBottom(1.)])))
          value
        />
        (switch rightValue {
          | None => ReasonReact.null
          | Some(Text(rv)) => {
            <Text value=rv
              style=Style.(combine(Styles.Text.listSubheader, style([
                switch rightValueColor { | Some(c) => color(c) | None => marginBottom(1.) },
                marginBottom(1.)
              ])))
            />
          }
          | Some(Action({ label, onPress, icon })) => {
            <MuiButton
              accent=true
              text=label
              onPress
              icon
            />
          }
        })
      </View>
    </View>
};
