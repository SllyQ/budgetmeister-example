open ExtUtils;

[@bs.module "react-native-datepicker"] external reactClass : ReasonReact.reactClass = "default";

let make =
    (
      ~minDate=?,
      ~maxDate=?,
      ~mode=?,
      ~format=?,
      ~showIcon=?,
      ~customStyles=?,
      ~date,
      ~onDateChange: string => unit,
      children
    ) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "minDate": minDate |> Js.Null_undefined.fromOption,
      "maxDate": maxDate |> Js.Null_undefined.fromOption,
      "mode": mode |> Js.Null_undefined.fromOption,
      "format": format |> Js.Null_undefined.fromOption,
      "customStyles": customStyles |> Js.Null_undefined.fromOption,
      "showIcon": showIcon |> jsOptBool |> Js.Null_undefined.fromOption,
      "date": date,
      "onDateChange": onDateChange
    }
  );
