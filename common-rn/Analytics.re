[@bs.module "react-native-fbsdk"] external appEventsLogger : 'a = "AppEventsLogger";

let internal_logEvent: (. string, int, Js.t({.})) => unit = appEventsLogger##logEvent;

let logEvent = (~name, ~toSum=1, ~params=?, ()) => {
  try {
    internal_logEvent(. name, toSum, params |> Js.Option.getWithDefault(Js.Obj.empty()) |> Obj.magic);
  } {
    | _ => Log.log("Analytics err")
  }
};
