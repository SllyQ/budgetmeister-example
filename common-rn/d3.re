module Arc = {
  type arcCfg = {.
    "innerRadius": float,
    "outerRadius": float,
    "startAngle": float,
    "endAngle": float,
    "padAngle": Js.Nullable.t(float)
  };

  type arc = (arcCfg) => string;

  [@bs.module "d3"] external arc_builder : unit => arc = "arc";

  let make = (~innerRadius, ~outerRadius, ~startAngle, ~endAngle, ~padAngle=?, ()) => {
    arc_builder()({
      "innerRadius": innerRadius,
      "outerRadius": outerRadius,
      "startAngle": startAngle,
      "endAngle": endAngle,
      "padAngle": padAngle |> Js.Nullable.fromOption
    })
  };
};

module Line = {
  [@bs.module "../../../../src/common-rn/d3.js"]
  external make : (~x: ('a => 'b), ~y: ('a => float)) => array('a) => string = "makeLine";
};

module Area = {
  [@bs.module "../../../../src/common-rn/d3.js"]
  external make : (~x: ('a => 'b), ~y: ('a => float), ~y0: ('a => float)) => array('a) => string = "makeArea";
};

module Scale = {
  /* TODO: This allows scales to be invoked (eg `x(0)`), but not all d3 scales map from float to float */
  type t = float => float;

  let apply = (a, t) => t(Obj.magic(a));

  /*
   type v =
     | Linear
     | Log
     | Pow
     | Sqrt
     | Time;
   */
  /* TODO: consider using a single make() and passing the scale type in (using v, above?): */
  [@bs.module "d3-scale"] external makeLinear : unit => t = "scaleLinear";
  [@bs.module "d3-scale"] external makeLog : unit => t = "scaleLog";
  [@bs.module "d3-scale"] external makePow : unit => t = "scalePow";
  [@bs.module "d3-scale"] external makeSqrt : unit => t = "scaleSqrt";
  [@bs.module "d3-scale"] external makeTime : unit => t = "scaleTime";
  [@bs.module "d3-scale"] external makeBand : unit => t = "scaleBand";

  /* Bindings for methods on range objects */
  /* TODO: implement missing methods */
  [@bs.send.pipe : t] external domain : array(float) => t = "";
  [@bs.send.pipe : t] external stringDomain : array(string) => t = "domain";
  [@bs.send.pipe : t] external range : array('a) => t = "";
  [@bs.send.pipe : t] external rangeRound : array('a) => t = "";
  [@bs.send.pipe : t] external clamp : Js.boolean => t = "";
  [@bs.send.pipe : t] external padding : float => t = "";
  [@bs.send.pipe : t] external ticks : int => array('a) = "";
  [@bs.send.pipe : t] external ticksAny : unit => array('a) = "ticks";
};

module Axis = {
  type t = unit => string;

  [@bs.module "d3"] external makeLeft : Scale.t => t = "axisLeft";
  [@bs.module "d3"] external makeBottom : Scale.t => t = "axisBottom";
};
