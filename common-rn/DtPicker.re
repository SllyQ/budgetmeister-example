[@bs.module "react-native-modal-datetime-picker"] external reactClass : ReasonReact.reactClass = "default";
let make = (~isVisible, ~onConfirm, ~onCancel, ~mode=?, ~minimumDate=?, ~maximumDate=?, ~date=?, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "isVisible": isVisible |> Js.Boolean.to_js_boolean,
      "onConfirm": onConfirm,
      "onCancel": onCancel,
      "mode": mode |> Js.Nullable.fromOption,
      "minimumDate": minimumDate |> Js.Nullable.fromOption,
      "maximumDate": maximumDate |> Js.Nullable.fromOption,
      "date": date |> Js.Nullable.fromOption
    }
  );
