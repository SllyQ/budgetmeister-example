open ReactNative;

let component = ReasonReact.statelessComponent("Divider");

let make = (~light=false, _) => {
  ...component,
  render: (_) =>
    <View
      style=Style.(
              style([
                paddingBottom(1.),
                widthPct(100.),
                backgroundColor(light ? "rgb(244, 244, 244)" : Colors.lightGrey)
              ])
            )
    />
};
