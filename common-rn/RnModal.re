open ExtUtils;

[@bs.module "react-native-modal"] external reactClass : ReasonReact.reactClass = "default";
let make = (~style=?, ~onBackdropPress=?, ~onBackButtonPress=?, ~isVisible, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "isVisible": isVisible |> jsBool,
      "hideOnBack": false,
      "onBackButtonPress": onBackButtonPress |> Js.Nullable.fromOption,
      "onBackdropPress": onBackdropPress |> Js.Nullable.fromOption,
      "style": style |> Js.Nullable.fromOption
    }
  );
