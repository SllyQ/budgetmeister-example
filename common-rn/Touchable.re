[@bs.module "react-native-platform-touchable"] external view : ReasonReact.reactClass = "default";

[@bs.send] external setOpacityTo : (ReasonReact.reactRef, float, int) => unit = "setOpacityTo";

let make =
    (
      ~accessible=?,
      ~accessibilityComponentType=?,
      ~accessibilityTraits=?,
      ~delayLongPress=?,
      ~delayPressIn=?,
      ~delayPressOut=?,
      ~disabled=?,
      ~hitSlop=?,
      ~style=?,
      ~onLayout=?,
      ~onPress: option(unit => unit)=?,
      ~onLongPress: option(unit => unit)=?,
      ~onPressIn=?,
      ~onPressOut=?,
      ~pressRetentionOffset=?,
      ~activeOpacity=?,
      ~focusedOpacity=?,
      ~tvParallaxProperties=?
    ) =>
  ReasonReact.wrapJsForReason(
    ~reactClass=view,
    ~props=
      Js.Undefined.(
        {
          "accessible": fromOption(UtilsRN.optBoolToOptJsBoolean(accessible)),
          "delayLongPress": fromOption(delayLongPress),
          "delayPressIn": fromOption(delayPressIn),
          "delayPressOut": fromOption(delayPressOut),
          "disabled": fromOption(UtilsRN.optBoolToOptJsBoolean(disabled)),
          "hitSlop": fromOption(hitSlop),
          "style": fromOption(style),
          "onLayout": fromOption(onLayout),
          "onPress": fromOption(onPress),
          "onLongPress": fromOption(onLongPress),
          "onPressIn": fromOption(onPressIn),
          "onPressOut": fromOption(onPressOut),
          "pressRetentionOffset": fromOption(pressRetentionOffset),
          "accessibilityComponentType":
            fromOption(
              UtilsRN.option_map(
                (x) =>
                  switch x {
                  | `none => "none"
                  | `button => "button"
                  | `radiobutton_checked => "radiobutton_checked-none"
                  | `radiobutton_unchecked => "radiobutton_unchecked"
                  },
                accessibilityComponentType
              )
            ),
          "accessibilityTraits":
            fromOption(
              UtilsRN.option_map(
                (traits) => {
                  let to_string =
                    fun
                    | `none => "none"
                    | `button => "button"
                    | `link => "link"
                    | `header => "header"
                    | `search => "search"
                    | `image => "image"
                    | `selected => "selected"
                    | `plays => "plays"
                    | `key => "key"
                    | `text => "text"
                    | `summary => "summary"
                    | `disabled => "disabled"
                    | `frequentUpdates => "frequentUpdates"
                    | `startsMedia => "startsMedia"
                    | `adjustable => "adjustable"
                    | `allowsDirectInteraction => "allowsDirectInteraction"
                    | `pageTurn => "pageTurn";
                  traits |> List.map(to_string) |> Array.of_list
                },
                accessibilityTraits
              )
            ),
          "focusedOpacity": fromOption(focusedOpacity),
          "activeOpacity": fromOption(activeOpacity),
          "tvParallaxProperties": fromOption(tvParallaxProperties)
        }
      )
  );
