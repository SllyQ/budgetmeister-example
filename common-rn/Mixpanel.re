[@bs.module "react-native-mixpanel"] external mixpanel: {.
  [@bs.meth] "track": string => unit,
  [@bs.meth] "trackWithProperties": string => Js.t({..}) => unit,
  [@bs.meth] "createAlias": string => unit,
  [@bs.meth] "identify": string => unit,
  [@bs.meth] "reset": unit => unit,
  [@bs.meth] "set": Js.t({..}) => unit,
  [@bs.meth] "increment": string => int => unit
} = "default";


let track = (eventName) => mixpanel##track(eventName);
let trackWithProperties = (eventName, properties) => mixpanel##trackWithProperties(eventName, properties);
let createAlias = (alias) => mixpanel##createAlias(alias);
let identify = (alias) => mixpanel##identify(alias);
let reset = () => mixpanel##reset();
let set = (properties) => mixpanel##set(properties);
let increment = (~by=1, counterName) => mixpanel##increment(counterName, by);
