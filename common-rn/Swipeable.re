[@bs.module "react-native-swipeable"] external reactClass : ReasonReact.reactClass = "default";

let make =
    (
      ~leftContent=?,
      ~rightContent=?,
      ~leftButtons=?,
      ~rightButtons=?,
      ~rightButtonContainerStyle=?,
      ~rightButtonWidth=?,
      ~onRightActionRelease=?,
      ~ref=?,
      children
    ) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "ref": ref |> Js.Nullable.fromOption,
      "leftContent": leftContent |> Js.Nullable.fromOption,
      "rightContent": rightContent |> Js.Nullable.fromOption,
      "leftButtons": leftButtons |> Js.Nullable.fromOption,
      "rightButtons": rightButtons |> Js.Nullable.fromOption,
      "rightButtonWidth": rightButtonWidth |> Js.Nullable.fromOption,
      "rightButtonContainerStyle": rightButtonContainerStyle |> Js.Nullable.fromOption,
      "onRightActionRelease": onRightActionRelease |> Js.Nullable.fromOption
    }
  );

module AnimatedView = {
  [@bs.scope "Animated"] [@bs.module "react-native"] external reactClass : ReasonReact.reactClass =
    "View";
  let make = (~style=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={"style": style |> Js.Null_undefined.fromOption}
    );
};

module Easing = {
  [@bs.scope "Easing"] [@bs.module "react-native"]
  external bezier : (float, float, float, float, unit) => 'a =
    "bezier";
};
