open ExtUtils;

[@bs.module "react-native-loading-spinner-overlay"] external reactClass : ReasonReact.reactClass = "default";
let make = (~visible, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "visible": visible |> jsBool
    }
  );
