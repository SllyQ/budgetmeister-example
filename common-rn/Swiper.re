open ExtUtils;
open ReactNative;

let buttonStyle = Style.(style([color(Colors.theme^.primary), fontSize(50.), fontFamily("Arial")]));

[@bs.module] external reactClass : ReasonReact.reactClass = "react-native-swiper";

let make = (~loadMinimal=?, ~loadMinimalSize=?, ~loadMinimalLoaded=?, ~showsButtons=?, ~index=?, ~loop=?, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "showsButtons": showsButtons |> jsOptBool |> Js.Nullable.fromOption,
      "index": index |> Js.Nullable.fromOption,
      "loop": loop |> jsOptBool |> Js.Nullable.fromOption,
      "activeDotColor": Colors.theme^.primary,
      "nextButton": <Text style=buttonStyle value={j|›|j} />,
      "prevButton": <Text style=buttonStyle value={j|‹|j} />,
      "loadMinimal": loadMinimal |> jsOptBool |> Js.Nullable.fromOption,
      "loadMinimalSize": loadMinimalSize |> Js.Nullable.fromOption,
      "loadMinimalLoaded": loadMinimalLoaded |> Js.Nullable.fromOption
    }
  );
