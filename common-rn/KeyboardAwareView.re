[@bs.module "react-native"] external reactClass : ReasonReact.reactClass = "KeyboardAvoidingView";

let make = (~behavior=?, ~contentContainerStyle=?, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "behavior": behavior |> Js.Nullable.fromOption,
      "contentContainerStyle": contentContainerStyle |> Js.Nullable.fromOption
    }
  );
