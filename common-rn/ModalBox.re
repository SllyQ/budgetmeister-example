open ExtUtils;

[@bs.module] external reactClass : ReasonReact.reactClass = "react-native-modalbox";
let make = (~isOpen, ~onClosed, ~style=?, ~position=?, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "position": position |> Js.Nullable.fromOption,
      "style": style |> Js.Nullable.fromOption,
      "isOpen": isOpen |> jsBool,
      "onClosed": onClosed
    }
  );
