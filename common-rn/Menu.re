[@bs.module] external reactClass : ReasonReact.reactClass = "react-native-menu";

let make = (~onSelect=?, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={"onSelect": onSelect |> Js.Nullable.fromOption}
  );

module Options = {
  [@bs.module "react-native-menu"] external reactClass : ReasonReact.reactClass = "MenuOptions";
  let make = (children) =>
    ReasonReact.wrapJsForReason(~reactClass, children, ~props=Js.Obj.empty());
};

module Option = {
  [@bs.module "react-native-menu"] external reactClass : ReasonReact.reactClass = "MenuOption";
  let make = (~icon=?, ~value=?, ~onPress=?, ~renderTouchable=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "value": value |> Js.Nullable.fromOption,
        "icon": icon |> Js.Nullable.fromOption,
        "onPress": onPress |> Js.Nullable.fromOption,
        "renderTouchable":
          switch renderTouchable {
          | Some(rt) => rt
          | None => (() => <Touchable />)
          }
      }
    );
};

module Trigger = {
  [@bs.module "react-native-menu"] external reactClass : ReasonReact.reactClass = "MenuTrigger";
  let make = (~renderTouchable=?, ~style=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "style": style |> Js.Nullable.fromOption,
        "renderTouchable":
          switch renderTouchable {
          | Some(rt) => rt
          | None => (() => <Touchable />)
          }
      }
    );
};

module Context = {
  [@bs.module "react-native-menu"] external reactClass : ReasonReact.reactClass = "MenuContext";
  let make = (~style=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={"style": Js.Null_undefined.fromOption(style), "lazyRender": 200.}
    );
};
