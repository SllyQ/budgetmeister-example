module View = {
  [@bs.module "react-native-animatable"] external reactClass : ReasonReact.reactClass = "View";
  let make = (~animation=?, ~duration=?, ~ref=?, ~iterationCount=?, ~easing=?, ~direction=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "ref": ref |> Js.Nullable.fromOption,
        "animation": animation |> Js.Nullable.fromOption,
        "duration": duration |> Js.Nullable.fromOption,
        "iterationCount": iterationCount |> C.Option.map(fun
          | `Iterations(n) => n
          | `Infinite => Obj.magic("infinite")
        ) |> Js.Nullable.fromOption,
        "easing": easing |> Js.Nullable.fromOption,
        "direction": direction |> Js.Nullable.fromOption
      }
    );
};
