open ExtUtils;

[@bs.val] external pi : float = "Math.PI";

[@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "default";
let make = (~width, ~height, ~fill=?, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "width": width,
      "height": height,
      "fill": fill |> Js.Nullable.fromOption
    }
  );

module Circle = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Circle";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module Ellipse = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Ellipse";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module G = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "G";
  let make = (~x=?, ~y=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "x": x |> Js.Nullable.fromOption,
        "y": y |> Js.Nullable.fromOption
      }
    );
};

module LinearGradient = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "LinearGradient";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module RadialGradient = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "RadialGradient";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module Line = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Line";
  let make = (~style=?, ~x1, ~y1, ~x2, ~y2, ~stroke=?, ~strokeWidth=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "x1": x1,
        "y1": y1,
        "x2": x2,
        "y2": y2,
        "stroke": stroke |> Js.Nullable.fromOption,
        "strokeWidth": strokeWidth |> Js.Nullable.fromOption,
        "style": style |> Js.Nullable.fromOption
      }
    );
};

module Path = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Path";
  let make = (~opacity=?, ~d, ~stroke=?, ~fill: option(string)=?, ~strokeWidth=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "d": d,
        "stroke": stroke |> Js.Nullable.fromOption,
        "opacity": opacity |> Js.Nullable.fromOption,
        "strokeWidth": strokeWidth |> Js.Nullable.fromOption,
        "fill": fill |> Js.Nullable.fromOption
      }
    );
};

module Polygon = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Polygon";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module Polyline = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Polyline";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module Rect = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Rect";
  let make = (~x, ~y, ~width, ~height, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "x": x,
        "y": y,
        "width": width,
        "height": height
      }
    );
};

module Symbol = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Symbol";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module Text = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Text";
  let make = (~x=?, ~y=?, ~dx=?, ~dy=?, ~stroke=?, ~fill=?, ~textAnchor=?, ~fontSize=?, ~fontWeight=?, ~fontFamily=?, ~verticalAlgin=?, ~transform=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "x": x |> Js.Nullable.fromOption,
        "y": y |> Js.Nullable.fromOption,
        "dx": dx |> Js.Nullable.fromOption,
        "dy": dy |> Js.Nullable.fromOption,
        "stroke": stroke |> Js.Nullable.fromOption,
        "fill": fill |> Js.Nullable.fromOption,
        "verticalAlgin": verticalAlgin |> Js.Nullable.fromOption,
        "textAnchor": textAnchor |> Js.Nullable.fromOption,
        "transform": transform |> Js.Nullable.fromOption,
        "fontSize": fontSize |> Js.Nullable.fromOption,
        "fontFamily": fontFamily |> Js.Nullable.fromOption,
        "fontWeight": fontWeight |> Js.Nullable.fromOption
      }
    );
};

module Use = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Use";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module Defs = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Defs";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};

module Stop = {
  [@bs.module "react-native-svg"] external reactClass : ReasonReact.reactClass = "Stop";
  let make = (children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props=Js.Obj.empty()
    );
};
