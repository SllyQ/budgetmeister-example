open ExtUtils;

[@bs.module "react-native-paper"] external defaultTheme : 'a = "DefaultTheme";

module Provider = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "Provider";
  let make = (~theme=?, children) =>
    ReasonReact.wrapJsForReason(~reactClass, children, ~props={
      "theme": switch theme {
        | Some(theme) => Js.Nullable.return(Js.Obj.assign(defaultTheme, theme))
        | None => Js.Nullable.undefined
      }
    });
};

module RadioButton = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "RadioButton";
  let make = (~value=?, ~checked, ~onPress=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "value": value |> Js.Nullable.fromOption,
        "checked": checked |> jsBool,
        "onPress": onPress |> Js.Nullable.fromOption
      }
    );
};

module TouchableRipple = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "TouchableRipple";
  let make = (~onPress, ~style=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "onPress": onPress,
        "style": style |> Js.Nullable.fromOption
      }
    );
};

module Checkbox = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "Checkbox";
  let make = (~onPress=?, ~checked, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "onPress": onPress |> Js.Nullable.fromOption,
        "checked": checked |> jsBool
      }
    );
};

module Dialog = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "Dialog";
  let make = (~dismissable=true, ~onDismiss, ~visible, ~style=?, ~theme=?, children) => {
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "dismissable": dismissable |> jsBool,
        "onDismiss": onDismiss,
        "visible": visible |> jsBool,
        "style": style |> Js.Nullable.fromOption,
        "theme": theme |> Js.Nullable.fromOption
      }
    );
  };

  module Title = {
    [@bs.module "react-native-paper"]
    external reactClass : ReasonReact.reactClass = "DialogTitle";
    let make = (~theme=?, ~style=?, ~value, _) => {
      ReasonReact.wrapJsForReason(
        ~reactClass,
        value,
        ~props={
          "style": style |> Js.Nullable.fromOption,
          "theme": theme |> Js.Nullable.fromOption
        }
      );
    };
  };

  module ScrollArea = {
    [@bs.module "react-native-paper"]
    external reactClass : ReasonReact.reactClass = "DialogScrollArea";
    let make = (~style=?, children) => {
      ReasonReact.wrapJsForReason(
        ~reactClass,
        children,
        ~props={
          "style": style |> Js.Nullable.fromOption
        }
      );
    };
  };

  module Content = {
    [@bs.module "react-native-paper"]
    external reactClass : ReasonReact.reactClass = "DialogContent";
    let make = (~style=?, children) => {
      ReasonReact.wrapJsForReason(
        ~reactClass,
        children,
        ~props={
          "style": style |> Js.Nullable.fromOption
        }
      );
    };
  };

  module Actions = {
    [@bs.module "react-native-paper"]
    external reactClass : ReasonReact.reactClass = "DialogActions";
    let make = (~style=?, children) => {
      ReasonReact.wrapJsForReason(
        ~reactClass,
        children,
        ~props={
          "style": style |> Js.Nullable.fromOption
        }
      );
    };
  };
};

module MuiButton = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "Button";
  let make = (~disabled=?, ~compact=?, ~raised=?, ~primary=?, ~dark=?, ~loading=?, ~icon=?, ~color=?, ~onPress, ~label, ~style=?, ~theme=?, _) => {
    ReasonReact.wrapJsForReason(
      ~reactClass,
      label,
      ~props={
        "disabled": disabled |> jsOptBool |> Js.Nullable.fromOption,
        "compact": compact |> jsOptBool |> Js.Nullable.fromOption,
        "raised": raised |> jsOptBool |> Js.Nullable.fromOption,
        "primary": primary |> jsOptBool |> Js.Nullable.fromOption,
        "dark": dark |> jsOptBool |> Js.Nullable.fromOption,
        "loading": loading |> jsOptBool |> Js.Nullable.fromOption,
        "icon": icon |> Js.Nullable.fromOption,
        "color": color |> Js.Nullable.fromOption,
        "onPress": onPress,
        "style": style |> Js.Nullable.fromOption,
        "theme": theme |> Js.Nullable.fromOption
      }
    );
  };
};

module Fab = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "FAB";
  let make = (~style=?, ~theme=?, ~small=?, ~dark=?, ~icon, ~color=?, ~onPress, children) => {
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "style": style |> Js.Nullable.fromOption,
        "theme": theme |> Js.Nullable.fromOption,
        "small": small |> jsOptBool |> Js.Nullable.fromOption,
        "dark": dark  |> jsOptBool|> Js.Nullable.fromOption,
        "color": color |> Js.Nullable.fromOption,
        "icon": icon,
        "onPress": onPress
      }
    );
  };
};

module TextField = {
  [@bs.module "react-native-paper"]
  external reactClass : ReasonReact.reactClass = "TextInput";
  let make = (~style=?, ~theme=?, ~value, ~onChangeText, ~onBlur=?, ~onFocus=?, ~numberOfLines=?,
    ~multiline=?, ~underlineColor=?, ~placeholder=?, ~label=?, ~disabled=?, ~autoCorrect=?,
    ~autoCapitalize=?, ~onSubmitEditing=?, ~returnKeyType=?, children
  ) => {
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "style": style |> Js.Nullable.fromOption,
        "theme": theme |> Js.Nullable.fromOption,
        "autoCorrect": autoCorrect |> jsOptBool |> Js.Nullable.fromOption,
        "autoCapitalize": autoCapitalize |> Js.Nullable.fromOption,
        "onChangeText": onChangeText,
        "value": value,
        "onBlur": onBlur |> Js.Nullable.fromOption,
        "onFocus": onFocus |> Js.Nullable.fromOption,
        "numberOfLines": numberOfLines |> Js.Nullable.fromOption,
        "multiline": multiline |> jsOptBool |> Js.Nullable.fromOption,
        "disabled": disabled |> jsOptBool |> Js.Nullable.fromOption,
        "label": label |> Js.Nullable.fromOption,
        "placeholder": placeholder |> Js.Nullable.fromOption,
        "onSubmitEditing": onSubmitEditing |> Js.Nullable.fromOption,
        "returnKeyType": returnKeyType |> Js.Nullable.fromOption,
        "underlineColor": underlineColor |> Js.Nullable.fromOption
      }
    );
  };
};
