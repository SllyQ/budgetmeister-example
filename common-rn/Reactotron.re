[@bs.scope "default"] [@bs.module "reactotron-react-native"] external log : ('a) => unit = "log";
[@bs.scope "default"] [@bs.module "reactotron-react-native"] external log2 : ('a, 'b) => unit = "log";
[@bs.scope "default"] [@bs.module "reactotron-react-native"] external log3 : ('a, 'b, 'c) => unit = "log";

let init = () => ();

Log.internal_log := Some(log);
Log.internal_log2 := Some(log2);
Log.internal_log3 := Some(log3);
