open ExtUtils;

[@bs.module "react-native-calendars"]
external reactClass : ReasonReact.reactClass = "Calendar";

type dateFormat = {
  .
  "year": int,
  "month": int,
  "day": int,
  "timestamp": float,
  "dateString": string
};

let make =
    (
      ~markingType=?,
      ~current=?,
      ~minDate=?,
      ~maxDate=?,
      ~onDayPress: option(dateFormat => unit)=?,
      ~onMonthChange: option(dateFormat => unit)=?,
      ~markedDates=?,
      ~dayComponent=?,
      children
    ) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "current": current |> Js.Nullable.fromOption,
      "minDate": minDate |> Js.Nullable.fromOption,
      "maxDate": maxDate |> Js.Nullable.fromOption,
      "onDayPress": onDayPress |> Js.Nullable.fromOption,
      "onMonthChange": onMonthChange |> Js.Nullable.fromOption,
      "markedDates": markedDates |> Js.Nullable.fromOption,
      "markingType": markingType |> Js.Nullable.fromOption,
      "dayComponent": dayComponent |> Js.Nullable.fromOption,
      "theme": {
        "textSectionTitleColor": Colors.black,
        "selectedDayBackgroundColor": Colors.theme^.primary,
        "selectedDayTextColor": Colors.white,
        "todayTextColor": Colors.theme^.primary,
        "dayTextColor": Colors.black,
        "textDisabledColor": Colors.grey,
        "dotColor": Colors.theme^.primary,
        "selectedDotColor": Colors.white,
        "arrowColor": Colors.theme^.primary,
        "monthTextColor": Colors.black
      }
    }
  );

module List = {
  [@bs.module "react-native-calendars"]
  external reactClass : ReasonReact.reactClass = "CalendarList";

  let make =
      (
        ~horizontal=?,
        ~pagingEnabled=?,
        ~markingType=?,
        ~current=?,
        ~minDate=?,
        ~maxDate=?,
        ~onDayPress: option(dateFormat => unit)=?,
        ~onMonthChange: option(dateFormat => unit)=?,
        ~markedDates=?,
        ~dayComponent=?,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "horizontal": horizontal |> jsOptBool |> Js.Nullable.fromOption,
        "pagingEnabled": pagingEnabled |> jsOptBool |> Js.Nullable.fromOption,
        "current": current |> Js.Nullable.fromOption,
        "minDate": minDate |> Js.Nullable.fromOption,
        "maxDate": maxDate |> Js.Nullable.fromOption,
        "onDayPress": onDayPress |> Js.Nullable.fromOption,
        "onMonthChange": onMonthChange |> Js.Nullable.fromOption,
        "markedDates": markedDates |> Js.Nullable.fromOption,
        "markingType": markingType |> Js.Nullable.fromOption,
        "dayComponent": dayComponent |> Js.Nullable.fromOption,
        "theme": {
          "textSectionTitleColor": Colors.black,
          "selectedDayBackgroundColor": Colors.theme^.primary,
          "selectedDayTextColor": Colors.white,
          "todayTextColor": Colors.theme^.primary,
          "dayTextColor": Colors.black,
          "textDisabledColor": Colors.grey,
          "dotColor": Colors.theme^.primary,
          "selectedDotColor": Colors.white,
          "arrowColor": Colors.theme^.primary,
          "monthTextColor": Colors.black
        }
      }
    );
}
