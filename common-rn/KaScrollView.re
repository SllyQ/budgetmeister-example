[@bs.module "react-native-keyboard-aware-scroll-view"] external reactClass : ReasonReact.reactClass = "KeyboardAwareScrollView";

let make = (~keyboardShouldPersistTaps: option(string)=?, ~contentContainerStyle=?, ~style=?, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "keyboardShouldPersistTaps": keyboardShouldPersistTaps |> Js.Nullable.fromOption,
      "contentContainerStyle": contentContainerStyle |> Js.Nullable.fromOption,
      "style": style |> Js.Nullable.fromOption
    }
  );
