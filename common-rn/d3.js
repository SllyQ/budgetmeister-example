import * as d3 from "d3"

export const makeLine = (x, y, data) => {
  return d3
    .line()
    .x(x)
    .y(y)(data)
}

export const makeArea = (x, y, y0, data) => {
  return d3
    .area()
    .x(x)
    .y1(y)
    .y0(y0)(data)
}
