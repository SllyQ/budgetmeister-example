[@bs.module "react-navigation"]
external drawerNav : 'a => 'b => ReasonReact.reactClass = "DrawerNavigator";

[@bs.module "react-navigation"]
external stackNav : 'c => 'd => ReasonReact.reactClass = "StackNavigator";

[@bs.module "react-navigation"]
external tabsNav : 'c => 'd => ReasonReact.reactClass = "TabNavigator";
