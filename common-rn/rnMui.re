open ExtUtils;

module ThemeProvider = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "ThemeProvider";
  let make = (children, ~uiTheme) =>
    ReasonReact.wrapJsForReason(~reactClass, children, ~props={"uiTheme": uiTheme});
};

module MuiButton = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass = "Button";

  let make =
      (
        ~disabled=?,
        ~upperCase=?,
        ~primary=?,
        ~accent=?,
        ~raised=?,
        ~icon=?,
        ~style=?,
        ~onLongPress=?,
        ~uppercase=?,
        ~text,
        ~onPress,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "disabled": disabled |> jsOptBool |> Js.Nullable.fromOption,
        "primary": primary |> jsOptBool |> Js.Nullable.fromOption,
        "accent": accent |> jsOptBool |> Js.Nullable.fromOption,
        "raised": raised |> jsOptBool |> Js.Nullable.fromOption,
        "upperCase": upperCase |> jsOptBool |> Js.Nullable.fromOption,
        "icon": icon |> Js.Nullable.fromOption,
        "style": style |> Js.Nullable.fromOption,
        "uppercase": uppercase |> jsOptBool |> Js.Nullable.fromOption,
        "onPress": onPress,
        "onLongPress": onLongPress |> Js.Nullable.fromOption,
        "text": text
      }
    );
};

module Icon = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass = "Icon";
  let make = (~size=?, ~color=?, ~style=?, ~name, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "color": color |> Js.Nullable.fromOption,
        "style": style |> Js.Nullable.fromOption,
        "name": name,
        "size": size |> Js.Nullable.fromOption
      }
    );
};

module IconToggle = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass = "Icon";
  let make = (~size=?, ~name=?, ~onPress, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "name": name |> Js.Null_undefined.fromOption,
        "size": size |> Js.Null_undefined.fromOption,
        "onPress": onPress
      }
    );
};

module ActionButton = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "ActionButton";
  let make = (~style=?, ~hidden=?, ~icon=?, ~onPress, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "icon": icon |> Js.Nullable.fromOption,
        "onPress": C.throttle(onPress),
        "hidden": hidden |> jsOptBool |> Js.Nullable.fromOption,
        "style": style |> Js.Nullable.fromOption
      }
    );
};

module BottomNavigation = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "BottomNavigation";
  let make = (~hidden=?, ~style=?, ~active, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "active": active,
        "style": style |> Js.Nullable.fromOption,
        "hidden": hidden |> jsOptBool |> Js.Nullable.fromOption
      }
    );
  module Action = {
    [@bs.module "react-native-material-ui"] [@bs.scope "BottomNavigation"]
    external reactClass : ReasonReact.reactClass =
      "Action";
    let make = (~label=?, ~icon, ~onPress, children) =>
      ReasonReact.wrapJsForReason(
        ~reactClass,
        children,
        ~props={
          "icon": icon,
          "label": label |> Js.Null_undefined.fromOption,
          "onPress": onPress,
          "style": {
            "container": {
              "backgroundColor": Colors.theme^.primary
            }
          }
        }
      );
  };
};

module Dialog = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass = "Dialog";
  let make = (children) => ReasonReact.wrapJsForReason(~reactClass, children, ~props={"_": ""});
  module Title = {
    [@bs.scope "Dialog"] [@bs.module "react-native-material-ui"]
    external reactClass : ReasonReact.reactClass =
      "Title";
    let make = (children) => ReasonReact.wrapJsForReason(~reactClass, children, ~props={"_": ""});
  };
  module Content = {
    [@bs.scope "Dialog"] [@bs.module "react-native-material-ui"]
    external reactClass : ReasonReact.reactClass =
      "Content";
    let make = (children) => ReasonReact.wrapJsForReason(~reactClass, children, ~props={"_": ""});
  };
  module Actions = {
    [@bs.scope "Dialog"] [@bs.module "react-native-material-ui"]
    external reactClass : ReasonReact.reactClass =
      "Actions";
    let make = (children) => ReasonReact.wrapJsForReason(~reactClass, children, ~props={"_": ""});
  };
};

module Checkbox = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "Checkbox";
  let make = (~disabled=?, ~label, ~checked, ~value=?, ~onCheck=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "label": label,
        "checked": checked |> jsBool,
        "disabled": disabled |> jsOptBool |> Js.Null_undefined.fromOption,
        "value": value |> jsOptBool |> Js.Null_undefined.fromOption,
        "onCheck": switch onCheck {
          | Some(onCheck) => Some((checked, value) => onCheck((checked, value)))
          | None => None
        } |> Js.Nullable.fromOption
      }
    );
};

module Toolbar = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass = "Toolbar";
  let make =
      (
        ~style=?,
        ~leftElement=?,
        ~onLeftElementPress=?,
        ~centerElement=?,
        ~rightElement=?,
        ~onRightElementPress=?,
        ~titleText=?,
        ~translucent=?,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "style": style |> Js.Nullable.fromOption,
        "leftElement": leftElement |> Js.Nullable.fromOption,
        "onLeftElementPress": onLeftElementPress |> Js.Nullable.fromOption,
        "centerElement": centerElement |> Js.Nullable.fromOption,
        "titleText": titleText |> Js.Nullable.fromOption,
        "rightElement": rightElement |> Js.Nullable.fromOption,
        "onRightElementPress": onRightElementPress |> Js.Nullable.fromOption,
        "translucent": translucent |> jsOptBool |> Js.Nullable.fromOption
      }
    );
};

module Dropdown = {
  [@bs.module "react-native-material-dropdown"] external reactClass : ReasonReact.reactClass =
    "Dropdown";
  let make = (~label, ~data, ~value, ~onChangeText, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "label": label,
        "data": data,
        "value": value,
        "onChangeText": (_val, i) => onChangeText((_val, i))
      }
    );
};

module ListItem = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "ListItem";
  let make =
      (
        ~dense=?,
        ~divider=?,
        ~onPress=?,
        ~onLongPress=?,
        ~leftElement=?,
        ~centerElement=?,
        ~rightElement=?,
        ~onLeftElementPress=?,
        ~onRightElementPress=?,
        ~numberOfLines=?,
        ~style=?,
        children
      ) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "dense": dense |> jsOptBool |> Js.Nullable.fromOption,
        "divider": divider |> jsOptBool |> Js.Nullable.fromOption,
        "onPress": onPress |> Js.Nullable.fromOption,
        "onLongPress": onLongPress |> Js.Nullable.fromOption,
        "leftElement": leftElement |> Js.Nullable.fromOption,
        "centerElement": centerElement |> Js.Nullable.fromOption,
        "rightElement": rightElement |> Js.Nullable.fromOption,
        "onLeftElementPress": onLeftElementPress |> Js.Nullable.fromOption,
        "onRightElementPress": onRightElementPress |> Js.Nullable.fromOption,
        "numberOfLines": numberOfLines |> Js.Nullable.fromOption,
        "style": style |> Js.Nullable.fromOption
      }
    );
};

module RippleFeedback = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "RippleFeedback";
  let make = (~onLongPress=?, ~onPress=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "onLongPress": onLongPress |> Js.Nullable.fromOption,
        "onPress": onPress |> Js.Nullable.fromOption
      }
    );
};

module TextField = {
  [@bs.module "react-native-material-textfield"] external reactClass : ReasonReact.reactClass =
    "TextField";
  let make = (~tintColor=?, ~label=?, ~title=?, ~prefix=?, ~suffix=?, ~error=?, ~autoCapitalize=?, ~autoCorrect,
    ~multiline=?, ~secureTextEntry=?, ~onSubmitEditing=?, ~keyboardType=?, ~returnKeyType=?, ~onChangeText,
    ~elipsizeMode=?, ~disabled=?, ~containerStyle=?, ~placeholder=?, ~value, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "containerStyle": containerStyle |> Js.Nullable.fromOption,
        "tintColor": Js.Option.getWithDefault(Colors.theme^.primary, tintColor),
        "label": label |> Js.Nullable.fromOption,
        "title": title |> Js.Nullable.fromOption,
        "prefix": prefix |> Js.Nullable.fromOption,
        "suffix": suffix |> Js.Nullable.fromOption,
        "error": error |> Js.Nullable.fromOption,
        "placeholder": placeholder |> Js.Nullable.fromOption,
        "autoCapitalize": autoCapitalize |> Js.Nullable.fromOption,
        "autoCorrect": autoCorrect |> jsBool,
        "multiline": multiline |> jsOptBool |> Js.Nullable.fromOption,
        "disabled": disabled |> jsOptBool |> Js.Nullable.fromOption,
        "secureTextEntry": secureTextEntry |> jsOptBool |> Js.Nullable.fromOption,
        "onSubmitEditing": onSubmitEditing |> Js.Nullable.fromOption,
        "keyboardType": keyboardType |> Js.Nullable.fromOption,
        "returnKeyType": returnKeyType |> Js.Nullable.fromOption,
        "elipsizeMode": elipsizeMode |> Js.Nullable.fromOption,
        "onChangeText": onChangeText,
        "value": value
      }
    );
};

module Snackbar = {
  /* Couldn't make it work propperly, so using react-native-snackbar-component for now */
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "Snackbar";
  let make = (~message, ~visible, ~timeout=?, ~onRequestClose, ~bottomNavigation=false, ~onActionPress=?, ~actionText=?, ~style=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "message": message,
        "visible": visible |> jsBool,
        "timeout": timeout |> Js.Nullable.fromOption,
        "onRequestClose": onRequestClose,
        "bottomNavigation": bottomNavigation |> jsBool,
        "onActionPress": onActionPress |> Js.Nullable.fromOption,
        "actionText": actionText |> Js.Nullable.fromOption,
        "style": style |> Js.Nullable.fromOption
      }
    );
};

module RadioButton = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "RadioButton";
  let make = (~label, ~checked, ~value, ~onSelect, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "label": label,
        "checked": checked |> jsBool,
        "value": value,
        "onSelect": (checked) => onSelect(checked)
      }
    );
};

module Badge = {
  [@bs.module "react-native-material-ui"] external reactClass : ReasonReact.reactClass =
    "Badge";
  let make = (~text=?, ~style=?, ~size=?, ~icon=?, ~primary=?, children) =>
    ReasonReact.wrapJsForReason(
      ~reactClass,
      children,
      ~props={
        "text": text |> Js.Nullable.fromOption,
        "style": style |> Js.Nullable.fromOption,
        "size": size |> Js.Nullable.fromOption,
        "primary": primary |> jsOptBool |> Js.Nullable.fromOption,
        "icon": icon |> Js.Nullable.fromOption
      }
    );
};
