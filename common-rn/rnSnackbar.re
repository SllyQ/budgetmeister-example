open ExtUtils;

[@bs.module "react-native-snackbar-component"] external reactClass : ReasonReact.reactClass =
  "default";

let make = (~textMessage=?, ~bottom=?, ~actionText=?, ~actionHandler=?, ~visible, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "visible": visible |> jsBool,
      "actionText": actionText |> Js.Null_undefined.fromOption,
      "actionHandler": actionHandler |> Js.Null_undefined.fromOption,
      "bottom": bottom |> Js.Null_undefined.fromOption,
      "accentColor": Colors.theme^.accent,
      "backgroundColor": "#323232",
      "textMessage": textMessage |> Js.Null_undefined.fromOption
    }
  );
