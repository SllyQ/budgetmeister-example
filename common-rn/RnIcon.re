[@bs.module "react-native-vector-icons/MaterialCommunityIcons"]
external reactClass : ReasonReact.reactClass =
  "default";

let make = (~size=?, ~color=?, ~style=?, ~name, children) =>
  ReasonReact.wrapJsForReason(
    ~reactClass,
    children,
    ~props={
      "color": color |> Js.Nullable.fromOption,
      "style": style |> Js.Nullable.fromOption,
      "name": name,
      "size": size |> Js.Option.getWithDefault(24.)
    }
  );
