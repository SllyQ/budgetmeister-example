open ReactNative;

module Line = {
  let component = ReasonReact.statelessComponent("BudgetSummaryLine");
  let make =
      (
        ~big=false,
        ~categoryId=?,
        ~cost,
        ~budget,
        ~label,
        ~totalWidth,
        ~digits=2,
        ~partOfPeriodPassed,
        ~onPress=?,
        _
      ) => {
    ...component,
    render: (_) => {
      let costStr = Js.Float.toFixedWithPrecision(~digits, cost);
      let budgetStr = Js.Float.toFixedWithPrecision(~digits, budget);
      <ListRow numberOfLines=2 dense=(! big) style=Style.(style([borderRightWidth(1.)])) onPress=?(onPress |> C.Option.map(cb => ListRow.Callback(cb))) >
        <View
          style=Style.(
                  style([
                    flexDirection(`row),
                    alignItems(`center),
                    paddingBottom(1.)
                  ])
                )>
          (
            switch categoryId {
            | None => ReasonReact.null
            | Some(cid) =>
              let {ListRow.icon, color} = CategoryStuff.getIconParams(cid);
              <View
                style=Style.(
                        style([padding(Styles.Spacing.s), paddingRight(6.)])
                      )>
                <LeftIcon icon color size=16. />
              </View>;
            }
          )
          <Row justifyContent=`spaceBetween style=Style.(style([flex(1.), paddingRight(Styles.Spacing.s)]))>
            <ListPrimary value={j|$(label)|j} />
            <ListPrimary value=budgetStr />
          </Row>
        </View>
        <View style=Style.(style([flexDirection(`row)]))>
          <View
            style=Style.(
                    style([
                      height(24.),
                      width(min(1., cost /. budget) *. totalWidth),
                      backgroundColor(
                        cost > budget ?
                          Colors.danger :
                          partOfPeriodPassed < cost /. budget ?
                            Colors.warning : "#9FA8DA"
                      ),
                      flexDirection(`row),
                      alignItems(`center),
                      justifyContent(`flexEnd)
                    ])
                  )
          >
            <ListPrimary style=Style.(style([paddingRight(Styles.Spacing.s)])) value={j|$(costStr)|j} numberOfLines=1 />
          </View>
          <View
            style=Style.(
                    style([
                      height(24.),
                      width((1. -. min(1., cost /. budget)) *. totalWidth),
                      backgroundColor(Colors.lightGrey)
                    ])
                  )
          />
        </View>
      </ListRow>;
    }
  };
};

let component = ReasonReact.statelessComponent("BudgetPeriodSummary");

let make = (~onBudgetExpensesRequested, ~totalWidth, ~userId, ~currency, ~expenses, ~budget: Budget.t, _) => {
  ...component,
  render: (_) => {
    let { Utils.start, finish } = Utils.getPeriodInfo(~period=budget.period.budgetingPeriod, ~date=budget.period.date, ());
    let thisPeriodExpenses =
      expenses
      |> Js.Array.filter((expense: Expense.t) =>
           expense.date
           |> DateFns.isWithinRange(start, finish)
         );
    let now = Js.Date.make();
    let partOfPeriodPassed = switch (budget.period.budgetingPeriod) {
      | Monthly(_) => {
        min(DateFns.differenceInCalendarDays(now, start), DateFns.getDaysInMonth(now)) /. DateFns.getDaysInMonth(now);
      }
      | BiWeekly(_) => {
        min(DateFns.differenceInCalendarDays(now, start), 14.) /. 14.;
      }
    };
    let thisPeriodTotalAmount = thisPeriodExpenses
     |> Array.map(Utils.getExpenseBudgetCost(~currency, ~userId))
     |> Array.fold_left((+.), 0.);
    <View
      key=(Budget.key(budget.id))
      style=Style.(style([flex(1.)]))>
      <ScrollView>
        <ListWrapper>
          <ListRow
            numberOfLines=1
            style=Style.(
                    style([marginBottom(20.)])
                  )>
            <Text
              style=Styles.Text.title
              value=Budget.formatPeriod(budget.period)
            />
          </ListRow>
          {
            if (budget.period.date |> DateFns.isSameMonth(now)) {
              switch (budget.period.budgetingPeriod) {
                | Monthly(_) => {
                  <Line
                    cost=(partOfPeriodPassed *. DateFns.getDaysInMonth(now))
                    budget=DateFns.getDaysInMonth(now)
                    label="Days passed"
                    big=true
                    digits=0
                    totalWidth
                    partOfPeriodPassed
                  />
                }
                | BiWeekly(_) => {
                  <Line
                    cost=(partOfPeriodPassed *. 14.)
                    budget=14.
                    label="Days passed"
                    big=true
                    digits=0
                    totalWidth
                    partOfPeriodPassed
                  />
                }
              }
            } else {
              ReasonReact.null
            }
          }
          {
            let totalBudgetAmount =
              budget.totalAmount;
            <Line
              cost=thisPeriodTotalAmount
              budget=totalBudgetAmount
              label="Total"
              big=true
              totalWidth
              onPress=(() => onBudgetExpensesRequested({ RParams.budgetId: budget.id, category: All }))
              partOfPeriodPassed
            />;
          }
          (
            switch budget.parts {
            | [||] => ReasonReact.null
            | parts =>
              <View>
                (
                  parts
                  |> Array.map(part => {
                       let thisCategoryCost =
                         thisPeriodExpenses
                         |> Js.Array.map(
                              (expense: Expense.t) =>
                              Utils.getExpenseBudgetCost(
                                ~categoryId=
                                  `WithChildren(
                                    part.Budget.categoryId
                                  ),
                                ~currency,
                                ~userId,
                                expense
                              )
                            )
                         |> Array.fold_left(
                              (+.),
                              0.
                            );
                       let category =
                         CategoryDb.get(
                           ~id=part.categoryId
                         );
                       switch category {
                       | None => ReasonReact.null
                       | Some(category) =>
                         let catCost = thisCategoryCost;
                         let catBudget =
                           part.amount;
                         let title =
                           category.title;
                         <Line
                           categoryId=part.
                                       categoryId
                           key=(
                             Category.key(
                               part.categoryId
                             )
                           )
                           label=title
                           cost=catCost
                           budget=catBudget
                           totalWidth
                           partOfPeriodPassed
                           onPress=(() => onBudgetExpensesRequested({ RParams.budgetId: budget.id, category: Category(part.categoryId) }))
                         />;
                       };
                     })
                  |> ReasonReact.array
                )
                {
                  let otherBudget =
                    budget.totalAmount
                    -. (
                      parts
                      |> Array.map(part =>
                           part.Budget.amount
                         )
                      |> Array.fold_left((+.), 0.)
                    );
                  if (otherBudget > 0.) {
                    let budgetedCost =
                      parts
                      |> Array.map(part =>
                           thisPeriodExpenses
                           |> Js.Array.map(
                                (
                                  expense: Expense.t
                                ) =>
                                Utils.getExpenseBudgetCost(
                                  ~categoryId=
                                    `WithChildren(
                                      part.Budget.categoryId
                                    ),
                                  ~currency,
                                  ~userId,
                                  expense
                                )
                              )
                           |> Array.fold_left(
                                (+.),
                                0.
                              )
                         )
                      |> Array.fold_left((+.), 0.);
                    let otherCostAmount =
                      thisPeriodTotalAmount
                      -. budgetedCost;
                    <Line
                      label="Other"
                      cost=otherCostAmount
                      budget=otherBudget
                      totalWidth
                      partOfPeriodPassed
                      onPress=(() => onBudgetExpensesRequested({ RParams.budgetId: budget.id, category: Other }))
                    />;
                  } else {
                    ReasonReact.null;
                  };
                }
              </View>
            }
          )
        </ListWrapper>
      </ScrollView>
    </View>
  }
}
