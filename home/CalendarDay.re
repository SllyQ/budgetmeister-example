open ReactNative;

let component = ReasonReact.statelessComponent("CalendarDay");

let make = (~state, ~marking, ~onPress, ~date, children) => {
  ...component,
  render: (_) => {
    let isToday = state === "today";
    let isSelected = Js.Nullable.toOption(marking##selected) |> Js.Option.getWithDefault(false);
    let isDisabled = state === "disabled";
    let dots = marking##dots |> Js.Nullable.toOption |> Js.Option.getWithDefault([||]);
    let baseTextStyle = Style.(style([fontSize(14.), color(Colors.black)]));
    let textStyle = Style.(switch (isToday, isSelected, isDisabled) {
      | (_, true, _) => combine(baseTextStyle, style([color(Colors.white)]))
      | (_, _, true) => combine(baseTextStyle, style([color(Colors.grey)]))
      | (true, _, _) => combine(baseTextStyle, style([color(Colors.theme^.primary)]))
      | _ => baseTextStyle
    });
    let textWrapStyle = Style.(style([
      width(32.),
      height(20.),
      borderRadius(3.),
      alignItems(`center),
      justifyContent(`center)
    ]) |> (isSelected ? combine(style([backgroundColor(Colors.theme^.primary)])) : a => a));
    <Touchable onPress=(() => onPress(date))>
      <View style=Style.(style([width(32.), height(26.), alignItems(`center), justifyContent(`center)]))>
        <View style=textWrapStyle>
          (ReasonReact.element(Text.make(~style=textStyle, children)))
        </View>
        <View style=Style.(style([flexDirection(`row), height(6.)]))>
          (dots
          |> Array.map(dot => {
            <View key=dot##key style=Style.(style([
              width(4.),
              height(4.),
              borderRadius(2.),
              backgroundColor(dot##color),
              margin(1.)
            ]))
            />
          })
          |> ReasonReact.array)
        </View>
      </View>
    </Touchable>
  }
};

let jsComponent =
  ReasonReact.wrapReasonForJs(
    ~component,
    (jsProps) => {
      make(~state=jsProps##state, ~marking=jsProps##marking, ~onPress=jsProps##onPress, ~date=jsProps##date, jsProps##children)
    }
  );
