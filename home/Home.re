open ReactNative;

type tab =
  | MonthlySummary
  | MonthlySummaryWithExpenses
  | BudgetSummary
  | Calendar;

type state = {
  selectedTab: tab,
};

type actions =
  | UpdateSelectedTab(tab)
  | SelectTab(tab);

let component = ReasonReact.reducerComponent("Home");

let make =
    (
      ~onCreateNewExpense: (~date: Js.Date.t=?, unit) => unit,
      ~onEditExpense,
      ~onEditRecurringExpense,
      ~onPeriodSummaryRequested,
      ~onBudgetExpensesRequested,
      ~onCategoryPickerRequested,
      _
    ) => {
  ...component,
  initialState: () => { selectedTab: MonthlySummaryWithExpenses },
  didMount: (_) => {
    SnackbarService.setBottom(56.);
    try {
      switch (AppAuthService.user^) {
        | Some(user) => {
          Js.Global.setTimeout(() => {
            if (Js.Option.isNone(Realm.getCfg("aliased"))) {
              Mixpanel.createAlias(user.User.id);
              Mixpanel.identify(user.User.id);
              Mixpanel.set({
                "$email": user.email,
                "$first_name": user.username
              });
              Realm.setCfg("aliased", "true");
            } else {
              Mixpanel.identify(user.User.id);
            }
          }, 1000) |> ignore;
        }
        | None => ()
      }
    } {
      | e => {
        Log.log(~key="Alias err", e);
      }
    };
  },
  willUnmount: (_) => {
    SnackbarService.resetBottom();
  },
  reducer: (action, _state) => switch action {
    | UpdateSelectedTab(selectedTab) => {
      ReasonReact.Update {
        selectedTab: selectedTab
      }
    }
    | SelectTab(selectedTab) => ReasonReact.SideEffects(({ send }) => {
      Mixpanel.trackWithProperties("HomeTab", {
        "tab": switch (selectedTab) {
          | MonthlySummary => "Monthly Summary"
          | MonthlySummaryWithExpenses => "Expenses"
          | BudgetSummary => "Budget Summary"
          | Calendar => "Calendar"
        }
      });
      Js.Global.setTimeout(() => {
        send(UpdateSelectedTab(selectedTab))
      }, 0) |> ignore
    })
  },
  render: ({ send, state: { selectedTab } }) => {
    <View style=Style.(style([flex(1.)]))>
      (
        switch selectedTab {
        | MonthlySummary => {
          <DebounceInitialRender key="MonthlySummary">
            <MonthlySummary />
          </DebounceInitialRender>
        }
        | MonthlySummaryWithExpenses => {
          <DebounceInitialRender key="MonthlySummaryWithExpenses">
            <MonthlyExpensesSummary onCreateNewExpense=onCreateNewExpense onEditExpense
              onCategoryPickerRequested
              onEditRecurringExpense
              onPeriodSummaryRequested
            />
          </DebounceInitialRender>
        }
        | BudgetSummary => {
          <DebounceInitialRender key="BudgetSummary">
            <BudgetSummary onBudgetExpensesRequested />
          </DebounceInitialRender>
        }
        | Calendar => {
          <DebounceInitialRender key="CalendarView">
            <CalendarView
              onExpenseEditRequested=onEditExpense
              onCategoryPickerRequested
              onExpenseCreateRequested=(date => onCreateNewExpense(~date, ()))
              onRecurringExpenseEditRequested=onEditRecurringExpense
            />
          </DebounceInitialRender>
        }
        }
      )
      <BottomNav.Container>
        <BottomNav.Tab
          label="Expenses"
          icon="currency-usd"
          active=(selectedTab == MonthlySummaryWithExpenses)
          onPress=(() => send(SelectTab(MonthlySummaryWithExpenses)))
        />
        <BottomNav.Tab
          label="Monthly"
          icon="calendar"
          active=(selectedTab == MonthlySummary)
          onPress=(() => send(SelectTab(MonthlySummary)))
        />
        <BottomNav.Tab
          label="Budget"
          icon="poll-box"
          active=(selectedTab == BudgetSummary)
          onPress=(() => send(SelectTab(BudgetSummary)))
        />
        <C_Alpha render=(() => {
          <BottomNav.Tab
            label="Calendar"
            icon="calendar-text"
            active=(selectedTab == Calendar)
            onPress=(() => send(SelectTab(Calendar)))
          />
        }) />
      </BottomNav.Container>
    </View>

  }
};
