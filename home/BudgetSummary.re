open ReactNative;

let component = ReasonReact.statelessComponent("BudgetSummary");

let make = (~onBudgetExpensesRequested, _) => {
  ...component,
  render: (_) => {
    let totalWidth =
      float_of_int(Dimensions.get(`window)##width) -. 2. *. Styles.Spacing.xl;
    <HelpWrapper helpComponent={
      title: "Budget Summary",
      message: `String("This is where you can see how well you are following the budgets you set out for yourself.")
    }>
      <C_Settings
        render=(
          settings =>
            <ScreenWrap scrollable=false>
              <Connect_Budgets
                render=(
                  budgets => {
                    if (Array.length(budgets) == 0) {
                      <BudgetSummaryEmpty />
                    } else {
                      <Connect_ActiveUser render=(user => {
                        let userId = user |> C.Option.map(user => user.User.id);
                        <C_Expenses
                          render=(
                            expenses => {
                              let sortedBudgets =
                                budgets
                                |> Common.iSort((b1, b2) =>
                                     DateFns.compareAsc(b1.Budget.period.date, b2.period.date)
                                   );
                              let now = Js.Date.make();
                              let initialIndex =
                                sortedBudgets
                                |> Js.Array.findIndex(budget =>
                                     budget.Budget.period.date |> DateFns.isSameMonth(now)
                                   )
                                |> (
                                  index =>
                                    index === (-1) ?
                                      Array.length(sortedBudgets) - 1 : index
                                );
                              <Swiper index=initialIndex loop=false loadMinimal=true>
                                (
                                  sortedBudgets
                                  |> Array.mapi((i, budget: Budget.t) => {
                                      <BudgetPeriodSummary
                                        key=string_of_int(i)
                                        expenses
                                        budget
                                        currency=settings.mainCurrency
                                        userId
                                        onBudgetExpensesRequested
                                        totalWidth
                                      />
                                  })
                                )
                              </Swiper>;
                            }
                          )
                        />
                      }) />
                    }
                  }
                )
              />
            </ScreenWrap>
        )
      />
    </HelpWrapper>
  }
};
