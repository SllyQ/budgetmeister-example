open ReactNative;

type state = {
  selectedDate: Js.Date.t,
  selectedMonth: Js.Date.t,
};


type actions =
  | SelectDay(Calendar.dateFormat)
  | MonthChanged(Calendar.dateFormat);

let component = ReasonReact.reducerComponent("CalendarView");

let getUniqueCategories = (expenses) => {
  let uniqueCategories = [||];
  expenses |> Array.iter((expense: Expense.t) => {
    switch expense.categorization {
      | Some(Split(parts)) => {
        parts |> Array.iter((part: Categorization.part) => {
          if (!Js.Array.includes(part.categoryId, uniqueCategories)) {
            uniqueCategories
            |> Js.Array.push(part.categoryId)
            |> ignore;
          }
        })
      }
      | Some(Single(part)) => {
        if (!Js.Array.includes(part.categoryId, uniqueCategories)) {
          uniqueCategories
          |> Js.Array.push(part.categoryId)
          |> ignore;
        }
      }
      | None => ()
    }
  });
  uniqueCategories
};

let getDotsForDay = (expenses, dayDate) => {
  expenses
    |> Js.Array.filter(expense => expense.Expense.date |> DateFns.isSameDay(dayDate))
    |> getUniqueCategories
    |> Array.mapi((i, categoryId) => {
      let { ListRow.color } = CategoryStuff.getIconParams(categoryId);
      { "key": i + 1, "color": CategoryStuff.color(color), "selectedDotColor": CategoryStuff.color(color) }
    });
};

let groupExpensesByDay = (expenses) => {
  let groups = Js.Dict.empty();
  let expensesLeft = ref(expenses);
  while (Array.length(expensesLeft^) > 0) {
    let date = expensesLeft^[0].Expense.date;
    let (dayExpenses, otherExpenses) = expensesLeft^ |> C.Array.split(expense => DateFns.isSameDay(date, expense.Expense.date));
    let key = date |> DateFns.format("YYYY-MM-DD");
    Js.Dict.set(groups, key, { "dots": getDotsForDay(dayExpenses, date) });
    expensesLeft := otherExpenses
  };
  groups
};

let make = (~onExpenseEditRequested, ~onExpenseCreateRequested, ~onRecurringExpenseEditRequested, ~onCategoryPickerRequested, _) => {
  ...component,
  initialState: () => {
    selectedDate: Js.Date.make(),
    selectedMonth: Js.Date.make()
  },
  reducer: (action, state) => switch action {
    | SelectDay(date) => {
      ReasonReact.Update {
      ...state,
      selectedDate: Js.Date.makeWithYMD(~year=float_of_int(date##year), ~month=float_of_int(date##month - 1), ~date=float_of_int(date##day), ())
    }}
    | MonthChanged(date) => ReasonReact.Update {
      ...state,
      selectedMonth: Js.Date.makeWithYM(~year=float_of_int(date##year), ~month=float_of_int(date##month - 1), ())
    }
  },
  render: ({ send, state }) => {
    <Connect_ActiveUser render=(user => {
      <C_Expenses render=(expenses => {
        let groups = groupExpensesByDay(expenses);
        let key = state.selectedDate |> DateFns.format("YYYY-MM-DD");
        let currVal = Js.Dict.get(groups, key);
        switch currVal {
          | None => Js.Dict.set(groups, key, Obj.magic({ "selected": true }))
          | Some(currVal) => Js.Dict.set(groups, key, Js.Obj.assign(currVal, { "selected": true }))
        };
        <ScreenWrap scrollable=false>
          <View style=Style.(style([height(285.), backgroundColor("white"), elevation(1.)]))>
            <Calendar.List
              horizontal=true
              pagingEnabled=true
              onDayPress=(date => send(SelectDay(date)))
              onMonthChange=(date => send(MonthChanged(date)))
              markedDates=groups
              markingType="multi-dot"
              dayComponent=CalendarDay.jsComponent
            />
          </View>
          <ScrollView>
            <ListWrapper>
              <C_Expenses render=(expenses => {
                let filteredExpenses = expenses |> Js.Array.filter(exp => DateFns.isSameDay(exp.Expense.date, state.selectedDate));
                <View>
                  <ListSubheader value=DateFns.format("MMMM Do", state.selectedDate) rightValue=Action({ label: "Expense", icon: "add", onPress: () => onExpenseCreateRequested(state.selectedDate) }) />
                  (if (Array.length(filteredExpenses) === 0) {
                    <ListRow numberOfLines=1>
                      <Text style=Styles.Text.listPrimary value="No expenses" />
                    </ListRow>
                  } else {
                    (filteredExpenses
                      |> Array.map((expense: Expense.t) =>
                        <ExpenseRow
                          user
                          key=DbExpense.key(expense.id)
                          expense
                          onEditExpense=onExpenseEditRequested
                          onEditRecurringExpense=onRecurringExpenseEditRequested
                          onCategoryPickerRequested
                        />
                      )
                      |> ReasonReact.array
                    )
                  })
                </View>
              }) />
            </ListWrapper>
          </ScrollView>
        </ScreenWrap>
      }) />
    }) />
  }
}
