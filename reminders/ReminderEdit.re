type state = {
  formValue: ReminderForm.value,
};

type actions =
  | UpdateFormValue(ReminderForm.value)
  | UpdateReminder;

let component = ReasonReact.reducerComponent("ReminderEdit");

let make = (~reminderId, ~onReminderUpdated, _) => {
  ...component,
  initialState: () => {
    let reminder = RemindersService.get().RemindersService.Cfg.reminders |> Js.Array.find(r => {
      r.Reminder.id == reminderId
    }) |> Js.Option.getExn;
    {
      formValue: ReminderForm.getValue(reminder)
    }
  },
  reducer: C.flip(state => fun
    | UpdateFormValue(formValue) => ReasonReact.Update {
      formValue: formValue
    }
    | UpdateReminder => ReasonReact.SideEffects((_) => {
      let reminder = {
        Reminder.id: reminderId,
        daysOfWeek: state.formValue.daysOfWeek,
        time: state.formValue.time,
        message: switch (state.formValue.message |> Js.String.trim) {
          | "" => None
          | msg => Some(msg)
        }
      };
      RemindersService.dispatch(UpdateReminder(reminder));
      onReminderUpdated()
    })
  ),
  render: ({ state: { formValue }, send}) => {
    <ScreenWrap
      fab=(
        <ThemeActionButton icon="save" onPress=(() => send(UpdateReminder)) />
      )
    >
      <ListWrapper>
        <ReminderForm value=formValue onValueChanged=(fv => send(UpdateFormValue(fv))) />
      </ListWrapper>
    </ScreenWrap>
  }
}
