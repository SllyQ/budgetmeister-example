open ReactNative;
open RnMui;

type value = {
  message: string,
  daysOfWeek: array(int),
  time: Js.Date.t
};

let empty = {
  daysOfWeek: [| 1, 2, 3, 4, 5, 6, 7 |],
  time: Js.Date.makeWithYMDHM((), ~year=2018., ~month=1., ~date=1., ~hours=19., ~minutes=30.),
  message: ""
};

let getValue = (reminder: Reminder.t) => {
  daysOfWeek: reminder.daysOfWeek,
  time: reminder.time,
  message: reminder.message |> Js.Option.getWithDefault("")
};

let dayNames = [|
  "",
  "Monday",
  "Tueday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
  "Sunday"
|];

type state = {
  showPicker: bool
};

type actions =
  | OpenPicker
  | ClosePicker
  | PickTime(Js.Date.t);

let component = ReasonReact.reducerComponent("ReminderForm");

let make = (~value, ~onValueChanged, _) => {
  ...component,
  initialState: () => { showPicker: false },
  reducer: C.flip(_state => fun
    | OpenPicker => ReasonReact.Update {
      showPicker: true
    }
    | ClosePicker => ReasonReact.Update {
      showPicker: false
    }
    | PickTime(date) => ReasonReact.UpdateWithSideEffects({
      showPicker: false
    }, (_) => onValueChanged({ ...value, time: date }))
  ),
  render: ({ send, state: { showPicker } }) => {
    <Connect_SelectedTheme render=(theme => {
      <Column>
        <ListRow>
          <TextField
            label="Message"
            placeholder="Don't forget to enter your expenses"
            autoCapitalize="sentences"
            autoCorrect=true
            value=value.message
            onChangeText=(message => onValueChanged({
              ...value,
              message
            }))
          />
        </ListRow>
        <ListRow numberOfLines=1 onPress=Callback(() => send(OpenPicker))>
          <Row alignItems=`center>
            <Text style=Style.(combine(Styles.Text.listPrimary, style([color(theme.primary), Styles.Text.robotoMedium])))
              value="Time: "
            />
            <Text style=Styles.Text.listPrimary value=(value.time |> DateFns.format("HH:mm")) />
          </Row>
        </ListRow>
        <ListSubheader value="Days of week" primary=true withDivider=true />
        (Belt.Array.range(1, 7)
        |> Array.map(x => {
          let isSelected = value.daysOfWeek |> Js.Array.includes(x);
          <ListRow numberOfLines=1
            key=string_of_int(x)
            onPress=Callback(() => onValueChanged({
              ...value,
              daysOfWeek: isSelected
                ? value.daysOfWeek |> Js.Array.filter(d => d != x)
                : value.daysOfWeek |> Js.Array.concat([| x |])
            }))
          >
            <Row alignItems=`center style=Style.(style([marginLeft(-8.)]))>
              <Paper.Checkbox
                checked=isSelected
              />
              <Text style=Styles.Text.listPrimary value=dayNames[x] />
            </Row>
          </ListRow>
        })
        |> ReasonReact.array)
        <DtPicker
          isVisible=showPicker
          onConfirm=(date => send(PickTime(date)))
          onCancel=(() => send(ClosePicker))
          mode="time"
        />
      </Column>
    }) />
  }
}
