type state = {
  formValue: ReminderForm.value
};

type actions =
  | UpdateFormValue(ReminderForm.value)
  | CreateReminder;

let component = ReasonReact.reducerComponent("ReminderCreate");

let make = (~onReminderCreated, _) => {
  ...component,
  initialState: () => {
    formValue: ReminderForm.empty
  },
  reducer: C.flip(state => fun
    | UpdateFormValue(formValue) => ReasonReact.Update {
      formValue: formValue
    }
    | CreateReminder => ReasonReact.SideEffects((_) => {
      let reminder = {
        Reminder.id: Reminder.getPlaceholderId(),
        daysOfWeek: state.formValue.daysOfWeek,
        time: state.formValue.time,
        message: switch (state.formValue.message |> Js.String.trim) {
          | "" => None
          | msg => Some(msg)
        }
      };
      RemindersService.dispatch(CreateReminder(reminder));
      Mixpanel.increment(EventConsts.reminderCreated);
      onReminderCreated()
    })
  ),
  render: ({ send, state: { formValue } }) => {
    <ScreenWrap
      fab=(
        <ThemeActionButton icon="save" onPress=(() => send(CreateReminder)) />
      )
    >
      <ListWrapper>
        <ReminderForm value=formValue onValueChanged=(fv => send(UpdateFormValue(fv))) />
      </ListWrapper>
    </ScreenWrap>
  }
}
