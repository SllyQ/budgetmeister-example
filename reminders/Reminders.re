open ReactNative;
open Paper;

let component = ReasonReact.statelessComponent("Reminders");

let days = [|
  "",
  "Mon",
  "Tue",
  "Wed",
  "Thu",
  "Fri",
  "Sat",
  "Sun"
|];

let make = (~onCreateReminder, ~onEditReminder, _) => {
  ...component,
  didMount: (_) => {
    RemindersService.dispatch(FetchReminders);
  },
  render: (_) => {
    <HelpWrapper helpComponent={
      title: "Reminders",
      message: `String("Reminders help you avoid forgetting to track your expenses. We recommend you to create a reminder for every evening so you take a look if you did not miss tracking expenses any day. Also reminders can have text of your choice, so you can use them to remind yourself to do any other activities you do daily/weekly.")
    }>
      <ScreenWrap fab=(
        <ThemeActionButton onPress=onCreateReminder />
      )>
        <RemindersService.Connect render=(state => {
          <ListWrapper>
            (state.reminders
            |> Array.mapi((i, reminder) => {
              <ListRow numberOfLines=2 key=string_of_int(i)
                onPress=(Actions({
                  actions: [|{
                    text: "Edit",
                    icon: Some("pencil"),
                    onPress: () => onEditReminder(reminder.Reminder.id)
                  }, {
                    text: "Delete",
                    icon: Some("delete"),
                    onPress: () => RemindersService.dispatch(DeleteReminder(reminder.id))
                  }|]
                }))
              >
                <ListPrimary value=(reminder.message |> Js.Option.getWithDefault("Don't forget to enter your expenses")) />
                <ListSecondary value=(
                  (reminder.Reminder.time |> DateFns.format("HH:mm"))
                  ++ " at "
                  ++ (reminder.daysOfWeek |> Js.Array.sortInPlace |> Array.map(i => days[i]) |> Js.Array.joinWith(", "))
                ) />
              </ListRow>
            }) |> ReasonReact.array)
          </ListWrapper>
        }) />
      </ScreenWrap>
    </HelpWrapper>
  }
}
