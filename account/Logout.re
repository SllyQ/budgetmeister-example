open ReactNative;

open RnMui;

type state = {
  newFriendEmail: string
};
type actions =
  | UpdateNewFriendEmail(string)
  | AddNewFriend;

let component = ReasonReact.reducerComponent("Logout");

let make = (_) => {
  /* let logout = (_, _) =>
    SyncService.sync()
    /* |> Js.Promise.then_((_) => GlobalDb.clear()) */
    |> C.Promise.consume((_) => AppAuthService.logout()); */
  {
    ...component,
    initialState: () => {
      newFriendEmail: ""
    },
    reducer: C.flip((state) => fun
      | UpdateNewFriendEmail(newFriendEmail) => ReasonReact.Update {
        newFriendEmail: newFriendEmail
      }
      | AddNewFriend => ReasonReact.SideEffects(({ send }) => {
        FriendsService.addFriend(~email=state.newFriendEmail, ())
        |> C.Promise.consume(fun
          | FriendsService.FriendAdded => send(UpdateNewFriendEmail(""))
          | EmailDoesNotExist => Alert.alert(~title="User with such email does not exist", ())
          | AlreadyFriends => Alert.alert(~title="Already friends", ())
          | UnknownError => Alert.alert(~title="Error", ~message="Unknown error has occured. Please check your internet connection and try again", ())
        )
      })
    ),
    render: ({ send, handle, state }) => {
      <ScreenWrap>
        <ListWrapper>
          <Connect_ActiveUser render=(fun
            | None => ReasonReact.null
            | Some(user) => {
              <ListRow numberOfLines=1>
                <Text value=("Logged in as " ++ user.User.username ++ " (" ++ user.email ++ ")")
                  style=Styles.Text.listPrimary
                />
              </ListRow>
            }
          ) />
          <ListSubheader value="Friends" primary=true />
          <Connect_Friends render=(fun
            | [||] => {
              <ListRow numberOfLines=2>
                <Text value="You have not added any friends yet" style=Styles.Text.listPrimary />
                <Text value="You can add friends to share expenses with them" style=Styles.Text.listSecondary />
              </ListRow>
            }
            | friends =>
              <View>
                (
                  friends |> Array.map((friend: User.t) => {
                  <ListRow numberOfLines=1 key=friend.email>
                    <Text style=Styles.Text.listPrimary value=friend.username />
                  </ListRow>
                  }) |> ReasonReact.array
                )
              </View>
          ) />
          <ListRow flexRow=true>
            <View style=Style.(style([flex(1.)]))>
              <TextField autoCapitalize="none" autoCorrect=false
                keyboardType="email-address"
                value=state.newFriendEmail
                onChangeText=(email => send(UpdateNewFriendEmail(email)))
                label="New friend email"
                onSubmitEditing=(() => send(AddNewFriend))
              />
            </View>
            <IconButton icon="plus" onPress=(() => send(AddNewFriend)) color=Colors.primary />
          </ListRow>
          /* <View style=Style.(style([paddingTop(Styles.Spacing.xxl)])) /> */
          /* <ListSubheader value="Logout" primary=true />
          <ListRow>
            <MuiButton text="Logout" primary=true onPress=(handle(logout)) raised=true />
          </ListRow> */
        </ListWrapper>
      </ScreenWrap>
    }
  }
};
