type state = {showLogin: bool};

type actions =
  | SwitchToLogin
  | SwitchToRegister;

let component = ReasonReact.reducerComponent("Account");

let make = (_) => {
  ...component,
  initialState: () => {showLogin: true},
  reducer: (action, _state) =>
    switch action {
    | SwitchToLogin => ReasonReact.Update({showLogin: true})
    | SwitchToRegister => ReasonReact.Update({showLogin: false})
    },
  render: ({send, state: {showLogin}}) =>
    <HelpWrapper helpComponent={
      title: "Account",
      message: `String("This is where you can login or register. If logged in, you can synchronize your data allowing you to use the application on multiple devices or simply have a backup if you lose your current device, or simply want to transfer data to a new one.")
    }>
      <C_Authenticated
        render=(
          (isAunthenticated) =>
            if (isAunthenticated) {
              <Logout />
            } else if (showLogin) {
              <Login onRequestRegister=((_) => send(SwitchToRegister))/>
            } else {
              <Register onRequestLogin=((_) => send(SwitchToLogin)) />
            }
        )
      />
    </HelpWrapper>
};
