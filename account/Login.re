open ReactNative;

open RnMui;

type state = {
  email: string,
  password: string,
  passwordRef: ref(option(ReasonReact.reactRef)),
  loggingIn: bool
};

type actions =
  | UpdateEmail(string)
  | UpdatePassword(string)
  | Login
  | EmailOrPasswordIncorrect
  | UnknownError;

let component = ReasonReact.reducerComponent("Login");

let setPasswordRef = (nullableRef, { ReasonReact.state }) => {
  state.passwordRef := nullableRef |> Js.Nullable.toOption
};

let focusPassword = (() , { ReasonReact.state }) => {
  switch (state.passwordRef^) {
    | Some(ref) => ReasonReact.refToJsObj(ref)##focus()
    | None => ()
  }
};

let make = (~onRequestRegister, _) => {
  ...component,
  initialState: () => {email: "", password: "", passwordRef: ref(None), loggingIn: false},
  reducer: (action, state) =>
    switch action {
    | UpdateEmail(email) => ReasonReact.Update({...state, email})
    | UpdatePassword(password) => ReasonReact.Update({...state, password})
    | Login =>
      ReasonReact.UpdateWithSideEffects(
        {
          ...state,
          loggingIn: true
        }, (
          ({ send }) => {
            let { email, password } = state;
            if (email == "" || password == "") {
              Alert.alert(~title="Error", ~message="Please enter email and password", ());
            } else if (!Js_re.test(email, Register.emailRegExp)) {
              Alert.alert(~title="Error", ~message="Invalid email", ());
            } else {
              AppAuthService.login(~email, ~password, ())
              |> C.Promise.map(fun
                | AppAuthService.LoggedInSucessfully(user) => {
                  Mixpanel.identify(user.User.id);
                  Mixpanel.track(EventConsts.loggedIn);
                }
                | EmailOrPasswordIncorrect => send(EmailOrPasswordIncorrect)
                | UnknownError => send(UnknownError)
              ) |> C.Promise.consume(SyncService.forceSync);
            }
          }
        )
      )
    | EmailOrPasswordIncorrect => ReasonReact.UpdateWithSideEffects({
      ...state,
      loggingIn: false
    }, (_) => {
      Alert.alert(~title="Error", ~message="Email or password is incorrect", ())
    })
    | UnknownError => ReasonReact.UpdateWithSideEffects({
      ...state,
      loggingIn: false
    }, (_) => {
      Alert.alert(~title="Error", ~message="Login failed for unknown reason. Please make sure that you are connected to the interned and try again.", ())
    })
    },
  render: ({send, handle, state: {email, password, loggingIn}}) =>
    <ScreenWrap>
      <LoadingOverlay visible=loggingIn />
      <ListRow>
        <TextField
          label="Email"
          keyboardType="email-address"
          autoCapitalize="none"
          returnKeyType="next"
          onSubmitEditing=handle(focusPassword)
          autoCorrect=false
          value=email
          onChangeText=((email) => send(UpdateEmail(email)))
        />
      </ListRow>
      <ListRow>
        <TextField
          ref=handle(setPasswordRef)
          onSubmitEditing=(() => send(Login))
          label="Password"
          autoCapitalize="none"
          autoCorrect=false
          secureTextEntry=true
          value=password
          onChangeText=((password) => send(UpdatePassword(password)))
        />
      </ListRow>
      <ListRow numberOfLines=1>
        <MuiButton text="Login" primary=true onPress=(() => send(Login)) raised=true />
      </ListRow>
      <MuiButton
        text="New user?"
        onPress=onRequestRegister
        primary=true
      />
    </ScreenWrap>
};
