open ReactNative;

open RnMui;

type state = {
  email: string,
  name: string,
  password: string,
  password2: string,
  registering: bool,
  emailRef: ref(option(ReasonReact.reactRef)),
  passwordRef: ref(option(ReasonReact.reactRef)),
  password2Ref: ref(option(ReasonReact.reactRef))
};

type actions =
  | UpdateName(string)
  | UpdateEmail(string)
  | UpdatePassword(string)
  | UpdatePassword2(string)
  | Register
  | EmailAlreadyExists
  | UnknownError;

let component = ReasonReact.reducerComponent("Register");

let emailRegExp = [%bs.raw{|/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/|}];

let setEmailRef = (nullableRef, { ReasonReact.state }) => {
  state.emailRef := nullableRef |> Js.Nullable.toOption
};

let focusEmail = (() , { ReasonReact.state }) => {
  switch (state.emailRef^) {
    | Some(ref) => ReasonReact.refToJsObj(ref)##focus()
    | None => ()
  }
};

let setPasswordRef = (nullableRef, { ReasonReact.state }) => {
  state.passwordRef := nullableRef |> Js.Nullable.toOption
};

let focusPassword = (() , { ReasonReact.state }) => {
  switch (state.passwordRef^) {
    | Some(ref) => ReasonReact.refToJsObj(ref)##focus()
    | None => ()
  }
};

let setPassword2Ref = (nullableRef, { ReasonReact.state }) => {
  state.password2Ref := nullableRef |> Js.Nullable.toOption
};

let focusPassword2 = (() , { ReasonReact.state }) => {
  switch (state.password2Ref^) {
    | Some(ref) => ReasonReact.refToJsObj(ref)##focus()
    | None => ()
  }
};


let make = (~onRequestLogin, _) => {
  ...component,
  initialState: () => {email: "", name: "", password: "", password2: "",
    emailRef: ref(None), passwordRef: ref(None), password2Ref: ref(None),
    registering: false
  },
  reducer: (action, state) =>
    switch action {
    | UpdateName(name) => ReasonReact.Update({...state, name})
    | UpdateEmail(email) => ReasonReact.Update({...state, email})
    | UpdatePassword(password) => ReasonReact.Update({...state, password})
    | UpdatePassword2(password2) => ReasonReact.Update({...state, password2})
    | Register =>
      ReasonReact.UpdateWithSideEffects({
        ...state,
        registering: true
      }, ({ send }) => {
          let { email, name, password, password2 } = state;
          if (email == "" || name == "" || password == "" || password2 == "") {
            Alert.alert(~title="Error", ~message="Please fill in all fields", ())
          } else if (!Js_re.test(email, emailRegExp)) {
            Alert.alert(~title="Error", ~message="Invalid email", ())
          } else if (Js.String.length(password) < 6) {
            Alert.alert(~title="Error", ~message="Password is too short", ())
          } else if (password != password2) {
            Alert.alert(~title="Error", ~message="Passwords don't match", ())
          } else {
            AppAuthService.register(~email, ~username=name, ~password, ())
            |> C.Promise.consume(fun
              | AppAuthService.RegisteredSuccessfully(user) => {
                Mixpanel.createAlias(user.User.id);
                Mixpanel.set({
                  "$email": user.email,
                  "$first_name": user.username
                });
                Realm.setCfg("aliased", "true");
                Mixpanel.track(EventConsts.registered);
              }
              | EmailAlreadyExists => send(EmailAlreadyExists)
              | UnknownError =>  send(UnknownError)
            );
          }
        }
      )
    | EmailAlreadyExists => ReasonReact.UpdateWithSideEffects({
      ...state,
      registering: false
    }, (_) => {
      Alert.alert(~title="Error", ~message="An account with this email address already exists", ())
    })
    | UnknownError => ReasonReact.UpdateWithSideEffects({
      ...state,
      registering: false
    }, (_) => {
      Alert.alert(~title="Error", ~message="Registration failed for unknown reason. Please make sure that you are connected to the internet and try again.", ())
    })
  },
  render: ({send, handle, state: {email, name, password, password2, registering}}) =>
    <ScreenWrap>
      <LoadingOverlay visible=registering />
      <ListRow>
        <TextField
          label="Name"
          autoCapitalize="none"
          autoCorrect=false
          value=name
          onChangeText=((name) => send(UpdateName(name)))
          returnKeyType="next"
          onSubmitEditing=handle(focusEmail)
        />
      </ListRow>
      <ListRow>
        <TextField
          label="Email"
          autoCapitalize="none"
          autoCorrect=false
          keyboardType="email-address"
          value=email
          onChangeText=((email) => send(UpdateEmail(email)))
          ref=handle(setEmailRef)
          returnKeyType="next"
          onSubmitEditing=handle(focusPassword)
        />
      </ListRow>
      <ListRow>
        <TextField
          label="Password"
          autoCapitalize="none"
          autoCorrect=false
          secureTextEntry=true
          value=password
          onChangeText=((password) => send(UpdatePassword(password)))
          ref=handle(setPasswordRef)
          returnKeyType="next"
          onSubmitEditing=handle(focusPassword2)
        />
      </ListRow>
      <ListRow>
        <TextField
          label="Repeat password"
          autoCapitalize="none"
          autoCorrect=false
          secureTextEntry=true
          value=password2
          ref=handle(setPassword2Ref)
          onChangeText=((password2) => send(UpdatePassword2(password2)))
          onSubmitEditing=(() => send(Register))
        />
      </ListRow>
      <ListRow numberOfLines=1>
        <MuiButton text="Register" primary=true onPress=((_) => send(Register)) raised=true />
      </ListRow>
      <MuiButton text="Already have an account?" primary=true onPress=onRequestLogin />
    </ScreenWrap>
};
